# -*- coding: utf-8 -*-
"""
Created on Fri Sep 15 10:46:15 2023

@author: abenhassine
"""

from sqlalchemy import  MetaData, Table, Column, Integer, PrimaryKeyConstraint,  UniqueConstraint,String
from sqlalchemy.exc import IntegrityError, OperationalError, ProgrammingError, TimeoutError
import pymssql
from dbtools.src.db_connexion import SqlConnector
from sqlalchemy import inspect

connector = SqlConnector()
con_mis = connector.connection()

con_mis.execute('USE QUANT_work')


class DatabaseManager:
    """
    A manager to handle database operations on a specific table using DataFrame.
    The SQL base is QUANT_work in KECH-DB03 (cannot be changed for now)
    Attributes:
        table_name (str): The name of the table in the database.
    """
    
    def __init__(self, table_name):
        """
        Initialize the DatabaseManager object.
        
        Args:
            table_name (str): The name of the table to be managed.
        """
        self.table_name = table_name
    def _confirm_action(self, action_description):
        """
        Prompts for user confirmation before proceeding with a database action.
        Logic/Tasks:
            1. Display action description
            2. Evaluate the user's response:
                - If "Y", the function returns True, indicating the action has been confirmed.
                - If "N", prints a cancellation message, returns False, and the action is halted.
                - If any other input is provided, inform the user of invalid input and prompt again.
            
        Args:
            action_description (str): Description of the action to be confirmed.
        
        Returns:
            bool: True if the user confirms the action, False otherwise.
        
        Usage example:
        ---------------
        import pandas as pd
        from sqlalchemy import  String
        import dbtools.src.DatabaseManager as db
        db_manager = db.DatabaseManager( "test_db")
        print(db_manager._confirm_action('database operation'))
        
        """
        while True:
            response = input(f"Confirm action of '{action_description}' on table '{self.table_name}'. Proceed? Y/N: ").strip().upper()
            if response == "Y":
                return True
            elif response == "N":
                print("Action canceled.")
                return False
            else:
                print("Invalid input. Please enter 'Y' for Yes or 'N' for No.")    
    def _handle_db_error(self, error):
        """
        Handles database errors and provides user-friendly messages.
        Logic/Tasks:
            1. Error Message Generation: Generate a user-friendly message based on the identified error type.
        Args:
            error (Exception): The database error encountered.
        
        Returns:
            str: A user-friendly message describing the error.
            
        Usage example:
        ---------------
        import pandas as pd
        from sqlalchemy import  String
        import dbtools.src.DatabaseManager as db
        db_manager = db.DatabaseManager( "test_db")
        print(db_manager._handle_db_error('text'))
        """
        if isinstance(error, IntegrityError):
            return "Data integrity error: There may be a duplicate entry or a primary/foreign key constraint fail."
        elif isinstance(error, ProgrammingError):
            return "Programming error: There might be an issue with the SQL syntax or the table might not exist."
        elif isinstance(error, TimeoutError):
            return "Timeout error: The database operation took too long and timed out."
        elif isinstance(error, OperationalError) or isinstance(error, pymssql._pymssql.OperationalError):
            if "String or binary data would be truncated" in str(error):
                return "Error: One or more fields are too long for the database."
            
        return f"Unexpected database error: {error}"
    def _table_exists(self):
        """
        Checks if the specified table exists in the database.
        Logic/Tasks:
            1. Existence Verification: Confirm whether the table exists in the database.
        Returns:
            bool: True if the table exists, False otherwise.
        Usage example:
        ---------------
        import pandas as pd
        from sqlalchemy import  String
        import dbtools.src.DatabaseManager as db
        db_manager = db.DatabaseManager( "test_db")
        print(db_manager._table_exists())
        """
        inspector = inspect(con_mis)
        print("Target table name:", self.table_name)
        return self.table_name in inspector.get_table_names()

    def create_table(self, df, column_types=None, auto_incremental_index=None, primary_keys=None, unique_columns=None):
        """
        Creates a new table in the database from a DataFrame.
        Logic/Tasks:
        
            1. Table Creation: Execute the SQL statement to create the new table in the database with the defined strcuture and insert data from the DataFrame.
        Args:
            df (DataFrame): The DataFrame to create the table from.
            column_types (dict, optional): Column types for the DataFrame columns.
            auto_incremental_index (str, optional): Column name for an auto-incrementing index.
            primary_keys (list, optional): List of columns to set as primary keys.
            unique_columns (list, optional): Columns to be set with unique constraints.
        
        Returns:
            None: Creates the table in the database.
        Usage example:
        ---------------
        import pandas as pd
        from sqlalchemy import  String
        import dbtools.src.DatabaseManager as db
        file_path = r"W:\Global_Research\Quant_research\.shared files\data_dictionary_erp.xlsx"
        df = pd.read_excel(file_path)
        column_formats = {
        'name': String(50),
        'readable_name': String(100),
        'description':String(200)
        
        }
        db_manager = db.DatabaseManager( "test_db")
        db_manager.create_table(df = df.copy(),column_types=column_formats, 
                        auto_incremental_index='indicator_id' 
                        ,primary_keys = ['name'])
        
        """
        
        if not self._confirm_action("creation" ):
            return
        if column_types is None:
            column_types = {}
        
        # Check if the table already exists
        if self._table_exists():
            print(f"Table '{self.table_name}' already exists. Use the replace_table function.")
            return
        # If an auto-incremental index is required, add it
        meta = MetaData()
        columns = []
        
        if auto_incremental_index:
            columns.append(Column(auto_incremental_index, Integer, primary_key=True, autoincrement=True))

        # Define other columns
        for col_name in df.columns:
            col_type = column_types.get(col_name, String)  # Default to VARCHAR(255) if not specified
            primary = col_name in primary_keys if primary_keys else False
            columns.append(Column(col_name, col_type, primary_key=primary))
            
            
            

            # If primary keys are specified, add a primary key constraint
        constraints = []

        # If unique columns are specified, add a unique constraint
        if unique_columns:
            constraints.append(UniqueConstraint(*unique_columns))

        # Define the table
        table = Table(self.table_name, meta, *columns, *constraints)

        # Create the table in the database
        
        meta.create_all(con_mis)
        
        # Insert data into the table
        with con_mis.begin() as conn:
            df.to_sql(self.table_name, conn, index=False, if_exists='append')

        print(f"Table '{self.table_name}' created successfully.")
        

    def replace_table(self, df, column_types=None, auto_incremental_index=None, primary_keys=None, unique_columns=None):
        """
        Replaces an existing table in the database with a new DataFrame.
        Logic/Tasks:
            1. deletes the table if already exists
            2. creates the table using the provided DataFrame and parameters.
        Args:
           df (DataFrame): The DataFrame to create the table from.
           column_types (dict, optional): Column types for the DataFrame columns.
           auto_incremental_index (str, optional): Column name for an auto-incrementing index.
           primary_keys (list, optional): List of columns to set as primary keys.
           unique_columns (list, optional): Columns to be set with unique constraints.
       
        Returns:
           None: Replaces the existing table with a new one.
        Usage example:
        ---------------
        import pandas as pd
        from sqlalchemy import  String
        import dbtools.src.DatabaseManager as db
        file_path = r"W:\Global_Research\Quant_research\.shared files\data_dictionary_erp.xlsx"
        df = pd.read_excel(file_path)
        column_formats = {
        'name': String(50),
        'readable_name': String(100),
        'description':String(200)
        
        }
        db_manager = db.DatabaseManager( "test_db")
        db_manager.replace_table(df = df.copy(),column_types=column_formats,
                                 auto_incremental_index='indicator_id',primary_keys = ['name'],unique_columns = ['name'])
        
        """
        if not self._confirm_action("Replacement" ):
            return
        try:
            # Drop the existing table if it exists
            if self._table_exists():
                self.drop_table()
            
            # Create the table using the provided DataFrame
            self.create_table(df, column_types, auto_incremental_index, primary_keys, unique_columns)
        except Exception as e:
            user_friendly_message = self._handle_db_error(e)
            print(user_friendly_message)
            
    
    def append_table(self, df):
        """
        Appends data from a DataFrame to an existing table in the database.
        Logic/Tasks:
            1. Table Existence Check: Verify
            2. Data Appending: Append the DataFrame data to the existing table.
        Args:
            df (DataFrame): The DataFrame containing data to append.
        
        Returns:
            None: Appends data to the existing table.
        Usage example:
        ---------------
        import pandas as pd
        from sqlalchemy import  String
        import dbtools.src.DatabaseManager as db
        file_path = r"W:\Global_Research\Quant_research\.shared files\data_dictionary_erp.xlsx"
        df = pd.read_excel(file_path)
        column_formats = {
        'name': String(50),
        'readable_name': String(100),
        'description':String(200)
        
        }
        db_manager = db.DatabaseManager( "test_db")
        db_manager.create_table(df = df.copy(),column_types=column_formats, 
                        auto_incremental_index='indicator_id' 
                        ,primary_keys = ['name'])
        data = {
            'name': ['pippo1'],
            'readable_name': [None],
            'description': ['My description']
        }
        
        # Create DataFrame
        df = pd.DataFrame(data)
        
        
        
        db_manager.append_table(df = df.copy())
        """
        if not self._confirm_action("Appendment" ):
            return
        try:
            if self._table_exists():
                df.to_sql(self.table_name, con_mis, index=False, if_exists='append')
                print(f"Data appended to '{self.table_name}'.")
            else:
                print(f"Table '{self.table_name}' does not exist. Cannot append.")
        except Exception as e:
            
            user_friendly_message = self._handle_db_error(e)
            print(user_friendly_message) 
            
    def delete_record(self, column_name, value):
        """
        Deletes records from the table based on a column value.
        Logic/Tasks:
            1. Record Deletion: Execute SQL command to delete all records where column “column_name” is equal to value.
        Args:
            column_name (str): The column name to match the value.
            value (str|int): The value to match for deletion.
        
        Returns:
            None: Deletes records from the table.
        Usage example:
        ---------------    
        import pandas as pd
        from sqlalchemy import  String
        import dbtools.src.DatabaseManager as db
        file_path = r"W:\Global_Research\Quant_research\.shared files\data_dictionary_erp.xlsx"
        df = pd.read_excel(file_path)
        column_formats = {
        'name': String(50),
        'readable_name': String(100),
        'description':String(200)
        
        }
        db_manager = db.DatabaseManager( "test_db")
        db_manager.create_table(df = df.copy(),column_types=column_formats, 
                        auto_incremental_index='indicator_id' 
                        ,primary_keys = ['name'])
        db_manager.delete_record('name','coe') 
        """
        if not self._confirm_action("Deletion" ):
            return
        try:
            if self._table_exists():
                
            
                # Create parameterized DELETE statement to prevent SQL injection
                delete_sql = f"DELETE FROM {self.table_name} WHERE {column_name} = :value"

                # Assuming `value` is the variable holding the value for the condition
                params = {'value': value}
                
                # Execute the SQL statement with parameters
                con_mis.execute( delete_sql, **params)
                
                print(f"Record with {column_name} {value} in '{self.table_name}' has been deleted.")
            else:
                print(f"Table '{self.table_name}' does not exist.")
        except Exception as e:
            user_friendly_message = self._handle_db_error(e)
            print(user_friendly_message)
    def modify_record(self, column_name, value, updates):
        """
        Modifies records in the table based on a column value.
        Logic/Tasks:
            1. Record Update: Execute SQL command to modify columns in “updates” using values in “updates” for all rows where column “column_name” is equal to value.
        Args:
            column_name (str): The column name to match the value.
            value (str|int): The value to match for modification.
            updates (dict): The updates to apply to the matching records.
        Returns:
            None: Modifies records in the table.
        Usage example:
        ---------------    
        import pandas as pd
        from sqlalchemy import  String
        import dbtools.src.DatabaseManager as db
        file_path = r"W:\Global_Research\Quant_research\.shared files\data_dictionary_erp.xlsx"
        df = pd.read_excel(file_path)
        column_formats = {
        'name': String(50),
        'readable_name': String(100),
        'description':String(200)
        
        }
        db_manager = db.DatabaseManager( "test_db")
        db_manager.create_table(df = df.copy(),column_types=column_formats, 
                        auto_incremental_index='indicator_id' 
                        ,primary_keys = ['name'])
        updates = {
            'name': 'risk_free',
            'readable_name': 'my_name'
            }
        
        db_manager.modify_record('name','r', updates=updates)
                
        """
        if not self._confirm_action("Modification" ):
            return
        try:
            if self._table_exists():
                columns_to_update = ", ".join([f"{col} = :{col}" for col in updates.keys()])
                update_sql = f"UPDATE {self.table_name} SET {columns_to_update} WHERE {column_name} = :where_value"
                
                # Prepare parameters for the execution, combining updates and the condition value
                params = updates.copy()  # Start with the updates
                params['where_value'] = value  # Add the value for the WHERE clause
                
                # Executing the SQL statement with parameters
                con_mis.execute( update_sql, **params)
                print(f"Records where {column_name} is {value} in '{self.table_name}' have been modified.")
            else:
                print(f"Table '{self.table_name}' does not exist.")
        except Exception as e:
            user_friendly_message = self._handle_db_error(e)
            print(user_friendly_message)
    def drop_table(self):
        """
        Drops the specified table from the database.
        
        Logic/Tasks:
            
            1. Table Dropping: If the table exists and the user confirms, drop the table from the database.
            
    
        Args:
            None
    
        Returns:
            None: If the operation is confirmed and successful, the table is dropped. Otherwise, an appropriate message is displayed.
    
        Usage example:
        ---------------
        import pandas as pd
        from sqlalchemy import  String
        import dbtools.src.DatabaseManager as db
        file_path = r"W:\Global_Research\Quant_research\.shared files\data_dictionary_erp.xlsx"
        df = pd.read_excel(file_path)
        column_formats = {
        'name': String(50),
        'readable_name': String(100),
        'description':String(200)
        
        }
        db_manager = db.DatabaseManager( "test_db")
        db_manager.create_table(df = df.copy(),column_types=column_formats, 
                        auto_incremental_index='indicator_id' 
                        ,primary_keys = ['name'])
        db_manager.drop_table()
        """
        if not self._confirm_action("Drop Table"):
            return
        try:
            # Check if the table exists
            if self._table_exists():
                drop_sql = f"DROP TABLE IF EXISTS {self.table_name}"
                con_mis.execute( drop_sql)
                print(f"Table '{self.table_name}' dropped successfully.")
            else:
                print(f"Table '{self.table_name}' does not exist. No action was taken.")
        except Exception as e:
            user_friendly_message = self._handle_db_error(e)
            print(user_friendly_message)     






     
    
    
    
        

    
#Usage example
"""
file_path = r"W:\Global_Research\Quant_research\.shared files\index_referential_init_no_id.xlsx"

# Read the Excel file into a pandas DataFrame
df = pd.read_excel(file_path)
#Here we refill ticker for primary key contraint
df['ticker'].fillna('KEP' + df.index.to_series().astype(str), inplace=True)

column_formats = {
    'name': String(100),
    'short_name': String(50),
    'ticker' : String(30),
    'ISIN' : String(32),
    'INDEXID' : String(50),
    'region' : String(30),
    'category' : String(30),
    'version' : String(30), 
    'currency' : String(3)
    
}
# Create an instance of DatabaseManager for our DataFrame and table
db_manager = DatabaseManager( "INDEX_REFERENTIAL")

db_manager.create_table(df = df.copy(),column_types=column_formats, 
                        auto_incremental_index='index_id' 
                        ,primary_keys = ['index_id'],
                        unique_columns = ['ticker'])

# Replace Table



db_manager.replace_table(df = df.copy(),column_types=column_formats, 
                        auto_incremental_index='index_id' 
                        ,primary_keys = ['index_id'],
                        unique_columns = ['ticker'])

data = {
    
    'name': ['test'],
    'short_name': [None],
    'ticker': ['CAC'],
    'ISIN': [''],
    'INDEXID': [''],
    'region': [''],
    'category': [''],
    'version': ['']
}

# Create DataFrame
df = pd.DataFrame(data)



db_manager.append_table(df = df.copy())




db_manager = DatabaseManager( "INDEX_REFERENTIAL")

#Modify line
updates = {
    'ticker': 'abc',
    'short_name': 'tst'
}

db_manager.modify_record('ticker', 'BEL20', updates=updates)
#db_manager.delete_record('ticker','AEX')
"""        

#