# -*- coding: utf-8 -*-
"""
Created on Wed May 10 16:00:35 2023

@author: ssureau
"""
import pandas as pd
import numpy as np
import dbtools.src.get_repository as rep

def add_stock_classif_to_data(df_data=[], dynamic=True, sector='GICS', date_field='date'):
    
    cnty_grps = {
                'Amsterdam' : 'Benelux',
                'Athens' : 'Unknown',
                'Brussels' : 'Benelux',
                'Budapest' : 'Unknown',
                'Copenhagen' : 'Scandies',
                'Dublin' : 'Unknown',
                'Helsinki' : 'Scandies',
                'Lisbon' : 'Spain**',
                'London' : 'UK',
                'Luxembourg' : 'Benelux',
                'Madrid' : 'Spain**',
                'Milan' : 'Italy',
                'NYSE' : 'US',
                'NYSE Amex' : 'US',
                'Nasdaq' : 'US',
                'Oslo' : 'Scandies',
                'Paris' : 'France',
                'Prague' : 'Unknown',
                'Stockholm' : 'Scandies',
                'Swiss' : 'Switzerland',
                'Toronto' : 'Canada',
                'Vienna' : 'Germany*',
                'Warsaw' : 'Unknown',
                'Xetra' : 'Germany*',
                'unknown' : 'Unknown'
                }
    sector_map = {
                'Communication Services':'Comm Svcs',
                'Consumer Staples':'Cons Staples',
                'Consumer Discretionary':'Cons Discr',
                'Information Technology':'Info Tech'
                }
    capi_map = {
               -1 : 'Smallest',
               0 : 'Large',
               1 : 'Mid',
               2 : 'Small'
               }
    style_map = {
                 1 : 'Growth',
                 0 : 'Value',
                 -1 : 'Other'
               }
    
    security_id = df_data.security_id.unique()
    
    start_date = df_data[date_field].min()
    end_date = df_data[date_field].max()
    end_date = pd.date_range(start=end_date, periods = 7)[-1]
    end_date = end_date.strftime('%Y%m%d')
    
    # style and capi
    df_style = rep.get_growth_value(start_date, end_date)
    df_lms = rep.get_flag_lms(start_date, end_date)
    if dynamic:
        df_style = df_style.shift(-1).stack().reset_index()
        df_lms = df_lms.shift(-1).stack().reset_index()
    else:
        df_style = df_style.shift(-1).stack(dropna = False).reset_index()
        df_lms = df_lms.shift(-1).stack(dropna = False).reset_index()
    df_lms.columns = [date_field, 'security_id', 'capi']
    df_style.columns = [date_field, 'security_id', 'style_gv']
    
    # country from trading id
    df_prim_trading = rep.mapping_from_security(security_id, code = 'prim_td_id').to_frame('td_id')
    df_trading = rep.get_trading_information()[['trading_destination_id', 'trading_name']].drop_duplicates(keep = 'last')
    df_trading.index = df_trading.trading_destination_id
    df_trading = df_trading.trading_name
    df_prim_trading['trading_name'] = df_prim_trading.td_id.map(df_trading)
    df_prim_trading['country'] = df_prim_trading.trading_name.map(cnty_grps)
    df_country = df_prim_trading['country']
    
    # sector
    df_sector = rep.mapping_sector_from_security(security_id, code = sector)
    if sector == 'GICS':
        df_sector = df_sector['GICS_SECTOR'].replace(sector_map)
    elif sector == 'ICB':
        df_sector = df_sector['ICB_INDUSTRY'].replace(sector_map)
    
    # add classification
    df_data = df_data.merge(df_lms, on = [date_field, 'security_id'], how = 'left')#.fillna(-1)
    df_data = df_data.merge(df_style, on = [date_field, 'security_id'], how = 'left')#.fillna(-1)
    
    # replace na by -1 only for capitalisation and style
    columns_to_replace = ['capi', 'style_gv']
    df_data[columns_to_replace] = df_data[columns_to_replace].fillna(-1)
    
    df_data.capi = df_data.capi.replace(capi_map)
    df_data.style_gv = df_data.style_gv.replace(style_map)
    
    df_data['sector'] = df_data.security_id.map(df_sector)
    df_data['country'] = df_data.security_id.map(df_country)
    df_data = df_data.sort_values(by=[date_field, 'security_id'])
    
    return df_data

def add_index_membership_to_data(index_ticker, df_data=[], dynamic=True, date_field='date'):
    start_date = df_data[date_field].min()
    end_date = df_data[date_field].max()
    end_date = pd.date_range(start=end_date, periods = 7)[-1]
    end_date = end_date.strftime('%Y%m%d')
    
    df_index = rep.get_index_comp(index_ticker = index_ticker, start_date = start_date, end_date = end_date)
    df_index = df_index.notna()*1
    df_index = df_index.fillna(0)
    df_index = df_index.shift(-1).stack().reset_index()
    df_index.columns = [date_field, 'security_id', index_ticker]
    
    if index_ticker in df_data.columns:
        df_data = df_data.drop(index_ticker,axis=1)
    df_data = df_data.merge(df_index, on = [date_field, 'security_id'], how = 'left').fillna(0)
    
    return df_data