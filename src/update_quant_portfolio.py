# -*- coding: utf-8 -*-
"""
Created on Wed Apr 10 10:47:37 2024

@author: nle
"""

import os, sys
import pandas as pd
from pandas.tseries.offsets import BDay
import numpy as np
from dbtools.src.db_connexion import SqlConnector
import dbtools.src.get_repository as rep
import backtest.src.backtest as bkt
from copy import deepcopy
connector = SqlConnector()
con_mis = connector.connection()

sys.path.insert(0, os.path.join(__file__, '../..'))
#sys.path.insert(0, os.path.join('C:\\python_projects\\dbtools\\src\\update_quant_portfolio.py', '../..'))
import dbtools.src.DatabaseManager as db

from config.quant_portfolio import QuantIndexConfig as Conf
from dbtools.config.quant_portfolio import QuantIndexPatch as Conf_patch
mapping_price_return_total_return = Conf.mapping_price_return_total_return
s_pr_tr = Conf.s_pr_tr
s_pr_tr_id = Conf.s_pr_tr_id
s_ticker_index_id = Conf.s_ticker_index_id
s_index_id_name = Conf.s_index_id_name


#%% general class
class QuantIndexReference():
    def __init__(self, config):
        """
        Initialize class
        Args:
            config (dict): dictionnary of in family_business.config.update_fb_index_config
        
        Example:
            .. code-block :: python
                
                import dbtools.src.update_quant_portfolio as qidx
                from copy import deepcopy
                import sys, os
                sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
                from config.quant_portfolio import QuantIndexConfig as Conf
                config = deepcopy(Conf.spec_fb)
                qir = qidx.QuantIndexReference(config)
                print(qir.df_index_ref)
        """
        self.config = config
        index_id = pd.Index(config.get('index_id', []))
        #if not index_id.empty:
        self.index_id = index_id.union(s_pr_tr_id.loc[s_pr_tr_id.index.intersection(index_id)].values)
        self.df_index_ref = self.get_index_ref()
        self.max_date_compo = self.df_index_ref.date_compo.min().strftime('%Y%m%d')
        self.max_date_level = self.df_index_ref.date_level.min().strftime('%Y%m%d')
        
        pass
    
    def get_index_ref(self):
        """
        Get index reference
        
        Task:
            1. Get last date of composition from QUANT_work..INDEX_COMPOSITION
            2. Get last date and last index level from QUANT_work..INDEX_LEVEL
            3. Mapping index ticker with index ids

        Returns:
            df_index_ref (df): reference of index
                date_compo: max date of index composition
                date_level: max date of index level
                index_level: index level on date_level
                
        Example:
            import dbtools.src.update_quant_portfolio as qidx
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            qir = qidx.QuantIndexReference(Conf.spec_fb)
            df_index_ref = qir.get_index_ref()
            print(df_index_ref)
        """
        index_id = self.index_id
        table_compo = self.config.get('table_compo', 'INDEX_COMPOSITION')
        table_level = self.config.get('table_level')
        indicator_field = self.config.get('table_indicator_field', 'index_id')
        date_field = self.config.get('table_date_field', 'DATE')
        req = f"""select {indicator_field}, max({date_field}) max_date
                    from QUANT_work..%s
                    group by {indicator_field}
                    order by {indicator_field}"""%table_compo
        df = pd.read_sql(req, con_mis)
        s_max_date_compo = pd.Series(index = df[indicator_field].values, data = df.max_date.values)
        s_max_date_compo = pd.to_datetime(s_max_date_compo)
        s_max_date_compo.index.name=indicator_field
        df_compo_ref = s_max_date_compo.to_frame('date_compo').reset_index()
        df_compo_ref.index = df_compo_ref[indicator_field].values
        df_compo_ref['date_level'] = pd.to_datetime(self.config.get('start_date','19990101'))
        df_compo_ref['index_level'] = 1
        
        if table_level:
            req = """select a.index_id, a.DATE, a.value
                    from QUANT_work..%s a inner join (
                    select index_id, max(DATE) max_date
                    from QUANT_work..%s
                    group by index_id) b
                    on a.index_id=b.index_id and a.DATE = b.max_date
                    order by a.index_id
                    """%(table_level, table_level)
            df_index_data = pd.read_sql(req, con_mis)
            df_index_data = df_index_data.set_index('index_id')
            df_index_data = df_index_data[df_index_data.index.isin(index_id)]
            df_index_ref = rep.mapping_from_index(index_id).reset_index()
            df_index_ref.index = df_index_ref.index_id
            
            df_index_ref['date_compo'] = df_index_ref.index_id.map(s_max_date_compo).fillna(pd.to_datetime('19990101'))
            df_index_ref['date_compo'] = pd.to_datetime(df_index_ref['date_compo'])
            df_index_ref['date_level'] = df_index_ref.index_id.map(df_index_data.DATE).fillna(pd.to_datetime('19990101'))
            df_index_ref['date_level'] = pd.to_datetime(df_index_ref['date_level'])
            df_index_ref['index_level'] = df_index_ref.index_id.map(df_index_data.value).fillna(1)
    
            return df_index_ref
        else:
            return df_compo_ref
    
    def filter_existing_data(self, df_db, s_start_date):
        """
        Filter existing date of df_db based on a series of reference dates
        
        Task:
            1. Filter data in df_db whose date <= series of reference dates
        Args:
            df_db (df): data in database format with columns DATE, index_id, ...
            s_start_date: series of start_date of import in QUANT_work.
                        Data in df_db with DATE<=start_date will be filtered
        Returns:
            df (df): df_db filtered
        
        Example:
            
            import dbtools.src.update_quant_portfolio as qidx
            import pandas as pd
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            qir = qidx.QuantIndexReference(Conf.spec_fb)
            df_index_ref = qir.get_index_ref()
            df_db = qidx.get_index_level(index_id=[91, 92], start_date='20230101', end_date='20230131')
            df_db = df_db.stack().reset_index()
            df_db.columns = ['DATE', 'index_id', 'value']
            s_start_date = pd.Series(index = [91, 92], data = ['20230125', '20230127'])
            df_db_filtered = qir.filter_existing_data(df_db, s_start_date)
            print('Before filter:\n',df_db)
            print('After filter:\n',df_db_filtered)
            
        """
        indicator_field = self.config.get('table_indicator_field', 'index_id')
        date_field = self.config.get('table_date_field', 'DATE')
        
        df = df_db.copy()
        df['start_date'] = df[indicator_field].map(s_start_date)
        df = df[df[date_field]>df['start_date']]
        df = df.drop(columns = ['start_date'])
        return df.copy()

class QuantIndexUpdater(QuantIndexReference):
    def __init__(self, config):
        super().__init__(config)
    """
    Example:
    .. code-block:: python
    
        ### This script is similar to function "run_update_compo" of the class ###
        import dbtools.src.update_quant_portfolio as qidx
        from copy import deepcopy
        from pandas.tseries.offsets import BDay
        import sys, os
        sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
        from config.quant_portfolio import QuantIndexConfig as Conf
        config = deepcopy(Conf.spec_fb)
        qiu = qidx.QuantIndexUpdater(config)

        ################# run_update_index ##################
        df = qiu.run_update_index(flag_to_sql = False)

        ############### Equivalent to ######################

        table_name = qiu.config['table_level']
        index_id = qiu.config['index_id']
        previous_rebal = qiu.get_previous_rebalance()
        s_start_date = qiu.df_index_ref.date_level
        end_date = qiu.get_end_date(s_start_date.max() + BDay(20))
        df_compo = qidx.get_index_compo(index_id, previous_rebal, end_date=end_date)
        params= qiu.get_param_backtest(df_compo, end_date)
        data_bkt = qiu.get_backtest_data(params)
        df_perf_db, df_perf_ts = qiu.backtest(data_bkt)
        df_perf_recomp = qiu.recompute_index_level(df_perf_db)
        df_perf_recomp_ts = df_perf_recomp.set_index(['DATE', 'index_id']).value.unstack()
    """
    pass

    def get_previous_rebalance(self):
        """
        Get previous rebalancing date of index ids
        
        Task:
            1. Get previous rebalancing date of index ids in QUANT_work..INDEX_COMPOSITION
        Args:
            index_id (list(int)): list of index id.
            
        Returns:
            previous_rebal (Timestamp)
        
        Example:
            
            import dbtools.src.update_quant_portfolio as qidx
            import pandas as pd
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            qir = qidx.QuantIndexUpdater(Conf.spec_kech)
            previous_rebal = qir.get_previous_rebalance()
            print('previous_rebal: %s'%previous_rebal)
        
        """
        index_id = self.index_id
        idx = ','.join([str(int(s)) for s in index_id])
        req = f"""select index_id, max(DATE) previous_rebal
                    from QUANT_work..INDEX_COMPOSITION 
                    where index_id in ({idx}) 
                    and DATE <(select max(DATE) from QUANT_work..INDEX_LEVEL 
                                   where index_id in ({idx}))
                    group by index_id"""
        df_date = pd.read_sql(req, con_mis)
        previous_rebal = df_date.previous_rebal.min()
        if pd.isna(previous_rebal):
            previous_rebal=pd.to_datetime('20100101')
        return previous_rebal
    
    def get_end_date(self, update_freq):
        """
        Get end date from a given update frequence
        
        Task:
            1. Create a range of 30 business days, ending with today
            2. Depending on given update frequence, idenify end date:
                * eom: end of previvous month
                * adhoc: last business day (today - 1 day)
        Args:
            update_freq (str): update frequence (eom, adhoc), or iso datetime format.
        
        Returns:
            end_date (Timestamp)
        
        Example:
            
            import dbtools.src.update_quant_portfolio as qidx
            import pandas as pd
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            qir = qidx.QuantIndexUpdater(Conf.spec_kech)
            end_date = qir.get_end_date('eom')
            print('end_date: %s'%end_date)
        """
        holidays = load_quant_holidays()
        today = pd.Timestamp.today().date()
        date_range = pd.bdate_range(end = today, periods = 30)
        date_range = date_range.difference(holidays)
        if update_freq=='eom':
            end_date = date_range[date_range.month == pd.offsets.MonthEnd().rollback(today).month][-1]
            end_date = end_date
        elif update_freq=='adhoc':
            end_date = date_range[-1] - BDay(1)
            end_date = end_date
        else:
            end_date = pd.to_datetime(update_freq)
        return end_date
    def get_param_backtest(self, df_compo, end_date):
        """
        Get parameters for backtest from df_compo
        
        Task:
            1. Create dict_compo from df_compo
            2. Define, universe of backtest, begin_date, start_date, rebal_dates
        Args:
            df_compo (df): composition of index with DATE, index_id, security_id, SECURITYRATIO
            end_date (str, Timestamp): end date of backtest, iso datetime format
        Returns:
            params (dict): parameters for backtest:
                end_date : end date of backtest
                begin_date: beginning of backtest
                start_date: begin_date - 15D
                rebal_dates: list of datetime based on df_compo['DATE']
                security_id: list of securities for backtest
                index_ticker: empty
                dict_compo: dict of df composition in timeserie format
        
        Example:
            
            import dbtools.src.update_quant_portfolio as qidx
            import pandas as pd
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            qir = qidx.QuantIndexUpdater(Conf.spec_kech)
            
            df_compo = qidx.get_index_compo(index_id=[95], start_date = '20230801', end_date='20231201')
            params = qir.get_param_backtest(df_compo, end_date = '20231201')
            print(params.keys())

        
        """
        
        dict_compo = {}
        security_id = pd.Index([])
        idx_ids = pd.Index(df_compo.index_id.unique()).difference(s_pr_tr_id.values)

        for idx_id in idx_ids:
            df_index = df_compo[df_compo.index_id==idx_id].copy()
            df_compo_tmp = df_index.set_index(['DATE', 'security_id']).SECURITYRATIO.unstack()
            dict_compo[idx_id] = df_compo_tmp.copy()
            security_id = security_id.union(df_compo_tmp.columns)

        begin_date = df_compo.DATE.min()
        start_date = begin_date - BDay(15)
        rebal_dates = pd.Index(df_compo.DATE.unique())


        params = {'end_date':end_date,
                  'start_date':start_date,
                  'begin_date':begin_date,
                  'rebal_dates':rebal_dates,
                  'security_id':security_id,
                  'index_ticker' : [],
                  'dict_compo' : deepcopy(dict_compo)
                  }
        return params
    
    def get_backtest_data(self, params, end_date = ''):
        """
        Get backtest data
        
        Task:
            1. Get backtest data base of params
            2. Add rebal_dates in data 'flag_lms', 'close_prc_total', 'close_prc', 'index_prc' if they are not yet in timeseries
            3. Add target weight in data
            
        Args:
            params (dict): out put of get_param_backtest
            end_date (str): end_date of backtest
        Returns:
            data_bkt (dict): backtest data, output of bkt.BacktestData.data_bkt
        
        Example:
            
            import dbtools.src.update_quant_portfolio as qidx
            import pandas as pd
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            qir = qidx.QuantIndexUpdater(Conf.spec_kech)
            
            df_compo = qidx.get_index_compo(index_id=[95], start_date = '20230801', end_date='20231201')
            params = qir.get_param_backtest(df_compo, end_date = '20231201')
            data_bkt = qir.get_backtest_data(params, end_date='20231201')
        """

        start_date = params.get('start_date')
        begin_date = params.get('begin_date')
        if end_date=='':
            end_date = params.get('end_date')
        security_id = params.get('security_id')
        index_ticker = params.get('index_ticker', [])
        rebal_dates = params.get('rebal_dates', [])
        # 1. Get backtest data base of params
        backtest_data = bkt.BacktestData(start_date=start_date, end_date=end_date, begin_date=begin_date,
                                     index_ticker=index_ticker, security_id=security_id,
                                     flag_fundamental_data = False)

        data_bkt = deepcopy(backtest_data.data_bkt)
        # Add rebal_dates in data if they are not yet in timeseries
        for k in ['flag_lms', 'close_prc_total', 'close_prc', 'index_prc']:
            df = data_bkt[k].copy()
            index = df.index.union(rebal_dates)
            df = df.reindex(index = index, method = 'ffill')
            data_bkt[k] = df.copy()
        # Add target weight in data
        dict_compo = params.get('dict_compo')
        data_bkt['target_weight'] = deepcopy(dict_compo)
# =============================================================================
#         dict_target = {}
#         for k in dict_compo.keys():
#             df = dict_compo[k].reindex(data_bkt['close_prc'].index, method = 'ffill')
#             df = df.loc[rebal_dates]
#             dict_target[k] = df.copy()
#         data_bkt['target_weight'] = deepcopy(dict_target)
# =============================================================================
        return deepcopy(data_bkt)
    
    def backtest(self, data_bkt):
        """
        Backtest
        
        Task:
            1. Backtest on backtest data
            2. Transform performance (net and gross) in db format with columns 'DATE', 'index_id', 'value'
        Args:
            data_bkt (dict): output of get_backtest_data
        Returns:
            df_perf_db (df): index level in database format
            df_perf_ts (df): index level in timeseries format
        
        Example:
            
            import dbtools.src.update_quant_portfolio as qidx
            import pandas as pd
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            qir = qidx.QuantIndexUpdater(Conf.spec_kech)
            
            df_compo = qidx.get_index_compo(index_id=[95], start_date = '20230801', end_date='20231201')
            params = qir.get_param_backtest(df_compo, end_date = '20231201')
            data_bkt = qir.get_backtest_data(params, end_date='20231201')
            df_perf_db, df_perf_ts = qir.backtest(data_bkt)
        """
        dict_target = deepcopy(data_bkt['target_weight'])
        ptf_analysis = bkt.PortfolioAnalysis(data_bkt, dict_target)

        df_perf = deepcopy(ptf_analysis.df_perf)
        df_perf_gross = deepcopy(ptf_analysis.df_perf_gross)
        df_perf_gross = df_perf_gross.rename(columns = s_pr_tr_id)
        df_perf_gross = df_perf_gross[df_perf_gross.columns.difference(df_perf.columns)]
        df_perf_ts = pd.concat((df_perf, df_perf_gross), axis = 1)

        df_perf_db = df_perf_ts.stack()
        df_perf_db = df_perf_db.reset_index()
        df_perf_db.columns = ['DATE', 'index_id', 'value']
        df_perf_db = df_perf_db.sort_values(by = ['index_id', 'DATE'])
        df_perf_db = df_perf_db.set_index(['index_id', 'DATE'])
        df_perf_db = df_perf_db.reset_index()

        return df_perf_db, df_perf_ts
    
    def recompute_index_level(self, df_perf_db):
        """
        Recompute index level from base on actual value of existing index level
        
        Task:
            1. Retrieve inception from value self.df_index_ref
            2. Compute factor and rebase index.
        Args:
            df_perf_db (df): output of backtest, index level in database format
        Returns:
            df (df): df_perf_db rebased on last index value
        
        Example:
            
            import dbtools.src.update_quant_portfolio as qidx
            import pandas as pd
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            qir = qidx.QuantIndexUpdater(Conf.spec_kech)
            
            df_perf_db = qidx.get_index_level(index_id=[95], start_date = '20230801', end_date='20231201')
            df_perf_db = df_perf_db.stack().reset_index()
            df_perf_db.columns = ['DATE', 'index_id', 'value']
            df_index_ref = qir.df_index_ref
            df_index_ref.loc[95, 'date_level'] = '2023-08-31'
            df_perf_recomp = qir.recompute_index_level(df_perf_db)
        
        """
        df_index_ref = self.df_index_ref.copy()
        s_level_date = pd.Series(index = df_index_ref.index_id.values, data = df_index_ref.date_level.values)
        s_inception_value = df_index_ref.set_index('index_id').index_level
        
        df = df_perf_db.copy()
        df['date_level'] = df.index_id.map(s_level_date)
        df = df[df.DATE>=df['date_level']]

        init_value = df.sort_values(by = ['index_id', 'DATE']).drop_duplicates(['index_id'], keep='first')
        init_value = init_value.set_index('index_id').value
        factor = init_value/s_inception_value
        
        df['factor'] = df.index_id.map(factor)
        df['value_recomputed'] = df['value']/df['factor']
        df = df.drop(columns = ['date_level', 'date_level', 'value', 'factor'])
        df = df.rename(columns = {'value_recomputed':'value'})
        return df
    def run_update_index(self, flag_to_sql = True):
        """
        Run update index
        
        Task:
            1. Compute index level from previous rebalancing date to end_date
            2. Recomputer index level and filter existing date and  before importing to database
            3. Import to sql
        Args:
            flag_to_sql (bool): whether import to table config['table_level'] in sql
        Returns:
            df_perf_recomp_ts (df) performance in timeseries format
        Example:
        .. code-block:: python
        
            import dbtools.src.update_quant_portfolio as qidx
            from copy import deepcopy
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            
            config_idx = deepcopy(Conf.spec_fb)
            qidx_updater = qidx.FBCompoUpdater(config_idx)
            df = qidx_updater.run_update_compo(flag_to_sql = False)
        """
        table_name = self.config['table_level']
        index_id = self.config['index_id']
        previous_rebal = self.get_previous_rebalance()
        end_date = self.get_end_date(self.config['update_freq'])
        s_start_date = self.df_index_ref.date_level
        
        df_compo = get_index_compo(index_id, previous_rebal, end_date=end_date)
        params= self.get_param_backtest(df_compo, end_date)
        data_bkt = self.get_backtest_data(params)
        df_perf_db, df_perf_ts = self.backtest(data_bkt)
        df_perf_recomp = self.recompute_index_level(df_perf_db)
        df_perf_recomp_ts = df_perf_recomp.set_index(['DATE', 'index_id']).value.unstack()
        
        df_to_import = self.filter_existing_data(df_perf_recomp, s_start_date)
        if df_to_import.empty:
            print('\n-------- All levels are up-to-date, nothing to import ------')
            return df_perf_recomp_ts
        if flag_to_sql:
            try:
                import_to_sql(df_to_import, table_name)
            except:
                print('--------- CANNOT IMPORT TO %s ---------' %table_name)
        return df_perf_recomp_ts
#%% COMPOSITION
class QuantCompoUpdater(QuantIndexReference):
    """
    Initialize Parent class for other dataset composition updater
    
    """
    def __init__(self, config):
        super().__init__(config)
        pass

    def eom_of_given_date(self, date):
        """
        Get the end of the month of a given date.
        
        Task:
            1. Create a range of 30 business days, ending with today
            2. Idenify end of previvous month

        Args:
            date (str): The date in string format (yyyymmdd).

        Returns:
            pd.Timestamp: The end of the month date.
        
        Example:
            
            import dbtools.src.update_quant_portfolio as qidx
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            qidx_updater = qidx.QuantCompoUpdater(Conf.spec_kech)
            eom = qidx_updater.eom_of_given_date('20230502')
            
        """
        holidays = load_quant_holidays()
        eom = pd.to_datetime(date) + BDay(30)
        date_range = pd.date_range(start = date, periods = 30, freq = BDay())
        date_range = date_range.difference(holidays)
        eom = date_range[date_range.month==pd.to_datetime(date).month][-1]
        return eom

    def list_files(self):
        """
        List files within the specified folder and return a Series containing file paths.
        
        Task:
            1. Look for config['filename'] in config['folder']
            2. Look for rebalancing date corresponding to config['filename']
        Args:
            
        Returns:
            pd.Series: Series containing file paths, index are rebalancing dates
        
        Example:
            
            import dbtools.src.update_quant_portfolio as qidx
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            qidx_updater = qidx.QuantCompoUpdater(Conf.spec_kech)
            files = qidx_updater.list_files()
            print(files)
        """
        filename = self.config['filename']
        folder = self.config['folder']
        start_date = self.config['start_date']
        list_path = pd.Series(dtype = 'object')
        rebalance = self.config['rebalance']
        for year in os.listdir(folder):
            path_year = os.path.join(folder, year)
            if not os.path.isdir(path_year):
                continue
            
            for rebal in os.listdir(path_year):
                path_rebal = os.path.normpath(os.path.join(path_year, rebal))
                if os.path.isfile(path_rebal):
                    f = path_rebal
                    rebal = rebal.split('.xlsx')[0]
                else:
                    f = filename.format(date=rebal)
                    if not os.path.isdir(path_rebal):
                        continue
                    if not (f in os.listdir(path_rebal)):
                        continue
                    
                    if pd.to_datetime(rebal) < pd.to_datetime(start_date):
                        continue
                    f = os.path.normpath(os.path.join(path_rebal, f))
                
                try:
                    reb_date = pd.to_datetime(rebal)
                except:
                    continue
                
                if rebalance in ['monthly', 'kech']:
                        reb_date = self.eom_of_given_date(rebal)
                elif rebalance in ['adhoc']:
                    reb_date = pd.to_datetime(rebal)
                else:
                    reb_date = pd.to_datetime(rebal)
                list_path.loc[pd.to_datetime(reb_date)] = f
        return list_path
    def load_compo_from_excel(self, file_path):
        """
        Load composition data from an Excel file.
        
        Task: None

        Args:
            file_path (str): The path to the Excel file.

        Returns:
            DataFrame: The composition data loaded from the Excel file.
        """
        pass
    def concatenate_excel(self, list_files):
        """
        Concatenate composition data from multiple Excel files.
        
        Task:
            1. Load and concatenate files
            2. If compo is emtpy, create an empty dataframe with 2 columns 'DATE', 'security_id'

        Args:
            list_files (pd.Series): Series containing file paths.

        Returns:
            DataFrame: The concatenated composition data in timeseries format
        
        Example:
            
            import dbtools.src.update_quant_portfolio as qidx
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            qidx_updater = qidx.KechCompoUpdater(Conf.spec_kech)
            files = qidx_updater.list_files()
            df_concat = qidx_updater.concatenate_excel(files.iloc[-2:])
            print(df_concat.head())
        """
        date_field = self.config.get('table_date_field', 'DATE')
        indicator_field = self.config.get('table_indicator_field', 'index_id')
        add_columns = self.config.get('additional_colums', [])
        columns = [date_field, indicator_field, 'security_id'] + add_columns
        df_ptf = pd.DataFrame(columns = columns)
        if list_files.empty:
            return df_ptf
        for date in list_files.index:
            df_tmp = self.load_compo_from_excel(list_files.loc[date])
            df_tmp[date_field] = date
            df_tmp.index.name = 'security_id'
            df_tmp = df_tmp.reset_index()
            if df_ptf.empty:
                df_ptf = df_tmp.copy()
            else:
                df_ptf = pd.concat((df_ptf, df_tmp))
        df_ptf = df_ptf.dropna(how = 'all', axis = 1)
        if df_ptf.empty:
            df_ptf = pd.DataFrame(columns = [date_field, 'security_id'])
        df_ptf = df_ptf.set_index([date_field, 'security_id']).reset_index()
        return df_ptf
    
    def transform_to_db_format(self, df_ptf):
        """
        Transform composition data to a database-compatible format.
        
        Task:
            1. Rename columns of timeseries df_ptf with corresponding index_id
            2. Convert to database format with SECURITYRATIO multiplied by 100
        Args:
            df_ptf (DataFrame): The composition data.

        Returns:
            DataFrame: The transformed composition data.
        
        Example:
            
            import dbtools.src.update_quant_portfolio as qidx
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            qidx_updater = qidx.KechCompoUpdater(Conf.spec_kech)
            files = qidx_updater.list_files()
            df_concat = qidx_updater.concatenate_excel(files.iloc[-2:])
            df_db = qidx_updater.transform_to_db_format(df_concat)
            print(df_db.head())
        """
        col_map = self.config['col_map']
        indicator_field = self.config.get('table_indicator_field', 'index_id')
        date_field = self.config.get('table_date_field', 'DATE')
        table_dict = self.config.get('table_data_dictionary', None)
        add_columns = self.config.get('additional_colums', None)
        set_index = [date_field, 'security_id']
        if add_columns:
            set_index = set_index + add_columns
        df_ptf = df_ptf.set_index(set_index)
        df_ptf = df_ptf[df_ptf.columns.intersection(col_map.keys())].rename(columns = col_map)
        if table_dict:
            df_dict = pd.read_sql(f"""select {indicator_field}, name from QUANT_work..{table_dict}""", con_mis)
            s_map_attr_id = df_dict.set_index('name')[indicator_field]
        else:
            s_map_attr_id = s_ticker_index_id.copy()
        df_ptf = df_ptf.rename(columns = s_map_attr_id)
        df_ptf.columns.name = indicator_field
        value_field = self.config.get('table_compo_value_field', 'SECURITYRATIO')
        df_ptf = df_ptf.stack().to_frame(value_field)
        df_ptf = df_ptf.reset_index()
        
        df_ptf = df_ptf.sort_values(by = [date_field, indicator_field, 'security_id']).drop_duplicates()
        return df_ptf

    def run_update_compo(self, flag_to_sql = True):
        """
        Run the composition update process.
        
        Task:
            1. Load compo from files
            2. Filter existing date before importing to database
            3. Add new compo to config['table_compo'] if flag_to_sql = True

        Args:
            flag_to_sql (bool): Whether to import data to SQL (default is True).

        Returns:
            DataFrame: The updated composition data.
        
        Example:
            
            import dbtools.src.update_quant_portfolio as qidx
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            qidx_updater = qidx.KechCompoUpdater(Conf.spec_kech)
            ################# run_update_compo ##################
            df = qidx_updater.run_update_compo(flag_to_sql = False)
        """
        date_compo = self.df_index_ref.date_compo
        table_name = self.config.get('table_compo','INDEX_COMPOSITION')
        # Load compo from files
        l_files = self.list_files()
        l_files = l_files[l_files.index>date_compo.min()]
        df_pr = self.concatenate_excel(l_files)
        df_pr = self.transform_to_db_format(df_pr)
        if self.config.get('table_indicator_field') in ['index_id']:
            df_gr = df_pr.copy()
            df_gr.index_id = df_gr.index_id.replace(s_pr_tr_id)
        else:
            df_gr = pd.DataFrame()
        df_compo = pd.concat((df_pr, df_gr)).drop_duplicates()
        # Filter existing date before importing to database
        df_compo = self.filter_existing_data(df_compo, date_compo)
        df_compo_ts = df_compo.copy()
        if df_compo_ts.empty:
            print('-------- All compositions are up-to-date, nothing to import ------')
            return df_compo_ts
        
        # Add new compo to config['table_compo'] if flag_to_sql = True
        if flag_to_sql:
            try:
                import_to_sql(df_compo, table_name)
            except:
                print('--------- CANNOT IMPORT TO %s ---------' %table_name)
        return df_compo_ts

class CoverageCompoUpdater(QuantCompoUpdater):
    """
    Add new composition of KC coverage into config['table_compo']
    
    
    Example:
        .. code-block:: python
            
            import dbtools.src.update_quant_portfolio as qidx
            from copy import deepcopy
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf

            config_idx = deepcopy(Conf.spec_cov)
            qidx_updater = qidx.CoverageCompoUpdater(config_idx)
            df = qidx_updater.run_update_compo(flag_to_sql = False)
    """

    def run_update_compo(self, flag_to_sql = True):
        """
        Add new composition of KC coverage into config['table_compo']
        Usage example of each function is reported separately but in the usgae of the class entirely
        
        Task:
            1. Retrieve rebal dates from last update and today.
            2. Load coverage universe on rebalancing date to update
            3. Add new compo to config['table_compo'] if flag_to_sql = True
        
        Args:
            flag_to_sql (bool): import to sql or not
        Returns:
            df_compo_ts (dataframe): composition in timeseries format
        
        Example:
            
            import dbtools.src.update_quant_portfolio as qidx
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            qidx_updater = qidx.CoverageCompoUpdater(Conf.spec_kech)
            ################# run_update_compo ##################
            df = qidx_updater.run_update_compo(flag_to_sql = False)
        """
        date_compo = self.df_index_ref.date_compo
        table_name = self.config.get('table_compo','INDEX_COMPOSITION')
        rebalance = self.config['rebalance']
        excl_sec_id = self.config.get('exclusion_security_id', [])
        excl_td_id = self.config.get('exclusion_trading_id', [])
        prev_rebal = date_compo.min() + BDay(1)
        today = pd.Timestamp.now().strftime('%Y%m%d')
        rebal_dates = bkt.Rebalance(prev_rebal, today, rebalance).rebal_date
        df_pr = pd.DataFrame(columns= ['DATE', 'index_id', 'security_id', 'SECURITYRATIO'])
        for rebal in rebal_dates:
            df = rep.get_kc_coverage_histo(rebal)
            df['td_id'] = df.security_id.map(rep.mapping_from_security(df.security_id.unique(), code = 'prim_td_id'))
            df = df[~df.security_id.isin(excl_sec_id)]
            df = df[~df.td_id.isin(excl_td_id)]
            df = df[['security_id']]
            
            df['index_id'] = self.config['index_id'][0]
            df['DATE'] = rebal
            df['SECURITYRATIO'] = 100/df.shape[0]
            if df_pr.empty:
                df_pr = df.copy()
            else:
                df_pr = pd.concat((df_pr, df))
        df_gr = df_pr.copy()
        df_gr.index_id = df_gr.index_id.replace(s_pr_tr_id)
        df_compo = pd.concat((df_pr, df_gr)).drop_duplicates()
        df_compo = self.filter_existing_data(df_compo, date_compo)
        df_compo_ts = df_compo.copy()#.set_index(['DATE', 'index_id', 'security_id']).SECURITYRATIO.unstack().T
        if df_compo_ts.empty:
            print('-------- All compositions are up-to-date, nothing to import ------')
            return df_compo_ts
        if flag_to_sql:
            try:
                import_to_sql(df_compo, table_name)
            except:
                print('--------- CANNOT IMPORT TO %s ---------' %table_name)
        
        return df_compo_ts

class KechCompoUpdater(QuantCompoUpdater):
    """
    Add new composition of KC beta profiles into config['table_compo']

    Example:
        .. code-block:: python
        
            ### This script is similar to function "run_update_compo" of the class ###
            import dbtools.src.update_quant_portfolio as qidx
            from copy import deepcopy
            import pandas as pd
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            config_idx = deepcopy(Conf.spec_kech)
            qidx_updater = qidx.KechCompoUpdater(config_idx)
            ################# run_update_compo ##################
            df = qidx_updater.run_update_compo(flag_to_sql = False)

            ############### Equivalent to ######################
            s_pr_tr_id = Conf.s_pr_tr_id
            date_compo = qidx_updater.df_index_ref.date_compo
            table_name = qidx_updater.config['table_compo']
            l_files = qidx_updater.list_files()
            l_files = l_files[l_files.index>=date_compo.min()]
            df_pr = qidx_updater.concatenate_excel(l_files)
            df_pr = qidx_updater.transform_to_db_format(df_pr)
            df_gr = df_pr.copy()
            df_gr.index_id = df_gr.index_id.replace(s_pr_tr_id)
            df_compo = pd.concat((df_pr, df_gr)).drop_duplicates()
            # df_compo = qidx_updater.filter_existing_data(df_compo, date_compo)
            
            df_compo_ts = df_compo.set_index(['DATE', 'index_id', 'security_id']).SECURITYRATIO.unstack().T
            ############################################################
    """

    def load_compo_from_excel(self, file_path):
        """
        Load composition from excel files
        
        Task:
            1. Get data composition of porfolios in config['portfolios'] from config['sheet_name']
            2. Add benchmark composition: all securities in universe
            3. Equally weighted (in %)
            
        Args:
            file_path (str): The path to the Excel file.

        Returns:
            DataFrame: The composition data loaded from the Excel file.
        
        Example:
            
            import dbtools.src.update_quant_portfolio as qidx
            from copy import deepcopy
            import pandas as pd
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            s_pr_tr_id = Conf.s_pr_tr_id
            config_idx = deepcopy(Conf.spec_kech)
            qidx_updater = qidx.KechCompoUpdater(config_idx)
            files = qidx_updater.list_files()
            df = qidx_updater.load_compo_from_excel(files.iloc[-1])
        """
        config = self.config
        sheet_name = config['sheet_name']
        list_ptf = list_ptf = config['portfolios']
        # Get data composition of porfolios in config['portfolios'] from config['sheet_name'] 
        df_kc = pd.read_excel(file_path, sheet_name = sheet_name, index_col = 0)

        df_kc = df_kc[df_kc.columns.intersection(list_ptf)]
        df_kc = df_kc[df_kc.index.notna()].replace({0:np.nan})

        df_kc.index.name = 'security_id'
        # Add benchmark composition: all securities in universe
        df_kc = (df_kc.notna()*1).replace({0:np.nan})
        df_kc['benchmark'] = 1
        # Equally weighted (in %)
        df_kc = df_kc.div(df_kc.sum())*100
        df_kc = df_kc.dropna(how = 'all', axis =1)

        return df_kc

class FactorSentimentUpdater(QuantCompoUpdater):
    """
    Add new composition of KC beta factor score into config['table_compo']
    
    Example:
        .. code-block:: python
        
            ### This script is similar to function "run_update_compo" of the class ###
            import dbtools.src.update_quant_portfolio as qidx
            from copy import deepcopy
            import pandas as pd
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            s_pr_tr_id = Conf.s_pr_tr_id
            config_idx = deepcopy(Conf.spec_factor_sentiment)
            qidx_updater = qidx.FactorSentimentUpdater(config_idx)
            ################# run_update_compo ##################
            df = qidx_updater.run_update_compo(flag_to_sql = False)

            ############### Equivalent to ######################

            date_compo = qidx_updater.df_index_ref.date_compo
            table_name = qidx_updater.config['table_compo']
            l_files = qidx_updater.list_files()
            l_files = l_files[l_files.index>=date_compo.min()]
            df_pr = qidx_updater.concatenate_excel(l_files)
            df_pr = qidx_updater.transform_to_db_format(df_pr)
            df_gr = df_pr.copy()
            df_gr.index_id = df_gr.index_id.replace(s_pr_tr_id)
            df_compo = pd.concat((df_pr, df_gr)).drop_duplicates()
            # df_compo = qidx_updater.filter_existing_data(df_compo, date_compo)
            
            df_compo_ts = df_compo.set_index(['DATE', 'index_id', 'security_id']).SECURITYRATIO.unstack().T
    """

    def load_compo_from_excel(self, file_path):
        """
        Load composition from excel files
        
        Task:
            1. Get data composition of porfolios in config['portfolios'] from config['sheet_name']
            2. Add insert date in asthe cutoff date

        
        Args:
            file_path (str): The path to the Excel file.

        Returns:
            DataFrame: The composition data loaded from the Excel file.
        
        Example:
            
            import dbtools.src.update_quant_portfolio as qidx
            from copy import deepcopy
            import pandas as pd
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            s_pr_tr_id = Conf.s_pr_tr_id
            config_idx = deepcopy(Conf.spec_factor_sentiment)
            qidx_updater = qidx.FactorSentimentUpdater(config_idx)
            files = qidx_updater.list_files()
            df = qidx_updater.load_compo_from_excel(files.iloc[-1])
        """
        config = self.config
        sheet_name = config['sheet_name']
        list_ptf = list_ptf = config['portfolios']
        # Get data composition of porfolios in config['portfolios'] from config['sheet_name']
        df_kc = pd.read_excel(file_path, sheet_name = sheet_name, index_col = 0)

        df_kc = df_kc[df_kc.columns.intersection(list_ptf)]
        df_kc = df_kc[df_kc.index.notna()]
        df_kc.index.name = 'security_id'
        # Add insert date in asthe cutoff date
        insert_date = pd.to_datetime(file_path.split('\\')[-2])
        df_kc['insert_date'] = insert_date
        return df_kc

class LBPCompoUpdater(QuantCompoUpdater):
    """
    Add new composition of LBP profiles into config['table_compo']
    
    Example:
        .. code-block:: python
        
            ### This script is similar to function "run_update_compo" of the class ###
            import dbtools.src.update_quant_portfolio as qidx
            from copy import deepcopy
            import pandas as pd
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            s_pr_tr_id = Conf.s_pr_tr_id
            config_idx = deepcopy(Conf.spec_lbp_v3)
            qidx_updater = qidx.LBPCompoUpdater(config_idx)
            ################# run_update_compo ##################
            df = qidx_updater.run_update_compo(flag_to_sql = False)

            ############### Equivalent to ######################

            date_compo = qidx_updater.df_index_ref.date_compo
            table_name = qidx_updater.config['table_compo']
            l_files = qidx_updater.list_files()
            l_files = l_files[l_files.index>=date_compo.min()]
            df_pr = qidx_updater.concatenate_excel(l_files)
            df_pr = qidx_updater.transform_to_db_format(df_pr)
            df_gr = df_pr.copy()
            df_gr.index_id = df_gr.index_id.replace(s_pr_tr_id)
            df_compo = pd.concat((df_pr, df_gr)).drop_duplicates()
            # df_compo = qidx_updater.filter_existing_data(df_compo, date_compo)
            
            df_compo_ts = df_compo.set_index(['DATE', 'index_id', 'security_id']).SECURITYRATIO.unstack().T
    """

    def load_compo_from_excel(self, file_path):
        """
        Load composition from excel files
        
        Task:
            1. Get data composition of porfolios in config['portfolios'] from config['sheet_name']
            2. Transform MorningStar_CD (cyclical and defensive) and sales_pct (global and domestic) to dummy
            3. Add benchmark composition: all securities in universe
            4. Equally weighted (in %)
            
        Args:
            file_path (str): The path to the Excel file.

        Returns:
            DataFrame: The composition data loaded from the Excel file.
        
        Example:
            
            import dbtools.src.update_quant_portfolio as qidx
            from copy import deepcopy
            import pandas as pd
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            s_pr_tr_id = Conf.s_pr_tr_id
            config_idx = deepcopy(Conf.spec_lbp_v3)
            qidx_updater = qidx.LBPCompoUpdater(config_idx)
            files = qidx_updater.list_files()
            df = qidx_updater.load_compo_from_excel(files.iloc[-1])

        """
        config = self.config
        sheet_name = config['sheet_name']
        list_ptf = list_ptf = config['portfolios']
        # Get data composition of porfolios in config['portfolios'] from config['sheet_name']
        df = pd.read_excel(file_path, sheet_name = sheet_name, index_col = 0)
        if 'member' in df.columns:
            df = df[df.member==1]
        df = df[df.columns.intersection(list_ptf)]
        df.index.name = 'security_id'
        df = df.dropna(how ='all', axis = 1)
        # Transform MorningStar_CD (cyclical and defensive) and sales_pct (global and domestic) to dummy
        if 'MorningStar_CD' in df.columns:
            df['cyclical'] = (df['MorningStar_CD']=='C')*1
            df['defensive'] = (df['MorningStar_CD']=='D')*1
            df = df.drop(columns = ['MorningStar_CD'])
        if 'sales_pct' in df.columns:
            df['global'] = (df['sales_pct']=='GL')*1
            df['domestic'] = (df['sales_pct']=='DO')*1
            df = df.drop(columns = ['sales_pct'])
        df = df.replace({0:np.nan})
        # Add benchmark composition: all securities in universe
        df['benchmark'] = 1
        # Equally weighted (in %)
        df = df.div(df.sum())*100
        df = df.dropna(how = 'all', axis =1)
        return df
    
class FBCompoUpdater(QuantCompoUpdater):
    """
    Add new composition of KC Family Businesses into config['table_compo']
    
    Example:
        .. code-block:: python
        
            ### This script is similar to function "run_update_compo" of the class ###
            import dbtools.src.update_quant_portfolio as qidx
            from copy import deepcopy
            import pandas as pd
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            config_idx = deepcopy(Conf.spec_fb)
            qidx_updater = qidx.FBCompoUpdater(config_idx)
            ################# run_update_compo ##################
            df = qidx_updater.run_update_compo(flag_to_sql = False)

            ############### Equivalent to ######################
            s_pr_tr_id = Conf.s_pr_tr_id
            date_compo = qidx_updater.df_index_ref.date_compo
            table_name = qidx_updater.config['table_compo']
            l_files = qidx_updater.list_files()
            l_files = l_files[l_files.index>=date_compo.min()]
            df_pr = qidx_updater.concatenate_excel(l_files)
            df_pr = qidx_updater.transform_to_db_format(df_pr)
            df_gr = df_pr.copy()
            df_gr.index_id = df_gr.index_id.replace(s_pr_tr_id)
            df_compo = pd.concat((df_pr, df_gr)).drop_duplicates()
            # df_compo = qidx_updater.filter_existing_data(df_compo, date_compo)
            
            df_compo_ts = df_compo.set_index(['DATE', 'index_id', 'security_id']).SECURITYRATIO.unstack().T
    """
    
    def load_compo_from_excel(self, file_path):
        """
        Load composition from excel files
        
        Task:
            1. Get data composition of porfolios in config['portfolios'] from config['sheet_name']
            2. Mapping security_id from KC company_id
            3. Exclude trading_id and security ids in config
            4. Equally weighted (in %)
            
        Args:
            file_path (str): Excel path of composition of FB
                            FB components are given by Research Equity (Kathleen)
        Returns:
            df_compo (df): composition of FB with 'security_id', 'index_id', 'SECURITYRATIO'
        
        Example:
            
            import dbtools.src.update_quant_portfolio as qidx
            from copy import deepcopy
            import pandas as pd
            import sys, os
            sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
            from config.quant_portfolio import QuantIndexConfig as Conf
            s_pr_tr_id = Conf.s_pr_tr_id
            config_idx = deepcopy(Conf.spec_fb)
            qidx_updater = qidx.FBCompoUpdater(config_idx)
            files = qidx_updater.list_files()
            df = qidx_updater.load_compo_from_excel(files.iloc[-1])
        """
        config = self.config
        sheet_name = config['sheet_name']
        list_ptf = list_ptf = config['portfolios']
        excl_sec_id = config.get('exclusion_security_id', [])
        excl_td_id = config.get('exclusion_trading_id', [])
        
        # Get data composition of porfolios in config['portfolios'] from config['sheet_name']
        df = pd.read_excel(file_path, sheet_name = sheet_name, index_col = 0)
        # Mapping security_id from KC company_id
        df['kc_company_id'] = df.index.str[1:].astype(int).astype(str)
        df['security_id'] = df.kc_company_id.map(rep.mapping_security_id_from(df.kc_company_id, code = 'kc_company_id'))
        # Exclude trading_id and security ids in config
        df['td_id'] = df.security_id.map(rep.mapping_from_security(df.security_id.unique(), code = 'prim_td_id'))
        df = df[~df.security_id.isin(excl_sec_id)]
        df = df[~df.td_id.isin(excl_td_id)]
        df = df.set_index('security_id').sort_index()
        df = df[~df.index.isin(excl_sec_id)]
        df = df[df.columns.intersection(list_ptf)]
        df = df.replace({0:np.nan})
        # Equally weighted (in %)
        df = df.div(df.sum())*100
        df = df.dropna(how = 'all', axis = 1)
        return df
#%% function

def patch_compo():
    """
    Patch for index composition
    Tasks:
        #. Load config
        #. Load existing composition in patch
        #. Load composition without patch
        #. Exclude security in config patch and recompute weight
        #. Keep new composition in patch and import to sql
    Args:
        
    Returns:
        Dataframes: df_compo_ts_corr, df_to_import_ts
    """
    print('-------- PATCH for index composition -------------')
    config = Conf_patch.config
    security_id_excluded = config['security_id_excluded']
    table_name = config['table_compo']
    
    df_compo_patch = pd.read_sql(f"""select * from QUANT_work..{table_name}""", con_mis)
    existing_patch = df_compo_patch.set_index(['DATE','index_id','security_id']).index
    df_compo_corr = pd.DataFrame(columns = df_compo_patch.columns)
    for s in security_id_excluded.keys():
        for k in security_id_excluded[s].keys():
            
            index_id = security_id_excluded[s][k]['index_id']
            start_date = security_id_excluded[s][k]['start_date']
            end_date = security_id_excluded[s][k]['end_date']
            id_str = ','.join(str(x) for x in index_id)
    
            df_compo = pd.read_sql(f"""select * from QUANT_work..INDEX_COMPOSITION 
                                   where index_id in({id_str})
                                   and DATE>'{start_date}'
                                   and DATE<'{end_date}'""", con_mis)
            df_compo_ts = df_compo.set_index(['DATE','index_id','security_id']).SECURITYRATIO.unstack()
            if s in df_compo_ts.columns:
                df_compo_ts_corr = df_compo_ts.dropna(subset = [s])
                df_compo_ts_corr = df_compo_ts_corr.drop(columns = [s])
            else:
                continue
            df_compo_ts_corr = df_compo_ts_corr.div(df_compo_ts_corr.sum(axis = 1), axis = 0)*100
            df_compo_corr_tmp = df_compo_ts_corr.stack().to_frame('SECURITYRATIO').reset_index()
            print(s, k, df_compo_corr_tmp.shape)
            df_compo_corr = pd.concat((df_compo_corr, df_compo_corr_tmp))

    df_to_import = pd.concat((df_compo_patch, df_compo_corr))
    df_to_import.DATE = pd.to_datetime(df_to_import.DATE)
    df_to_import = df_to_import.drop_duplicates(keep = False)
    
    
    df_to_import_ts = df_to_import.set_index(['DATE','index_id','security_id']).SECURITYRATIO
    df_to_import_ts = df_to_import_ts[~df_to_import_ts.index.isin(existing_patch)]
    df_to_import = df_to_import_ts.reset_index().sort_values(['DATE','index_id','security_id'])

    df_to_import_ts = df_to_import_ts.unstack()
    if not df_to_import.empty:
        import_to_sql(df_to_import, table_name)
    return df_to_import_ts

def import_to_sql(df, table_name):
    """
    Import to SQL
    
    Task:
        1. Import df to table_name
    
    Args:
        df (df): data to import to table
        table_name (str): table name to import data
    
    Example:
        .. code-block:: python
        
            import dbtools.src.update_quant_portfolio as qidx
            import pandas as pd
            df = pd.DataFrame()
            df = qidx.import_to_sql(df, 'INDEX_LEVEL')
    """

    db_quant_index = db.DatabaseManager(table_name)
    db_quant_index.append_table(df = df.copy())
    pass

def load_quant_holidays():
    """
    Load quant holidays from QUANT_work..quant_holidays
    
    Task:
        1. Load quant holidays from QUANT_work..quant_holidays
    
    Example:
        .. code-block:: python
        
            import dbtools.src.update_quant_portfolio as qidx
            qidx.load_quant_holidays()
    """
    holidays = pd.read_sql('select * from QUANT_work..quant_holidays', con_mis)
    holidays = pd.to_datetime(holidays['date'].values)
    return holidays

def get_index_level(index_id, start_date, end_date, patch=False):
    """
    Get index component from QUANT_work..INDEX_LEVEL
    
    Task:
        1. Get index component from QUANT_work..INDEX_LEVEL
    Args:
        index_id (list(int)): list of quant index ids.
        start_date (str): start date.
        end_date (str): end date.
        patch (bool): if true, load index level from INDEX_LEVEL_PATCH

    Returns:
        df (df): index level in ts format
    
    Example:
        .. code-block:: python
        
            import dbtools.src.update_quant_portfolio as qidx
            df = qidx.get_index_level([95], '20230101', '20230131')
    """
    sec_str = ','.join([str(int(s)) for s in index_id])
    req = """
     select DATE, index_id, value from QUANT_work..INDEX_LEVEL
     where index_id in (%s)
     and DATE>='%s' and DATE<='%s'
     
    """ %(sec_str, start_date, end_date)
    df = pd.read_sql(req, con_mis)
    if patch:
        req = """
         select DATE, index_id, value from QUANT_work..INDEX_LEVEL_PATCH
         where index_id in (%s)
         and DATE>='%s' and DATE<='%s'
         
        """ %(sec_str, start_date, end_date)
        df_patch = pd.read_sql(req, con_mis)
        df = pd.concat((df, df_patch))
        df = df.drop_duplicates(subset = ['DATE', 'index_id'], keep = 'last')
    df.DATE = pd.to_datetime(df.DATE)
    df = df.set_index(['DATE', 'index_id']).value.unstack()
    df.index.name = None
    df.columns.name = None
    return df

def get_index_compo(index_id, start_date, end_date, patch=False):
    """
    Get index component from QUANT_work..INDEX_COMPOSITION
    
    Task:
        1. Get index component from QUANT_work..INDEX_COMPOSITION
    Args:
        index_id (list): list of index ids
        start_date (str): start date
        end_date (str): end date
        patch (bool): if true, load index level from INDEX_COMPOSITION_PATCH
        df (df): composition index in db format
    
    Example:
        .. code-block:: python
        
            import dbtools.src.update_quant_portfolio as qidx
            df = qidx.get_index_compo([95], '20230101', '20231231')
    """
    
    sec_str = ','.join([str(int(s)) for s in index_id])
    req = """
     select DATE, index_id, security_id, SECURITYRATIO from QUANT_work..INDEX_COMPOSITION
     where index_id in (%s)
     and DATE>='%s' and DATE<='%s'
    """ %(sec_str, start_date, end_date)
    df = pd.read_sql(req, con_mis).set_index(['DATE', 'index_id'])
    
    if patch:
        req = """
         select DATE, index_id, security_id, SECURITYRATIO from QUANT_work..INDEX_COMPOSITION_PATCH
         where index_id in (%s)
         and DATE>='%s' and DATE<='%s'
        """ %(sec_str, start_date, end_date)
        df_patch = pd.read_sql(req, con_mis).set_index(['DATE', 'index_id'])
        df_compo = df[~df.index.isin(df_patch.index)]
        df = pd.concat((df_compo, df_patch))
    
    df = df.reset_index()
    df.DATE = pd.to_datetime(df.DATE)
    df.SECURITYRATIO = df.SECURITYRATIO/100
    df = df.sort_values(['DATE', 'index_id', 'security_id'])
    return df

def analyze_index(index_id, start_date, end_date):
    """
    Analyze index performance from QUANT_work..INDEX_LEVEL
    
    Task:
        1. Get index level (price return and gross return)
        2. For each index _id, compute performance by bkt.analyze_performance then concate to dataframe output
    Args:
        index_id: list of index ids
        start_date (str): start date
        end_date (str): end date
    Returns:
        df_index (df): output of get_index_data, index level in ts format
        df_index_perf (df): performance (total return, annual return, vol,...)
    
    Example:
        .. code-block:: python
        
            import dbtools.src.update_quant_portfolio as qidx
            df = qidx.analyze_index([95], '20230101', '20231231')
    """
    idx = pd.Index(index_id).drop_duplicates()
    idx = idx.union(s_pr_tr_id.loc[s_pr_tr_id.index.intersection(idx).values])
    df_index = get_index_level(idx, start_date, end_date)

    df_index_perf = pd.DataFrame()
    for i in index_id:

        index_gr = s_pr_tr_id.loc[s_pr_tr_id.index.intersection([i])]
        if index_gr.empty:
            index_gr = i
        else:
            index_gr = index_gr.iloc[0]
        df_idx = df_index[[i,index_gr]]
        df_idx.columns = ['price_return', 'gross_return']
        df_perf_ana = bkt.analyze_performance(df_idx, yearly = False)
        df_perf_ana.index.name = 'period'
        df_perf_ana = df_perf_ana.reset_index()
        df_perf_ana['index_id'] = i
        df_perf_ana = df_perf_ana.set_index(['index_id','period'])
        
        df_index_perf = pd.concat((df_index_perf, df_perf_ana))
    return df_index, df_index_perf

#%%

# =============================================================================
# kc_compo = FBCompo(Conf.spec_fb)
# df = kc_compo.run_update_compo()
# 
# kc_compo = FBCompo(Conf.spec_fb)
# date_compo = kc_compo.df_index_ref.date_compo
# l_files = kc_compo.list_files()
# l_files = l_files[l_files.index>date_compo.min()]
# df_pr = kc_compo.concatenate_excel(l_files)
# df_pr = kc_compo.transform_to_db_format(df_pr)
# df_gr = df_pr.copy()
# df_gr.index_id = df_gr.index_id.replace(s_pr_tr_id)
# df_compo = pd.concat((df_pr, df_gr)).drop_duplicates()
# df_compo = kc_compo.filter_existing_data(df_compo, date_compo)
# 
# s = pd.Series(['cat', 'dog', np.nan, 'rabbit'])
# s.map({'cat': 'kitten', 'dog': 'puppy'})
# 
# import dbtools.src.get_market_data as mkt
# security_id = df_compo[df_compo.index_id==93].security_id.values
# rep.mapping_from_security(security_id, code ='bbg')
# 
# df = mkt.get_corp_action_histo(security_id, '20240325', end_date)['adj_prc']
# df[df!=1].stack()
# 
# 6276 in security_id
# 
# df_prc = data_bkt['close_prc'][security_id].iloc[-5:]
# 
# bkt.Rebalance('20240101', '20240417', frequence='monthly').rebal_date
# =============================================================================
