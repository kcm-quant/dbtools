# -*- coding: utf-8 -*-
"""
Created on Thu Jan 16 11:08:12 2025

@author: nle
"""
import pandas as pd
import dbtools.src.get_repository as rep
from apitools.src.apitools import ReferenceBbg
import numpy as np

#%%
def get_perimeter(perimeter, security_id = [], date_ref=''):
    """
    Retrieves a set of security IDs based on the specified perimeter and reference date.

    Task:
        #. Determine the reference date (`date_ref`) if not provided.
        #. Handle different types of input for `perimeter`:
            - Numeric values to query a quant perimeter.
            - Specific keywords (e.g., 'kech', 'KECH') to retrieve historical coverage.
            - Strings to fetch index components for a given date.
            - Lists, arrays, or indices to recursively process each element and aggregate results.
        #. Consolidate results into a single set of unique security IDs.

    Args:
        perimeter (int, float, str, list, np.ndarray, or pd.Index): 
            The parameter defining the scope of security IDs to retrieve. Possible values include:
            - Numeric values corresponding to a quantitative perimeter.
            - Specific strings like 'kech' or 'KECH' to trigger predefined queries.
            - Strings representing indices or lists of perimeters to process recursively.
        security_id (list): list of security_id
        date_ref (str, optional, default=''): 
            The reference date used in queries. Defaults to today's date if not specified or set to an empty string.

    Returns:
        pd.Index: A set of unique security IDs aggregated based on the input perimeter.

    Example:

    .. code-block:: python
        # Example usage of get_perimeter
        ids = get_perimeter(['kech', 1, 'CAC'], date_ref = '20250110')
    """
    # Default date_ref is set to today if not provided
    if date_ref in ['', None]:
        date_ref = pd.to_datetime('today').strftime('%Y%m%d')
    
    security_id = pd.Index(security_id)  # Initialize an empty index to store security IDs

    # Case 1: Numeric perimeter
    if isinstance(perimeter, (int, float)):
        sec_id = rep.get_quant_perimeter(perimeter, date_ref, date_ref)
        security_id = security_id.union(sec_id)
    
    # Case 2: Specific string perimeter ('kech' or 'KECH')
    elif perimeter in ['kech', 'KECH']:
        sec_id = rep.get_kc_coverage_histo(date_ref).index
        security_id = security_id.union(sec_id)
    
    # Case 3: String perimeter for index components
    elif isinstance(perimeter, str):
        sec_id = rep.get_index_comp(perimeter, start_date=date_ref, end_date=date_ref).columns
        security_id = security_id.union(sec_id)
    
    # Case 4: Recursive processing for list-like perimeters
    elif isinstance(perimeter, (list, np.ndarray, pd.Index)):
        for k in perimeter:
            sec_id = get_perimeter(k, date_ref='')
            security_id = security_id.union(sec_id)

    return security_id



def check_referential(perimeter, security_id = [], date_ref='',
                      map_quant_ref={'bbg': 'security',
                                     'fund_ticker': 'EQY_FUND_TICKER', 
                                     'sedol': 'ID_SEDOL1'}):
    """
    Compares security reference data from an internal referential with Bloomberg's referential.

    Task:
        #. Set default `date_ref` to today's date if not provided.
        #. Ensure 'bbg' is included in the mapping dictionary (`map_quant_ref`).
        #. Retrieve security IDs based on the provided perimeter.
        #. Load internal reference data (`df_ref_quant`) and Bloomberg reference data (`df_ref_bbg`).
        #. Compare corresponding fields from the two sources and identify mismatches.
        #. Return a DataFrame with differences for further review.

    Args:
        perimeter (int, float, str, list, np.ndarray, or pd.Index): 
            Defines the scope of security IDs to process. Can be numeric, string, or list-like.
        
        security_id (list): list of security_id
        
        date_ref (str, optional, default=''): 
            The reference date for queries. Defaults to today's date if not provided or empty.
        
        map_quant_ref (dict, optional, default={'bbg': 'security', 'fund_ticker': 'EQY_FUND_TICKER', 'sedol': 'ID_SEDOL1'}): 
            A mapping dictionary specifying fields to compare between internal and Bloomberg referential data.

    Returns:
        pd.DataFrame: A DataFrame containing discrepancies between internal and Bloomberg reference data.

    Example:

    .. code-block:: python
        # Example usage of check_referential
        differences = check_referential([], [2, 8], '')
    """
    # Set default date_ref to today's date if not provided
    if date_ref in ['', None]:
        date_ref = pd.to_datetime('today').strftime('%Y%m%d')
    
    # Ensure 'bbg' is included in map_quant_ref
    if 'bbg' not in map_quant_ref:
        map_quant_ref['bbg'] = 'security'
    
    # Step 1: Load perimeter
    security_id = get_perimeter(perimeter, security_id, date_ref='')

    # Step 2: Load internal reference data
    df_ref_quant = pd.DataFrame(index=security_id, columns=map_quant_ref)
    for field in map_quant_ref:
        df_ref_quant[field] = rep.mapping_from_security(security_id, field)

    # Create mapping of Bloomberg IDs to internal IDs
    s_bbg_id = pd.Series(index=df_ref_quant['bbg'].values, data=df_ref_quant['bbg'].index)

    # Step 3: Load Bloomberg reference data
    symbols = list((df_ref_quant['bbg'] + ' Equity'))
    d = ReferenceBbg(symbols)
    df_ref_bbg = d.get(['EQY_FUND_TICKER', 'ID_SEDOL1'])
    
    # Process Bloomberg data
    df_ref_bbg['security'] = df_ref_bbg['security'].str[:-7]  # Remove suffix from security strings
    df_ref_bbg = df_ref_bbg.astype(str)  # Ensure data type consistency
    df_ref_bbg['security_id'] = df_ref_bbg['security'].map(s_bbg_id)  # Map to internal security IDs
    df_ref_bbg = df_ref_bbg.set_index('security_id')  # Set security_id as index
    df_ref_bbg = df_ref_bbg.reindex(security_id)  # Align with internal reference data

    # Step 4: Compare internal and Bloomberg data
    cols_merged = np.array([[i, j] for i, j in map_quant_ref.items()]).flatten()  # Flatten mapping for comparison
    df_ref_merged = df_ref_quant.merge(df_ref_bbg, left_index=True, right_index=True)  # Merge data
    df_ref_merged = df_ref_merged[cols_merged]  # Select relevant columns
    df_ref_merged.index.name = 'security_id'  # Set index name

    # Create comparison DataFrame to store mismatch indicators
    df_comp = pd.DataFrame(index=df_ref_quant.index, columns=cols_merged)
    for i, j in map_quant_ref.items():
        mismatch = df_ref_quant[i] != df_ref_bbg[j]  # Identify mismatches
        df_comp[i] = mismatch
        df_comp[j] = mismatch

    # Filter mismatched rows
    df_diff_bool = df_comp[df_comp].dropna(how='all')  # Retain rows with any mismatches
    df_diff_bool['bbg'] = True  # Mark as having Bloomberg mismatches

    # Filter reference data to include only mismatched rows/columns
    df_check = df_ref_merged[df_diff_bool].dropna(how='all', axis=0).dropna(how='all', axis=1)
    if df_check.empty:
        print('----------- Check Referential OK ------------')
    else:
        print('----------- Check Referential KO ------------')
        print(df_check.to_string())
        print('-----------------------------')
    return df_check


