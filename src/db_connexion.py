# -*- coding: utf-8 -*-
import platform
import urllib
from sqlalchemy import create_engine, text
import os
import json

config_file = os.path.join(
    os.path.dirname(__file__), "../config/db_connexion.json")

with open(config_file) as f:
        config = json.load(f)

class SqlConnector:
    """
    Connexion to DB KECH-DB03
    """
    
    _PROD_SERVER = config['PROD_SERVER']
    _CRYSTAL_UAT_SERVER = config['CRYSTAL_UAT_SERVER']
    _MATLAB_SERVER = config['MATLAB_SERVER']
    _FLEXPORT_SERVER = config['FLEXPORT_SERVER']
    _PROD_LOGIN = config['PROD_LOGIN']
    _MATLAB_LOGIN = config['MATLAB_LOGIN']
    _QUANT_DB = config['QUANT_DB']
    _CRYSTAL_DB = config['CRYSTAL_DB']
    _FLEXPORT_DB = config['FLEXPORT_DB']
    _TIMEOUT = config['TIMEOUT'] # TIMEOUT en secondes.

    def __init__(self):
        
        """
        Initializes the SqlConnector instance.
        """
        self.engine_creator = create_engine

    def connection(self,server='production'):
        """
        Establishes a connection to a specified SQL server.

        Args:
            server (str, optional): The server to connect to. Defaults to 'production'.
                Valid options include 'production', 'Matlab', 'FLEXPORT', and 'crystal_uat'.

        Returns:
            engine: A SQLAlchemy engine instance for executing SQL queries.

        Tasks:
            1- Determine the server and corresponding connection string.
            2- Creates a SQLAlchemy engine for the specified server.
            3- Assigns the execution custom function to the engine object.
        Usage Example:
        ---------------
            from dbtools.src.db_connexion import SqlConnector
            connector = SqlConnector()
            con_mis = connector.connection()
        """
        if server=='production':
            if platform.system() == 'Windows' :
                connection_str = "mssql+pymssql://@%s/%s" % (self._PROD_SERVER,
                                                             self._QUANT_DB)
                engine = self.engine_creator(connection_str,
                                             connect_args={'timeout': self._TIMEOUT})
            else :
                server_name, port_number = self._PROD_SERVER.split(':')
                username = list(self._PROD_LOGIN.keys())[0]  
                password = self._PROD_LOGIN[username]
                connection_str = (
                    "DRIVER={ODBC Driver 17 for SQL Server};"
                    f"INSTANCE=BDDINST01;"
                    f"SERVER={server_name}.keplercm.lan;"
                    f"PORT={port_number};DATABASE={self._QUANT_DB};"
                    f"UID={username};PWD={password}"
                )
                params = urllib.parse.quote_plus(connection_str)
                engine = self.engine_creator("mssql+pyodbc:///?odbc_connect=%s" % params)
        elif server=='Matlab':
            username = list(self._MATLAB_LOGIN.keys())[0]  
            password = self._MATLAB_LOGIN[username]
            connection_str = f"mysql+pymysql://{username}:{password}@{self._MATLAB_SERVER}"
            engine = self.engine_creator(connection_str)

        elif server=="FLEXPORT":
            connection_str = "mssql+pymssql://@%s/%s" % (self._FLEXPORT_SERVER,
                                                         self._FLEXPORT_DB)
            engine = self.engine_creator(connection_str,
                                         connect_args={'timeout': self._TIMEOUT})
        
        elif server=='crystal_uat':
            if platform.system() == 'Windows' :
                connection_str = "mssql+pymssql://@%s/%s" % (self._CRYSTAL_UAT_SERVER,
                                                             self._CRYSTAL_DB)
                engine = self.engine_creator(connection_str,
                                             connect_args={'timeout': self._TIMEOUT})
        self.engine = engine
        engine.execute = self.execute
        
        return engine
    def execute(self,query, **params) : 
        """
        Executes an SQL query using the established connection.

        Args:
            query (str): The SQL query to execute.
            **params: Optional parameters to pass to the query.

        Returns:
            result: The result of the query execution.

        Tasks:
            1- Execute the provided SQL query using the connected engine.
            2- Handle and commit the transaction if necessary.
            3- Return the query execution result.

        Usage Example:
        ---------------
            from dbtools.src.db_connexion import SqlConnector
            connector = SqlConnector()
            con_mis = connector.connection()
            con_mis.execute('USE QUANT_work')

        """
        with self.engine.connect() as connection:
            statement = text(query).bindparams(**params)
            result = connection.execute(statement)
            connection.commit() 
            return result    

class MyEnv:
    """
    Environment for repository
    """
    read_only_repository = "x:\\kc_repository_new"
    get_tick = "get_tick\\ft"
    

    