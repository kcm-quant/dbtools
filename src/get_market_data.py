# -*- coding: utf-8 -*-
"""
**This module loads data from table MARKET_DATA, and QUANT_work**

Created on Mon Mar 28 16:26:44 2022

@author: tle (tle@keplercheuvreux.com)


Corporate actions
------------------------------
#. :py:func:`get_corp_action_histo`
#. :py:func:`adjust_corp_action`

Daily price
------------------------------
#. :py:func:`adjust_fx`
#. :py:func:`get_trading_daily`
#. :py:func:`get_index_prices`
#. :py:func:`get_index_ohlc`
#. :py:func:`get_index_level`
#. :py:func:`get_ffama_data`

Fundamental Data
------------------------------
#. :py:func:`get_fund_data`
#. :py:func:`transform_fund_data`
#. :py:func:`fill_fund_data`
#. :py:func:`compute_dividend`

EPS growth
------------------------------
#. :py:func:`compute_coe`
#. :py:func:`check_pe_fading_coe`
#. :py:func:`compute_organic_growth`

Other
--------
#. :py:func:`get_region_sales`

"""

import os
from dbtools.src.db_connexion import SqlConnector
import dbtools.src.get_repository as rep
from datetime import datetime
import pandas as pd
import numpy as np
import requests
from scipy.optimize import fsolve

connector = SqlConnector()
con_mis = connector.connection()

def adjust_fx(df_temp, ccy_ref='EUR'):
    """
    Adjust data to ccy_ref

    Args:
        df_temp (DataFrame): trading data (ex: 'turnover', 'prc', 'qty_1er_limit', 'tick_size')
            date in index, security_id in column
        ccy_ref (str): currency to convert (USD or EUR)

    Returns:
        DataFrame (date in index, security_id in column)

    .. exec_code::

        import dbtools.src.get_market_data as mkt
        security_id = [2, 8, 391, 512]
        dict_local = mkt.get_trading_daily(security_id=security_id, start_date='20220826',
                                            end_date= '20220905', field=['close_prc'], ccy_ref='')
        df_local = dict_local['close_prc']['MAIN']
        df_eur = mkt.adjust_fx(df_local, ccy_ref='EUR')
        print('------Close price in local ccy-----------')
        print(df_local.head())
        print('------Close price in EUR-----------')
        print(df_eur.head())
    """

    start_date = df_temp.index.min()
    end_date = df_temp.index.max()
    security_id = df_temp.columns
    ccy = rep.mapping_from_security(security_id=security_id, code='ccy')
    group_ccy = {key: ccy[ccy == key].index for key in ccy.unique()}
    fx_histo = rep.get_fx_histo(ccy.unique(), start_date, end_date, ccy_ref=ccy_ref)
    fx_histo = fx_histo.reindex(df_temp.index).ffill(limit=2)
    df_out = df_temp.copy()
    for key in group_ccy.keys():
        df_out[group_ccy[key]] = df_out[group_ccy[key]].mul(fx_histo[key], axis='index')

    return df_out

def get_corp_action_histo(security_id, start_date, end_date):
    """
    Read adj factor from table MARKET_DATA..CORP_ACTION_ADJUSTMENT_FACTOR into a DataFrame.

    Args:
        security_id (list): list of security_id
        start_date (str): %Y%m%d
        end_date (str): %Y%m%d

    Returns:
        dict of DataFrames
             keys ['adj_volume', 'adj_prc', 'adj_prc_special', 'adj_prc_regular']

    .. exec_code::

        import dbtools.src.get_market_data as mkt
        security_id = [2, 8, 391, 512]
        dict_ajd = mkt.get_corp_action_histo(security_id=security_id, start_date = '20220820', end_date = '20220901')
        print('Keys of adjustment dictionary: %s' % list(dict_ajd.keys()))
        print('Example of "adj_volume":')
        print(dict_ajd['adj_volume'])
    """
    sec_str = ",".join(map(str, security_id))
    cols = {'F0': 'adj_volume', 'F1_agg': 'adj_prc',
            'F2': 'adj_prc_special', 'F3': 'adj_prc_regular'}
    query = """select date, security_id, F0, F1, F1_agg, F2, F3
        from MARKET_DATA..CORP_ACTION_ADJUSTMENT_FACTOR
        where security_id in (%s)
        and date >= '%s' and date <= '%s'
        order by date, security_id
        """ % (sec_str, start_date, end_date)
    df_temp = pd.read_sql(query, con_mis).set_index(['date', 'security_id'])
    

    split = {}
    for col in cols:
        temp = df_temp[col].unstack()
        temp = temp.reindex(columns = security_id)
        split[cols[col]] = temp.rename_axis(columns=None, index=None)
    return split


def adjust_corp_action(df_temp, code, split_adj=True, special_adj=False, regular_adj=False):
    """
    Adjust price, volume due to corporate actions

    Args:
        df_temp (DataFrame): DataFrame from market_data
        code (str): 'is_volume' or 'is_prc', type of corporate data to adjust
        split_adj (bool):  adjust price, volume due to split
        special_adj (bool): adjust price with special dividend
        regular_adj (bool): adjust price with regular cash dividend

    Returns:
        DataFrame, df_temp adjusted with split

    .. exec_code::

        import dbtools.src.get_market_data as mkt
        security_id = [2, 8, 391, 512]
        dict_trading = mkt.get_trading_daily(security_id=security_id, start_date='20220826', split_adj=False,
                                            end_date= '20220905', field=['volume'], ccy_ref='EUR')
        df_trading_no_adj = dict_trading['volume']['MAIN']
        df_trading_adj = mkt.adjust_corp_action(df_trading_no_adj, split_adj=True, code = 'is_volume')
        print('------Close price in with adjustment-----------')
        print(df_trading_no_adj.head())
        print('------Close price with adjustment------------')
        print(df_trading_adj.head())
    """
    start_date = datetime.strftime(df_temp.index.min(), '%Y%m%d')
    end_date = datetime.strftime(df_temp.index.max(), '%Y%m%d')
    security_id = df_temp.columns
    split_histo = get_corp_action_histo(security_id, start_date, end_date)

    volume = 1/split_histo['adj_volume']
    split_prc = pd.DataFrame(1, index=volume.index, columns=volume.columns)
    split_volume = pd.DataFrame(1, index=volume.index, columns=volume.columns)

    if code == 'is_volume':
        if split_adj:
            split_volume = volume * split_volume
        split_code = split_volume
    elif code == 'is_prc':
        if split_adj:
            prc = split_histo['adj_prc'].copy().fillna(1)
            split_prc = prc * split_prc
        if special_adj:
            prc_special = split_histo['adj_prc_special'].copy().fillna(1)
            split_prc = prc_special * split_prc
        if regular_adj:
            regular_adj = split_histo['adj_prc_regular'].copy().fillna(1)
            split_prc = regular_adj * split_prc
        split_code = split_prc

    split_code = split_code.sort_index(ascending=False) \
        .shift(1).fillna(1).cumprod()
    split_code = split_code.reindex(df_temp.index).fillna(1)
    df_temp = df_temp * split_code
    df_temp = df_temp.rename_axis(columns=None, index=None)
    return df_temp


def _correct_EXO(df):
    """
    Concatenate data  of EXO IM (795445) and EXO NA (10174467)
    As if EXO IM (795445) changed listing from Italy to Amsterdam (EXO NA (10174467))
    """
    
    df_temp = df.copy()
    security_id = df.security_id.unique()
    df_tmp = df_temp[df_temp.security_id.isin([10174467, 795445])]
    
    df_temp = pd.concat((df_temp, df_tmp)).drop_duplicates(keep = False)
    
    field = df_temp.columns.difference(['date', 'trading_destination_id', 'security_id'])
    df_ = df_tmp.set_index(['date', 'trading_destination_id', 'security_id']).unstack()
    df_exo = pd.DataFrame()
    if not df_.empty:
        for f in field:
            df = df_[f].ffill(axis = 1).bfill(axis = 1).stack().to_frame(f)
            df_exo = pd.concat((df_exo, df), axis = 1)
        df_exo = df_exo.reset_index()[df_temp.columns]
    
    df_temp = pd.concat((df_temp, df_exo))
    df_temp = df_temp[df_temp.security_id.isin(security_id)]
    return df_temp

def check_change_trading_id(df):
    """
    Check for security_id which change primary trading
    
    Args:
        df (Dataframe): Database format
    """
    df_temp = df.copy()
    security_id = df_temp.security_id.unique()
    histo_td = rep.get_histo_reference_change(security_id, code = 'prim_td_id')
    count_td = histo_td.security_id.value_counts()
    sec_check = count_td[count_td>1].index
    for sec in sec_check:
        td_ = histo_td[histo_td.security_id==sec].primary_trading_destination_id
        df_temp.loc[(df_temp.security_id==sec) & (df_temp.trading_destination_id.isin(td_)), 'is_prim'] = True
    
    return df_temp

def get_trading_daily_old(security_id, start_date, end_date, ccy_ref='EUR',
                      field=['turnover', 'close_prc'],
                      trading_destinations=['MAIN'], flag_consolidated=False,
                      split_adj=True, special_adj=False, regular_adj=False):
    """
    Read trading daily from MARKET_DATA..trading_daily

    Args:
        security_id (list): list of security_id
        start_date (str): %Y%m%d
        end_date (str): %Y%m%d
        ccy_ref (str): in ['EUR', 'USD', ''], currency to convert to, if '', keep local currency
        trading_destinations (list): list, trading destination. ['MAIN'], ['MTF'], ['all'] or ['MAIN', other td_id], for ex ['MAIN', 189] or [td_id (int)]

                - ['MAIN']: get data on primary market
                - ['MTF']: get consolidated data on ALL MTFs (not primary market)
                - ['all']: get consolidated data on primary market and MTFs
                - ['MAIN', other td_id]: get consolidated data on primary market and given td_id
                - [td_id (int)]: get consolidated data on given td_id(s)
        flag_consolidated (bool): if True, compute consolidated market and add to output
        field (list): list of str, field to retrieve in MARKET_DATA
            [turnover, close_prc, open_prc, close_turnover,intraday_turnover,cross_turnover,...]
        split_adj (bool): adjust price, volume due to split (refer to adjust_corp_action)
        special_adj (bool): adjust price with special dividend (refer to adjust_corp_action)
        regular_adj (bool): adjust price with regular cash dividend (refer to adjust_corp_action)

    Returns:
        dict of dict of dataframes,
            with first keys are given in param field,
            second keys are trading_destination plus main and, consolidated market
        for ex:

            input:
                - field=['close_prc', 'turnover']
                - trading_destinations = ['MAIN', 159, 289]

            output: dict_out: dict
                        - close_prc: dict
                            - MAIN: dataframe
                            - 159: dataframe
                            - 189: dataframe
                            - CONS: dataframe

                        - turnover: dict
                            - MAIN: dataframe
                            - 159: dataframe
                            - 189: dataframe
                            - CONS: dataframe

    .. exec_code::

        import dbtools.src.get_market_data as mkt
        security_id = [2, 8, 391, 512]
        dict_trading = mkt.get_trading_daily(security_id=security_id, start_date='20220826', end_date= '20220905',
                                            split_adj=True, trading_destinations = ['MAIN', 289], flag_consolidated = True,
                                            field=['volume', 'close_prc'], ccy_ref='EUR')
        print('1st keys out dictionary output: %s' %list(dict_trading.keys()))
        dict_prc = dict_trading['close_prc']
        dict_vlm = dict_trading['volume']
        print('2rd keys out dictionary output: %s' %list(dict_prc.keys()))
        print()
        print('Example Main market dataframe output:')
        print(dict_vlm['MAIN'])
        print()
        print('Example market CBOE (td_id=159) dataframe output:')
        print(dict_vlm[289])
        print()
        print('Example consolidated market dataframe output:')
        print(dict_vlm['CONS'])
    """
    sec = pd.to_numeric(security_id, errors = 'coerce')
    if (10174467 in security_id) or (795445 in security_id):
        sec = pd.Index(security_id).union([10174467, 795445]).dropna()
    sec_str = ",".join(map(str, sec))
    fld_str = ",".join(field)
    

    
    prim_td = rep.mapping_from_security(security_id, 'prim_td_id')
    query = """
            select distinct security_id, date, trading_destination_id, %s
            from MARKET_DATA..trading_daily
            where security_id in (%s)
            and date between '%s' and '%s'
            """ % (fld_str, sec_str, start_date, end_date)
    # Load data, prim market, and delete trading destination NaN
    df_temp = pd.read_sql(query, con_mis)
    df_temp = _correct_EXO(df_temp)
    df_temp.date = pd.to_datetime(df_temp.date)
    df_temp['is_prim'] = df_temp.security_id.map(prim_td) == df_temp.trading_destination_id
    df_temp = check_change_trading_id(df_temp)
    df_temp = df_temp.set_index(['date', 'security_id'])
    df_temp = df_temp.dropna(subset=['trading_destination_id'])

    # trading_destinations: MAIN is primary market,
    # MTF is not primary market,
    # 'all': all markets (MTF and MAIN)
    if 'MAIN' in trading_destinations:
        df_temp = df_temp[df_temp.trading_destination_id.isin(trading_destinations)
                     | df_temp.is_prim]
    elif 'MTF' in trading_destinations:
        df_temp = df_temp[~df_temp.is_prim]
        trading_destinations = df_temp.trading_destination_id.unique()
    elif 'all' in trading_destinations:
        df_temp = df_temp.copy()
        trading_destinations = df_temp.trading_destination_id.unique()
    else:
        df_temp = df_temp[df_temp.trading_destination_id.isin(trading_destinations)]

    # aggregation in case of more than 1 trading destinations to get consolidated data
    path_config = os.path.join(os.path.dirname(__file__), '..')
    aggr_filename = 'config/consolidated_market_aggregation.txt'
    try:
        f = open(os.path.join(path_config, aggr_filename), "r")
    except:
        folder = 'C:/python_tools/dbtools/'
        f = open(folder + aggr_filename, "r")
    aggr = eval(f.read())

    df_cons = pd.DataFrame(index=df_temp.index.unique(), columns=df_temp.columns)
    if flag_consolidated:
        for key, value in aggr.items():
            cols = df_temp.columns.intersection(value)
            if len(cols) == 0:
                continue
            if key == 'sum':
                cols = df_temp.columns.intersection(value)
                df_cons[cols] = df_temp.groupby(['date', 'security_id']).sum()[cols]
            elif key == 'is_prim':
                cols = df_temp.columns.intersection(value)
                df_cons[cols] = df_temp[df_temp.is_prim][cols]
            elif key == 'nansum':
                cols = df_temp.columns.intersection(value)
                df_cons[cols] = df_temp.fillna(0).groupby(['date', 'security_id']).sum()[cols]
            elif key == 'min':
                cols = df_temp.columns.intersection(value)
                df_cons[cols] = df_temp.groupby(['date', 'security_id']).min()[cols]
            elif key == 'max':
                cols = df_temp.columns.intersection(value)
                df_cons[cols] = df_temp.groupby(['date', 'security_id']).max()[cols]
            elif key == 'sumweightedby':
                cols = df_temp.columns.intersection(value.keys())
                val = df_temp.columns.intersection(value.values())
                denom = df_temp.groupby(['date', 'security_id']).sum()[val]
                num = df_temp[cols].mul(df_temp[val].values) \
                    .groupby(['date', 'security_id']).sum()[cols]
                df_cons[cols] = num.div(denom.values)

        df_cons.trading_destination_id = 'CONS'
        df_cons.is_prim = False

    df_cons = pd.concat((df_temp, df_cons)).dropna(how='all')

    # fields to convert to ccy_ref
    fx_adj = ['turnover', 'prc', 'qty_1er_limit', 'tick_size']
    temp = np.array(len(df_temp.columns) * [False])
    for col in fx_adj:
        temp = df_temp.columns.str.contains(col) | temp
    fx_adj = df_temp.columns[temp]

    # fields to adjust with split
    prc_split = df_cons.columns[df_cons.columns.str.contains('prc')]
    volume_split = df_cons.columns[df_cons.columns.str.contains('volume')]

    # format df_cons to dict
    dict_out = {}
    td_id = list(trading_destinations) + ['MAIN', 'CONS']
    td_id = pd.Index(td_id).drop_duplicates(keep='last')
    for col in field:
        dict_out[col] = {}
        for td in td_id:
            temp = df_cons[df_cons.trading_destination_id == td][col].unstack()
            temp = temp.reindex(columns=security_id)
            dict_out[col][td] = temp.rename_axis(index=None, columns=None)

        temp = df_cons[df_cons.is_prim][col].unstack()
        temp = temp.reindex(columns=security_id)
        dict_out[col]['MAIN'] = temp.rename_axis(index=None, columns=None)

    # adjustment and convert to ccy
    for col in field:
        for td in td_id:
            if len(dict_out[col][td]) == 0:
                continue
            if (ccy_ref != '') and (col in fx_adj):
                temp = adjust_fx(dict_out[col][td], ccy_ref)
                dict_out[col][td] = temp.rename_axis(index=None, columns=None)
            if col in prc_split:
                temp = adjust_corp_action(dict_out[col][td], 'is_prc',
                                          split_adj, special_adj, regular_adj)
            elif col in volume_split:
                temp = adjust_corp_action(dict_out[col][td], 'is_volume',
                                          split_adj, special_adj, regular_adj)
            else:
                continue
            dict_out[col][td] = temp.rename_axis(index=None, columns=None)

    return dict_out

def get_trading_daily(security_id, start_date, end_date, ccy_ref='EUR',
                      field=['turnover', 'close_prc'],
                      trading_destinations=['MAIN'], flag_consolidated=False,
                      split_adj=True, special_adj=False, regular_adj=False):
    """
    Read trading daily from MARKET_DATA..trading_daily

    Args:
        security_id (list): list of security_id
        start_date (str): %Y%m%d
        end_date (str): %Y%m%d
        ccy_ref (str): in ['EUR', 'USD', ''], currency to convert to, if '', keep local currency
        trading_destinations (list): list, trading destination. ['MAIN'], ['MTF'], ['all'] or ['MAIN', other td_id], for ex ['MAIN', 189] or [td_id (int)]

                - ['MAIN']: get data on primary market
                - ['MTF']: get consolidated data on ALL MTFs (not primary market)
                - ['all']: get consolidated data on primary market and MTFs
                - ['MAIN', other td_id]: get consolidated data on primary market and given td_id
                - [td_id (int)]: get consolidated data on given td_id(s)
        flag_consolidated (bool): if True, compute consolidated market and add to output
        field (list): list of str, field to retrieve in MARKET_DATA
            [turnover, close_prc, open_prc, close_turnover,intraday_turnover,cross_turnover,...]
        split_adj (bool): adjust price, volume due to split (refer to adjust_corp_action)
        special_adj (bool): adjust price with special dividend (refer to adjust_corp_action)
        regular_adj (bool): adjust price with regular cash dividend (refer to adjust_corp_action)

    Returns:
        dict of dict of dataframes,
            with first keys are given in param field,
            second keys are trading_destination plus main and, consolidated market
        for ex:

            input:
                - field=['close_prc', 'turnover']
                - trading_destinations = ['MAIN', 159, 289]

            output: dict_out: dict
                        - close_prc: dict
                            - MAIN: dataframe
                            - 159: dataframe
                            - 189: dataframe
                            - CONS: dataframe

                        - turnover: dict
                            - MAIN: dataframe
                            - 159: dataframe
                            - 189: dataframe
                            - CONS: dataframe

    .. exec_code::

        import dbtools.src.get_market_data as mkt
        security_id = [2, 8, 391, 512]
        dict_trading = mkt.get_trading_daily(security_id=security_id, start_date='20220826', end_date= '20220905',
                                            split_adj=True, trading_destinations = ['MAIN', 289], flag_consolidated = True,
                                            field=['volume', 'close_prc'], ccy_ref='EUR')
        print('1st keys out dictionary output: %s' %list(dict_trading.keys()))
        dict_prc = dict_trading['close_prc']
        dict_vlm = dict_trading['volume']
        print('2rd keys out dictionary output: %s' %list(dict_prc.keys()))
        print()
        print('Example Main market dataframe output:')
        print(dict_vlm['MAIN'])
        print()
        print('Example market CBOE (td_id=159) dataframe output:')
        print(dict_vlm[289])
        print()
        print('Example consolidated market dataframe output:')
        print(dict_vlm['CONS'])
    """
    sec = pd.to_numeric(security_id, errors = 'coerce')
    if (10174467 in security_id) or (795445 in security_id):
        sec = pd.Index(security_id).union([10174467, 795445]).dropna()
    sec_str = ",".join(map(str, sec))
    fld_str = ",".join(field)
    

    
    prim_td = rep.mapping_from_security(security_id, 'prim_td_id')
    query = """
            select distinct security_id, date, trading_destination_id, %s
            from MARKET_DATA..trading_daily
            where security_id in (%s)
            and date between '%s' and '%s'
            """ % (fld_str, sec_str, start_date, end_date)
    # Load data, prim market, and delete trading destination NaN
    df_temp = pd.read_sql(query, con_mis)
    df_temp = _correct_EXO(df_temp)
    df_temp.date = pd.to_datetime(df_temp.date)
    df_temp['is_prim'] = df_temp.security_id.map(prim_td) == df_temp.trading_destination_id
    df_temp = check_change_trading_id(df_temp)
    df_temp = df_temp.set_index(['date', 'security_id'])
    df_temp = df_temp.dropna(subset=['trading_destination_id'])

    # trading_destinations: MAIN is primary market,
    # MTF is not primary market,
    # 'all': all markets (MTF and MAIN)
    if 'MAIN' in trading_destinations:
        df_temp = df_temp[df_temp.trading_destination_id.isin(trading_destinations)
                     | df_temp.is_prim]
    elif 'MTF' in trading_destinations:
        df_temp = df_temp[~df_temp.is_prim]
        trading_destinations = df_temp.trading_destination_id.unique()
    elif 'all' in trading_destinations:
        df_temp = df_temp.copy()
        trading_destinations = df_temp.trading_destination_id.unique()
    else:
        df_temp = df_temp[df_temp.trading_destination_id.isin(trading_destinations)]

    # aggregation in case of more than 1 trading destinations to get consolidated data
    path_config = os.path.join(os.path.dirname(__file__), '..')
    aggr_filename = 'config/consolidated_market_aggregation.txt'
    try:
        f = open(os.path.join(path_config, aggr_filename), "r")
    except:
        folder = 'C:/python_tools/dbtools/'
        f = open(folder + aggr_filename, "r")
    aggr = eval(f.read())

    df_cons = pd.DataFrame(index=df_temp.index.unique(), columns=df_temp.columns)
    if flag_consolidated:
        for key, value in aggr.items():
            cols = df_temp.columns.intersection(value)
            if len(cols) == 0:
                continue
            if key == 'sum':
                cols = df_temp.columns.intersection(value)
                df_cons[cols] = df_temp.groupby(['date', 'security_id']).sum()[cols]
            elif key == 'is_prim':
                cols = df_temp.columns.intersection(value)
                df_cons[cols] = df_temp[df_temp.is_prim][cols]
            elif key == 'nansum':
                cols = df_temp.columns.intersection(value)
                df_cons[cols] = df_temp.fillna(0).groupby(['date', 'security_id']).sum()[cols]
            elif key == 'min':
                cols = df_temp.columns.intersection(value)
                df_cons[cols] = df_temp.groupby(['date', 'security_id']).min()[cols]
            elif key == 'max':
                cols = df_temp.columns.intersection(value)
                df_cons[cols] = df_temp.groupby(['date', 'security_id']).max()[cols]
            elif key == 'sumweightedby':
                cols = df_temp.columns.intersection(value.keys())
                val = df_temp.columns.intersection(value.values())
                denom = df_temp.groupby(['date', 'security_id']).sum()[val]
                num = df_temp[cols].mul(df_temp[val].values) \
                    .groupby(['date', 'security_id']).sum()[cols]
                df_cons[cols] = num.div(denom.values)

        df_cons.trading_destination_id = 'CONS'
        df_cons.is_prim = False
    
    if df_cons.isna().all().all():# if df_cons is completely empty
        df_cons= df_temp.dropna(how='all')
    else:
        df_cons = pd.concat((df_temp.dropna(axis=1,how='all'), df_cons.dropna(axis=1,how='all')))

    # fields to convert to ccy_ref
    fx_adj = ['turnover', 'prc', 'qty_1er_limit', 'tick_size']
    temp = np.array(len(df_temp.columns) * [False])
    for col in fx_adj:
        temp = df_temp.columns.str.contains(col) | temp
    fx_adj = df_temp.columns[temp]

    # fields to adjust with split
    prc_split = df_cons.columns[df_cons.columns.str.contains('prc')]
    volume_split = df_cons.columns[df_cons.columns.str.contains('volume')]

    # format df_cons to dict
    dict_out = {}
    td_id = list(trading_destinations) + ['MAIN', 'CONS']
    td_id = pd.Index(td_id).drop_duplicates(keep='last')
    for col in field:
        dict_out[col] = {}
        for td in td_id:
            temp = df_cons[df_cons.trading_destination_id == td][col].unstack()
            temp = temp.reindex(columns=security_id)
            dict_out[col][td] = temp.rename_axis(index=None, columns=None)

        temp = df_cons[df_cons.is_prim][col].unstack()
        temp = temp.reindex(columns=security_id)
        dict_out[col]['MAIN'] = temp.rename_axis(index=None, columns=None)

    # adjustment and convert to ccy
    for col in field:
        for td in td_id:
            if len(dict_out[col][td]) == 0:
                continue
            if (ccy_ref != '') and (col in fx_adj):
                temp = adjust_fx(dict_out[col][td], ccy_ref)
                dict_out[col][td] = temp.rename_axis(index=None, columns=None)
            if col in prc_split:
                temp = adjust_corp_action(dict_out[col][td], 'is_prc',
                                          split_adj, special_adj, regular_adj)
            elif col in volume_split:
                temp = adjust_corp_action(dict_out[col][td], 'is_volume',
                                          split_adj, special_adj, regular_adj)
            else:
                continue
            dict_out[col][td] = temp.rename_axis(index=None, columns=None)

    return dict_out

def get_index_prices(index_ticker, start_date, end_date, price_type='close'):
    """
    Get index prices from MARKET_DATA..HISTOINDEXTIMESERIES

    Args:
        index_ticker (str or list of str): bloomberg ticker of index
        start_date (str): %Y%m%d
        end_date (str): %Y%m%d
        price_type (str, optional): "close"(default)/open/high/low

    Returns:
        Dataframe, Price of index, date in index, index_ticker in columns

    .. exec_code::

        import dbtools.src.get_market_data as mkt
        df = mkt.get_index_prices(index_ticker = ['SXXP', 'CAC', 'SBF120'], start_date = '20220826', end_date = '20220901')
        print(df)
    """
    index_ticker = [index_ticker] if isinstance(index_ticker, str) else index_ticker
    index_str = "','".join(index_ticker)
    
    # mapping price type
    price_type_dict = {'close':'PX_LAST_EOD',
                       'open':'PX_OPEN',
                       'high':'PX_HIGH',
                       'low':'PX_LOW'};
    price_type_field = price_type_dict[price_type]
    df_attribute = pd.read_sql("""select ATTRIBUTEID from KGR..ATTRIBUTE where FILEDMNEMONIC= '%s'
                               """%(price_type_field), con_mis)
    ATTRIBUTEID = df_attribute.ATTRIBUTEID
    
    query = """
            select * from MARKET_DATA.. HISTOINDEXTIMESERIES
            where CODEBLOOMBERG in ('%s') 
            and ATTRIBUTEID = %d
            and DATE between '%s' and '%s'
            order by DATE
            """ % (index_str, ATTRIBUTEID.iloc[0], start_date, end_date)
    df_temp = pd.read_sql(query, con_mis)
    df_temp = df_temp.set_index(['DATE', 'CODEBLOOMBERG']).VALUE.unstack()
    return df_temp

def get_index_ohlc(index_ticker, start_date, end_date):
    """
    Get index prices from MARKET_DATA..INDICE_DAILY

    Args:
        index_ticker (str): bloomberg ticker of index
        start_date (str): %Y%m%d
        end_date (str): %Y%m%d
        price_type (str, optional): "close"(default)/open/high/low

    Returns:
        Dataframe, Price of index, date in index, index_ticker in columns

    .. exec_code::

        import dbtools.src.get_market_data as mkt
        df = mkt.get_index_ohlc(index_ticker = 'SXXP', start_date = '20220826', end_date = '20220901')
        print(df)
    """
    df_index = pd.read_sql("""select * from QUANT_work..INDEX_REFERENTIAL where ticker= '%s'
                               """%(index_ticker), con_mis)
    INDEXID = df_index.INDEXID
    
    query = """
            select DATE, OPEN_PRC, HIGH_PRC, LOW_PRC, CLOSE_PRC from MARKET_DATA..INDICE_DAILY
            where INDEXID = '%s'
            and DATE between '%s' and '%s'
            order by DATE
            """ % (INDEXID.iloc[0], start_date, end_date)
    df_temp = pd.read_sql(query, con_mis)
    df_temp = df_temp.set_index('DATE')
    return df_temp


def get_ffama_data():
    """
    Get French Fama 3 factors
    Details on https://mba.tuck.dartmouth.edu/pages/faculty/ken.french/data_library.html.

    Args:

    Returns:
        DataFrame French Fama 3 factors.


    .. exec_code::

        import dbtools.src.get_market_data as mkt
        df = mkt.get_ffama_data()
        print(df.iloc[-5:])
    """
    sql = """
            select date, FF_Mkt_RF, FF_Size, FF_Value, RF
            from QUANT_work..Kch_sxxp_index_level
            order by date
        """
    df_ffama = pd.read_sql(sql, con = con_mis)
    df_ffama = df_ffama.set_index('date').dropna(how = 'all')
    return df_ffama


def get_index_level():
    """
    Get KC index level from QUANT_work..Kch_sxxp_index_level

    Returns:
        DataFrame

    .. exec_code::

        import dbtools.src.get_market_data as mkt
        df = mkt.get_index_level()
        print(df.iloc[-5:])
    """
    df_temp = pd.read_sql("select * from QUANT_work..Kch_sxxp_index_level order by date", con_mis)
    df_temp = df_temp.set_index(['date']).rename_axis(index=None)
    return df_temp


def get_region_sales(security_id):
    """
    Get stock name from table QUANT_work..sbf_sxxp_sales into a DataFrame.

    Args:
        security_id (list): list of security

    Returns:
        DataFrame

    ..exec_code::

        import dbtools.src.get_market_data as mkt
        security_id = [2, 8, 391, 512]
        df = mkt.get_region_sales(security_id = security_id)
        print(df.iloc[-5:])
    """
    sec_str = ",".join(map(str, security_id))

    query = """
        select * from QUANT_work..sbf_sxxp_sales
        where security_id in (%s)
        order by security_id
        """ % sec_str

    df_out = pd.read_sql_query(query, con_mis)
    df_out = df_out.sort_values('security_id')
    df_out.index.name = None
    return df_out


def get_fund_data(security_id, start_date, end_date):
    """
    Load fundamental data from QUANT_work..level2_fundvar_prod
    !!!!!! Frequency weekly (every Friday) !!!!!!

    Args:
        security_id (list): list of security_id
        start_date (str): yyyymmdd
        end_date (str): yyyymmdd

    Returns:
        Dict of DataFrame, security_in column, date in index

    .. exec_code::

        import dbtools.src.get_market_data as mkt
        security_id = [2, 8, 391, 512]
        dict_fund = mkt.get_fund_data(security_id = security_id, start_date = '20180805', end_date = '20220826')
        print(list(dict_fund.keys()))
        print(dict_fund['CUR_MKT_CAP'])
    """
    sec_str = ",".join(map(str, security_id))

    query = """select ticker, date, security_id,

            PX_LAST_N_N_N as PX_LAST_no_adj_loc,
            PX_LAST_N_N_N_EUR as PX_LAST_no_adj,
    
            PX_LAST_N_N_Y as PX_LAST_capchg_loc,
            PX_LAST_N_N_Y_EUR as PX_LAST_capchg,

            PX_LAST_N_Y_Y as PX_LAST_adj_loc,
            PX_LAST_N_Y_Y_EUR as PX_LAST_adj,

            PX_LAST_Y_Y_Y as PX_LAST_tot_ret_loc,
            PX_LAST_Y_Y_Y_EUR as PX_LAST_tot_ret,

            isnull(PX_TO_BOOK_RATIO, PX_TO_BOOK_RATIO_yearly) as PX_TO_BOOK_RATIO,
            isnull(PX_TO_SALES_RATIO, PX_TO_SALES_RATIO_yearly) as PX_TO_SALES_RATIO,
                
            CASE WHEN BEST_EPS_BF<=0 THEN NULL ELSE BEST_PE_RATIO_BF end as BEST_PE_rebuilt,
            CASE WHEN BEST_BPS_BF<=0 THEN NULL ELSE BEST_PX_BPS_RATIO_BF end as BEST_PB_rebuilt,
            
            PX_LAST_N_Y_Y * isnull(EARN_YLD, EARN_YLD_yearly) as TRAIL_12M_EPS_rebuilt,
            
            CASE WHEN isnull(TOT_COMMON_EQY, TOT_COMMON_EQY_yearly)<=0 THEN NULL
            ELSE isnull(TOT_DEBT_TO_COM_EQY, TOT_DEBT_TO_COM_EQY_yearly) END as DEBT_TO_EQUITY_rebuilt,

            
            BEST_ROE_BF, BEST_EPS_1GY, BEST_EPS_2GY, BEST_EPS_3GY,
            BEST_EPS_BF, BEST_EPS_2BF,
            BEST_EPS_STDDEV_BF,
            
            BEST_EPS_STDDEV_BF / PX_LAST_N_N_Y_fundccy as BEST_EPS_STDDEV_BF_rebuilt,
            BEST_EPS_STDDEV_BF / PX_LAST_N_N_Y as BEST_EPS_STDDEV_BF_rebuilt_old,
    
            CUR_MKT_CAP_Y_Y_Y_EUR as CUR_MKT_CAP, EQY_FREE_FLOAT_PCT
            from QUANT_work..level2_fundvar_prod
            where security_id in (%s)
            and date between '%s' and '%s'
            order by date, security_id""" % (sec_str, start_date, end_date)

    df_fund = pd.read_sql_query(query, con_mis)

    df_fund = df_fund.set_index(['date', 'security_id']).drop('ticker', axis=1)
    dict_fund = {}
    for key in df_fund.columns:
        temp = df_fund[key].unstack().sort_index()
        dict_fund[key] = temp.rename_axis(index=None, columns=None)
    return dict_fund


def transform_fund_data(dict_fund, price_eur):
    """
    Adjust fundamental data.
        - forward fill by :py:func:`fill_fund_data`
        - dividend by :py:func:`compute_dividend`
        - growth score by :py:func:`compute_organic_growth`

    Args:
        dict_fund (dict): output of function :py:func:`get_fund_data`
        price_eur (DataFrame or dict): close price in EUR or dictionary contains close_prc in EUR

    Returns:
        dict, fundamental data adjusted (forward fill, dividend and organic_growth)

    .. exec_code::

        import dbtools.src.get_market_data as mkt
        import pandas as pd
        security_id = [2, 8, 391, 512]
        dict_fund = mkt.get_fund_data(security_id, start_date = '20180805', end_date = '20220826')
        dict_trading = mkt.get_trading_daily(security_id = security_id, start_date = '20180805', end_date = '20220826', field = ['close_prc'])
        price_eur = dict_trading['close_prc']['MAIN']
        fund_data = mkt.transform_fund_data(dict_fund, price_eur)
        added_key = pd.Index(fund_data.keys()).difference(pd.Index(dict_fund.keys()))
        print('Additional keys in fundamental dictionary')
        print(added_key)
        print('Example on CUR_MKT_CAP:')
        print('Before transforming')
        print(dict_fund['CUR_MKT_CAP'])
        print()
        print('After transforming')
        print(fund_data['CUR_MKT_CAP'])
    """
    dict_fund_adj = fill_fund_data(dict_fund.copy(), price_eur)
    dict_fund_adj = compute_dividend(dict_fund_adj.copy(), price_eur)
    dict_fund_adj = compute_organic_growth(dict_fund_adj.copy())
    return dict_fund_adj

def fill_fund_data(dict_fund, price_eur):
    """
    Forward fill fundamental data

    Args:
        dict_fund (dict): output of function :py:func:`get_fund_data`
        price_eur (DataFrame or dict): close price in EUR or dictionary contains close_prc in EUR

    Returns:
        dict_fund forward filled

    .. exec_code::

        import dbtools.src.get_market_data as mkt
        security_id = [2, 8, 391, 512]
        dict_fund = mkt.get_fund_data(security_id, start_date = '20180805', end_date = '20220826')
        dict_trading = mkt.get_trading_daily(security_id = security_id, start_date = '20180805', end_date = '20220826', field = ['close_prc'])
        price_eur = dict_trading['close_prc']['MAIN']
        fund_data = mkt.transform_fund_data(dict_fund, price_eur)
        print('Example on CUR_MKT_CAP:')
        print('Before transforming')
        print(dict_fund['CUR_MKT_CAP'])
        print()
        print('After transforming')
        print(fund_data['CUR_MKT_CAP'])
    """
    if isinstance(price_eur, dict):
        for key in price_eur.keys():
            dict_fund[key] = price_eur[key]
        price_eur = price_eur['close_prc']

    dict_fund['close_prc'] = price_eur
    cols = dict_fund[list(dict_fund.keys())[0]].columns

    price_eur = price_eur.reindex(columns=cols)
    date_keep = price_eur.index

    for col in dict_fund.keys():
        if col in ['CUR_MKT_CAP']:
            share_weekly = dict_fund[col].div(price_eur.reindex(dict_fund[col].index,
                                                                method='ffill'))
            # we ffill share_outstanding for the following one month
            share_weekly = share_weekly.ffill(limit=4)
            df_fill = share_weekly.reindex(date_keep, method='ffill').mul(price_eur)

        elif col in ['EQY_FREE_FLOAT_PCT']:
            # we ffill pct_ff for the following one month
            df_fill = dict_fund[col].ffill(limit=4) \
                .reindex(date_keep, method='ffill')
        else:
            df_fill = dict_fund[col].reindex(date_keep, method='ffill')
            if col in ['BEST_PB_rebuilt', 'BEST_PE_rebuilt']:
                df_fill = df_fill
            elif 'BEST_' in col:
                df_fill = df_fill.ffill(limit=21 * 3)
            elif 'PX_TO_' in col:
                df_fill = df_fill.div(dict_fund['PX_LAST_adj']) \
                    .ffill(limit=21 * 7).mul(dict_fund['PX_LAST_adj'])
            elif col in ['TRAIL_12M_EPS_rebuilt']:
                df_fill = df_fill.ffill(limit=21 * 7)
            elif col in ['DEBT_TO_EQUITY_rebuilt']:
                df_fill = df_fill.ffill(limit=21 * 14)
        dict_fund[col] = df_fill
    
    return dict_fund

def compute_dividend(dict_fund, price_eur):
    """
    Compute dividend from fundamental data

    Args:
        dict_fund (dict): output of function :py:func:`get_fund_data`
        price_eur (DataFrame or dict): close price in EUR or dictionary contains close_prc in EUR

    Returns:
        dict, fundamental data with additional field EQY_DVD_YLD_12M_rebuilt

    .. exec_code::

        import dbtools.src.get_market_data as mkt
        import pandas as pd
        security_id = [2, 8, 391, 512]
        dict_trading = mkt.get_trading_daily(security_id = security_id, start_date = '20180805', end_date = '20220826', field = ['close_prc'])
        price_eur = dict_trading['close_prc']['MAIN']
        dict_fund = mkt.get_fund_data(security_id, start_date = '20180805', end_date = '20220826')
        fund_data = mkt.compute_dividend(dict_fund, price_eur)

        added_key = pd.Index(fund_data.keys()).difference(pd.Index(dict_fund.keys()))
        print('Additional keys in fundamental dictionary')
        print(added_key)
        print()
        print(fund_data['EQY_DVD_YLD_12M_rebuilt'])

    """

    prc = dict_fund[list(dict_fund.keys())[0]]
    ticker = rep.mapping_from_security(prc.columns, code='bbg')
    flag_suisse = (ticker.str.contains(" SW| SE", regex=True))
    sec_id_suisse = flag_suisse.loc[flag_suisse].index
    px_last_base = dict_fund['PX_LAST_adj']
    px_last_base[sec_id_suisse] = dict_fund['PX_LAST_capchg'][sec_id_suisse]

    # dict_fund['PX_LAST_adj'] => dvd_regular, dict_fund['PX_LAST_capchg'] => dvd_special

    dvd_yield_daily = ((dict_fund['PX_LAST_tot_ret'].shift(-1)) / dict_fund['PX_LAST_tot_ret'] /
                       ((px_last_base.shift(-1)) / px_last_base) - 1)
    # technical tiny or negative value to be replaced by 0
    dvd_yield_daily[dvd_yield_daily < 0.0005] = 0
    dvd_yield_daily.iloc[-1, :] = 0  # technical NAN to be replaced by 0
    dvd_amount_daily = dvd_yield_daily * px_last_base
    dvd_max = (np.floor((1 - dvd_amount_daily.isna()).sum() / 64) + 2).astype(int)

    # exclude the trivial dividend due to numerical error.
    for col in dvd_amount_daily.columns:
        dvd_i = dvd_amount_daily[col]

        dvd_candidate = dvd_i.loc[dvd_i > 0].nlargest(dvd_max[col])

        if not dvd_candidate.empty:
            dvd_rebuilt = dvd_candidate.reindex(dvd_i.index, fill_value=0)
            dvd_rebuilt[dvd_i.isna()] = np.nan
            dvd_amount_daily[col] = dvd_rebuilt

    dict_fund['EQY_DVD_YLD_daily_rebuilt'] = dvd_amount_daily / px_last_base
    dict_fund['EQY_DVD_YLD_daily_rebuilt'][dict_fund['EQY_DVD_YLD_daily_rebuilt'] < 0.0005] = np.nan

    nb_day_per_month = 20
    nb_day_x1 = round(nb_day_per_month * 2.5)

    dvd_amount_all = dvd_amount_daily.rolling(nb_day_per_month * 13,
                                              min_periods=nb_day_per_month * 13).sum()
    dvd_amount_ex_x1 = dvd_amount_daily.rolling(nb_day_per_month * 13 - nb_day_x1,
                                                min_periods=nb_day_per_month * 13 - nb_day_x1).sum()

    dvd_count_all = (dvd_amount_daily > 0).rolling(nb_day_per_month * 13,
                                                   min_periods=nb_day_per_month * 13).sum()
    dvd_count_ex_x1 = (dvd_amount_daily > 0).rolling(nb_day_per_month * 13 - nb_day_x1,
                                            min_periods=nb_day_per_month * 13 - nb_day_x1).sum()
    dvd_count_x1 = dvd_count_all - dvd_count_ex_x1
    dvd_count_x2 = (dvd_amount_daily > 0).rolling(nb_day_per_month,
                                                  min_periods=nb_day_per_month).sum()

    dvd_amount_12m = dvd_amount_all * ((dvd_count_x1 + dvd_count_x2) < 2) + \
                     dvd_amount_ex_x1 * ((dvd_count_x1 + dvd_count_x2) == 2)

    dict_fund['EQY_DVD_YLD_12M_rebuilt'] = dvd_amount_12m / px_last_base

    return dict_fund


def compute_organic_growth(dict_fund):
    """
    Compute score organic_growth

    Args:
        dict_fund (dict): output of function :py:func:`get_fund_data`

    Returns:
        dict, fundamental data with additional filed 'EPS_GROWTH_organic'

    .. exec_code::

        import dbtools.src.get_market_data as mkt
        import pandas as pd
        security_id = [2, 8, 391, 512]
        dict_fund = mkt.get_fund_data(security_id, start_date = '20180805', end_date = '20220826')
        fund_data = mkt.compute_organic_growth(dict_fund)
        added_key = pd.Index(fund_data.keys()).difference(pd.Index(dict_fund.keys()))
        print('Additional keys in fundamental dictionary')
        print(added_key)
        print()
        print(fund_data['EPS_GROWTH_organic'])
    """
    # fill empty data

    eps3gy = dict_fund['BEST_EPS_3GY'].ffill()
    eps2gy = dict_fund['BEST_EPS_2GY'].ffill()
    eps1gy = dict_fund['BEST_EPS_1GY'].ffill()
    eps24mf = dict_fund['BEST_EPS_2BF'].ffill()
    dt_v = dict_fund['BEST_EPS_BF'].copy()

    # week of year for weighting
    # TODO when python reinstalled: w = dict_fund['BEST_EPS_BF'].index.isocalendar().week/52

    dt_v['Date'] = dt_v.index
    w = dt_v['Date'].apply(lambda x: x.weekofyear) / 52
    w[w > 1] = 1

    # step 1: generate mensual series 2nd friday of each month
    freq_word = pd.tseries.offsets.WeekOfMonth(week=1, weekday=4)
    df_y = eps1gy.resample(freq_word, closed='right', label='right').last()

    # Step 2: generate annual series at end of year
    df_y = df_y.resample('Y').ffill()

    # prepare array for 6 years
    # np_EPS_y = np.empty #df_y.values
    # Step 1: shift by N years (shift(1): année précédente)
    tmp = df_y.shift(1)
    # Step 2: forward fill on a daily basis
    tmp = tmp.reindex(eps1gy.index, method='bfill')
    # Step 3: stack with previous years
    np_eps_y = tmp.values

    for y in range(5):  # 0:4
        # Step 1: shift by N years (shift(1): année précédente)
        tmp = df_y.shift(y + 2)
        # Step 2: forward fill on a daily basis
        tmp = tmp.reindex(eps1gy.index, method='bfill')
        # Step 3: stack with previous years
        np_eps_y = np.dstack((np_eps_y, tmp.values))

    nb_na = np.sum(np.isnan(np_eps_y), axis=2)

    # initialize growth with forward growth
    np_gr_y_1_2 = eps2gy.values / eps1gy.where(eps1gy != 0).values - 1
    np_gr_y = eps3gy.values / eps2gy.where(eps2gy != 0).values - 1

    np_gr_y = np.dstack((np_gr_y, np_gr_y_1_2))
    np_gr_y = np.dstack((np_gr_y, eps1gy.values / np_eps_y[:, :, 0] - 1))

    # initialize denom with forward EPS
    np_denom_y = eps2gy.values
    np_denom_y = np.dstack((np_denom_y, eps1gy.values))
    np_denom_y = np.dstack((np_denom_y, np_eps_y[:, :, 0]))

    # add past growth and denom to array
    for y in range(5):  # 0:4
        np_gr_y = np.dstack((np_gr_y, np_eps_y[:, :, y] / np_eps_y[:, :, y + 1] - 1))
        np_denom_y = np.dstack((np_denom_y, np_eps_y[:, :, y + 1]))

    # replace infinite by NaN
    np_gr_y[np_gr_y == np.inf] = np.nan
    np_denom_y[np_denom_y == np.inf] = np.nan
    np_gr_y[np_denom_y == np.nan] = np.nan
    # np_gr_y[np_denom_y==np.inf]= np.inf

    # cap each value
    with np.errstate(invalid='ignore'):
        np_gr_y[np_denom_y < 0] = np.nan
        np_gr_y[np_gr_y < 0.02] = 0.02
        np_gr_y[np_gr_y > 0.2] = 0.2

    # prepare for average growth: will weight first and last component
    g_shape = np_gr_y.shape
    w = np.repeat(w.values, g_shape[1]).reshape((g_shape[0], g_shape[1]))

    gi = np_gr_y[:, :, 1:(g_shape[2] - 1)]
    ge_first = np_gr_y[:, :, 0]
    ge_last = np_gr_y[:, :, g_shape[2] - 1]
    gr_mat = w * ge_first + (1 - w) * ge_last
    gr_mat = np.dstack((gr_mat, gi))
    # gr_mat = w*ge_first
    # gr_mat = np.dstack((gr_mat, gi))
    # gr_mat = np.dstack((gr_mat, (1-w)*ge_last))

    gr = np.nanmean(gr_mat, axis=2)
    gr[nb_na > 0] = np.nan

    # final cap
    gr[gr < 0.02] = 0.02
    gr[gr > 0.2] = 0.2
    gr[eps24mf.values < 0] = np.nan

    df_eps_growth = pd.DataFrame(gr, columns=eps1gy.columns, index=eps1gy.index)

    dict_fund['EPS_GROWTH_organic'] = df_eps_growth.copy()
    return dict_fund


def compute_coe(g_org, gc, pe):
    """
    compute COE
    """
    date_ref = pe.index
    df_coe = pe.copy()
    df_coe[:] = np.nan

    for i, date_i in enumerate(date_ref):
        print('%.1f%% - %s' % (i / len(date_ref) * 100, date_i))
        g_org_use = g_org.loc[date_i, :]
        gc_use = gc.loc[date_i, :]
        pe_use = pe.loc[date_i, :]

        pct_available = ((g_org_use.notna()) & (gc_use.notna()) & (pe_use.notna())).mean()

        if pct_available < 0.1:
            df_coe.iloc[i, :] = np.nan
        else:
            for j, stock_j in enumerate(g_org_use.index):

                x_guess = 0.05 if np.isnan(df_coe.iloc[i - 1, j]) else df_coe.iloc[i - 1, j]

                param_in = [0.02, g_org_use.loc[stock_j], gc_use.loc[stock_j], pe_use.loc[stock_j]]

                if not np.any(np.isnan(param_in)):
                    x_sol = fsolve(check_pe_fading_coe, x_guess, args=param_in, xtol=10e-4)
                    x_sol = x_sol[0]
                else:
                    x_sol = np.nan

                df_coe.iloc[i, j] = x_sol

    # var_output = df_coe.T.copy().reindex(pe.index, method='ffill')
    # var_output = df_coe.reindex(pe.index, method='ffill')

    return df_coe


def check_pe_fading_coe(coe, data_params=[0.02, 0.146, 0.126, 26.7]):
    """
    pe_check_fading_coe
    """

    ginf, g_org, gc, pe = data_params

    e1 = 1
    e2 = e1 * (1 + gc)
    e3 = e2 * (1 + g_org)
    e4 = e3 * (1 + g_org)
    e5 = e4 * (1 + (ginf + g_org) / 2)

    arr_eps = np.array([e1, e2, e3, e4, e5])
    arr_discount = np.cumprod(1 / (np.repeat(coe, 5) + 1))

    arr_eps_act = arr_eps * arr_discount
    epsfin = arr_eps_act[-1] * (1 + ginf) / (coe - ginf)

    sum_eps_act = epsfin + np.sum(arr_eps_act)

    return pe - sum_eps_act
def get_trading_destinations(security_ids, start_date, end_date):
    """
    Fetch unique trading destinations for given securities within a specified date range.
    
    Parameters
    ----------
    security_ids : list or np.ndarray
        Security IDs for which trading destinations need to be fetched.
        
    start_date, end_date: datetime
        Start and end dates for fetching trading destinations.
        
    Returns
    -------
    DataFrame
        Contains security_id and their corresponding trading_destination_id from the database.
    """
    
    assert type(security_ids) in [list, np.ndarray], 'security_ids should be a list or ndarray'
    assert type(start_date) == datetime and type(end_date) == datetime, "'start_date' and 'end_date' must both be datetime"
    
    sec_str = ",".join(map(str, security_ids))
    
    query = f"""
        SELECT security_id, trading_destination_id
        FROM MARKET_DATA..trading_daily
        WHERE security_id in ({sec_str})
        AND date BETWEEN '{start_date.strftime("%Y%m%d")}' AND '{end_date.strftime("%Y%m%d")}'
        AND trading_destination_id IS NOT NULL
    """
    
    output = pd.read_sql(query, con_mis)
    return output