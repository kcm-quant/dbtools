# -*- coding: utf-8 -*-
"""
Created on Wed Nov  8 16:09:59 2023

@author: nle
"""
import os
import pandas as pd
from dbtools.src.db_connexion import SqlConnector
import dbtools.src.get_repository as rep
import dbtools.src.DatabaseManager as db
from pandas.tseries.offsets import BDay
# from copy import deepcopy
connector = SqlConnector()
con_mis = connector.connection()
import json
from tqdm import tqdm
from datetime import datetime
from pandas.tseries.offsets import BMonthEnd, BMonthBegin, CustomBusinessDay
from apitools.src.apitools import EndOfDayBbg # used in parse function in config

class QuantWorkUpdater:
    def __init__(self, dataset_name, config_file_name = "quant_work_updater.json"):
        """
        Initialize the QuantWorkUpdater with the provided dataset name and a default config file name
        Args:
            dataset_name (str): keys of dataset name
            config_file_name (str): config file name
        
        Example:
            from dbtools.src.QuantWorkUpdater_v2 import QuantWorkUpdater
            from pandas.tseries.offsets import BDay
            loader = QuantWorkUpdater(dataset_name = "LEVEL_INDEX", config_file_name = 'quant_work_updater.json')
            config = loader.load_config()
    
            last_date = loader.get_table_last_date() + BDay(1)
            due_date = loader.get_date_update_due()
            perimeter = loader.get_perimeter()
            tickers = loader.get_tickers_from_referential(perimeter)
            df_idx_lvl = loader.get_data(last_date, due_date)
        """
        self.dataset_name = dataset_name
        script_dir = os.path.dirname(os.path.abspath(__file__))
        parent_dir = os.path.dirname(script_dir)
        config_dir = os.path.join(parent_dir, 'config')
        config_path = os.path.join(config_dir, config_file_name)
        self.config_path = config_path
        config = self.load_config()
        self.config = config[dataset_name]
        
        
    def load_config(self):
        """
        Load congif file
        """
        with open(self.config_path, 'r') as file:
            config = json.load(file)
        return config
        
  
    
    def get_table_last_date(self):
        """
        Retrive last update date in table.
        Task: 
            # Load max (date) in table if no date exists, take start_date_default in config
        Returns:
            datetime: last update date in table
        """
        # Extract table metadata
        table_name = self.config.get("table_name")
        #table_perimeter_field = self.config.get("table_perimeter_field")
        #table_perimeter = self.config.get("table_perimeter")

        # Formulate your SQL query here based on the table metadata
        #perimeter_values_str = ', '.join([f"'{value}'" if isinstance(value, str) else str(value) for value in table_perimeter])
        sql_query = f"SELECT MAX(DATE) FROM {table_name}"
        result = pd.read_sql(sql_query, con_mis)
        if result.empty or result.iloc[0, 0] is None:
            last_available_date = self.config.get('start_date_default')
            return pd.to_datetime(last_available_date)
        last_available_date = result.iloc[0, 0]
       
        return last_available_date
    
    def get_security_date_ref(self, attribute_id=1):
        """
        Retrive securities min and max data date for a given attribute
        Task:
            #. Add condition on attribute_id if this colums exists in config["table_columns"]
            #. Load max (date) and min(date) in table (with condition on attribute_id)
            #. Convert dates to pd.to_datetime
        Args:
            attribute_id (int): value of attribute id
        Returns:
            result: DataFrame, security_id in index and [data_date_min, data_date_max] in columns
        Example:
            .. code-block:: python
                from dbtools.src.QuantWorkUpdater_v2 import QuantWorkUpdater
                loader = QuantWorkUpdater(dataset_name = "FUNDAMENTAL_DATA_STOCK", config_file_name = 'quant_work_updater.json')
                df_sec_date_ref = loader.get_security_date_ref()
        """
        # Extract table metadata
        table_name = self.config.get("table_name")
        table_perimeter_field = self.config.get("table_perimeter_field")
        #table_perimeter = self.config.get("table_perimeter")

        # Formulate your SQL query here based on the table metadata

        if 'attribute_id' in self.config.get("table_columns"):
            condition = f"where attribute_id = {attribute_id}"
        else:
            condition=''
        sql_query = f"""select {table_perimeter_field}, min(DATE) data_date_min, max(DATE) data_date_max
                        from {table_name}
                        {condition}
                        group by {table_perimeter_field}"""
        result = pd.read_sql(sql_query, con_mis)
        result = result.set_index(table_perimeter_field)
        result['data_date_min'] = pd.to_datetime(result['data_date_min'])
        result['data_date_min'] = pd.to_datetime(result['data_date_min'])

        return result
    
    def get_date_update_due(self):
        """
        Determines the dates when the next updates are due for each table in a given dataset based on their update frequencies.
        Task:
            # Load table_update_frequency from config
            # Determine the next update date
                "EOM": last business day of month
                "ADHOC": None
                "QKECH" Last business day of the quarter
        Returns:
            date (datetime): Date when updates are due, 
            None for each table with 'ADHOC' update frequency.

        Raises:
            ValueError: If the table_update_frequency value for any table is invalid.
            """

        table_update_frequency = self.config.get("table_update_frequency", None)

        # Determine the update date based on the frequency
        if table_update_frequency == 'EOM':
            # Calculate the last business day of the previous month
            last_month = datetime.today().month - 1 or 12
            year = datetime.today().year - (1 if last_month == 12 else 0)
            date = last_business_day_of_month(year, last_month)
        elif table_update_frequency == 'ADHOC':
            # Set None for 'ADHOC' frequency
            date = None
        elif table_update_frequency == 'QKECH':
            # Determine the closest past quarter business day
            date = closest_past_quarter_business_day(datetime.today().year, datetime.today().month)
        else:
            # Raise an error if the frequency value is not recognized
            raise ValueError("Invalid table_update_frequency value")

        return date
    
    
    def get_perimeter(self, start_date = None, end_date = None):
        """
        Get perimeter to update
        Task:
            # Load perimeter_type from config
            # Determine perimeter
                "fixed": list of ids fixed in config
                "static": all ids existing in the table
                "dynamic" all ids in a given perimeter, including new ids entering recently
        Returns:
            perimeter, list of ids (security_id, index_id,...)
        """
        perimeter_type = self.config.get('table_perimeter_type')
        if perimeter_type=='fixed':
            perimeter = self.config.get('table_perimeter')
        elif perimeter_type=='static':
            req = f"""SELECT distinct {self.config.get('table_perimeter_field')} as perimeter
                        from {self.config.get('table_name')}"""
            perimeter = pd.read_sql(req, con_mis).perimeter
            perimeter = list(perimeter.values)
        elif perimeter_type=='dynamic':
            table_perimeter = self.config.get('table_perimeter')
            if table_perimeter=='level 2':
                perimeter = rep.get_quant_perimeter(2)
            elif table_perimeter=='level 1':
                perimeter = rep.get_quant_perimeter(1)
            else:
                table_perimeter = self.config.get('table_perimeter')
                if start_date==None:
                    start_date = datetime.now().strftime('%Y%m%d')
                    end_date = datetime.now().strftime('%Y%m%d')
                df_index = rep.get_index_comp(index_ticker=table_perimeter, start_date=start_date, end_date=end_date, shift = True)
            
            perimeter = list(df_index.columns)
        return perimeter
    
    def get_tickers_from_referential(self,ids):
        """
        Mapping ticker from ids (security_id, index_id,...)

        Task:
            # Load reference_ticker from config
            # Determine the corresponding tickers
                'index_ticker': Quant work index ticker
                'fundamental_ticker':bloomberge fundamental ticker
                'equity_ticker': bloomberge equity ticker
            #. Complete tickers with security_type (equity or index)
        Args:
                ids (list): list of ids (security_id, index_id,...)
        Returns:
            tickers (series): tickers in value, ids in index
        Example:
            .. code-block:: python
                from dbtools.src.QuantWorkUpdater_v2 import QuantWorkUpdater
                loader = QuantWorkUpdater(dataset_name = "FUNDAMENTAL_DATA_INDEX", config_file_name = 'quant_work_updater.json')
                df_ticker = loader.get_tickers_from_referential([1, 2])
        """
        
        if self.config.get('reference_ticker')=='index_ticker':
            tickers = rep.mapping_from_index(ids, code = 'ticker').to_frame('ticker')
            tickers['delisted_date'] = datetime(2999, 12, 31).date()
            tickers['currency'] = rep.mapping_from_index(ids, 'currency').replace({'GBX':'GBP'})
        elif self.config.get('reference_ticker')=='fundamental_ticker':
            tickers = rep.mapping_from_security(ids, code = 'fund_ticker').to_frame('ticker')
            tickers['delisted_date'] = rep.mapping_from_security(ids, 'end_date_max')
            tickers['currency'] = rep.mapping_from_security(ids, 'ccy').replace({'GBX':'GBP'})
        elif self.config.get('reference_ticker')=='equity_ticker':
            tickers = rep.mapping_from_security(ids, code = 'bbg')
            tickers['delisted_date'] = rep.mapping_from_security(ids, 'end_date_max')
            tickers['currency'] = rep.mapping_from_security(ids, 'ccy').replace({'GBX':'GBP'})
        
        security_type = self.config.get('data_source',{}).get("security_type",'')
        tickers['full_ticker'] = tickers.ticker + " " + security_type
        return tickers
    
# =============================================================================
#     def get_referential_from_tickers(self,tickers):
#         '''
#         IN CONSTRUCTION
#         Mapping ids from tickers (security_id, index_id,...)
#         Args:
#             tickers (list): list of tickers
#         Task:
#             # Load reference_ticker from config
#             # Determine the corresponding ids
#                 'index_ticker': Quant work index ids
#                 'fundamental_ticker': security_id
#                 'equity_ticker': security_id
#         Returns:
#             ids (series): tickers in index, ids in value
#         '''
#         if self.config.get('table_perimeter_field')=='index_id':
#             ids = rep.mapping_index_from_ticker(tickers, code = 'index_id')
#         elif self.config.get('reference_ticker')=='fundamental_ticker':
#             ids = rep.mapping_security_id_from(tickers, code = 'fund_ticker')
#         elif self.config.get('reference_ticker')=='equity_ticker':
#             ids = rep.mapping_security_id_from(tickers, code = 'bbg')
#         return ids
# =============================================================================

    def mapping_ticker_to_security_id(self, df_data_with_ticker, df_symbols):
        """
        Mapping columns security tickers in df_data_with_ticker to security id
        
        Task:
            #. Create mapping ticker to security ids
            #. Mapping for unique ticker-security_id
            #. Mapping for non unique ticker-security_id

        Args:
            df_data_with_ticker (DataFrame): Data in datetime format. Columns are security tickers
            df_symbols (Series): output of get_tickers_from_referential, tickers in value, ids in index
        Returns:
            df_tmp (DataFrame): Data tin datetime format. Columns are security id
        
        Example:
            .. code-block:: python
                from dbtools.src.QuantWorkUpdater_v2 import QuantWorkUpdater
                import pandas as pd
                loader = QuantWorkUpdater(dataset_name = "FUNDAMENTAL_DATA_STOCK", config_file_name = 'quant_work_updater.json')
                df_symbols = loader.get_tickers_from_referential([8, 23, 283812,292445, 292446, 10174308])
                tickers = df_symbols.full_ticker.unique()
                df_data_with_ticker = pd.DataFrame(columns = tickers, index = ['20241213'], data = [range(len(tickers))])
                df_data_with_sec = loader.mapping_ticker_to_security_id(df_data_with_ticker, df_symbols)
        """
        
        df_symbols_dup = df_symbols[df_symbols.full_ticker.duplicated(keep = False)]
        d_symbols = pd.Series(index = df_symbols.full_ticker.values, data = df_symbols.index)
        d_symbols = d_symbols[~d_symbols.index.duplicated()]
        df_tmp = df_data_with_ticker.rename(columns = d_symbols)
        for ticker in df_symbols_dup.full_ticker.unique():
            sec_ids = df_symbols_dup[df_symbols_dup.full_ticker==ticker].index
            sec_exist = df_tmp.columns.intersection(sec_ids)
            if not sec_exist.empty:
                value_dup = df_tmp[sec_exist[0]]
                for sec in sec_ids.difference(df_tmp.columns):
                    df_tmp[sec] = value_dup
        return df_tmp

    def get_data_bloomberg(self, df_symbols, param, start_date, end_date, start_date_default):
        """
        Get data from bloomberg
        
        Task:
            #. Get paramters from config
            #. Load data by group: currency x date
                * Existing ids: from start_date_corr = min(start_date, data_date + BDay(1))
                * New ids: from start_date_default
            #. Concatenate data

        Args:
            df_symbols (DataFrame): reference data, oupput of function get_tickers_from_referential, 
                with additional columns [data_date_max, data_date_min, flag_exist]
            param (dict): parameters to retrieve data
            start_date (str, datetime): start date
            end_date (str, datetime): end date
            start_date_default (str, datetime): default start date
        Returns:
            df_tmp (DataFrame): Data tin datetime format.
        
        Example:
            .. code-block:: python
                from dbtools.src.QuantWorkUpdater_v2 import QuantWorkUpdater
                import pandas as pd
                loader = QuantWorkUpdater(dataset_name = "FUNDAMENTAL_DATA_STOCK", config_file_name = 'quant_work_updater.json')
                df_symbols = loader.get_tickers_from_referential([8, 23, 283812,292445, 292446, 10174308])
                df_symbols['flag_exist']= True
                df_symbols['data_date_max']=pd.to_datetime('20240101')
                df_symbols['data_date_min']=pd.to_datetime('20240101')
                param = {
                        "data_frequence":"DAILY",
                        "function": "EndOfDayBbg",
                        "field": ["BEST_EPS"],
                        "currency": "local",
                        "overrides": {
                            "BEST_FPERIOD_OVERRIDE": "BF"
                            },
                        "setParams": {
                            "adjustmentNormal": False,
                            "adjustmentAbnormal": False,
                            "adjustmentSplit": False
                            }
                        }
                df_data_bbg = loader.get_data_bloomberg(df_symbols, param, start_date='20240103', end_date='20240105', start_date_default='20240101')
        """
        data_frequence = param.get('data_frequence', None)
        function = param.get('function')
        fields = param.get('field')
        overrides = param.get('overrides')
        setParams = param.get('setParams')
        # set data_frequence
        if data_frequence in ['DAILY', 'WEEKLY', 'MONTHLY', 'QUARTERLY', 'YEARLY', None]:
            setParams['periodicitySelection']= data_frequence
        else:
            raise Exception('data_frequence: Only [DAILY, WEEKLY, MONTHLY, QUARTERLY, YEARLY, None] accepted')

        df_exist = pd.DataFrame()
        df_new = pd.DataFrame()
        print('---- Loading from %s to %s -----'%(start_date, end_date))
        # Load data for each currency
        for ccy in df_symbols['currency'].unique():
            print('---- Loading for currency %s -----'%(ccy))
            setParams['currency'] = ccy
            
            # retrieve existing security
            df_sec_exist = pd.DataFrame()
            df_symbols_exist = df_symbols.loc[df_symbols.currency.isin([ccy]) & df_symbols.flag_exist]
            # Load data for each data_date_max
            for data_date in df_symbols_exist.data_date_max.unique():
                df_symbols_exist_tmp = df_symbols_exist.loc[df_symbols_exist.data_date_max==data_date]
                symbols_exist = df_symbols_exist_tmp.full_ticker.drop_duplicates().to_list()
                start_date_corr = min(pd.to_datetime(start_date), data_date + BDay(1))
                
                
                if len(symbols_exist)!=0:
                    tst_exist = eval(function)(symbols_exist, start_date_corr, end_date, overrides = overrides, setParams = setParams)
                    df_tmp_exist = tst_exist.get(fields=fields).get(fields[0], pd.DataFrame())
                    df_tmp_exist = self.mapping_ticker_to_security_id(df_tmp_exist, df_symbols_exist_tmp)
                    df_sec_exist = pd.concat((df_sec_exist, df_tmp_exist), axis = 1)
            #############---------------------#############
            # retrieve NEW security
            df_symbols_new = df_symbols.loc[df_symbols.currency.isin([ccy]) & (~df_symbols.flag_exist)]
            symbols_new = df_symbols_new.full_ticker.drop_duplicates().to_list()
            df_sec_new = pd.DataFrame()
            if len(symbols_new)!=0:
                print(f'---------- {ccy} : Below security_id is new in perimeter ----------')
                print(df_symbols_new.ticker.to_list())
                tst_new = eval(function)(symbols_new, start_date_default, end_date, overrides = overrides, setParams = setParams)
                df_tmp_new = tst_new.get(fields=fields).get(fields[0], pd.DataFrame())
                df_sec_new = self.mapping_ticker_to_security_id(df_tmp_new, df_symbols_new)
            #############---------------------#############
            df_exist = pd.concat((df_exist, df_sec_exist), axis = 1)
            df_new = pd.concat((df_new, df_sec_new), axis = 1)

        df_tmp = pd.concat((df_exist, df_new), axis = 1)
        return df_tmp

    def get_data(self, start_date, end_date):
        '''
        IN CONSTRUCTION
        Get data from source
        Task:
            # Load perimeter and referential data
            # Load data from depending on source_name
            # Transform data to compatible database format
        Args:
            start_date : datetime forrmat
            end_date : datetime forrmat
        Returns:
            dataframe: comptaible database format for table.
        '''
        ids = self.get_perimeter(start_date = start_date, end_date=end_date)
        
        table_field = self.config.get('table_field')
        table_data_dictionary = self.config.get('table_data_dictionary', None)

        if table_data_dictionary:
            s_mapping_name = get_mapping_from_dictionnary(table_data_dictionary, key='name', value='attribute_id')
        else:
            s_mapping_name = {}
        d_data = {}
        source_name = self.config.get('data_source',{}).get("source_name",'')
        
        if source_name == "Bloomberg":
            data_spec = self.config.get('data_source',{}).get("data_spec",{})
            start_date_default = self.config.get('start_date_default','19980101')
            print('-------------- Loading from %s ------------------'%source_name)

            for name, param in tqdm(data_spec.items()):
                df_symbols = self.get_tickers_from_referential(ids)
                df_symbols = df_symbols[df_symbols['delisted_date']>pd.to_datetime(start_date).date()]
                if param.get('currency')!='local':
                    df_symbols['currency'] = param.get('currency')
                self.df_symbols = df_symbols.copy()
                # get start date default per attribute_id. if df_date_ref, get start_date_default from config, else min(date in table, start_date)
                attribute_id = s_mapping_name.get(name)
                df_date_ref = self.get_security_date_ref(attribute_id)
                if df_date_ref.empty:
                    start_date_default = self.config.get('start_date_default','19980101')
                else:
                    start_date_default = min(pd.to_datetime(start_date), 
                                             df_date_ref.data_date_min.min())
                    start_date_default = start_date_default.strftime('%Y%m%d')
                df_symbols = self.df_symbols.merge(df_date_ref, left_index =True, right_index =True, how = 'left')
                df_symbols['flag_exist'] = df_symbols['data_date_min'].notna()
                df_symbols['data_date_min'] = df_symbols['data_date_min'].fillna(start_date_default)
                df_symbols['data_date_max'] = df_symbols['data_date_max'].fillna(datetime(2999,12,31))
                # Load data using get_data_bloomberg
                df_tmp = self.get_data_bloomberg(df_symbols, param, start_date, end_date, start_date_default)
                d_data[name] = df_tmp.copy()


            # transform data into format (index_id, DATE, value)
            df_data = transform_dict_to_df(d_data, self.config.get('table_date_field'))
            df_data = df_data.rename(index = s_mapping_name)
            df_data.columns.name = self.config.get("table_perimeter_field")
            
        if df_data.empty:
            df_db = pd.DataFrame(columns = self.config.get('table_columns'))
        else:
            df_db = df_data.stack().to_frame(table_field).reset_index()

        df_db['insert_date'] = pd.to_datetime(datetime.now().strftime('%Y%m%d'))
        df_db = df_db[self.config.get('table_columns')].drop_duplicates(keep = 'first')
        
        sec_missing = df_symbols.index.difference(df_db[self.config.get("table_perimeter_field")])
        self.df_symbols_check = pd.DataFrame()
        if len(sec_missing)>0:
            print('!!!!!!!!!!!!! Check if below securities are delisted !!!!!!!!!!!!!')
            print(df_symbols.loc[sec_missing])
            self.df_symbols_check = df_symbols.loc[sec_missing].copy()
        return df_db
    
    def import_to_sql(self, df_db):
        """
        Import df_db to table_name
        Task: 
            # no task, only to avoid long lines to import
        Args:
            df_db (df): data frame in database format, compatible with table to import
            
        """
        table_name = self.config.get('table_name')
        tmp = table_name.split('..')[-1]
        db_quant_index = db.DatabaseManager(tmp)
        db_quant_index.append_table(df = df_db.copy())
        pass


def get_mapping_from_dictionnary(table_dictionnary, key='attribute_id', value='name'):
    """
    get mapping from data dictionnary table
    Task:
        # retrive meta data in data dictionnary table
        # create mapping
    Args:
        table_dictionnary (str): data dictionnary table
        key (str): name of a column in the table as a key to map
        value (str): name of a column in the table to retrieve
    Returns:
        pd.Series of mapping
    """
    df = pd.read_sql(f"select * from {table_dictionnary}", con_mis)
    s_map = pd.Series(index = df[key].values, data=df[value].values)
    return s_map

def transform_dict_to_df(data, date_field):
    """
    Transform a dictionnary of data frames of time series to a dataframe databases format multi-index
    Task:
        #. transforme df time series to df database with additional column "attribute_id" whose value is key of dict
        #. Add insert_date columns as today in data
    Args:
        data (dict): dictionnary of data frames
    Returns:
        df_data (df): dataframe multi-index
    """
    df_data = pd.DataFrame()
    for k in data.keys():
        df = data[k].copy()
        df.index.name = date_field
        df['attribute_id'] = k
        df = df.reset_index().set_index(['attribute_id', date_field])
        df_data = pd.concat((df_data, df))
    return df_data

def last_business_day_of_month(year, month):
    """
    Returns the last business day of the given year and month.
    Task:
        # last business day of the given year and month.
    Args:
        year (int): The year
        month (int): The month

    Returns:
        datetime.date: The last business day of the specified month and year.
    
    Example:
        last_business_day_of_month(2023, 12)
    """
    # Create a datetime object for the first day of the given month
    date = pd.Timestamp(year=year, month=month, day=1)

    # Get the last business day of the month
    last_business_day = pd.to_datetime(date + BMonthEnd(1)).date()

    return last_business_day

def closest_past_quarter_business_day(year, month):
    """
    Returns the first business day of the closest among the past (March, June, September, December) for the given year and month.
    This is the update frequency for KECH Beta profiles.
    Task:
        # Finding the most recent quarter month before the given year and month
    Args:
        year (int): The year
        month (int): The month

    Returns:
        datetime.date: The last business day of the specified month and year.
    Example:
        closest_past_quarter_business_day(2023, 12)
    """
    quarter_months = [3, 6, 9, 12]
    # Finding the most recent quarter month before the given year and month
    if month not in quarter_months:
        # If the given month is not a quarter month, adjust to the previous quarter
        past_quarter_months = [m for m in quarter_months if m < month]
        if not past_quarter_months:
            # If no past quarter months in this year, use the last quarter of the previous year
            quarter_month = quarter_months[-1]
            year -= 1
        else:
            # Use the latest past quarter month in this year
            quarter_month = max(past_quarter_months)
    else:
        quarter_month = month

    # Get the first business day of the identified quarter month
    date = pd.Timestamp(year=year, month=quarter_month, day=1)
    first_business_day = pd.to_datetime(date + BMonthBegin(1) - CustomBusinessDay(1)).date()
    return first_business_day