# -*- coding: utf-8 -*-
"""
**This module generate file to load data and update to QUANT_work**

Created on Thu Jun 30 09:02:47 2022

@author: tle (tle@keplercheuvreux.com)

#. :py:class:`QuantWorkLoader`
#. :py:class:`AdjustmentLoader`
#. :py:class:`FundamentalLoader`
#. :py:class:`IndexLevelLoader`
#. :py:class:`GicsLoader`
#. :py:class:`SalesEuropeLoader`
#. :py:class:`CalendarLoader`

"""

# %% Part 1: Function

from datetime import datetime
import pandas as pd
import os
import numpy as np
from openpyxl import Workbook
from dbtools.src.db_connexion import SqlConnector
import dbtools.src.get_repository as rep
import dbtools.src.get_market_data as mkt
import dbtools.src.bcp_import_to_sql as bcp_import_to_sql
import requests
import ssl
ssl._create_default_https_context = ssl._create_unverified_context


connector = SqlConnector()
con_mis = connector.connection()
class QuantWorkLoader:
    """
    This class loads data update for QUANT_work tables.
    The class methods covers all the process from defining the universe of securities to creating a final SQL table.

    Args:
        start_date (str): yyyymmdd
        end_date (str): yyyymmdd, last friday by default
        perimeter (int, str or list): Perimeter of data to update
            - if perimeter is an integer, perimeter is a data level
            - if perimeter is a str, perimeter is an index ticker
            - if perimeter is a list of integer: perimeter is a list of security_id
            - if perimeter is a list of string: perimeter is a list of ticker
        
        code: code in case perimeter is list of int to use in rep.mapping_from_security
        path_to_save: folder to save, default W:/Global_Research/Quant_research/projets/data/dynamic universe/quant_work
        path_config: json file in format 'split', default W:/Global_Research/Quant_research/team/tle/dbtools/

    Attributes:
        df_db (DataFrame): Data frame in database format to import to QUANT_work
        path_to_load (str): excel path of value loaded from Bloomberg
    """

    def __init__(self, perimeter=1, start_date='20100101', end_date = '', code ='fund_ticker',
                 path_to_save='W:/Global_Research/Quant_research/projets/data/dynamic universe/quant_work/',
                 path_config='W:/Global_Research/Quant_research/projets/data/dynamic universe/quant_work/'):

        self.perimeter = perimeter
        self.start_date = start_date
        if end_date!='':
            self.end_date = end_date
        else: # initialize end date by last friday
            end_date = pd.date_range(end = datetime.today().date(), periods=6)
            end_date=end_date[end_date.weekday==4][-1]
            self.end_date = end_date.strftime('%Y%m%d')

        self.path_to_save = path_to_save
        self.path_config = path_config

        # if perimeter is integer, perimeter is a data level
        if isinstance(perimeter,int):
            self.ticker = self.get_ticker()
        #if perimeter is a list of integer: perimeter is a list of security_id
        elif isinstance(perimeter, (list, tuple, np.ndarray, pd.Index)):
            if isinstance(perimeter[0], (int, float, np.int64)):
                self.ticker = rep.mapping_from_security(perimeter, code=code)
                #if perimeter is a list of string: perimeter is a list of ticker
            elif ' ' in perimeter[0]:
                self.ticker = pd.Series(perimeter)
            else:
                self.ticker = self.get_ticker()
        elif isinstance(perimeter,str):
            self.ticker = self.get_ticker()

        self.df_db = pd.DataFrame()
        self.path_to_load = ''

    def get_ticker(self):
        """Load ticker"""

    def create_macro(self):
        """Create macro excel"""

    def get_from_excel(self):
        """
        Get data from excel file and stack, then affect data to self.df_db
        """
        path_to_load = self.path_to_load
        sheetname = pd.ExcelFile(path_to_load).sheet_names
        sheetname = pd.Index(sheetname)
        sheetname = sheetname[sheetname!='Sheet']
        # load security_id and ticker in sheetname 'Sheet' in excel file to map
        sec_map_ticker = pd.read_excel(path_to_load, sheet_name = 'Sheet', index_col=0).squeeze()
        # Read sheets in the workbook
        df_db_stack = pd.DataFrame()
        for j in range(len(sheetname)):
            sheet = sheetname[j]
            print('No.%d - %s'%(j, sheet))
            df_table = pd.read_excel(path_to_load, sheet_name = sheet,
                               header=3, index_col = 0, na_values=['#N/A Invalid Security'])
            df_table = df_table.rename_axis(columns = ['ticker'])
            # mapping ticker in columns to find security_id
            df_transp = df_table.T#.dropna(how = 'all')
            df_transp['security_id'] = sec_map_ticker.index
            df_transp = df_transp.set_index(['security_id'], append = True)
            df_transp = df_transp.rename_axis(index = ['ticker', 'security_id'], columns = 'date')
            # Transform each sheet format table to format database sql
            df_stack = df_transp.stack(dropna=False).to_frame(sheet)
            df_db_stack = pd.concat((df_db_stack, df_stack), axis = 1)
            print(df_db_stack.shape)

        df_db_stack = df_db_stack.sort_values(by = ['date', 'security_id']).reset_index()
        self.df_db = df_db_stack.copy()
        return None

    def import_to_sql(self, sql_table_name, drop_existing = True):
        """
        import self.df_db to QUANT_work..[sql_table_name]

        Args:
            sql_table_name (str): table name to create
            drop_existing (bool): drop existing table or not
        """
        con_mis.execute('USE QUANT_work')
        if drop_existing:
            try:
                con_mis.execute("drop table IF EXISTS %s" %sql_table_name)
            except:
                print('Drop existing table: QUANT_work..%s'%sql_table_name)

        pd.bulk_copy(con_mis, sql_table_name, self.df_db)
        print('Data are imported to %s' %sql_table_name)
        return None

class FundamentalLoader(QuantWorkLoader):
    """Load fundamental data to update in QUANT_work"""

    def get_ticker(self):
        """
        Load fundamental ticker of given perimeter
        """
        perimeter = self.perimeter
        path_config = self.path_config

        level_id = rep.get_quant_perimeter(level=perimeter)
        fund_ticker = rep.mapping_from_security(level_id, code='fund_ticker')

        config_file = 'config/old_fund_for_missing_fund.json'
        path_config = os.path.join(os.path.dirname(__file__), '..')
        try:
            ticker_old = pd.read_json(os.path.join(path_config, config_file), orient = 'split')
        except:
            ticker_old = pd.read_json(path_config + config_file, orient = 'split')

        ticker_old = ticker_old.FUNDAMENTAL_TICKER
        level_na = level_id.difference(fund_ticker.index)
        fund_missing = ticker_old.loc[level_na.intersection(ticker_old.index)]
        fund_ticker_full = pd.concat([fund_ticker, fund_missing]).sort_index()
        df_temp = pd.DataFrame(fund_ticker_full)
        df_temp.columns = ['FUNDAMENTAL_TICKER']
        df_temp = df_temp.rename_axis(index='security_id')

        return fund_ticker_full

    def create_macro(self):
        """
        Create macro excel, file saved in
        W:/Global_Research/Quant_research/projets/data/dynamic universe/quant_work/fundamental/fundamental_value_yyyymmdd.xlsx
        """
        ticker = self.ticker
        start_date = self.start_date
        end_date = self.end_date
        path_to_save = self.path_to_save
        path_config = self.path_config

        config_file = 'config/fundamental.json'
        try:
            df_config = pd.read_json('C:/python_tools/dbtools/' + config_file, orient = 'split')
        except:
            df_config = pd.read_json(path_config + config_file, orient = 'split')

        ticker = pd.Series(ticker)
        start_date = datetime.strptime(start_date, '%Y%m%d')
        end_date = datetime.strptime(end_date, '%Y%m%d')

        fund_level2_new = Workbook()
        nb_stocks = len(ticker)
        sheet_temp = fund_level2_new.active
        sheet_temp['A1'] = 'security_id'
        sheet_temp['B1'] = 'FUNDAMENTAL_TICKER'
        for i in range(nb_stocks):
            sheet_temp['A%s' % (i + 2)] = ticker.index[i]
            sheet_temp['B%s' % (i + 2)] = ticker.values[i]

        for index in range(len(df_config)):
            sheet_temp = fund_level2_new.create_sheet(df_config.sheet_name[index])
            for cell in ['A1', 'B1', 'C1', 'D1']:
                sheet_temp[cell] = df_config[cell][index]
            sheet_temp['B2'] = start_date
            sheet_temp['B3'] = end_date
            sheet_temp['A2'] = 'start_date'
            sheet_temp['A3'] = 'end_date'
            sheet_temp['A4'] = 'Date'

            for i in range(nb_stocks):
                ticker_row = sheet_temp.cell(row=4, column=i + 2, value=ticker.iloc[i])
                coor = ticker_row.coordinate
                if i == 0:
                    formula = df_config.formula1[index]
                    formula_row = sheet_temp.cell(row=5, column=1, value=formula)

                else:
                    if df_config.sheet_name[index] == 'PX_LAST_N_N_Y_fundccy':
                        formula = df_config.formula2[index] % (coor[:-1], coor[-1],
                                coor[:-1], coor[-1], coor[:-1], coor[-1], coor[:-1], coor[-1])
                    else:
                        formula = df_config.formula2[index] % (coor[:-1], coor[-1])
                    formula_row = sheet_temp.cell(row=5, column=i + 2, value=formula)
        file_to_save = path_to_save + 'fundamental/fundamental_formula_%s.xlsx' %end_date.strftime('%Y%m%d')
        file_to_save_value = file_to_save.replace('formula', 'value')
        fund_level2_new.save(file_to_save)
        fund_level2_new.save(file_to_save_value)
        print('File macro fundamental is saved in %s' % (file_to_save_value))
        self.path_to_load = file_to_save_value
        return None

    def correct_fundccy(self):
        """
        For the PX_LAST_N_N_Y_fundccy there are no values available for three tickers
        (#N/A Invalid value), stocks IAP LN, RGU LN, HBMN SW
        -> before definite resolution, replace with the local currency prices (PX_LAST_N_N_Y)
        """
        df_db = self.df_db.copy()
        index_to_correct = df_db.ticker.isin(['IAP LN', 'RGU LN', 'HBMN SW'])
        fundccy_to_correct = df_db.loc[index_to_correct,'PX_LAST_N_N_Y']
        df_db.loc[index_to_correct, 'PX_LAST_N_N_Y_fundccy'] = fundccy_to_correct
        self.df_db = df_db.copy()
        return None
    
    def correct_EXO(self):
        """
        Concatenate fundamental data  of EXO IM (795445) and EXO NA (10174467)
        As if EXO IM (795445) changed listing from Italy to Amsterdam (EXO NA (10174467))
        """
        df_db = self.df_db.copy()
        df_ = df_db[df_db.security_id.isin([795445, 10174467])]

        df_db = pd.concat((df_db, df_)).drop_duplicates(keep = False)

        df_tmp = df_.set_index(['ticker', 'security_id', 'date']).unstack().T
        df_tmp = df_tmp.fillna(axis = 1, method = 'ffill').fillna(axis = 1, method = 'bfill').T
        df_tmp = df_tmp.stack().reset_index()

        df_db = pd.concat((df_db, df_tmp)).reset_index(drop = True)
        self.df_db = df_db.copy()
        return None
    def correct_DSFIR(self):
        change_date = pd.to_datetime('20230421')
        df_db = self.df_db.copy()
        df_ = df_db[df_db.security_id.isin([13073])]
        df_ = df_.loc[df_.date >= change_date]

        df_patch = pd.read_sql('select * from QUANT_work..level2_fundvar_patch_13073', con_mis)
        df_patch['date'] = pd.to_datetime(df_patch['date'])
        df_patch = df_patch.loc[df_patch.date < change_date]
        df_patch['ticker'] = df_['ticker'].iloc[0]
        
        df_db = df_db[~df_db.security_id.isin([13073])]
        df_db = pd.concat((df_db, df_patch, df_))
        self.df_db = df_db.copy()
        return None
        



class IndexLevelLoader(QuantWorkLoader):
    """Load index_level data to update in QUANT_work"""
    def get_ticker(self):
        """
        Get indices ticker to update
        """
        index_tickers = ['SV2P', 'SD3P', 'STVP', 'STGP', 'M9EUEV', 'MXEUQU', 'MXEU',
                         'MXEUHDVD', 'MXEU000G', 'SXXEWP', 'SXEWGR', 'SXXP', 'SXXGR', 'CAC', 'CACEW', 'SBF120',
                         'ENFAM', 'ENFAG']
        return index_tickers

    def create_macro(self):
        """
        Create macro excel, file saved in
        W:/Global_Research/Quant_research/projets/data/dynamic universe/quant_work/index_level/index_value_yyyymmdd.xlsx
        """
        ticker = self.ticker
        start_date = self.start_date
        end_date = self.end_date
        path_to_save = self.path_to_save
        path_config = self.path_config

        config_file = 'config/index_level.json'
        path_config = os.path.join(os.path.dirname(__file__), '..')
        try:
            df_config = pd.read_json(os.path.join(path_config, config_file), orient = 'split')
        except:
            df_config = pd.read_json(path_config + config_file, orient = 'split')

        ticker = pd.Series(ticker)
        start_date = datetime.strptime(start_date, '%Y%m%d')
        end_date = datetime.strptime(end_date, '%Y%m%d')

        index_workbook = Workbook()
        nb_stocks = len(ticker)
        sheet_temp = index_workbook.active
        sheet_temp['A1'] = 'security_id'
        sheet_temp['B1'] = 'Index'
        for i in range(nb_stocks):
            sheet_temp['A%s' % (i + 2)] = ticker.index[i]
            sheet_temp['B%s' % (i + 2)] = ticker.values[i]

        for index in range(len(df_config)):
            sheet_temp = index_workbook.create_sheet(df_config.sheet_name[index])
            for cell in ['A1', 'B1', 'C1', 'D1']:
                sheet_temp[cell] = df_config[cell][index]
            sheet_temp['B2'] = start_date
            sheet_temp['B3'] = end_date
            sheet_temp['A2'] = 'start_date'
            sheet_temp['A3'] = 'end_date'
            sheet_temp['A4'] = 'Date'

            for i in range(nb_stocks):
                ticker_row = sheet_temp.cell(row=4, column=i + 2, value=ticker.iloc[i])
                coor = ticker_row.coordinate
                if i == 0:
                    formula = df_config.formula1[index]
                    formula_row = sheet_temp.cell(row=5, column=1, value=formula)

                else:
                    formula = df_config.formula2[index] % (coor[:-1], coor[-1])
                    formula_row = sheet_temp.cell(row=5, column=i + 2, value=formula)
        file_to_save = path_to_save + 'index_level/index_formula_%s.xlsx' %end_date.strftime('%Y%m%d')
        file_to_save_value = file_to_save.replace('formula', 'value')
        index_workbook.save(file_to_save)
        index_workbook.save(file_to_save_value)
        print('File macro index level is saved in %s' % (file_to_save_value))

        self.path_to_load = file_to_save_value
        return self.path_to_load

    def get_from_excel(self):
        self.df_db = pd.read_excel(self.path_to_load, header = 3, index_col = 0, sheet_name = 1,
                                   na_values=['#N/A Invalid Security'])
        self.df_db.columns = self.df_db.columns + ' Index'
        return None
    
    def _get_ffama_data(self, zone_ffama='europe'):
        """
        Get French Fama 3 factors
        Details on https://mba.tuck.dartmouth.edu/pages/faculty/ken.french/data_library.html.
        Save file in path_to_save= W:/Global_Research/Quant_research/projets/backtest/Excel/fama_french_data_yyyymmdd.xlsx

        Args:
            zone_ffama (str): in [research, europe, europe_mom]

        Returns:
            DataFrame French Fama 3 factors.
            Save file in path_to_save = W:/[...]/backtest/Excel/fama_french_data_yyyymmdd.xlsx

        .. exec_code::

            import dbtools.src.get_market_data as mkt
            df = mkt.get_ffama_data()
            print(df.iloc[-5:])
        """
        path_to_save = 'W:/Global_Research/Quant_research/projets/backtest/Excel/'
        dict_ffama = {'research': 'ftp/F-F_Research_Data_Factors_daily_CSV.zip',
                      'europe': 'ftp/Europe_3_Factors_Daily_CSV.zip',
                      'europe_mom': 'ftp/Europe_Mom_Factor_Daily_CSV.zip'}
        url_ffama = 'https://mba.tuck.dartmouth.edu/pages/faculty/ken.french/'
        url_ffama = url_ffama + dict_ffama[zone_ffama]
        print(url_ffama)
        status = requests.get(url_ffama, verify = False).status_code
        if status != 200:
            filename = 'Error_%s.txt' % datetime.today().strftime('%Y%m%d')
            error = 'ERROR %d: URL french fama model does not work: %s\n' % (status, url_ffama)
            f = open(filename, "a")
            f.write(error)
            f.close()
            print(error)
            return 'ERROR'
        df_ffama = pd.read_csv(url_ffama, header=2, index_col=0).dropna(how='all')
        df_ffama = df_ffama.rename(columns={'Mkt-RF': 'FF_Mkt_RF',
                                            'SMB': 'FF_Size',
                                            'HML': 'FF_Value'})
        df_ffama.index = pd.to_datetime(df_ffama.index.astype(str))
        df_ffama = df_ffama.rename_axis(index=None, columns=None)
        try:
            filename = 'fama_french_data_%s.xlsx' % df_ffama.index[-1].strftime('%Y%m%d')
            if not os.path.exists(path_to_save + filename):
                df_ffama.to_excel(path_to_save + filename)
            print('French Fama data saved in : %s' % path_to_save + filename)
        except:
            print('Path is not valid: %s' % path_to_save)
        return df_ffama
    
    def add_ffama_data(self):
        """
        Add french Fama data into index level
        """
        index_level = self.df_db.copy()
        ffama = self._get_ffama_data()
        ffama = ffama.reindex(index_level.index)

        index_level = pd.concat([index_level, ffama], axis=1)
        index_level = index_level.rename_axis(index=['date'], columns=None)
        index_level = index_level.reset_index()
        self.df_db = index_level.copy()
        return None



class GicsLoader(QuantWorkLoader):
    """Load GICS sector data to update in QUANT_work"""
    def get_ticker(self):
        """
        Get missing GICS ticker top update in Referential
        """
        perimeter = self.perimeter
        level1 = rep.get_quant_perimeter(perimeter)
        sector = rep.mapping_sector_from_security(level1, code='GICS')
        sector = sector.dropna(subset=['GICS_INDUSTRY', 'GICS_SECTOR', 'GICS_INDUSTRY_GROUP'])
        gics_missing = level1.difference(sector.index)
        ticker = rep.mapping_from_security(gics_missing, code='bbg')
        return ticker

    def create_macro(self):
        """
        Create macro excel file to load data GICS
        W:/Global_Research/Quant_research/projets/data/dynamic universe/quant_work/gics/gics_value_yyyymmdd.xlsx
        """
        path_to_save = self.path_to_save
        ticker = self.ticker
        end_date = self.end_date

        gics = pd.Series({'A': 'security_id', 'B': 'ticker',
                              'C': 'GICS_SECTOR', 'D': 'GICS_INDUSTRY_GROUP',
                              'E': 'GICS_INDUSTRY'})
        icb = pd.Series({'F': 'ICB_SECTOR_NUM', 'G': 'ICB_SECTOR_NAME',
                         'H': 'ICB_SUPERSECTOR_NUM', 'I': 'ICB_SUPERSECTOR_NAME',
                         'J': 'ICB_INDUSTRY_NUM', 'K': 'ICB_INDUSTRY_NAME'})
        gics_book = Workbook()
        sheet_temp = gics_book.active
        for (key, value) in gics.items():
            for (i, (ind, tick)) in enumerate(ticker.items()):
                if i == 0:
                    sheet_temp[key + str(i + 1)] = value

                if key == 'A':
                    sheet_temp[key + str(i + 2)] = ind
                elif key == 'B':
                    sheet_temp[key + str(i + 2)] = tick
                else:
                    sheet_temp[key + str(i + 2)] = '=BDP(B%d&" Equity",%s$1)' % (i + 2, key)
        sheet_temp.title = 'BBG formula'
        file_to_save = path_to_save + 'gics/gics_formula_%s.xlsx' %end_date
        file_to_save_value = file_to_save.replace('formula', 'value')
        gics_book.save(file_to_save)
        gics_book.save(file_to_save_value)
        print('File macro GICS is saved in %s' % file_to_save_value)

        self.path_to_load = file_to_save_value
        return self.path_to_load

    def get_from_excel(self):
        self.df_db = pd.read_excel(self.path_to_load, header = 0, na_values=['#N/A Invalid Security'])
        return None

class AdjustmentLoader(QuantWorkLoader):
    """
    Load and compute adjustment factor to update in QUANT_work
    W:/Global_Research/Quant_research/projets/data/dynamic universe/adjustment_factor/LaTEX/Adjustment factors.pdf
    """

    def get_ticker(self):
        """
        get adjustment ticker to update
        """
        perimeter = self.perimeter
        start_date = self.start_date
        end_date = self.end_date

        security_id = rep.get_quant_perimeter(perimeter, start_date, end_date)
        df_adj_fac = mkt.get_corp_action_histo(security_id, start_date, end_date)['adj_prc']
        sec_id_existing = df_adj_fac.columns
        sec_id_missing = security_id.difference(df_adj_fac.columns)
        s_end = rep.mapping_from_security(sec_id_existing, code = 'end_date')
        # Only consider security whose end_date > last_year
        sec_keep = s_end[s_end>(pd.to_datetime(end_date)-pd.Timedelta(days=365)).date()].index
        # check late listing
        sec_id_check = sec_keep[df_adj_fac[sec_keep].isna().sum() > 0]
        sec_id_check = sec_id_missing.union(sec_id_check)
        # isin in level1 but the security_is not
        #isin_l1 = rep.mapping_from_security(security_id,code='isin')
        #df_l1 = rep.mapping_security_id_from(isin_l1, code ='isin')
        #missing_l1 = df_l1[~df_l1.security_id.isin(security_id)].security_id.values
        #sec_id_check = sec_id_check.union(missing_l1)

        sec_add_adj_fac = rep.mapping_from_security(sec_id_check, code='bbg')
        return sec_add_adj_fac

    def create_macro(self):
        """
        Create macro excel, file saved in
        W:/Global_Research/Quant_research/projets/data/dynamic universe/quant_work/adjustment/adjustment_value_yyyymmdd.xlsx
        """
        start_date_adj = datetime(datetime.now().year - 10,1,1)
        ticker = self.ticker
        start_date = self.start_date
        end_date = self.end_date
        path_to_save = self.path_to_save
        path_config = self.path_config

        config_file = 'config/adjustment.json'
        path_config = os.path.join(os.path.dirname(__file__), '..')
        try:
            df_config = pd.read_json(os.path.join(path_config, config_file), orient = 'split')
        except:
            df_config = pd.read_json(path_config + config_file, orient = 'split')

        ticker = pd.Series(ticker)
        start_date = datetime.strptime(start_date, '%Y%m%d')
        end_date = datetime.strptime(end_date, '%Y%m%d')

        adjustment_workbook = Workbook()
        nb_stocks = len(ticker)
        sheet_temp = adjustment_workbook.active
        sheet_temp['A1'] = 'security_id'
        sheet_temp['B1'] = 'Index'
        for i in range(nb_stocks):
            sheet_temp['A%s' % (i + 2)] = ticker.index[i]
            sheet_temp['B%s' % (i + 2)] = ticker.values[i]

        for index in range(len(df_config)):
            sheet_temp = adjustment_workbook.create_sheet(df_config.sheet_name[index])
            for cell in ['A1', 'B1', 'C1', 'D1']:
                sheet_temp[cell] = df_config[cell][index]
            sheet_temp['B2'] = start_date_adj
            sheet_temp['B3'] = end_date
            sheet_temp['A2'] = 'start_date'
            sheet_temp['A3'] = 'end_date'
            sheet_temp['A4'] = 'Date'

            for i in range(nb_stocks):
                ticker_row = sheet_temp.cell(row=4, column=i + 2, value=ticker.iloc[i])
                coor = ticker_row.coordinate
                if i == 0:
                    formula = df_config.formula1[index]
                    formula_row = sheet_temp.cell(row=5, column=1, value=formula)

                else:
                    formula = df_config.formula2[index] % (coor[:-1], coor[-1])
                    formula_row = sheet_temp.cell(row=5, column=i + 2, value=formula)
        file_to_save = path_to_save + 'adjustment/adjustment_formula_%s.xlsx' %end_date.strftime('%Y%m%d')
        file_to_save_value = file_to_save.replace('formula', 'value')
        adjustment_workbook.save(file_to_save)
        adjustment_workbook.save(file_to_save_value)
        print('File macro ahjustment is saved in %s' % (file_to_save_value))
        self.path_to_load = file_to_save_value
        return self.path_to_load

    def compute_adjustment_factor(self):
        """
        Recompute adjustment factor base on price and volume, method detailed in
        W:/Global_Research/Quant_research/projets/data/dynamic universe/adjustment_factor/LaTEX/Adjustment factors.pdf
        """

        sheetname = ['PX_LAST_N_N_N', 'PX_LAST_N_N_Y', 'PX_LAST_N_Y_N', 'PX_LAST_Y_N_N',
                     'PX_VOLUME_N_N_N', 'PX_VOLUME_N_N_Y']
        cols = ['prc_ref', 'prc_cap_chg', 'prc_cash_abnormal', 'prc_cash_normal', 'vlm_ref', 'vlm_cap_chg']

        df_prc_vlm = self.df_db.copy()
        df_prc_vlm = df_prc_vlm.rename(columns=dict(zip(sheetname, cols)))
        # force NaN volume if price change is Nan for a given day
        df_prc_vlm.loc[df_prc_vlm.prc_ref.isna(), cols] = np.nan

        df_ratio = self.compute_change_ratio(df_prc_vlm)

        ## trucate data base on begin_date_min, end_date_max
        df_ratio = df_ratio.sort_values(['security_id', 'date']).reset_index(drop=True)
        # trucate the adjustment periode between begin_date_min and end_date_max
        nb_repeat = df_ratio.groupby('security_id').count()['ticker']
        date_security = pd.concat((rep.mapping_from_security(df_ratio.security_id.unique(), 'begin_date_min'),
                          rep.mapping_from_security(df_ratio.security_id.unique(), 'end_date_max')), axis = 1)
        begin_date_min = date_security.begin_date_min.repeat(nb_repeat).reset_index(drop=True)
        end_date_max = date_security.end_date_max.repeat(nb_repeat).reset_index(drop=True)

        df_ratio.date = df_ratio.date.dt.date
        df_trunc = df_ratio[(df_ratio.date >= begin_date_min) & (df_ratio.date <= end_date_max)]

        # compute ajustment
        df_trunc['F1_agg'] = (df_trunc.prc_ref / df_trunc.prc_cap_chg)
        df_trunc['F2'] = (df_trunc.prc_ref / df_trunc.prc_cash_abnormal)
        df_trunc['F3'] = (df_trunc.prc_ref / df_trunc.prc_cash_normal)
        df_trunc['F0'] = (df_trunc.vlm_cap_chg / df_trunc.vlm_ref)

        # ############## additional processing for F0 ####################
        # "F0" Processing 1: F0 is forced to 1 if NaN AND at the same time F1_agg is not nan
        flag_bad = (df_trunc.F0.isna()) & (df_trunc.F1_agg.notna())
        df_trunc['F0'][flag_bad] = 1

        # "F0" Processing 2: F0 is forced to 1 if F1_agg = 1 (rectifying rounding issues in F0)
        df_trunc['F0'][df_trunc.F1_agg == 1] = 1

        # create F1
        df_trunc['F1'] = df_trunc.F1_agg / df_trunc.F0
        df_trunc.F1 = df_trunc.F1

        df_trunc = df_trunc.fillna(1)
        df_trunc = df_trunc.sort_values(['date', 'security_id']).reset_index(drop=True)

        df_trunc[['F1_agg', 'F1', 'F2', 'F3']] = df_trunc[['F1_agg', 'F1', 'F2', 'F3']].round(5)
        df_trunc['F0'] = df_trunc['F0'].round(2)
        
        df_dup_isin_missing = self.add_adjustment_dup_isin()
        
        cols = df_dup_isin_missing.columns
        self.df_db = pd.concat((df_trunc[cols], df_dup_isin_missing)).drop_duplicates()
        return None

    def compute_change_ratio(self, df_stack):
        """
        compute change ratio between referential and adjusted volume (or price)
        Forward fill weekly (limit=5) to avoid holiday

        Args:
            df_stack (DataFrame): DataFrame in format database.
        """

        sec_map_ticker = df_stack[['security_id', 'ticker']].drop_duplicates()
        sec_map_ticker = sec_map_ticker.set_index('security_id').squeeze()

        df_stack = df_stack.set_index(['date', 'security_id']).drop(columns = 'ticker')
        df_unstack = df_stack.unstack().fillna(method = 'ffill', limit = 5)

        # for the purpose of computing pct_chg
        df_unstack = df_unstack.replace({0:np.nan})
        df_unstack = df_unstack.pct_change(1, fill_method=None) + 1
        # clean to adapt to output format database
        df_db_stack = df_unstack.stack(dropna = False).reset_index()
        df_db_stack['ticker'] = df_db_stack.security_id.map(sec_map_ticker)
        df_db_stack  = df_db_stack.set_index(['ticker', 'security_id', 'date'])
        df_db_stack = df_db_stack.reset_index().sort_values(['date', 'security_id'])
        return df_db_stack
    
    def add_adjustment_dup_isin(self):
        end_date = self.end_date
        level1 = rep.get_quant_perimeter(1)

        isin_l1 = rep.mapping_from_security(level1,code='isin')

        df_l1 = rep.mapping_security_id_from(isin_l1, code ='isin')
        df_l1['ISIN'] = df_l1.index

        df_l1['end_date'] = df_l1.security_id.map(rep.mapping_from_security(df_l1.security_id, 'end_date_max'))
        df_l1['begin_date'] = df_l1.security_id.map(rep.mapping_from_security(df_l1.security_id, 'begin_date_min'))



        map_sec_isin = pd.Series(data = df_l1.ISIN.values, index = df_l1.security_id.values)


        df_miss = df_l1[~df_l1.security_id.isin(level1)]
        df_isin_duplicated = df_l1[df_l1.ISIN.isin(df_miss.ISIN)]

        df_dup_isin_sec_l1 = df_l1[df_l1.ISIN.isin(df_miss.ISIN) & (~df_l1.security_id.isin(df_miss.security_id))]
        df_nb_dup = pd.concat((df_isin_duplicated.ISIN.value_counts(),
                               df_dup_isin_sec_l1.ISIN.value_counts(),
                               df_miss.ISIN.value_counts()),
                              axis =1)
        df_dup_isin_sec_l1 = df_dup_isin_sec_l1.sort_values(by = ['ISIN', 'end_date', 'security_id'])
        df_dup_isin_sec_l1 = df_dup_isin_sec_l1.drop_duplicates('ISIN', keep = 'last')


        df_nb_dup.columns = ['nb of sec id', 'in level1', 'not in level1']

        dict_adj_dup = mkt.get_corp_action_histo(security_id=df_dup_isin_sec_l1.security_id, start_date='20100101', end_date=end_date)
        dict_adj_miss = mkt.get_corp_action_histo(security_id=df_miss.security_id, start_date='20100101', end_date=end_date)
        dict_adj_exist = dict_adj_miss.copy()

        df_to_fill = pd.DataFrame()
        df_to_fill_exist = pd.DataFrame()
        for key in dict_adj_dup.keys():
            df_tmp_dup = dict_adj_dup[key]
            df_tmp_miss = dict_adj_miss[key].reindex(index = df_tmp_dup.index, columns = df_miss.security_id.values)
            df_tmp_exist = dict_adj_exist[key].reindex(index = df_tmp_dup.index, columns = df_miss.security_id.values)
            
            df_tmp_miss = df_tmp_miss.rename(columns = map_sec_isin)
            df_tmp_dup = df_tmp_dup.rename(columns = map_sec_isin)
            
            df = df_tmp_dup[df_tmp_miss.columns]
            df.columns = df_miss.security_id.values
            df_tmp_miss.columns = df_miss.security_id.values
            
            df_tmp_miss[df_tmp_miss.isna()] = df[df_tmp_miss.isna()]
            
            df_tmp_diff = (df_tmp_miss.fillna(-1) - df_tmp_exist.fillna(-1)).replace({0:np.nan})
            df_tmp_fill = df_tmp_miss[df_tmp_diff.notna()].dropna(how = 'all')
            dict_adj_miss[key] = df_tmp_miss.copy()
            
            df_tmp = df_tmp_fill.stack().to_frame(key)
            df_to_fill = pd.concat((df_to_fill, df_tmp), axis = 1)

        dict_cols = {'adj_volume':'F0', 'adj_prc':'F1_agg',
                'adj_prc_special':'F2', 'adj_prc_regular':'F3'}

        df_to_fill = df_to_fill.rename(columns = dict_cols)
        df_to_fill['F0'] = df_to_fill['F0'].round(2)
        df_to_fill['F1'] = df_to_fill['F1_agg']/df_to_fill['F0']
        df_to_fill['F1'] = df_to_fill['F1'].round(5)

        df_to_fill = df_to_fill.reset_index()

        df_to_fill = df_to_fill.rename(columns = {'level_0':'date', 'level_1':'security_id'})
        return df_to_fill

class SalesEuropeLoader(QuantWorkLoader):

    def get_ticker(self):
        """
        Get ISIN of perimeter, list of index ticker (SXXP and SBF120)
        
        - Save file in W:/Global_Research/Quant_research/projets/data/dynamic universe/quant_work/sales_europe/
        - TODO:
            - Send file to ASTOR Sylvie and ask to update sales data
            - Save file updated in name "yyyymmdd Datastream Sales breakdown.xlsx"
        """
        perimeter = self.perimeter
        start_date = self.start_date
        end_date = self.end_date
        folder = 'W:/Global_Research/Quant_research/projets/data/dynamic universe/quant_work/sales_europe/'
        filename = '%s_Sales_Europe.xlsx' %end_date
        
        security_id = pd.Index([])
        for index in perimeter:
            index_comp = rep.get_index_comp(index, start_date, end_date)
            security_id = security_id.union(index_comp.columns, sort = False)
        
        isin = rep.mapping_from_security(security_id, code = 'isin')
        isin = isin.drop_duplicates(keep = 'first')
        if not (filename in os.listdir(folder)):
            isin.to_excel(folder + filename)
        print('File save in %s' %(folder + filename))
        print('Send file to ASTOR Sylvie and ask to update sales data')
        print('Save file updated in name %s "yyyymmdd Datastream Sales breakdown.xlsx"' %folder)
        return isin
    
    def get_from_excel(self):
        """
        Load data from "yyyymmdd Datastream Sales breakdown.xlsx" and transform to database format
        """
        end_date = self.end_date
        ticker = pd.Series(self.ticker.index, self.ticker.values)
        folder = 'W:/Global_Research/Quant_research/projets/data/dynamic universe/quant_work/sales_europe/'
        filename = '%s Datastream Sales breakdown.xlsx' %end_date
        flag_europe = pd.read_excel(folder + 'flag_europe.xlsx')
        flag_europe.region = flag_europe.region.str.lower().str.replace('.', ' ', regex=False).str.rstrip()
        flag_europe = flag_europe.drop_duplicates()
        flag_europe = pd.Series(flag_europe.flag.values, flag_europe.region.values)

        df_sales = pd.read_excel(folder + filename, sheet_name = 'Datastream - and final Data',
                                 header = 3, index_col = 4).dropna(how = 'all')
        df_sales = df_sales[df_sales.columns[df_sales.columns.astype(str).str.contains('SEGMENT')]]

        df_region = df_sales[df_sales.columns[df_sales.columns.astype(str).str.contains('DESCRIPTION')]]
        df_region.columns = df_region.columns.str.extract('(\d+)', expand=False).astype(int)
        df_region = df_region.stack().to_frame('region')
        df_region.region = df_region.region.str.lower().str.replace('.', ' ', regex=False).str.rstrip()
        df_region['flag'] = df_region.region.map(flag_europe)
        df_region_unknown = df_region[df_region['flag'].isna()]

        if len(df_region_unknown)>0:
            filename_unknown = folder + end_date + '_unknown_region.xlsx'
            df_region_unknown.drop_duplicates().to_excel(filename_unknown)
            raise Exception ('Fill Unknown region in %s and add to %sflag_europe.xlsx, then rerun' %(filename_unknown, folder))


        df_amount = df_sales[df_sales.columns[df_sales.columns.astype(str).str.contains('SALES')]]
        df_amount.columns = df_amount.columns.str.extract('(\d+)', expand=False).astype(int).values
        df_amount = df_amount.stack().to_frame('sales')

        df_sales_to_import = pd.concat((df_amount, df_region), axis = 1)
        df_sales_to_import = df_sales_to_import.reset_index()\
                            .rename(columns ={'level_1': 'region_id', 'ISIN CODE':'ISIN'})
        df_sales_to_import.region_id = df_sales_to_import.region_id - 1
        df_sales_to_import['security_id'] = df_sales_to_import.ISIN.map(ticker)
        df_sales_to_import['date'] = pd.to_datetime(end_date)

        self.df_db = df_sales_to_import
        return None


class CalendarLoader:
    """
    This class updates our QUANT_work data for the TRADINGBUSINESSCALENDAR table.
    It reads an Excel file that we download from Bloomberg containing the next exchange's closing days.

    Args:
        begin_date (str): yyyymmdd
        end_date (str): yyyymmdd
        calendar_file (str): name of the Excel file containing the Bloomberg holiday's calendar
    
    Attributes:
        calendar (DataFrame): Table that will be inserted into the SQL table.
    """

    REPOSITORY_PATH = "W:\\Global_Research\\Quant_research\\projets\\data\\Checklist IT template\\calendar\\"
    CON = SqlConnector().connection()
    TABLENAME = 'TRADINGBUSINESSCALENDAR'

    def __init__(self, begin_date='20220921', end_date='20230521', calendar_file="calendar 09-2022 09-2023.xlsx"):
        self.begin_date = datetime.strptime(begin_date, '%Y%m%d')
        self.end_date = datetime.strptime(end_date, '%Y%m%d')
        self.calendar_path = self.REPOSITORY_PATH + calendar_file
        self.calendar = None

    def load_bloomberg_calendar(self):
        """
        Reads the Excel file and merges it with the referential data.
        """
        refcompl = pd.read_sql(
            """select EXCHANGE, CDRCODE from KGR..EXCHANGEREFCOMPL where CDRCODE is not null""",
            self.CON,
        )
        calendar = pd.read_excel(self.calendar_path)
        calendar = calendar.merge(refcompl, how='left', left_on='CDR Code', right_on='CDRCODE')
        self.calendar = calendar

    def prepare_calendar(self):
        """
        Filters events with empty Trading or Exchange.
        Filters the calendar to keep the events between the specified range.
        Keeps only full days events.
        """
        self.calendar = self.calendar.loc[self.calendar['Trading Information'].notnull() &
                                          self.calendar['EXCHANGE'].notnull() &
                                          self.calendar['Date'].between(pd.Timestamp(self.begin_date),
                                                                        pd.Timestamp(self.end_date))]
        self.calendar = self.calendar.drop_duplicates()
        self.calendar = self.calendar[self.calendar['Trading Information'] == "No Trading"]

    def import_to_sql(self):
        """
        Inserts the prepared calendar into QUANT_work, with coherence checks done before the insertion.
        """
        if len(self.calendar) == 0:
            raise ValueError('Empty Calendar')
        if self.calendar[['Date', 'EXCHANGE']].duplicated().any():
            raise ValueError('There are duplicated lines')
    
        # Load the current data in KGR table for comparison
        kgr_table = pd.read_sql(
            """select top 10000 * from KGR..TRADINGBUSINESSCALENDAR ORDER BY CLOSEDDAY DESC""",
            self.CON,
        )
    
        # Ensure data types match and rename CLOSEDDAY to Date for consistency
        self.calendar['Date'] = self.calendar['Date'].astype(str)
        self.calendar['EXCHANGE'] = self.calendar['EXCHANGE'].astype(int)
        self.calendar = self.calendar[['EXCHANGE', 'Date']]
    
        kgr_table['EXCHANGE'] = kgr_table['EXCHANGE'].astype(int)
        kgr_table['CLOSEDDAY'] = kgr_table['CLOSEDDAY'].astype(str)
    
        
        # Concatenate the original calendar with a modified copy where EXCHANGE is set to 103
        self.calendar = pd.concat([
            self.calendar,
            self.calendar[self.calendar['EXCHANGE'] == 102].assign(EXCHANGE=103)
        ])
    
        # Filter kgr_table for the same date range as self.calendar (for comparison purposes only)
        date_min = self.calendar['Date'].min()
        date_max = self.calendar['Date'].max()
        kgr_table_filtered = kgr_table[
            (kgr_table['CLOSEDDAY'] >= date_min) & (kgr_table['CLOSEDDAY'] <= date_max)
        ]
    
        # Find rows in self.calendar that are not in kgr_table (using original kgr_table)
        calendar_not_in_kgr = self.calendar.merge(kgr_table, how='left', left_on=['EXCHANGE', 'Date'], right_on=['EXCHANGE', 'CLOSEDDAY'], indicator=True)
        calendar_not_in_kgr = calendar_not_in_kgr[calendar_not_in_kgr['_merge'] == 'left_only']
        count_calendar_not_in_kgr = len(calendar_not_in_kgr)
    
        # Find rows in kgr_table_filtered that are not in self.calendar (using filtered kgr_table)
        kgr_not_in_calendar = kgr_table_filtered.merge(self.calendar, how='left', left_on=['EXCHANGE', 'CLOSEDDAY'], right_on=['EXCHANGE', 'Date'], indicator=True)
        kgr_not_in_calendar = kgr_not_in_calendar[kgr_not_in_calendar['_merge'] == 'left_only']
        count_kgr_not_in_calendar = len(kgr_not_in_calendar)
    
        # Print the results
        print('Number of lines in self.calendar not in kgr_table:', count_calendar_not_in_kgr)
        print('Number of lines in kgr_table not in self.calendar:', count_kgr_not_in_calendar)
        
        self.calendar.rename({'Date':'CLOSEDDAY'},axis = 1,inplace=True)
        self.calendar['CLOSEDDAY'] = pd.to_datetime(self.calendar['CLOSEDDAY']).dt.date
        # Switch to QUANT_work database and insert data
        self.CON.execute('USE QUANT_work')
        self.calendar.to_sql(self.TABLENAME, self.CON, if_exists='replace', index=False)
    
        return None




    
    

class QuantHolidaysRebal:
    """
    This class import quant holidays/ MSCI rebalancing to QUANT_work..quant_holidays/QUANT_work..msci_rebalancing
    """
    def __init__(self):
        pass

    def weigh_stoxx_holidays(self,
                             filename = 'dissemination_calendar_2023.csv',
                             subfolder = 'stoxx hoidays/',
                             folder = 'W:/Global_Research/Quant_research/projets/quant_note/liquidity_factor_portfolios/txt_file/'):
        """
        Aggregate weight for each holidays.
        Weight for each counntry in file excel folder + "Market holidays.xlsx"
    
        Args:
            
            filename (str); file holidays csv saved in
                folder + "stoxx hoidays/"
                with 3 columns: 'holidays','country_code','exchange_name'
    
        Returns:
            flag_foliday: series of holidays in index, weight in values
        """
        
        df_stoxx23 = pd.read_csv(folder + subfolder + filename, sep =';').drop_duplicates()
        df_stoxx23.columns = ['holidays','country_code','exchange_name']
        df_stoxx23['flag_holidays'] = 1
        
        df_ct_wt = pd.read_excel(folder + 'Market holidays.xlsx', sheet_name= 'Country weights', index_col = 0)
        df_ct_wt = df_ct_wt[df_ct_wt.index.notna()].fillna(0)
        
        df_stoxx23 = df_stoxx23[df_stoxx23.country_code.isin(df_ct_wt.index)]
        
        df_holiday = df_stoxx23.set_index(['holidays', 'country_code'])['flag_holidays'].unstack()
        
        df_wt = df_ct_wt[['SXXP']].T
        df_wt.index = [df_holiday.index[0]]
        df_wt = df_wt.reindex(df_holiday.index, method = 'ffill')[df_holiday.columns]
        
        df_wt_hol = (df_holiday * df_wt).replace({0:np.nan}).dropna(how = 'all', axis = 0).dropna(how = 'all', axis = 1)
        df_wt_hol.index = pd.to_datetime(df_wt_hol.index, format = '%d.%m.%Y')
        df_wt_hol = df_wt_hol.sort_index()
        flag_foliday = df_wt_hol.sum(axis = 1).sort_values(ascending = False)
        print('Check variable: "flag_foliday" to define threshold for holidays:\%s' %flag_foliday)
        return flag_foliday
    
    
    def load_existing_date(self, sql_table_name):
        """
        load exisiting holidays/MSCI rebalancing
        """
        dict_table = {'quant_holidays':'W:/Global_Research/Quant_research/projets/quant_note/liquidity_factor_portfolios/txt_file/holidays.txt',
                      'msci_rebalancing':'W:/Global_Research/Quant_research/projets/quant_note/liquidity_factor_portfolios/txt_file/annual_rebal_MSCI.txt'
            }
        
        holiday_file = dict_table[sql_table_name]# annual_rebal_MSCI/holidays
        
        df_date = pd.read_csv(holiday_file, converters={'rebalancing': str, 'holidays':str})
        df_date.columns = ['date']
        return df_date
    
    def import_to_sql(self, df_date, sql_table_name):
        """
        import self.df_db to QUANT_work..[sql_table_name]
    
        Args:
            df_date (DataFrame, list): DataFrame/list of date (yyyymmdd) to import
            sql_table_name (str): table name to add dates
        """
        df_tmp = self.load_existing_date(sql_table_name)
        if isinstance(df_date, (list, np.ndarray, pd.Index)):
            df_date = pd.to_datetime(df_date)
            df_date = pd.DataFrame(data = df_date, columns = ['date'])
        df_date = pd.concat((df_date, df_tmp)).drop_duplicates()
        try:
            df_existing = pd.read_sql("select * from QUANT_work..%s"%sql_table_name, con_mis)
            df_date = pd.concat((df_existing, df_date)).drop_duplicates()
            pd.read_sql("drop table QUANT_work..%s" %sql_table_name, con = con_mis)
        except:
            print('Creating table: QUANT_work..%s'%sql_table_name)
    
        df_date.date = pd.to_datetime(df_date.date)
        df_date = df_date.sort_values(by ='date').drop_duplicates()
        con_mis.execute('USE QUANT_work')
        pd.bulk_copy(con_mis, sql_table_name, df_date)
        print('Data are imported to QUANT_work..%s' %sql_table_name)
        return None


