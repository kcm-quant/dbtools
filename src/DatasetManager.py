# -*- coding: utf-8 -*-
"""
Created on Tue Jan  2 16:22:38 2024

@author: krulik
"""


import numpy as np
import dbtools.src.get_repository as rep
import json
import os
from dbtools.src.db_connexion import SqlConnector
connector = SqlConnector()
con_mis = connector.connection()
import pandas as pd
from pandas.tseries.offsets import BMonthEnd
from datetime import datetime, timedelta

quant_holidays = pd.Index(pd.read_sql('select * from QUANT_work..quant_holidays', con_mis).date)
class DatasetManager:

    def __init__(self, config_file_name = 'datasets_manager.json'):
        """
        initialises the class by reading configurations.

        Args:
            config_file_name (str): The name of the configuration file, common to all datasets.

        Returns:
            self.config (dict): configuration within the object

            """
        script_dir = os.path.dirname(os.path.abspath(__file__))
        parent_dir = os.path.dirname(script_dir)
        config_dir = os.path.join(parent_dir, 'config')
        config_path = os.path.join(config_dir, config_file_name)
        self.config_path = config_path
        self.config = self.load_config()

    def load_config(self):
        """
        initialises the class by reading configurations.

        Args:
            self: object containing at this point the path to configuration file

        Returns:
            config (dict): configuration dictionary

            """
        with open(self.config_path, 'r') as file:
            config = json.load(file)
        return config

    def get_date_update_due(self, dataset):
        """
        Determines the dates when the next updates are due for each table in a given dataset based on their update frequencies.

        Args:
            dataset (str): The name of the dataset for which the update dates are to be determined.

        Returns:
            dict: A dictionary with table names as keys and strings representing the dates when updates are due, 
            formatted as 'YYYY-MM-DD', as values. Returns None for each table with 'ADHOC' update frequency.

        Raises:
            ValueError: If the table_update_frequency value for any table is invalid.
        
        Tasks:
            1- Extracts the update frequency for each table from config and organises the due date computation based on the 'table_update_frequency' config.
            2- calculates the logic of recnstructing the 'table_update_frequency" = EOM.
            3- Formats the output into a dictionary with keys being the table names and date conversion to string
 
        Usage example:
            ---------------------------------------------------------------
            your_instance = DatasetManager()
            dataset_name = 'FAMILY_BUSINESS'
            update_dates = your_instance.get_date_update_due(dataset_name)

            """

        dataset_dict = self.config[dataset]
        update_dates = {}

        # Iterate over each table in the dataset
        for table_key, table_info in dataset_dict.items():
            if table_key.startswith("table_"):
                table_update_frequency = table_info.get("table_update_frequency", None)

                # Determine the update date based on the frequency
                if table_update_frequency == 'EOM':
                    # Calculate the last business day of the previous month
                    last_month = datetime.today().month - 1 or 12
                    year = datetime.today().year - (1 if last_month == 12 else 0)
                    date = last_weekday_of_month(year, last_month)
                elif table_update_frequency == 'EBDOM':
                    # Calculate the last business day of the previous month
                    last_month = datetime.today().month - 1 or 12
                    year = datetime.today().year - (1 if last_month == 12 else 0)
                    date = last_business_day_of_month(year, last_month)
                elif table_update_frequency == 'ADHOC':
                    # Set None for 'ADHOC' frequency
                    date = None
                elif table_update_frequency == 'QKECH':
                    # Determine the closest past quarter business day
                    date = closest_past_quarter_business_day(datetime.today().year, datetime.today().month)
                elif table_update_frequency == 'EOMp2BD':
                        # Determine the closest past day which is two business days after the previous month end
                        date = closest_eom_plus_two_business_days(datetime.today().year, datetime.today().month)
                else:
                    # Raise an error if the frequency value is not recognized
                    raise ValueError(f"Invalid table_update_frequency value for {table_key}")

                # Format the date and add to the dictionary
                update_dates[table_info.get("table_name", table_key)] = date.strftime("%Y-%m-%d") if date else None

        return update_dates
    
    def check_if_dataset_up_to_date(self, dataset):
        """
        Checks if the entire dataset is up-to-date.

        Args:
            dataset (str): The name of the dataset to check.

        Returns:
            bool: True if the entire dataset is up-to-date, False otherwise.
        
        Tasks:
            1- Loops over the tables belonging to the dataset to extract the due date and transfers it to the method 'check_table_data' which performs the comparison of the due dates with the latest avaibale date.
            2- Handles explicitly the case where the the expected date is None (like, for ex. for the Family Business)
            
        Usage example:
            ---------------------------------------------------------------
            your_instance = DatasetManager()
            dataset_name = 'FAMILY_BUSINESS'
            update_status = your_instance.check_if_dataset_up_to_date(dataset_name)

        """
        # Get the expected update dates for each table in the dataset
        expected_update_dates = self.get_date_update_due(dataset)
        # Dictionary to store the update status of each table
        update_status = {}

        for table_key, table_info  in self.config[dataset].items():
            if table_key.startswith("table_"):
                table_name = table_info.get("table_name", "")
                print(f'--------{table_name}------------')
                expected_date = expected_update_dates.get(table_name)
                if expected_date is not None:
                    # Check if each table in the dataset is up-to-date
                    is_up_to_date = self.check_table_data(table_name, table_info, expected_date)
                    update_status[table_name] = is_up_to_date
                else:
                    update_status[table_name] = True  # or handle as appropriate if no expected date is found

        return update_status

    def check_table_data(self, table_name, table_info, expected_update_date):
        """
        Checks if the data in a single table is not inferior to the expected update date.

        Args:
            table_key (str): The key of the table in the config.
            table_info (dict): The metadata dictionary for the table.
            expected_update_date (str): The expected update date in 'DD-MM-YYYY' format.

        Returns:
            bool: True if the table is up-to-date, False otherwise.
            
        Tasks:
            1- Constructs perimeter list based on the table configs.
            2- Creates and runs an sql query to extract max date for the perimeter defined above
            3- Ensures conversion to of both date (last and expected) to datetime format for proper comparison
            4- Returns comparison result
            
        Usage example:
            ---------------------------------------------------------------
            your_instance = DatasetManager()
            dataset_name = 'FAMILY_BUSINESS'
            table_name = 'INDEX_COMPOSITION'
            table_info = your_instance.config[dataset_name].get(f"table_{table_name}", {})
            your_instance.check_table_data(table_name, table_info, expected_date)
        """
        # Extract table metadata
        table_name = table_info.get("table_name")
        table_perimeter_field = table_info.get("table_perimeter_field")
        table_perimeter = table_info.get("table_perimeter")
        table_perimeter_type = table_info.get("table_perimeter_type","None")
        table_date_field = table_info.get("table_date_field","DATE")
        
        if table_perimeter_type == "index_static": 
            table_perimeter_list  = rep.get_index_comp(table_perimeter, start_date = expected_update_date, end_date=expected_update_date).columns.to_list()
        elif table_perimeter_type== "KC_plus_index":
            table_perimeter_list = rep.get_kc_plus_index_comp(table_perimeter, start_date = expected_update_date, end_date=expected_update_date).columns.to_list()
        elif table_perimeter_type == "fixed":
            table_perimeter_list = table_perimeter   
            
        perimeter_values_str = ', '.join([f"'{value}'" if isinstance(value, str) else str(value) for value in table_perimeter_list])
        # Formulate your SQL query here based on the table metadata
        sql_query = f"SELECT MAX({table_date_field}) FROM {table_name} WHERE {table_perimeter_field} IN ({perimeter_values_str});"
        result = pd.read_sql(sql_query, con_mis)
        if result.empty or result.iloc[0, 0] is None:
            return False
        last_available_date = result.iloc[0, 0]
        #print(f"Before calling .date(), type: {type(result.iloc[0, 0])}, value: {result.iloc[0, 0]} in table {table_name}")
        if isinstance(last_available_date, pd.Timestamp):
            # If the result is a pandas Timestamp, convert to datetime.date
            last_available_date = last_available_date.date()
        #last_available_date = result.iloc[0, 0].date()  # The problematic line
        print(f'Last available date is: {last_available_date}')
        expected_update_date_dt = datetime.strptime(expected_update_date, '%Y-%m-%d').date()
        #print(f'Last available date is {last_available_date}, type is {type(last_available_date)}')
        print(f'Expected date is: {expected_update_date_dt}')
        return last_available_date>= expected_update_date_dt
    
    def fetch_client_output(self, dataset, begin_date, end_date):
        """
        Fetches the dataset extract in the format suitable to share with clients: pivoted with renamed indicators as columns, 
        added code identifiers.

        Args:
            dataset (str): The name of the dataset.
            begin_date (str): Begin date of the data extract in 'DD-MM-YYYY' format.
            end_date (str): Begin date of the data extract in 'DD-MM-YYYY' format.

        Returns:
            df (dataframe): data extract pivoted as described above.
            
        Tasks:
            1- Fetches the dataset without transformations, based on the value of the "client_output_table" key of the dataset.
            2- pivotes the dataset based on indicator_field value of the table
            3- adds object identifiers (stock ids or index ids)
            4- applies overrides if defined in config (like aubstituting the dataset values by ones , which we might want for the 
               index component dataset)
            
        Usage example:
            ---------------------------------------------------------------
            your_instance = DatasetManager()
            dataset_name = 'BETA_PROFILES_KECH'
            begin_date = '2024-01-02'
            end_date= '2024-01-30'
            your_instance.fetch_client_output(dataset_name, begin_date, end_date)
        """
        # Extract table extract
        table_info =  self.config[dataset][self.config[dataset]["client_output_table"]]
        df = self.fetch_table_extract(table_info, begin_date,end_date)
        data_field = table_info.get("table_fields")
        if self.config[dataset]["client_output_override"]=="ones":
            data_field = table_info.get("table_fields")
            df.loc[df[data_field].notnull(), data_field] = 1  # Fill NaN with 1 and assign back


        # Pivot
        object_id = table_info.get("table_object_field")
        indicator_id = table_info.get("table_indicator_field")
        date_field = table_info.get("table_date_field")
        pivot_df = df.pivot_table(index=[date_field, object_id], columns=indicator_id, values=data_field).reset_index()
        
        # Rename indicator columns with readable names
        data_dict_table_name = table_info.get("table_data_dictionary")
        sql_query = f"SELECT * FROM {data_dict_table_name};"
        mapping_df = pd.read_sql(sql_query, con_mis)
        # Create a dictionary for column renaming
        readable_name = table_info.get("table_data_dictionary_readable_name")
        column_mapping = dict(zip(mapping_df[indicator_id], mapping_df[readable_name]))
        # Rename the columns
        pivot_df.rename(columns=column_mapping, inplace=True)
        date_field = table_info.get("table_date_field")
        pivot_df.rename(columns={date_field: 'date'}, inplace=True) # standard output name for clients

        # Add stock identifiers
        if object_id =="security_id":
            renamed_df = add_stock_identifiers(pivot_df, sedol=True)
        elif object_id =="index_id":
            renamed_df = add_index_identifiers(pivot_df)
        return renamed_df
    
        
        
    
    def fetch_table_extract(self,table_info, begin_date,end_date):
        """
        Fetches the dataset extract as is based on the table info. Attention, it ill not return all the table, but the part that belongs to 
        the given dataset (it is encoded in the perimeter field of table_info)

        Args:
            table_info (dict): dictionary of table info, can be extracted from the configs based on table key
            begin_date (str): Begin date of the data extract in 'DD-MM-YYYY' format.
            end_date (str): Begin date of the data extract in 'DD-MM-YYYY' format.

        Returns:
            result (dataframe): data extract as is from the table.
            
        Tasks:
            1- Fetches the dataset without transformations, based on the value of the table_info dict.
            
        Usage example:
            ---------------------------------------------------------------
            your_instance = DatasetManager()
            dataset_name = 'BETA_PROFILES_KECH'
            begin_date = '2024-01-02'
            end_date= '2024-01-30'
            table_info =  your_instance.config["table_2"]
            your_instance.fetch_table_extract(table_info, begin_date, end_date)
        """
        table_name = table_info.get("table_name")
        table_perimeter_field = table_info.get("table_perimeter_field")
        table_perimeter = table_info.get("table_perimeter")
        table_perimeter_type = table_info.get("table_perimeter_type","None")
        table_date_field = table_info.get("table_date_field","DATE")
        
        if table_perimeter_type == "index_static": # fetch the list of security_id for an index
            table_perimeter_list  = rep.get_index_comp(table_perimeter, start_date = begin_date, end_date=end_date).columns.to_list()
        elif table_perimeter_type== "KC_plus_index":
            table_perimeter_list = rep.get_kc_plus_index_comp(table_perimeter, start_date = begin_date, end_date=end_date).columns.to_list()    
        elif table_perimeter_type == "fixed":
            table_perimeter_list = table_perimeter
        perimeter_values_str = ', '.join([f"'{value}'" if isinstance(value, str) else str(value) for value in table_perimeter_list])
        
        # Formulate your SQL query here based on the table metadata
        sql_query = f"SELECT * FROM {table_name} WHERE {table_perimeter_field} IN ({perimeter_values_str}) AND {table_date_field} >= '{begin_date}' AND {table_date_field} <= '{end_date}';"
        result = pd.read_sql(sql_query, con_mis)
        return result
    def get_date_range(self, table_name, default_start_date = '2021-01-01'):
        """IGNORE FOR CODE REVIEW, A POSSIBLE NEW METHOD, TBD IF NEEDED """
        query = f"SELECT MAX(DATE) FROM {table_name}"
        result = pd.read_sql(query, con_mis)
        # If the table is empty, start from a default date, else from the last available date
        start_date = result[0] if result[0] is not None else default_start_date
        #end_date = datetime.datetime.now().strftime('%Y-%m-%d')# and using the current date as the end date
        return start_date

    def produce_values(self, table_name, start_date, end_date):
        """IGNORE FOR CODE REVIEW, A POSSIBLE NEW METHOD, TBD IF NEEDED """
        # Logic to generate or calculate the values to be inserted/updated
        # This could involve complex calculations or simply fetching data
        pass

    def append_values(self, table_name, values):
        """IGNORE FOR CODE REVIEW, A POSSIBLE NEW METHOD, TBD IF NEEDED """
        # Logic to append the new values to the table
        # This might involve inserting or updating rows in the database
        pass

    def update_table(self, table_name):
        """IGNORE FOR CODE REVIEW, A POSSIBLE NEW METHOD, TBD IF NEEDED """
        if table_name in self.config:
            start_date, end_date = self.get_date_range(table_name)
            values = self.produce_values(table_name, start_date, end_date)
            self.append_values(table_name, values)
        else:
            print(f"Table {table_name} is not in the configuration or is not your responsibility.")

    def run_update_for_table(self, table_name):
        """IGNORE FOR CODE REVIEW, A POSSIBLE NEW METHOD, TBD IF NEEDED """
        # This method allows team members to update only their tables
        self.update_table(table_name)
    def get_start_date(self):
        """IGNORE FOR CODE REVIEW, A POSSIBLE NEW METHOD, TBD IF NEEDED """
        
        req = """select a.index_id, a.DATE, a.value
                from QUANT_work..INDEX_LEVEL a inner join (
                select index_id, max(DATE) max_date
                from QUANT_work..INDEX_LEVEL
                group by index_id) b
                on a.index_id=b.index_id and a.DATE = b.max_date
                order by a.index_id
                """
        df_index_data = pd.read_sql(req, con_mis)
        df_index_data = df_index_data.set_index('index_id')
        
        
        pass
    def create_macro(self):
        """IGNORE FOR CODE REVIEW, A POSSIBLE NEW METHOD, TBD IF NEEDED """
        pass
    def build_macro(self):
        """IGNORE FOR CODE REVIEW, A POSSIBLE NEW METHOD, TBD IF NEEDED """
        pass
    def get_data(self):
        """IGNORE FOR CODE REVIEW, A POSSIBLE NEW METHOD, TBD IF NEEDED """
        pass


def add_stock_identifiers(df_ind, sedol=True):
    """
    Replaces the security_id column of the table with the set of columns represented stock identifiers . The current list is fixed to:
        ['ISIN', 'TRADING_CCY', 'PRIMARY_MIC', 'SEDOL', 'SECURITY_NAME', 'TICKER_BLOOMBERG']
    NB. The codes are point-in -time, meaning that the replacement is done separately for each (security_id, date) pair
    
    Args:
        df_ind (dataframe): pivoted dataframe of dataset client output containing columns 'date' , 'security_id' and others

    Returns:
        df_ind_renamed (dataframe): same dataset extract, with security_id column eliminated and replaced by the codes as above
        
    Tasks:
        1- Fetches the codes based on security_ids.
        2- creates output in the exact order of codes as above
        
    Usage example:
        ---------------------------------------------------------------
        your_instance = DatasetManager()
        dataset_name = 'BETA_PROFILES_KECH'
        begin_date = '2024-01-02'
        end_date= '2024-01-30'
        table_info =  your_instance.config["table_2"]
        your_instance.fetch_table_extract(table_info, begin_date, end_date)
    """
    if sedol:
        #ref_cols = ['ISIN', 'TRADING_CCY', 'PRIMARY_MIC', 'SEDOL', 'SECURITY_NAME','TICKER_BLOOMBERG']
        codes = ['isin','ccy','prim_td_id','sedol','secname','bbg']
    else:
        #ref_cols = ['ISIN', 'TRADING_CCY', 'PRIMARY_MIC', 'SECURITY_NAME','TICKER_BLOOMBERG']
        codes = ['isin','ccy','prim_td_id','secname','bbg']
        df_ind['SEDOL'] = np.nan
    
    df_sec_dt = df_ind.drop_duplicates(subset=['date', 'security_id'])
    df_sec_dt = df_sec_dt.reset_index()
    
    dt = df_sec_dt.date.unique()
    df_ref = pd.DataFrame()
    for d in dt:
        dt_df = pd.DataFrame()
        for c in codes:
            df_sec = df_sec_dt[df_sec_dt.date==d]
            vect = rep.mapping_from_security(security_id=df_sec['security_id'], code=c, date_ref=d)
            dt_df = pd.concat([dt_df, vect.to_frame()],axis=1)
        dt_df['date']=d
        df_ref = pd.concat([df_ref, dt_df])
    df_ref = df_ref.reset_index()
    df_ref.rename(columns={'index':'security_id', 'PRIM_TD_ID':'trading_destination_id'}, inplace=True)
    df_ind = df_ind.merge(df_ref, on=['date','security_id'],how='left')
    
    EQ = pd.read_sql("""select * from KGR..EXCHANGE_QUANT""", con_mis)
    df_ind = df_ind.merge(EQ[['trading_destination_id','OPERATING_MIC']], on='trading_destination_id', how='left')
    
    df_ind.rename(columns={'CCY':'TRADING_CCY',
                           'OPERATING_MIC':'PRIMARY_MIC',
                           'SECNAME':'SECURITY_NAME',
                           'reference':'TICKER_BLOOMBERG'}, inplace=True)
    

    # re-order columns with DATE and stock identifiers first
    first_cols = ['date', 'ISIN', 'TRADING_CCY', 'PRIMARY_MIC', 'SEDOL', 'SECURITY_NAME', 'TICKER_BLOOMBERG']
    other_cols = df_ind.columns[~df_ind.columns.isin(first_cols)].tolist()
    df_ind_renamed = df_ind[first_cols+other_cols]
    df_ind_renamed_dropped = df_ind_renamed.drop(columns=['security_id', 'trading_destination_id'])
    
    return df_ind_renamed_dropped

def add_index_identifiers(df_ind):
    """
    Replaces the index_id column of the table with the set of columns represented index identifiers . The current list is fixed to:
        ['index_name', 'index_ticker']
    NB. The codes are NOT point-in-time, the index referential is static
    
    Args:
        df_ind (dataframe): pivoted dataframe of dataset client output containing columns 'date' , 'index_id' and others

    Returns:
        df_ind_renamed (dataframe): same dataset extract, with index_id column eliminated and replaced by the codes as above
        
    Tasks:
        1- Fetches the codes based on index_ids.
        2- creates output in the exact order of codes as above
        
    Usage example:
        ---------------------------------------------------------------
        your_instance = DatasetManager()
        dataset_name = 'BETA_PROFILES_KECH'
        begin_date = '2024-01-02'
        end_date= '2024-01-30'
        table_info =  your_instance.config["table_2"]
        your_instance.fetch_table_extract(table_info, begin_date, end_date)
    """

    mapping_df = pd.read_sql("""select * from QUANT_work..INDEX_REFERENTIAL""", con_mis)
    merged_df = pd.merge(df_ind, mapping_df[['index_id', 'name', 'ticker']], on='index_id', how='left')

    # Drop the 'index_id' column
    final_df = merged_df.drop(columns=['index_id'])

    final_df.rename(columns={'name':'index_name',
                           'ticker':'index_ticker'}, inplace=True)
    

    # re-order columns with date and index identifiers first
    first_cols = ['date', 'index_name', 'index_ticker']
    other_cols = final_df.columns[~final_df.columns.isin(first_cols)].tolist()
    final_df = final_df[first_cols+other_cols]
    
    return final_df

def last_business_day_of_month(year, month):
    """
    Returns the last business day of the given year and month.
    N.B. cannot handle holidays for the moment, add in future upgrades.

    Args:
    year (int): The year
    month (int): The month

    Returns:
    datetime.date: The last business day of the specified month and year.
    
    Tasks:
        1- Constructs the last BD from month and year
        
        
    Usage example:
        ---------------------------------------------------------------
        last_business_day_of_month(2024, 12)
    """
    # Find the end of the previous month
    first_day_current_month = datetime(year, month, 1)
    last_business_day = pd.to_datetime(first_day_current_month + BMonthEnd(1)).date()
    
    bdates_range = pd.bdate_range(end = last_business_day, periods=5)
    # Delete holidays
    bdates_range = bdates_range.difference(quant_holidays)
    # Find two business days after the previous month's end
    last_business_day = bdates_range[-1]

    return last_business_day

def last_weekday_of_month(year, month):
    """
    Returns the last week day of the given year and month.
    N.B. cannot handle holidays for the moment, add in future upgrades.

    Args:
    year (int): The year
    month (int): The month

    Returns:
    datetime.date: The last business day of the specified month and year.
    
    Tasks:
        1- Constructs the last BD from month and year
        
        
    Usage example:
        ---------------------------------------------------------------
        last_weekday_of_month(2025, 11)
    """
    # Create a datetime object for the first day of the given month
    date = pd.Timestamp(year=year, month=month, day=1)

    # Get the last business day of the month
    last_business_day = pd.to_datetime(date + BMonthEnd(1)).date()

    return last_business_day

def closest_past_quarter_business_day(year, month):
    """
    Returns the last business day of the most recent past quarter month (February, May, August, November).
    N.B. cannot handle holidays for the moment, add in future upgrades.
    
    Args:
    year (int): The year
    month (int): The month

    Returns:
    datetime.date: The last business day of the closest past quarter month.
    
    Tasks:
        1- Constructs the last BD of the most recent past quarter
        
        
    Usage example:
        ---------------------------------------------------------------
        closest_past_quarter_business_day(2023, 12)
    """
    quarter_months = [2, 5, 8, 11]

    # Adjust the logic to ensure it selects the past quarter month correctly
    if month in quarter_months:
        # If the current month is a quarter month, exclude it from the selection
        past_quarter_months = [m for m in quarter_months if m < month]
    else:
        # Otherwise, consider all quarter months up to and including the current month
        past_quarter_months = [m for m in quarter_months if m <= month]
    
    if not past_quarter_months:
        # If no quarter months in or before this month, go to last quarter of the previous year
        quarter_month = quarter_months[-1]
        year -= 1
    else:
        # Use the latest quarter month in or before this month
        quarter_month = max(past_quarter_months)

    # Get the last business day of the identified quarter month
    date = pd.Timestamp(year=year, month=quarter_month, day=1)
    last_business_day = pd.to_datetime(date + BMonthEnd(1)).date()
    return last_business_day



def closest_eom_plus_two_business_days(year, month):
    """
    Returns the day that is the second business day of month.
    N.B. cannot handle holidays for the moment, add in future upgrades.
    
    Args:
    year (int): The year
    month (int): The month

    Returns:
    datetime.date: The date two business days after the end of month.
    
    Tasks:
        1- Constructs the date that is two business days after the last working day of the given year, given month. 
        2- Specifies explicitly two business days (periods = 3)
        
        
    Usage example:
        ---------------------------------------------------------------
        closest_eom_plus_two_business_days(2023, 12)
    """
    
    # Find the end of the previous month
    first_day_current_month = datetime(year, month, 1)
    bdates_range = pd.bdate_range(first_day_current_month, periods=5)
    # Delete holidays
    bdates_range = bdates_range.difference(quant_holidays)
    # Find two business days after the previous month's end
    two_business_days_later = bdates_range[1]
    return two_business_days_later