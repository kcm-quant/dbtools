# -*- coding: utf-8 -*-
"""
Created on Thu Jan 25 14:55:19 2024

@author: ssureau
"""

import pandas as pd
import os
import json
from datetime import date

from dbtools.src.db_connexion import SqlConnector
connector = SqlConnector()

import xml.etree.ElementTree as ET

from sqlalchemy import  Integer, String, Date
import dbtools.src.DatabaseManager as db


#%% CONFIG & COMMON VARIABLES
def _load_config(mode):
    """Load configurations from 'account_ref.json' file.
    
    Task 1: returns config content for corresponding mode. Modes are keys of the config file account_ref.json.
    
    Args:
        mode (String): id_mapping, legacy, dynamic or static
    
    Returns:
        config_data (Dictionary): config content
            
    Use example: 
        import dbtools.src.get_account_ref as acc
        config_data = acc._load_config('id_mapping')
    """
    config_folder_path = os.path.join(os.path.dirname(__file__), '..', 'config')
    config_file_name = "account_ref.json"
    config_file_path = os.path.join(config_folder_path, config_file_name)
    with open(config_file_path, 'r') as config_file:
        config_data = json.load(config_file)
        config_data = config_data[mode]
 
    return config_data

dict_mapp = _load_config('id_mapping')
type_mapping_groups = _load_config('type_mapping_groups')
legacy_config = _load_config('legacy')
dyn_config = _load_config('dynamic')
static_config = _load_config('static')
hybrid_config = _load_config('hybrid')

con_legacy = connector.connection(server=legacy_config['server']) 
con_dyn = connector.connection(server=dyn_config['server']) 
con_static = connector.connection(server=static_config['server']) 
con_hybrid = connector.connection(server=hybrid_config['server']) 

#%% APPLY TO DATA USING OPTION LEGACY OR DYNAMIC
def add_account_ref_to_data(df_data, mode='legacy', account_id_col='account_id', date_col='date',
                            add_dyn_ref_to_cache=False, group_types=True, add_client_country = False):
    """ Adds client_name, client_type and client_region corresponding to client id for given dates
    
    Task 1: calls the dedicated function to add client name, type and region according selected mode 
    
    Args:
        df_data (DataFrame): data
        mode (String): 'legacy' (default), 'dynamic' or 'hybrid'
        account_id_col (String): 'account_id' (default) or any other column name for the crystal id for clients
        date_col (String): 'date' (default) or any other column name for dates
        add_dyn_ref_to_cache (Boolean): False by default, True to save a pickle used for future calls (until pickle is deleted)
        group_types (Boolean): True (default) for applying grouping of types based on config / False

    Returns:
        df (DataFrame): same as df_data, with additional columns to indicate client_type and client_region
            in 'dynamic' mode, two other columns are added: begin_date and end_date refering to the validity of client_type and client_region info

    Use example: 
        import pandas as pd
        import dbtools.src.get_account_ref as acc
        data = {'account_id': [1, 2, 3, 4, 5],
            'date': ['2024-01-01', '2024-01-02', '2024-01-03', '2024-01-04', '2024-01-05'],
            'value': [100, 200, 300, 400, 500]}
        df_data = pd.DataFrame(data)
        df_result_legacy = acc.add_account_ref_to_data(df_data)        
        df_result_dynamic = acc.add_account_ref_to_data(df_data, mode='dynamic')
        df_result_hybrid = acc.add_account_ref_to_data(df_data, mode='hybrid')
    """
    if mode=='legacy':
        df = add_account_classification_to_data(df_data, account_id_col=account_id_col, date_col=date_col)
    elif mode=='static':
        df = add_static_ref_to_data(df_data, account_id_col=account_id_col, date_col=date_col, group_types=group_types)
    elif mode=='dynamic':
        df = add_dyn_ref_to_data(df_data, account_id_col=account_id_col, date_col=date_col,
                                 add_dyn_ref_to_cache=add_dyn_ref_to_cache, group_types=group_types)
    elif mode=='hybrid':
        df = add_hyb_ref_to_data(df_data, account_id_col=account_id_col, date_col=date_col, group_types=group_types)
    else: 
        raise ValueError("Mode must be one of ['legacy', 'static', 'dynamic', 'hybrid']")
    if not add_client_country:
        df = df[df.columns.difference(['IdCountry'], sort = False)]
    return df

#%% 1. LEGACY / STATIC ACCOUNT REFERENTIAL    
def get_static_crystal_info():
    """Get static crystal name and ids per client (ranking, type, country)
    
    Task 1: reads crystal info using table and column names from static config
        
    Args: 
        None
        
    Returns: 
        df_static (DataFrame): columns from config e.g. 'account_id','client_name','IdRanking','IdTypeAccount','IdCountry'
            
    Use example: 
        import dbtools.src.get_account_ref as acc
        df_crystal = acc.get_static_crystal_info()
    """
    df_crystal = pd.read_sql("""select %s as account_id,%s as client_name,%s,%s,%s from %s..[%s]"""%(
        static_config['sql_id_col'],static_config['sql_name_col'],static_config['ranking']['sql_col'],static_config['type']['sql_col'],static_config['country']['sql_col'],
        static_config['sql_base'],static_config['sql_table']), con_static)
    
    return df_crystal

def get_account_classification():
    """Get account classification in KGR
    
    Task 1: retrieve data from static sql view, based on column names in config
    Task 2: assigns column names account_id, client_name, client_type, client_region
    
    Args: 
        None
    
    Returns:
        df_acc (DataFrame): containing static (latest) correspondance between account_id, name, type, region

    Use example: 
        import dbtools.src.get_account_ref as acc
        df_acc = acc.get_account_classification()
    """
    df_acc = pd.read_sql("""select %s account_id, %s client_name, 
                         %s client_type, %s client_region 
                         from %s..%s"""%(legacy_config['sql_id_col'],legacy_config['sql_name_col'],
                         legacy_config['sql_type_col'],legacy_config['sql_region_col'],
                         legacy_config['sql_base'],legacy_config['sql_table']), con_legacy)
    
    df_static = get_static_crystal_info()
    
    df_acc = df_static.merge(df_acc, on=['account_id', 'client_name'], how='inner')
    
    return df_acc

def add_account_classification_to_data(df_data, account_id_col='account_id', date_col='date'):
    """ Adds client_name, client_type and client_region corresponding to client id for given dates
        Using static (latest) state of client referential and mapping from SQL view
    
    Task 1: selects only 'client_name','client_type','client_region' from referential
    Task 2: attributes referential to data
    Task 3: applies patch for specific clients present in config file
    
    Args:
        df_data (DataFrame): data
        account_id_col (String): 'account_id' (default) or any other column name for the crystal id for clients
        date_col (String): 'date' (default) or any other column name for dates
    
    Returns:
        df_init (DataFrame): same as df_data, with additional columns to indicate client_name, client_type and client_region
 
    Use example: 
        import pandas as pd
        import dbtools.src.get_account_ref as acc
        data = {'account_id': [1, 2, 3, 4, 5],
            'date': ['2024-01-01', '2024-01-02', '2024-01-03', '2024-01-04', '2024-01-05'],
            'value': [100, 200, 300, 400, 500]}
        df_data = pd.DataFrame(data)
        df_result = acc.add_account_classification_to_data(df_data)        
    """
    df_acc = get_account_classification()
    
    df_init = df_data.merge(df_acc[['account_id','client_name','client_type','client_region']], on = 'account_id')
    
    # patch for 1 client which changed type from Hedge funds to Funds as of October 2023 --> moved into json config
    # df_init.loc[(df_init['account_id']==2146856223)&(df_init['date']<='2023-09-28'), 'client_type']='Hedge funds'
    acc_patch = legacy_config['account_patch']
    for account_id_str, patch_data in acc_patch.items():
        account_id = int(account_id_str)  # Convert account_id from string to integer
        if 'account_id' in df_init.columns and account_id in df_init['account_id'].values:
            from_date = patch_data.get('from')
            to_date = patch_data.get('to')
            client_type = patch_data.get('client_type')
    
            mask = (df_init['account_id'] == account_id) & (df_init['date'] >= from_date) & (df_init['date'] <= to_date)
            df_init.loc[mask, 'client_type'] = client_type
        #else:
            #print(f"Account ID {account_id} not found or 'account_id' column not present in the DataFrame.")
    
    return df_init


def get_static_ref(group_types=True):
    """Get account ref based on static crystal table and mappings in config
    
    Task 1: retrieve data from static crystal table
    Task 2: applies the mapping from config
    
    Args: 
        group_types (Boolean): True (default) for applying grouping of types based on config / False
    
    Returns:
        df_static (DataFrame): containing static (latest) correspondance between account_id, name, type, region

    Use example: 
        import dbtools.src.get_account_ref as acc
        df_acc = acc.get_static_ref()
    """
    df_static = get_static_crystal_info()
    
    df_static = apply_type_country_mappings(df_static, group_types=group_types)
    
    return df_static

def add_static_ref_to_data(df_data, account_id_col='account_id', date_col='date', group_types=True):
    """ Adds client_name, client_type and client_region corresponding to client id for given dates
        Using static (latest) state of client referential and mapping from config
    
    Task 1: selects only 'client_name','client_type','client_region' from referential
    Task 2: attributes referential to data
    Task 3: applies patch for specific clients present in config file
    
    Args:
        df_data (DataFrame): data
        account_id_col (String): 'account_id' (default) or any other column name for the crystal id for clients
        date_col (String): 'date' (default) or any other column name for dates
        group_types (Boolean): True (default) for applying grouping of types based on config / False
    
    Returns:
        df_init (DataFrame): same as df_data, with additional columns to indicate client_name, client_type and client_region
 
    Use example: 
        import pandas as pd
        import dbtools.src.get_account_ref as acc
        data = {'account_id': [1, 2, 3, 4, 5],
            'date': ['2024-01-01', '2024-01-02', '2024-01-03', '2024-01-04', '2024-01-05'],
            'value': [100, 200, 300, 400, 500]}
        df_data = pd.DataFrame(data)
        df_result = acc.add_account_classification_to_data(df_data)        
    """
    df_acc = get_static_ref(group_types=group_types)
    
    df_init = df_data.merge(df_acc[['account_id','client_name','client_type','client_region', 'IdCountry']], on = 'account_id')
    
    # patch for 1 client which changed type from Hedge funds to Funds as of October 2023 --> moved into json config
    # df_init.loc[(df_init['account_id']==2146856223)&(df_init['date']<='2023-09-28'), 'client_type']='Hedge funds'
    acc_patch = legacy_config['account_patch']
    for account_id_str, patch_data in acc_patch.items():
        account_id = int(account_id_str)  # Convert account_id from string to integer
        if 'account_id' in df_init.columns and account_id in df_init['account_id'].values:
            from_date = patch_data.get('from')
            to_date = patch_data.get('to')
            client_type = patch_data.get('client_type')
    
            mask = (df_init['account_id'] == account_id) & (df_init['date'] >= from_date) & (df_init['date'] <= to_date)
            df_init.loc[mask, 'client_type'] = client_type
        #else:
            #print(f"Account ID {account_id} not found or 'account_id' column not present in the DataFrame.")
    
    return df_init


#%% MAPPINGS (CAN BE USED FOR STATIC OR DYNAMIC)
def account_type_by_ranking(row):
    """ Returns the client type corresponding to IdRanking if present in config dictionary
    
    Task 1: reads config dictionary to retrieve quant client type from crystal ranking
    
    Args:
        row: row of a DataFrame containing IdRanking
    
    Returns:
        type_name (String): na if not found in config dictionary

    Use example: 
        import dbtools.src.get_account_ref as acc
        dict_mapp ={"Retail": [11], "Technical": [6]}
        sample_row = {'IdRanking': 2}
        result = acc.account_type_by_ranking(sample_row)
    """
    IdRanking = row[static_config['ranking']['sql_col']]
    ranking_mapping = dict_mapp['ranking'] #already corresponding o Name in KGR..Ranking
    
    for rk_name, rk_data in ranking_mapping.items():
        if (IdRanking in rk_data):
            return rk_name
        
    return None
  
def account_type_by_type(row):
    """Returns the client type corresponding to IdTypeAcc or 'type_id' if present in config dictionary
    
    Task 1: reads config dictionary to retrieve quant client type from crystal id or type
    
    Args: 
        row: row of a DataFrame containing IdTypeAcc and Id
    
    Returns:
        type_name (String): na if not found in config dictionary

    Use example: 
        import dbtools.src.get_account_ref as acc
        sample_row = {'IdTypeAcc': 21,  # Example IdTypeAcc value
            'account_id': 2146852073}  # Example account_id value}
        sample_row = {'IdTypeAcc': 24,  # Example IdTypeAcc value
            'account_id': 2146851781}  # Example account_id value}
        result = acc.account_type_by_type(sample_row)
    """
    
    type_mapping = dict_mapp['type']
    priority_order = dict_mapp['type_mapping_priority']

    IdTypeAcc = row[static_config['type']['sql_col']]
    Id = row['account_id']
    for type_name in priority_order:
        type_data = type_mapping[type_name]
        if (IdTypeAcc in type_data['type_id'] or Id in type_data['account_id']):
            return type_name
        
    return None
  

def account_type_mapping(row):
    """ Returns the client type corresponding to IdRanking (if mapped), then IdTypeAcc or 'type_id' if present in config dictionary
 
    Task 1: starts treatment by ranking mapping
    Task 2: if ranking not mapped, then treats type mapping
    Task 3: affects 'Unknown' if not mapped
    
    Args:
        row: row of a DataFrame containing IdRanking, IdTypeAcc and Id
     
    Returns:
        type_name (String): 'Unknown' if not found in config dictionary

    Use example: 
        import dbtools.src.get_account_ref as acc
        sample_row = {'IdRanking': 11,  # Example IdRanking value
            'IdTypeAcc': 2,  # Example IdTypeAcc value
            'account_id': 2146859679}  # Example account_id value
        result = acc.account_type_mapping(sample_row)
    """
        
    # first treat ranking info
    type_name = account_type_by_ranking(row)
    
    # second treat type info based on type id and specific account_id
    if type_name is None:
        type_name = account_type_by_type(row)
        
    # third affect Unknown to the rest
    if type_name is None:
        type_name = 'Unknown'
    
    return type_name


def account_country_mapping(row):
    """ Returns the client region corresponding to IdCountry if present in config dictionary
    
    Task 1: reads config dictionary to retrieve quant client region from crystal country
    Task 2: affects 'Unknown' if not mapped
    
    Args:
        row: row of a DataFrame containing IdCountry
    
    Returns:
        country_name (String): 'Unknown' if not found in config dictionary

    Use example: 
        import dbtools.src.get_account_ref as acc
        sample_row = {'IdCountry': 'US'}  # Example IdCountry value
        result = acc.account_country_mapping(sample_row)
    """
    IdCountry = row['IdCountry']
    
    country_mapping = dict_mapp['country']

    for country_name, country_data in country_mapping.items():
        if (IdCountry in country_data):
            return country_name
    
    return 'Unknown'
    

def apply_type_country_mappings(df_ref, group_types=True):
    """ add client_type and client_region columns to dyn_ref, based on type and country mapping
        from crystal ids to quant names
    
    Task 1: apply type and country mapping to all rows of dyn_ref
    Task 2: if asked by option group_types, applies type grouping from config dictionary
    
    Args:
        dyn_ref (DataFrame): containing dynamic referential account_id, client_name, IdRanking, IdTypeAcc, IdCountry by begin_date and end_date
        group_types (Boolean): True (default) for applying grouping of types based on config / False

    Returns:
        dyn_ref (DataFrame): dynamic referential with two additional columns client_type, client_region

    Use example: 
        import pandas as pd
        import dbtools.src.get_account_ref as acc
        data_dyn_ref = {
            'account_id': [1, 2, 2],
            'client_name': ['Client1', 'Client2', 'Client2'],
            'IdRanking': [101, 6, 11],
            'IdTypeAcc': [8, 22, 22],
            'IdCountry': ['US', 'CA', 'FR'],
            'begin_date': ['2022-01-01', '2022-01-01', '2022-01-01'],
            'end_date': ['2022-12-31', '2022-12-31', '2022-12-31']}
        dyn_ref = pd.DataFrame(data_dyn_ref)
        final_dyn_ref = acc.apply_type_country_mappings(dyn_ref)
    """
    df_ref['client_type'] = df_ref.apply(account_type_mapping, axis=1)
    df_ref['client_region'] = df_ref.apply(account_country_mapping, axis=1)
    
    # mapping groups if option true 
    if group_types:
        for group, values in type_mapping_groups.items():
            df_ref['client_type'] = df_ref['client_type'].replace(values, group)
    
    return df_ref

#%% 2. DYNAMIC ACCOUNT REFERENTIAL
def get_dynamic_ref(add_to_cache=False, extend_end_date=False, group_types=True):
    """ Constructs complete and readable dynamic referential
    
    Task 1: Checks if referential already present in the local cache (scratch folder), and, if present, returns the cached version 
    Task 2: Retrieved the full list of client account ids for which we must look for changes
    Task 3: coordinates call to functions to request changes and construct dynamic referential
    Task 4: saves dynamic referential as pickle if asked by paramter 'add_to_cache'
    Task 5: replaces the latest values in the column 'end_date' by '22001231' to allow forward use of referential
    
    Args:
        add_to_cache (Boolean): False by default
        extend_end_date (Boolean): False by default, 
            if True, changes max of end date to 31 Dec 2200
    
    Returns:
        dyn_ref (DataFrame): dynamic referential containing client id, name, type, region with begin_date, end_date

    Use example: 
        import dbtools.src.get_account_ref as acc
        dyn_ref = acc.get_dynamic_ref(add_to_cache=True)
    """
    try: 
        cache_filename = get_cache_filename()
        dyn_ref = pd.read_pickle(cache_filename)
        
    except FileNotFoundError:
        df_clients = pd.read_sql("""select distinct %s as account_id from %s..%s"""%(static_config['sql_id_col'], 
                                                                            static_config['sql_base'], static_config['sql_table']), con_static)       
        
        df_ranking, df_type, df_country = get_changes_by_changetype(df_clients)
        df_ranking, df_type, df_country = from_names_to_ids(df_ranking, df_type, df_country)
        df_combined = concat_changes(df_ranking, df_type, df_country)
        dyn_ref = create_dynamic_client_referential(df_combined)
        dyn_ref = apply_type_country_mappings(dyn_ref, group_types=group_types)
        
        if add_to_cache: 
            add_df_to_cache(dyn_ref)
            
    if extend_end_date:
        dyn_ref = apply_extended_end_date(dyn_ref)
        
    return dyn_ref

def get_cache_filename():
    """ Retrieves filename for cache folder
    
    Task 1: determines cache folder as 'scratch'
    Task 2: determines cache name as 'dyn_ref.pkl'
    
    Args:
        None
    
    Returns:
        cache_filename (String): scratch/dyn_ref.pkl

    Use example:
        import dbtools.src.get_account_ref as acc
        cache_filename = acc.get_cache_filename()
    """
    cache_folder = os.path.join(os.path.dirname(__file__), '..', 'scratch')# r'C:\python_tools\dbtools\config'
    cache_filename = cache_folder+'/dyn_ref.pkl'
    return cache_filename

def delete_from_cache():
    """ Deletes cache file if any
    
    Task 1: tests if cache file exists
    Task 2: deletes file
    
    Args:
        None
    
    Returns:
        None

    Use example: 
        import dbtools.src.get_account_ref as acc
        acc.delete_from_cache()
    """
    cache_filename = get_cache_filename()
    try:
        os.remove(cache_filename)
        print(f"{cache_filename} deleted successfully.")
    except FileNotFoundError:
        print(f"{cache_filename} not found.")
    return None

def add_df_to_cache(df):
    """ Saves a dataframe into cache
    
    Task 1: saves dataframe into pickle, using filename defined externally
    
    Args: 
        df (DataFrame): data to save into cache
        
    Return: 
        None
    
    Use example: 
        import pandas as pd
        import dbtools.src.get_account_ref as acc
        data = {'Name': ['Alice', 'Bob', 'Charlie'],
            'Age': [25, 30, 35],
            'City': ['New York', 'Los Angeles', 'Chicago']}
        df = pd.DataFrame(data)
        acc.add_df_to_cache(df)
    """
    cache_filename = get_cache_filename()
    df.to_pickle(cache_filename)
    return None

def apply_extended_end_date(dyn_ref):
    """ affects '22001231' date as end_date to latest lines in dyn_ref
        enables to apply latest state of referential to all data dates after dyn_ref construction
    
    Task 1: affects '22001231' date as end_date to latest lines in dyn_ref
    
    Args: 
        dyn_ref (DataFrame): dynamic referential containing 'end_date'
        
    Returns: 
        dyn_ref (DataFrame): dynamic referential with modified end_date
        
    Use example: 
        import pandas as pd
        import dbtools.src.get_account_ref as acc
        dyn_ref = pd.DataFrame({'id': [1, 2, 3, 4, 5],
                                'end_date': ['2023-01-01', '2022-12-31', '2023-03-15', '2024-02-01', '22001231']})
        dyn_ref = acc.apply_extended_end_date(dyn_ref)
    """
    end_date = dyn_ref['end_date'].max()
    dyn_ref.loc[dyn_ref['end_date']==end_date,'end_date']='22001231'
    
    return dyn_ref

def add_dyn_ref_to_data(df, account_id_col='account_id', date_col='date',
                        add_dyn_ref_to_cache=False, group_types=True):
    """ Adds client_name, client_type and client_region corresponding to client id for given dates
        Using dynamic state of client referential
        
    Task 1: gets dynamic referential with end_date extended to '22001231'
    Task 2: selects only 'client_name','client_type','client_region' from referential
    Task 3: attributes dynamic referential to data based on date
    Task 4: removes lines with client not found in referential
    
    Args:
        df_data (DataFrame): data
        account_id_col (String): 'account_id' (default) or any other column name for the crystal id for clients
        date_col (String): 'date' (default) or any other column name for dates
    
    Returns:
        result_df (DataFrame): same as df_data, with additional columns to indicate client_name, client_type and client_region
            and begin_date and end_date
 
    Use example: 
        import pandas as pd
        import dbtools.src.get_account_ref as acc
        data = {'account_id': [1, 2, 3, 4, 5],
            'date': ['2024-01-01', '2024-01-02', '2024-01-03', '2024-01-04', '2024-01-05'],
            'value': [100, 200, 300, 400, 500]}
        df_data = pd.DataFrame(data)
        df_result = acc.add_dyn_ref_to_data(df_data)        
    """
    dyn_ref = get_dynamic_ref(add_to_cache=add_dyn_ref_to_cache, 
                           extend_end_date=True, group_types=group_types)
    dyn_ref = dyn_ref[['account_id','client_name','client_type','client_region', 'IdCountry','begin_date','end_date']]
    dyn_ref = dyn_ref.rename(columns={'account_id':account_id_col})
    
    # merge dyn_ref to df
    result_df = pd.merge(df, dyn_ref, on=account_id_col, how='left')
    
    # keep lines with corresponding begin date and end date
    mask = (result_df[date_col] >= result_df['begin_date']) & (result_df[date_col] < result_df['end_date'])
    result_df_date = result_df[mask]

    # concat to keep lines not matched (na values for account type and account country)    
    # result_df = pd.concat([result_df[result_df['begin_date'].isna()], result_df_date])
    
    result_df = result_df
    return result_df

#%% 2.0. initial investigations
def get_all_changes_for_account(account_id):
    """
    OBSOLETE: ONLY USED DURING INITIAL INVESTIGATIONS
    
    import dbtools.src.get_account_ref as acc 
    df = acc.get_all_changes_for_account(2146863926)
    """    
    # SQL query to fetch data from the table
    sql_query = """SELECT Id, %s as account_id, XMLLog FROM [%s].[dbo].[%s] where %s = %d"""%(
        dyn_config['sql_id_col'], dyn_config['sql_base'], dyn_config['sql_table'], dyn_config['sql_id_col'], account_id)
    df = pd.read_sql(sql_query, con_dyn)
    
    result_data = []    
    # Iterate through the rows
    for index, row in df.iterrows():
        xml_log = row['XMLLog']
        
        # Parse the XML data
        root = ET.fromstring(xml_log)
    
        for log in root.findall(".//Log[Type='Modified']"):
            result_data.append(add_type_to_list(row, log)) 
    
    result_df = pd.DataFrame(result_data)
    return result_df

def analyse_all_changes(df_clients):
    """
    OBSOLETE: ONLY USED DURING INITIAL INVESTIGATIONS
    """
    list_of_dataframes = []
    for index, row in df_clients.iterrows():
        df = get_all_changes_for_account(row['account_id'])
        list_of_dataframes.append(df)
    
    df_changes = pd.concat(list_of_dataframes, ignore_index=True)
    
    df_changes['RecordDate'] = pd.to_datetime(df_changes['RecordDate'], utc=True)
    df_changes['RecordDate'] = df_changes['RecordDate'].dt.tz_convert(None)
    
    df_changes.to_excel(r'C:\python_projects\kc_datasets_qfd\notebooks\all_changes.xlsx')
    
    df_types = df_changes.groupby('Field').count()
    df_types.to_excel(r'C:\python_projects\kc_datasets_qfd\notebooks\count_changes.xlsx')
    return df_changes

# QUANT TYPE CHANGES
def _treat_type_changes_separately(df_ranking, df_type, sql_account):
    """
    OBSOLETE: ONLY USED DURING INITIAL INVESTIGATIONS
    """
    # isolate account_ids where ranking has changed!
    mask = df_type.account_id.isin(df_ranking.account_id)
    df_type_ranking = df_type[mask]
    df_type_alone = df_type[~mask]
    
    sql_account_ranking = sql_account[['account_id',static_config['ranking']['sql_col']]]
    df_type_alone = df_type_alone.merge(sql_account_ranking, on='account_id', how='left')
    
    # case 1: ranking stable, only type changed
    df_type_alone = apply_account_type_mapping_by_type(df_type_alone)
    df_type_alone = df_type_alone[df_type_alone['NewType'] != df_type_alone['OldType']]

    # case 2: ranking changes
    df_ranking = apply_account_type_mapping_by_ranking(df_ranking)
    df_type_ranking = df_type_ranking.merge(sql_account_ranking, on='account_id', how='left')

    return None

def apply_account_type_mapping_by_ranking(df):
    """
    OBSOLETE: ONLY USED DURING INITIAL INVESTIGATIONS
    """
    df['IdRanking'] = df['OldId']
    df['OldType'] = df.apply(account_type_by_ranking, axis=1)
    df['IdRanking'] = df['NewId']
    df['NewType'] = df.apply(account_type_by_ranking, axis=1)
    df = df.drop(columns='IdRanking')
    return df

def apply_account_type_mapping_by_type(df):
    """
    OBSOLETE: ONLY USED DURING INITIAL INVESTIGATIONS
    """
    df['IdTypeAcc'] = df['OldId']
    df['OldType'] = df.apply(account_type_by_type, axis=1)
    df['IdTypeAcc'] = df['NewId']
    df['NewType'] = df.apply(account_type_by_type, axis=1)
    df = df.drop(columns='IdTypeAcc')
    return df


# QUANT COUNTRY CHANGES
def _treat_country_changes_separately(df_country):
    """
    OBSOLETE: ONLY USED DURING INITIAL INVESTIGATIONS
    """
    df_country = apply_account_country_mapping(df_country)
    df_country = df_country[df_country['NewCountry'] != df_country['OldCountry']]
    return None

def apply_account_country_mapping(df):
    """
    OBSOLETE: ONLY USED DURING INITIAL INVESTIGATIONS
    """
    df['IdCountry'] = df['OldId']
    df['OldCountry'] = df.apply(account_country_mapping, axis=1)
    df['IdCountry'] = df['NewId']
    df['NewCountry'] = df.apply(account_country_mapping, axis=1)
    df = df.drop(columns='IdCountry')
    return df
#%% 2.1. function to look for account type changes
def add_type_to_list(row, log, chg_type='all'):
    """ transforms log into dictionary including change type info that can be insert to dataframe later on
    
    Task 1: takes Id and account_id from row input
    Task 2: adds other informations from log input into a single dictionary
    
    Args:
        row: row of a dataframe containing Id and account_id columns
        log: xml log 
        chg_type (String)
    
    Returns:
        chg (Dictionary): columns 'Id', 'account_id', 'RecordDate', 'Field', 'ChangeType', 'OldValue', 'NewValue'

    Use example: 
        import pandas as pd
        import dbtools.src.get_account_ref as acc
        from dbtools.src.db_connexion import SqlConnector
        connector = SqlConnector()
        con_dyn = connector.connection(server='crystal_uat')
        import xml.etree.ElementTree as ET
        # real example data
        df = pd.read_sql('SELECT * FROM Crystal.[dbo].[AccountLog] where IdAccount = 2146856223', con_dyn)
        df = df.rename(columns={'IdAccount':'account_id'})
        row = df.loc[0,:]
        xml_log = row['XMLLog']
        root = ET.fromstring(xml_log)  
        for log in root.findall(".//Log[Type='Modified'][Field='Account Modified Account Type']"): 
            chg = acc.add_type_to_list(row, log, chg_type='type')
    """
    record_date = log.find("RecordDate").text
    record_date = pd.to_datetime(record_date)
    field = log.find("Field").text
    old_value = log.find("OldValue").text
    new_value = log.find("NewValue").text
    chg = {'Id': row['Id'], 'account_id': row['account_id'], 
                        'RecordDate': record_date, 'Field': field, 'ChangeType': chg_type, 'OldValue': old_value, 'NewValue': new_value}
    return chg

def get_classif_changes_for_account(account_id):
    """ Retrieves changes in classification for a given client
    
    Task 1: isolates crystal ranking changes
    Task 2: isolates crystal type changes 
    Task 3: isolates crystal country changes 
    
    Args:
        account_id (Integer): id of the client
    
    Returns:
        df_ranking_chg (DataFrame): changes in crystal ranking
        df_type_chg (DataFrame): changes in crystal type
        df_cnty_chg (DataFrame): changes in crystal country

    Use example: 
        import dbtools.src.get_account_ref as acc
        df_ranking_chg, df_type_chg, df_cnty_chg = acc.get_classif_changes_for_account(2146859679)
    """
    # checks with changes found manually and added to get_kc_flows
    # df = get_classif_changes_for_account(962875489)
    
    # SQL query to fetch data from the table
    sql_query = """SELECT Id, %s as account_id, XMLLog FROM [%s].[dbo].[%s] where %s = %d"""%(
        dyn_config['sql_id_col'], dyn_config['sql_base'], dyn_config['sql_table'], dyn_config['sql_id_col'], account_id)
    df = pd.read_sql(sql_query, con_dyn)
    
    if not df.empty:
        ranking_chg = []    
        type_chg = []
        cnty_chg = []
        # Iterate through the rows
        for index, row in df.iterrows():
            xml_log = row['XMLLog']
            
            # print("The value of IdAccount is:", account_id)
            
            # if xml_log=='die':
            #     print("Warning: The value of xml_log is 'die'.")
            # elif len(xml_log)==0:
            #     print("Warning: The value of xml_log is empty.")
            # else: 
            if xml_log != 'die' and len(xml_log) != 0:
                # Parse the XML data
                root = ET.fromstring(xml_log)    
                # RANKING CHANGES
                for log in root.findall(".//Log[Type='Modified'][Field='Account Modified Ranking']"):
                    ranking_chg.append(add_type_to_list(row, log, chg_type='ranking')) 
                for log in root.findall(".//Log[Type='Modified'][Field='Account Modified IdRanking']"):
                    ranking_chg.append(add_type_to_list(row, log, chg_type='ranking')) 
                    
                # TYPE CHANGES
                for log in root.findall(".//Log[Type='Modified'][Field='Account Modified Account type']"):
                    type_chg.append(add_type_to_list(row, log, chg_type='type')) 
                for log in root.findall(".//Log[Type='Modified'][Field='Account Modified Account Type']"):
                    type_chg.append(add_type_to_list(row, log, chg_type='type')) 
                for log in root.findall(".//Log[Type='Modified'][Field='Account Modified IdTypeAcc']"):
                    type_chg.append(add_type_to_list(row, log, chg_type='type')) 
                    
                # COUNTRY CHANGES
                for log in root.findall(".//Log[Type='Modified'][Field='Account Modified Country']"):
                    cnty_chg.append(add_type_to_list(row, log, chg_type='country')) 
                for log in root.findall(".//Log[Type='Modified'][Field='Account Modified IdCountry']"):
                    cnty_chg.append(add_type_to_list(row, log, chg_type='country')) 
    
        df_ranking_chg = pd.DataFrame(ranking_chg)
        df_type_chg = pd.DataFrame(type_chg)
        df_cnty_chg = pd.DataFrame(cnty_chg)
        
        df_ranking_chg = treat_for_sev_changes_same_day(df_ranking_chg)
        df_type_chg = treat_for_sev_changes_same_day(df_type_chg)
        df_cnty_chg = treat_for_sev_changes_same_day(df_cnty_chg)

    else: 
        df_ranking_chg = pd.DataFrame()
        df_type_chg = pd.DataFrame()
        df_cnty_chg = pd.DataFrame()
        
    return df_ranking_chg, df_type_chg, df_cnty_chg


def treat_for_sev_changes_same_day(df):
    """ identifies daily change even if several for 1 day
        e.g. from Asset Managers to Corporate to Asset Managers on the same day
    
    Task 1: replaces RecordDate time format by ChangeDate date-only column 
    Task 2: identifies day changes as first RecordDate to last RecordDate of each day
    Task 3: ignores day changes where Value ended up being the same as before
    
    Args:
        df (DataFrame): Id','account_id','RecordDate','Field','ChangeType' columns
        
    Returns: 
        df_date (DataFrame): Id','account_id','ChangeDate','Field','ChangeType' columns
            
    Use example: 
        import pandas as pd
        import dbtools.src.get_account_ref as acc
        data = {'Id': [5815, 5815, 5815, 5815],
            'account_id': [190386942, 190386942, 190386942, 190386942],
            'RecordDate': ['2016-05-06 08:35:19.788166300', '2016-05-06 08:35:43.538030200', '2016-05-06 08:36:07.563725600', '2016-05-06 08:36:29.867561800'],
            'Field': ['Account type', 'Account type', 'Account type', 'Account type'],
            'ChangeType': ['type', 'type', 'type', 'type'],
            'OldValue': ['Asset Managers', 'Corporate', 'Asset Managers', 'Corporate'],
            'NewValue': ['Corporate', 'Asset Managers', 'Corporate', 'Asset Managers']}
        df = pd.DataFrame(data)
        df_date = acc.treat_for_sev_changes_same_day(df)
    """
    if not df.empty:
        df['RecordDate'] = pd.to_datetime(df['RecordDate'], utc=True)
        df['RecordDate'] = df['RecordDate'].dt.tz_convert(None)
        df = df.sort_values(by='RecordDate', ascending=False)
        df['ChangeDate'] = df['RecordDate'].dt.date
        
        min_dates = df.groupby(['Id','account_id','ChangeDate','Field','ChangeType'])['RecordDate'].idxmin()
        max_dates = df.groupby(['Id','account_id','ChangeDate','Field','ChangeType'])['RecordDate'].idxmax()
        
        min_values = df.loc[min_dates, ['Id','account_id','ChangeDate','Field','ChangeType','OldValue']]
        max_values = df.loc[max_dates, ['Id','account_id','ChangeDate','Field','ChangeType','NewValue']]
        
        # Merge the DataFrames
        df_date = pd.merge(min_values, max_values, on=['Id','account_id','ChangeDate','Field','ChangeType'], how='inner')
        
    else: 
        df_date = df
    
    return df_date

#%% 2.2. get ranking / type / country changes
def get_changes_by_changetype(df_clients):
    """ concatenates changes for all clients in df_clients
    
    Task 1: Iterates over clients in df_clients to retreive classificaiton changes and concatenates the results in agregated outputs
    
    Args:
        df_clients (DataFrame): contains a column named 'account_id'
    
    Returns:
        df_ranking_chg (DataFrame): changes in crystal ranking
        df_type_chg (DataFrame): changes in crystal type
        df_cnty_chg (DataFrame): changes in crystal country
        
    Use example: 
        import pandas as pd
        import dbtools.src.get_account_ref as acc
        data = {'account_id': [2146856223, 962875489, 2025393256, 953652638, 2146852068, 2146859679]}
        df_clients = pd.DataFrame(data)
        df_ranking, df_type, df_country = acc.get_changes_by_changetype(df_clients)
    """
    ranking_chg = []    
    type_chg = []
    cnty_chg = []
    for index, row in df_clients.iterrows():
        df_r, df_t, df_c = get_classif_changes_for_account(row['account_id'])
        ranking_chg.append(df_r)
        type_chg.append(df_t)
        cnty_chg.append(df_c)
       
    df_ranking = pd.concat(ranking_chg, ignore_index=True)
    df_type = pd.concat(type_chg, ignore_index=True)
    df_country = pd.concat(cnty_chg, ignore_index=True)
    return df_ranking, df_type, df_country

def concat_changes(df_ranking, df_type, df_country):
    """ concatenates ranking, type and country changes into a single dataframe 
    
    Task 1: concatenates ranking, type and country changes into a single dataframe
    Task 2: sorts changes by ChangeDate
    
    Args:
        df_ranking_chg (DataFrame): changes in crystal ranking
        df_type_chg (DataFrame): changes in crystal type
        df_cnty_chg (DataFrame): changes in crystal country
        
    Returns:
        df_combined (DataFrame): all changes regrouped vertically

    Use example: 
        import pandas as pd
        import dbtools.src.get_account_ref as acc
        # Define mock data for changes
        data_ranking = {'RecordDate': ['2024-02-12 10:30:00', '2024-02-10 08:15:00'],
            'IdRanking': [1, 2],
            'ChangeType': ['Update', 'Delete']}
        df_ranking = pd.DataFrame(data_ranking)
        data_type = {'RecordDate': ['2024-02-11 14:45:00', '2024-02-09 12:00:00'],
            'IdTypeAcc': [11, 12],
            'ChangeType': ['Insert', 'Update']}
        df_type = pd.DataFrame(data_type)
        data_country = {'RecordDate': ['2024-02-10 09:00:00', '2024-02-08 16:20:00'],
            'IdCountry': ['US', 'UK'],
            'ChangeType': ['Update', 'Delete']}
        df_country = pd.DataFrame(data_country)
        df_combined = acc.concat_changes(df_ranking, df_type, df_country)
    """
    df_combined = pd.concat([df_ranking, df_type, df_country], axis=0)
    df_combined = df_combined.sort_values(by='ChangeDate', ascending=False)
    
    return df_combined


#%% 2.3. from values to ids
def attribute_ids(df, sql):
    """ gets OldId and NewId based on names (OldValue and NewValue)
    
    Task 1: adds OldId and NewId to dataframe df
    
    Args:
        df (DataFrame): changes with OldValue and NewValue
        sql (DataFrame): mapping between id and name
    
    Returns:
        df (DataFrame): changes with additional columns OldId and NewId

    Use example: 
        import pandas as pd
        import dbtools.src.get_account_ref as acc
        data_changes = {'OldValue': ['Value1', 'Value2'],
            'NewValue': ['Value3', 'Value4']}
        df_changes = pd.DataFrame(data_changes)
        data_sql = {'Id': [1, 2, 3, 4],
            'Name': ['Value1', 'Value2', 'Value3', 'Value4']}
        df_sql = pd.DataFrame(data_sql)
        result = acc.attribute_ids(df_changes, df_sql)
    """
    sql_old = sql[['Id','Name']]
    sql_old.columns=['OldId','OldValue']
    df = pd.merge(df, sql_old, on='OldValue',  how='left')
    sql_new = sql[['Id','Name']]
    sql_new.columns=['NewId','NewValue']
    df = pd.merge(df, sql_new, on='NewValue',  how='left')
    return df

def from_names_to_ids(df_ranking, df_type, df_country):
    """ Adds columns OldId and NewId to each of change dataframes
    
    Task 1: chooses with sql table to apply to each dataframe of change
    Task 2: reads sql mapping tables based on config informations and passes it to add Ids to dataframes 
    Task 3: only keeps mapping ranking values contained in config informations
    
    Args:
        df_ranking_chg (DataFrame): changes in crystal ranking
        df_type_chg (DataFrame): changes in crystal type
        df_cnty_chg (DataFrame): changes in crystal country
        
    Returns:
        df_ranking_chg (DataFrame): changes in crystal ranking
        df_type_chg (DataFrame): changes in crystal type
        df_cnty_chg (DataFrame): changes in crystal country
        
    Use example: 
        import pandas as pd
        import dbtools.src.get_account_ref as acc
        data_ranking_chg = {'OldValue': ['Gold', 'Technical'],
            'NewValue': ['Silver', 'Retail']}
        df_ranking_chg = pd.DataFrame(data_ranking_chg)
        data_type_chg = {'OldValue': ['Banks', 'Corporate'],
            'NewValue': ['Brokers Dealers', 'Hedge Funds']}
        df_type_chg = pd.DataFrame(data_type_chg)
        data_country_chg = {
            'OldValue': ['ALBANIA', 'AUSTRALIA'],
            'NewValue': ['ANGOLA', 'BOSNIA AND HERZEGOVINA']}
        df_country_chg = pd.DataFrame(data_country_chg)
        result_ranking, result_type, result_country = acc.from_names_to_ids(df_ranking_chg, df_type_chg, df_country_chg)
    """
    sql_ranking = pd.read_sql("""select * from KGR..%s"""%(static_config['ranking']['sql_table']), con_static)
    sql_ranking['Id'] = sql_ranking[static_config['ranking']['sql_map_col']]
    df_ranking = attribute_ids(df_ranking, sql_ranking)
    all_ranking_ids = set.union(*[set(ids) for ids in dict_mapp['ranking'].values()])
    df_ranking = df_ranking[(df_ranking.OldId.isin(all_ranking_ids)) | (df_ranking.NewId.isin(all_ranking_ids))]
    
    sql_type = pd.read_sql("""select * from KGR..[%s]"""%(static_config['type']['sql_table']), con_static)
    sql_type['Id'] = sql_type[static_config['type']['sql_map_col']]
    df_type = attribute_ids(df_type, sql_type)
    
    sql_country = pd.read_sql("""select * from KGR..[%s]"""%(static_config['country']['sql_table']), con_static)
    sql_country['Id'] = sql_country[static_config['country']['sql_map_col']]
    df_country = attribute_ids(df_country, sql_country)
    return df_ranking, df_type, df_country

#%% 2.4. create dynamic client referential
def append_change_to_dyn_ref(dyn_ref, row):
    """ changes dynamic referential to integrate a past change 
    
    Task 1: identifies the last line to modify in dynamic referential (for this account, the smallest end date)
    
    Args:
        dyn_ref (DataFrame): temporary dynamic referential
        row: row of dataframe containing changes
    
    Returns:
        dyn_ref (DataFrame): modified dynamic referential to integrate change from row

    Use example: 
        import pandas as pd
        import dbtools.src.get_account_ref as acc
        data_dyn_ref = {'account_id': [1, 2, 3],
            'client_name': ['Client1', 'Client2', 'Client3'],
            'ranking_id': [101, 102, 103],
            'type_id': [201, 202, 203],
            'country_id': [301, 302, 303],
            'begin_date': ['2022-01-01', '2022-01-01', '2022-01-01'],
            'end_date': ['2022-12-31', '2022-12-31', '2022-12-31']}
        dyn_ref = pd.DataFrame(data_dyn_ref)
        sample_row = {'account_id': 2,
            'client_name': 'Client2',
            'ChangeType': 'ranking',
            'OldId': 201,
            'NewId': 205,
            'ChangeDate': '2022-06-15'}
        result_dyn_ref = acc.append_change_to_dyn_ref(dyn_ref, sample_row)
    """
    # find the line from df_change with IdAccount = row['IdAccount'] and the lowest end_date
    filter_condition = (dyn_ref['account_id'] == row['account_id'])
    min_end_date_idx = dyn_ref[filter_condition]['end_date'].idxmin()
    
    # if previous change was on the same day: modify this line
    if dyn_ref.at[min_end_date_idx, 'begin_date']==pd.to_datetime(row['ChangeDate']):
        if row['ChangeType']=='ranking':
            dyn_ref.at[min_end_date_idx, static_config['ranking']['sql_col']]=row['OldId']
        elif row['ChangeType']=='type':
            dyn_ref.at[min_end_date_idx, static_config['type']['sql_col']]=row['OldId']
        elif row['ChangeType']=='country':
            dyn_ref.at[min_end_date_idx, static_config['country']['sql_col']]=row['OldId']
        
    # if not, only change begin_date add a new line
    else: 
        # change begin_date for this line in df_change to begin_date = row['ChangeDate']
        dyn_ref.at[min_end_date_idx, 'begin_date'] = pd.to_datetime(row['ChangeDate']) #+ pd.Timedelta(days=1)
    
        # add new line to df_change with begin_date = np.nan and end_date = row['ChangeDate']
        new_line = pd.DataFrame({'account_id': [row['account_id']],
                                 'client_name': [dyn_ref.at[min_end_date_idx, 'client_name']],
                                 static_config['ranking']['sql_col']: [dyn_ref.at[min_end_date_idx, static_config['ranking']['sql_col']]],
                                 static_config['type']['sql_col']: [dyn_ref.at[min_end_date_idx, static_config['type']['sql_col']]],
                                 static_config['country']['sql_col']: [dyn_ref.at[min_end_date_idx, static_config['country']['sql_col']]],
                                 'begin_date': [pd.to_datetime('19000101')],
                                 'end_date': [pd.to_datetime(row['ChangeDate'])]}) # + pd.Timedelta(days=1)
        
        if row['ChangeType']=='ranking':
            new_line[static_config['ranking']['sql_col']]=row['OldId']
        elif row['ChangeType']=='type':
            new_line[static_config['type']['sql_col']]=row['OldId']
        elif row['ChangeType']=='country':
            new_line[static_config['country']['sql_col']]=row['OldId']
        
        dyn_ref = pd.concat([dyn_ref, new_line], ignore_index=True)
    
    
    return dyn_ref


def create_dynamic_client_referential(df_combined):
    """ starts from latest state of referential and modifies to integrate past changes 
    
    Task 1: gets latest state of referential using static config
    Task 2: treats each change one-by-one to construct dynamic referential
    Task 3: chooses end_date as today
    
    Args:
        df_combined (DataFrame): containing all changes
        
    Returns:
        dyn_ref (DataFrame): containing dynamic referential account_id, client_name, IdRanking, IdTypeAcc, IdCountry by begin_date and end_date

    Use example: 
        import pandas as pd
        import dbtools.src.get_account_ref as acc
        chgs = {'account_id': [2, 3],
            'client_name': ['Client2','Client3'],
            'ChangeType': ['ranking','ranking'],
            'OldId': [201, 6],
            'NewId': [205, 11],
            'ChangeDate': ['2022-06-15','2023-09-01']}
        df_combined = pd.DataFrame(chgs)
        result_dyn_ref = acc.append_change_to_dyn_ref(df_combined)
    """
    # IdAccount, IdRanking, IdTypeAcc, IdCountry
    sql_account = get_static_crystal_info()
    
    sql_account['begin_date'] = pd.to_datetime('19000101')
    sql_account['end_date'] = pd.to_datetime(date.today()) #pd.to_datetime('20240405')
    
    # client not in changes
    chg_mask = sql_account['account_id'].isin(df_combined['account_id'])
    df_nochange = sql_account[~chg_mask].copy()
    
    # when there is a change, start for last known state (sql_account) and apply changes retroactively
    df_change = sql_account[chg_mask].copy()
   
    # loop on each change to add corresponding line in df_change
    dyn_ref = df_change.copy()
    for index, row in df_combined.iterrows():
        dyn_ref = append_change_to_dyn_ref(dyn_ref, row)
    
    dyn_ref = pd.concat([df_nochange, dyn_ref], axis=0)
    return dyn_ref




#%% 2.5. insert into SQL
def push_dynamic_to_sql(dyn_ref):
    """ replaces sql table containing dynamic referential with dyn_ref provided in input
    
    Task 1: applies date format to begin_date and end_date
    Task 2: defines the properties of SQL table (columns format and keys)
    Task 3: replace table (name from config) by input dataframe
        
    Args: 
        dyn_ref (DataFrame): dynamic referential containing columns 
            account_id, client_name
            static_config['ranking']['sql_col'], static_config['type']['sql_col'], static_config['country']['sql_col']
            begin_date, end_date, client_type, client_region
    
    Returns: 
        None
        
    Use example: 
        import dbtools.src.get_account_ref as acc
        acc.delete_from_cache()
        dyn_ref = acc.get_dynamic_ref(add_to_cache=True, extend_end_date = False)
        acc.push_dynamic_to_sql(dyn_ref)
    """
    
    # colum formats
    dyn_ref[static_config['ranking']['sql_col']] = dyn_ref[static_config['ranking']['sql_col']].astype(int)
    dyn_ref[static_config['type']['sql_col']] = dyn_ref[static_config['type']['sql_col']].astype(int)
    
    dyn_ref['begin_date'] = pd.to_datetime(dyn_ref['begin_date'])
    dyn_ref['begin_date'] = dyn_ref['begin_date'].dt.date
    
    dyn_ref['end_date'] = pd.to_datetime(dyn_ref['end_date'])
    dyn_ref['end_date'] = dyn_ref['end_date'].dt.date
    
    # drops client type and region before pushing to SQL (they are based on config mapping, not crystal info)
    dyn_ref = dyn_ref.drop(columns=['client_type','client_region'])
    
    # table design
    db_manager = db.DatabaseManager( hybrid_config['sql_table'] )
    column_formats = {
        'account_id': Integer,
        'client_name': String(200),
        static_config['ranking']['sql_col']: Integer,
        static_config['type']['sql_col'] : Integer,
        static_config['country']['sql_col'] : String(2),
        'begin_date' : Date,
        'end_date' : Date}

    # replace
    db_manager.replace_table(df = dyn_ref.copy(),column_types=column_formats, 
                            auto_incremental_index=None,
                            unique_columns = ['account_id','begin_date','end_date'])
    
    return None

#%% 3. HYBRID DYNAMIC SQL + STATIC
def get_hybrid_ref(group_types=True):
    """ gets hybrid referential
        Hybrid is defined as (dynamic up to latest already available date + static afterwards) 
    
    Task 1: combines dynamic and static parts of referential
    Task 2: assigns begin_date and end_date to static part    
        
    Args: 
        None
        
    Returns: 
        hyb_ref (DataFrame): dynamic referential containing columns 
            account_id, client_name
            static_config['ranking']['sql_col'], static_config['type']['sql_col'], static_config['country']['sql_col']
            begin_date, end_date, client_type, client_region
        
    Use example: 
        import dbtools.src.get_account_ref as acc
        hyb_ref = acc.get_hybrid_ref(group_types=False)
    """
    dyn_part = get_dynamic_part_for_hybrid(group_types=group_types)
    stat_part = get_static_part_for_hybrid(begin_date=dyn_part['end_date'].max(), end_date='22001231',group_types=group_types)
    hyb_ref = pd.concat([dyn_part, stat_part], axis=0)
    
    return hyb_ref


def get_dynamic_part_for_hybrid(group_types=True):
    """ gets dynamic state of referential that is available in sql table
    
    Task 1: reads sql dynamic referential as given by hybrid_config
    Task 2: applies config mapping to sql table
    Task 3: applies date format to begin_date and end_date
    Task 4: applies date format to begin_date and end_date
        
    Args: 
        None
        
    Returns: 
        dyn_ref (DataFrame): dynamic referential containing columns 
            account_id, client_name
            static_config['ranking']['sql_col'], static_config['type']['sql_col'], static_config['country']['sql_col']
            begin_date, end_date, client_type, client_region
            
    Use example: 
        import dbtools.src.get_account_ref as acc
        dyn_ref = acc.get_dynamic_part_for_hybrid()
    """
    
    dyn_ref = pd.read_sql("""select * from %s..%s"""%(hybrid_config['sql_base'],hybrid_config['sql_table']), con_hybrid)
    
    # drops client type and region if exist (they are based on config mapping, not SQL content)
    # dyn_ref = dyn_ref.drop(columns=['client_type','client_region'])
    # apply mappings on crystal info from SQL table
    dyn_ref = apply_type_country_mappings(dyn_ref, group_types=group_types)    
    
    dyn_ref['begin_date'] = pd.to_datetime(dyn_ref['begin_date'])
    #dyn_ref['begin_date'] = dyn_ref['begin_date'].dt.date
    dyn_ref['end_date'] = pd.to_datetime(dyn_ref['end_date'])
    #dyn_ref['end_date'] = dyn_ref['end_date'].dt.date
    return dyn_ref
    

def get_static_part_for_hybrid(begin_date='19000101', end_date='22001231', group_types=True):
    """ gets latest state of referential and adds begin date and end date from inputs 
    
    Task 1: gets latest state of referential
    Task 2: adds begin date and end date as columns
    
    Args:
        begin_date (date)
        end_date (date)
        
    Returns:
        df_acc (DataFrame): containing static referential, with columns
                account_id, client_name
                static_config['ranking']['sql_col'], static_config['type']['sql_col'], static_config['country']['sql_col']
                client_type, client_region
                + begin_date and end_date
    
    Use example: 
        import dbtools.src.get_account_ref as acc
        df_acc = acc.get_static_part_for_hybrid(begin_date='19000101', end_date='22001231')
    """
    # IdAccount, IdRanking, IdTypeAcc, IdCountry
    df_acc = get_static_ref(group_types=group_types)
    df_acc['begin_date'] = pd.to_datetime(begin_date)
    #df_acc['begin_date'] = df_acc['begin_date'].dt.date
    df_acc['end_date'] = pd.to_datetime(end_date)
    #df_acc['end_date'] = df_acc['end_date'].dt.date
    
    return df_acc

def add_hyb_ref_to_data(df, account_id_col='account_id', date_col='date', group_types=True):
    """ Adds client_name, client_type and client_region corresponding to client id for given dates
        Using hybrid state of client referential
        Hybrid is defined as (dynamic up to latest already available date + static afterwards) 
        
    Task 1: selects only 'client_name','client_type','client_region' from referential
    Task 2: attributes dynamic referential to data based on date
    Task 3: removes lines with client not found in referential
    
    Args:
        df_data (DataFrame): data
        account_id_col (String): 'account_id' (default) or any other column name for the crystal id for clients
        date_col (String): 'date' (default) or any other column name for dates
    
    Returns:
        result_df (DataFrame): same as df_data, with additional columns to indicate client_name, client_type and client_region
            and begin_date and end_date
 
    Use example: 
        import pandas as pd
        import dbtools.src.get_account_ref as acc
        data = {'account_id': [1, 2, 3, 4, 5],
            'date': ['2024-01-01', '2024-01-02', '2024-01-03', '2024-01-04', '2024-01-05'],
            'value': [100, 200, 300, 400, 500]}
        df_data = pd.DataFrame(data)
        df_result = acc.add_hyb_ref_to_data(df_data)        
    """
    hyb_ref = get_hybrid_ref(group_types=group_types)
    hyb_ref = hyb_ref[['account_id','client_name','client_type','client_region', 'IdCountry', 'begin_date','end_date']]
    hyb_ref = hyb_ref.rename(columns={'account_id':account_id_col})
    
    # merge dyn_ref to df
    result_df = pd.merge(df, hyb_ref, on=account_id_col, how='left')
    
    # keep lines with corresponding begin date and end date
    mask = (result_df[date_col] >= result_df['begin_date']) & (result_df[date_col] < result_df['end_date'])
    result_df_date = result_df[mask]

    # concat to keep lines not matched (na values for account type and account country)    
    # result_df = pd.concat([result_df[result_df['begin_date'].isna()], result_df_date])
    
    result_df = result_df_date
    
    return result_df