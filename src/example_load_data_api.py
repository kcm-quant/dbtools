# -*- coding: utf-8 -*-
"""
Created on Wed Oct 25 10:58:10 2023

@author: nle
"""

from kc_bbg_api.connection import ApiSettings,KCBBGClient
from kc_bbg_api import historical_data,intraday_tick_data,reference_data,indexcomposition_data,intraday_bar_data
from datetime import datetime
import pandas as pd

import numpy as np
import os, sys

import dbtools.src.get_repository as rep
import dbtools.src.get_market_data as mkt
import dbtools.config.fundamental_config as config
import dbtools.src.bcp_import_to_sql as bcp_import_to_sql

from dbtools.src.db_connexion import SqlConnector
connector = SqlConnector()
con_mis = connector.connection()


#ApiSettings.api_url = 'http://uatifcweb903.keplercm.lan:8082'
ApiSettings.user_name = "bbg.quant-research.user"
ApiSettings.user_password = "pZ_B5LJ-QgfOO0Vpk_Yq"


start_date = '20130101'# '20221003'#20160614
end_date = '20230519' #'20221015'#20160620



def get_histo_data(tickers, fields, start_date, end_date, overrides, params):
    df = historical_data(securities=list(tickers),
                                fields=fields,
                                startDate=start_date, endDate=end_date,
                                overrides=overrides,
                                setParams=params)

    df = df.drop_duplicates(keep = 'last').set_index(['date', 'security'])[fields[0]].unstack()
    
    tickers_fix = tickers[tickers.str.contains('/')]
    tickers_fix = dict(zip(tickers_fix.str.replace('/',''), tickers_fix))
    df = df.rename(columns = tickers_fix)
    
    df = df.reindex(columns = tickers)
    df.columns = tickers.index
    return df

#%% Simple example for a bloomberg field ("conf_tmp") with a list of ticker ("ticker")
perimeter = [13474, 34097, 257,  1313623,  2769415,  2743387, 10174313,
              309900,  84, 183255, 5479]
ticker = rep.mapping_from_security(perimeter, code = 'fund_ticker')
ticker = ticker + ' Equity'

conf_tmp = {'fields': ['TOT_DEBT_TO_COM_EQY'],
             'overrides': None,
             'setParams': {'adjustmentNormal': False,
              'adjustmentAbnormal': False,
              'adjustmentSplit': True,
              'periodicitySelection': 'WEEKLY'}}

df = get_histo_data(tickers = ticker,
                     start_date = start_date,
                     end_date = end_date,
                     fields = conf_tmp.get('fields'),
                     overrides = conf_tmp.get('overrides'),
                     params = conf_tmp.get('setParams'))

#%% Example from a config.py file

conf = config.Config()
dict_adj_conf = conf.dict_adj.copy()
dict_fund_conf = conf.dict_fund.copy()

# reduce champs in config fundamental data for loading
for k in ['PX_LAST_N_N_Y_fundccy', 'PX_LAST_N_N_N', 'PX_LAST_N_N_Y', 'PX_LAST_N_Y_Y', 
        'PX_LAST_N_N_N_EUR', 'PX_LAST_N_N_Y_EUR', 'PX_LAST_N_Y_Y_EUR',
        'BEST_EPS_3GY', 'BEST_EPS_BF', 'BEST_EPS_2BF',
         'BEST_PE_RATIO_BF', 'BEST_PX_BPS_RATIO_BF', 'BEST_ROE_BF', 
         'PX_TO_SALES_RATIO', 'PX_TO_SALES_RATIO_yearly',
         'TOT_DEBT_TO_COM_EQY', 'TOT_DEBT_TO_COM_EQY_yearly']:
    dict_fund_conf.pop(k)

level2 = rep.get_quant_perimeter(2)


perimeter = [13474, 34097, 257,  1313623,  2769415,  2743387, 10174313,
              309900,  84, 183255, 5479]

fund_ticker_l2 = rep.mapping_from_security(level2, code = 'fund_ticker')
fund_ticker_l2 = fund_ticker_l2 + ' Equity'

fund_ticker = fund_ticker_l2.loc[perimeter]#sample(200)
security_id = fund_ticker.index

dict_data = {}
t_s = datetime.now()
for k in dict_fund_conf.keys():
    conf_tmp = dict_fund_conf[k]
    ts = datetime.now()
    df = get_histo_data(tickers = fund_ticker,
                         start_date = start_date,
                         end_date = end_date,
                         fields = conf_tmp.get('fields'),
                         overrides = conf_tmp.get('overrides'),
                         params = conf_tmp.get('setParams'))
    te = datetime.now()
    t_exec = te-ts
    print('--------- %s: \t %s -------' %(k, t_exec))
    dict_data[k] = df.sort_index(axis = 1).copy()
t_e = datetime.now()
t_total = t_e - t_s
print('------------- Load Fundamental data in %s ----------------' %t_total)

#%%
k = 'BEST_BPS_BF'
conf_tmp = dict_fund_conf[k]

ticker = pd.Series(['AF FP Equity', 'SWMA SS Equity'])
df = get_histo_data(tickers = ticker,
                     start_date = start_date,
                     end_date = end_date,
                     fields = conf_tmp.get('fields'),
                     overrides = conf_tmp.get('overrides'),
                     params = conf_tmp.get('setParams'))

df = df.rename(columns = ticker)






















