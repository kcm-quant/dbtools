# -*- coding: utf-8 -*-
"""
**This module loads data from Referential table KGR, and data Kepler Cheuvreux via API**

Created on Fri Mar 18 15:08:41 2022

@author: tle (tle@keplercheuvreux.com)

Data from Kepler Cheuvreux API
------------------------------
#. :py:func:`get_kc_analyst`
#. :py:func:`get_kc_sector`
#. :py:func:`get_kc_company`
#. :py:func:`get_kc_rating`

Data from QUANT_work
------------------------------
#. :py:func:`load_holidays`

Data from Referential KGR
------------------------------
Mapping
~~~~~~~~~~~~~~~~~~~~~~
#. :py:func:`mapping_from_security`
#. :py:func:`mapping_sector_from_security`
#. :py:func:`mapping_country_from_security_id`
#. :py:func:`mapping_bbg_mtf_from_security`
#. :py:func:`mapping_security_id_from`
#. :py:func:`mapping_from_td_id`
#. :py:func:`get_ticker_quant_from_ticker_bbg`

Get data from KGR
~~~~~~~~~~~~~~~~~~~~~~
#. :py:func:`get_quant_perimeter`
#. :py:func:`get_index_comp`
#. :py:func:`get_flag_lms`
#. :py:func:`get_fx_histo`
#. :py:func:`get_trading_information`
#. :py:func:`get_euronext_mcap`
#. :py:func:`get_kc_coverage_screening`
#. :py:func:`get_kc_coverage_histo`
#. :py:func:`get_kc_plus_index_comp`

Add stocks classification:
#. :py:func:`add_dynamic_stock_capi`
#. :py:func:`add_dynamic_stock_style`
#. :py:func:`add_stock_country`
#. :py:func:`add_stock_sector`

Data from QUANT_work
------------------------------
Mapping
~~~~~~~~~~~~~~~~~~~~~~
#. :py:func:`mapping_index_from_ticker`
#. :py:func:`mapping_from_index`

"""
from datetime import datetime
import pandas as pd
import numpy as np
import requests
import os
from dbtools.src.db_connexion import SqlConnector
from pandas.tseries.offsets import BDay

pd.set_option('display.max_rows', 20)

connector = SqlConnector()
con_mis = connector.connection()


path_config = os.path.join(os.path.dirname(__file__), '..', 'config')
config_mapping_name = 'mapping_name.json'

f = open(os.path.join(path_config, config_mapping_name))
d_map_name = eval(f.read())
f.close()

name_mapping = d_map_name.get('reference_columns')
reference_type_id = pd.Series(d_map_name.get('reference_type_id'))
sector_map = d_map_name.get('sector_map')
capi_map = d_map_name.get('capi_map')
country_name = d_map_name.get('country_name_map')
zone_name = d_map_name.get('zone_map')
style_gv_map = d_map_name.get('style_gv_map')
country_iso = d_map_name.get('country_iso')




def load_holidays():
    """
    Load holidays from QUANT_work..quant_holidays

    Returns:
        holidays (pd.Index, DatetimeIndex)
    """
    holidays = pd.read_sql('select * from QUANT_work..quant_holidays', con_mis)
    holidays = pd.to_datetime(holidays['date'].values)
    return holidays

def load_holidays_with_tdid(td_id):
    """
    Load holidays from QUANT_work..quant_holidays of the selected trading destinations

    Returns:
        holidays (pd.Index, DatetimeIndex)"""
    # Build the SQL query with placeholders for each element in td_id
    td_id_str = ','.join([str(int(s)) for s in td_id])
    query = 'SELECT * FROM KGR..TRADINGBUSINESSCALENDAR WHERE EXCHANGE IN (%s)'%(td_id_str)
    
    # Execute the SQL query using pandas' read_sql function
    holidays = pd.read_sql(query, con_mis)
    
    # Convert the 'CLOSEDDAY' column values to datetime format
    holidays = pd.to_datetime(holidays['CLOSEDDAY'].values)
    
    # Return the processed list of holidays
    return holidays

def get_kc_analyst():
    """
    Get KC Analyst in API via url http://prdresres903.keplercm.lan:8080/ServiceResearch/analysts

    Returns:
        DataFrame with ID, Name, Last name

    .. exec_code::

        import dbtools.src.get_repository as rep
        kc_analyst = rep.get_kc_analyst().head(3)
        print(kc_analyst)
    """
    url_analysts = 'http://prdresres903.keplercm.lan:8080/ServiceResearch/analysts'
    req = requests.get(url_analysts)
    res = eval(req.text[1:-1].replace('true', '"true"').replace('null', '"unknown"'))
    df_analyst = pd.DataFrame(res)
    df_analyst.index = df_analyst.Id
    df_analyst = df_analyst[['FirstName', 'LastName']]
    df_analyst = df_analyst.sort_index()
    return df_analyst


def get_kc_sector():
    """
    Get KC sector in API via url http://prdresres903.keplercm.lan:8080/ServiceResearch/sectors

    Returns:
         DataFrame with ID, Name, Description
    .. exec_code::

        import dbtools.src.get_repository as rep
        kc_sector = rep.get_kc_sector().head(3)
        print(kc_sector)
    """
    url_sectors = 'http://prdresres903.keplercm.lan:8080/ServiceResearch/sectors'
    req = requests.get(url_sectors)
    res = eval(req.text[1:-1])
    df_sector = pd.DataFrame(res)
    df_sector.index = df_sector.Id
    df_sector = df_sector[['Name', 'Description']]
    df_sector = df_sector.sort_index()
    return df_sector


def get_kc_company():
    """
    Get KC companies in API via url

    Returns:
         DataFrame with ID, Name, Isin, Code Bloomberg, Currency
    .. exec_code::

        import dbtools.src.get_repository as rep
        kc_company = rep.get_kc_company().head(3)
        print(kc_company)
    """
    url_companies = 'http://prdresres903.keplercm.lan:8080/ServiceResearch/companies'
    list_keep = ['Name', 'ISIN', 'BloombergCode', 'CurrencyId']
    req = requests.get(url_companies)
    res = eval(req.text[1:-1].replace('null', '"unknown"') \
               .replace('true', '"true"') \
               .replace('false', '"false"'))
    df_company = pd.DataFrame(res)
    df_company.index = df_company.Id
    df_company = df_company[list_keep]
    df_company = df_company.sort_index()
    df_company.columns = df_company.columns.str.upper()
    return df_company

def get_kc_rating():
    """
    Get KC companies rating in API via url http://prdresres903.keplercm.lan:8080/ServiceResearch/CompaniesDetails/Texts

    Returns:
         DataFrame with 'CompanyId', 'Rating', 'DateUpdate'
    .. exec_code::

        import dbtools.src.get_repository as rep
        kc_rating = rep.get_kc_rating().head(3)
        print(kc_rating)
    """
    url_rating = 'http://prdresres903.keplercm.lan:8080/ServiceResearch/CompaniesDetails/Texts'
    req = requests.get(url_rating)
    res = eval(req.text[1:-1].replace('null', '"unknown"'))
    df_rating = pd.DataFrame(res)
    df_rating = df_rating[df_rating.DataKey=='Rating']
    df_rating = df_rating.sort_values(['CompanyId', 'DataValue', 'DateUpdate'])

    df_rating = df_rating.drop_duplicates(subset = ['CompanyId', 'DateUpdate'], keep = 'last')
    df_rating.index = df_rating.CompanyId.values
    df_rating = df_rating.sort_index()
    df_rating = df_rating[['CompanyId', 'DataValue', 'DateUpdate']]
    df_rating = df_rating.rename(columns = {'DataValue': 'Rating'})
    df_rating.columns = df_rating.columns.str.upper()
    return df_rating


def get_quant_perimeter(level=1, start_date='19900101', end_date='20991231'):
    """
    Get perimeter de Quant research

    Args:
        level (int):, Data level (1 or 2)
        start_date (str):, %Y%m%d range date of perimeter
        end_date (str):, %Y%m%d range date of perimeter

    Returns:
        pd.Index, security_id of given level

    .. exec_code::

        import dbtools.src.get_repository as rep
        quant_perimeter = rep.get_quant_perimeter()[:5]
        print(quant_perimeter)
    """
    query = """
            select distinct security_id
            from KGR..HISTO_SECURITY_QUANT
            where primary_trading_destination_id is not null
            and security_id in (select distinct security_id 
                                from KGR..HISTO_QUANT_PERIMETER
                                where level = '%d'
                                and DATE<='%s')
            and not (begin_date >= '%s' or end_date <= '%s')
            group by security_id
            order by security_id
            """ % (level, end_date, end_date, start_date)
    df_temp = pd.read_sql(query, con_mis)
    df_temp.index = df_temp.security_id
    df_temp = df_temp.index
    return df_temp


def get_index_comp(index_ticker, start_date, end_date, shift = False):
    """
    Get composition Index from KGR..HISTO_INDEXCOMPONENT_QUANT

    Args:
        index_ticker (str): Bloomberg index ticker.
        start_date (str): %YYYY%mm%dd
        end_date (str): %YYYY%mm%dd
        shift (bool): dataframe of composition of real date (True) or 1-day lag (False) compo

    Returns:
        DataFrame, weight of components in index (date in index, security in columns)

    .. exec_code::

        import dbtools.src.get_repository as rep
        df = rep.get_index_comp(index_ticker='SXXP', start_date = '20220901', end_date = '20220906')
        print(df)
    """
    end_date_sql = pd.to_datetime(end_date) + BDay(1)
    end_date_sql = end_date_sql.strftime('%Y%m%d')
    
    query = """
            select distinct DATE , security_id, 
            SECURITYRATIO as weight, BLOOMBERGCODE as ticker
            from KGR..HISTO_INDEXCOMPONENT_QUANT 
            where DATE between '%s' and '%s'
            and INDEXCODEBLOOMBERG = '%s'
            and security_id is not null
            order by DATE
            """ % (start_date, end_date_sql, index_ticker)
    df_temp = pd.read_sql(query, con_mis).drop_duplicates()
    df_temp = df_temp.set_index(['DATE'])
    df_temp.index = pd.to_datetime(df_temp.index).normalize()
    df_temp = df_temp.set_index('security_id', append=True).weight.unstack()
    df_temp = df_temp.rename_axis(columns=None, index=None)
    df_temp = df_temp[df_temp.index.weekday <= 4]
    if shift:
        df_temp = df_temp.shift(-1, freq = BDay())
    df_temp = df_temp[(df_temp.index>=start_date) & (df_temp.index<=end_date)]

    return df_temp


def get_histo_reference_change(security_id, code = 'prim_td_id'):
    """
    get histo reference change in KGR..HISTO_SECURITY_QUANT

     Args:
         security_id (list): list of security_id
         code (str): what to get from security

             - in ['prim_td_id', 'ccy', 'isin', 'sedol', 'figi', 'secname', 'cficode']
    
    Returns:
        DataFrame

    """
    s_map = pd.Series({'PRIM_TD_ID':'primary_trading_destination_id'})
    
    code = code.upper()
    sec_str = ','.join([str(int(s)) for s in security_id])
    
    field = s_map[code] if code in s_map.index else code
    query = """select min(begin_date) date, security_id, %s
    from KGR..HISTO_SECURITY_QUANT
    where security_id in (%s)
    group by security_id, %s
    order by min(begin_date)
    """ %(field, sec_str, field)
    
    df_tmp = pd.read_sql(query, con_mis)
    df_tmp.date = pd.to_datetime(df_tmp.date)
    return df_tmp
    
    
    

def mapping_from_security(security_id, code='isin', date_ref=''):
    """
    Mapping from security_id to 'prim_td_id', 'ccy', 'isin', 'sedol', 'fig',
    'secname', 'cficode', 'bbg', 'fund_ticker', 'kc_company_id', 'ric', 'stoxxid'

    Args:
        security_id (list): list of security_id
        code (str): what to get from security

            - in ['security_id', 'prim_td_id', 'ccy', 'isin', 'sedol', 'figi', 'secname', 'cficode', 'bbg', 'fund_ticker',
                    'kc_company_id', 'ric', 'stoxxid', 'start_date', 'end_date', 'start_date_min', 'end_date_max']
            - if code =='date': return data frame with min(begin_date) and max(end_date)
        date_ref (str): date %Y%m%d, reference date

    Returns:
        Series or DataFrame, depend on given code
            - Series of mapping, security_id in index, value is the mapping result if code given

    .. exec_code::

        import dbtools.src.get_repository as rep
        df = rep.mapping_from_security(security_id=[2, 8, 391, 512], code = 'ISIN')
        print(df)
    """

    code = code.upper()
    sec_str = ','.join([str(int(s)) for s in security_id])

    query = f"""
            select distinct *
            from KGR..HISTO_SECURITY_QUANT 
            where security_id in ({sec_str})
            """
    if code in reference_type_id.index:
        query = """
                select distinct r.security_id, r.reference, r.begin_date, r.end_date, r.last_in_date
                from KGR..HISTO_SECURITY_QUANT_REFERENCE r, KGR..HISTO_SECURITY_QUANT q
                where r.security_id = q.security_id
                and r.trading_destination_id = q.primary_trading_destination_id
                and r.security_id in (%s)
                and reference_type_id = %d
                order by r.end_date 
                """ % (sec_str, reference_type_id[code])
        code = 'reference' if code != '' else ''
    elif code in ['begin_date_min', 'end_date_max', 'BEGIN_DATE_MIN', 'END_DATE_MAX']:
        code = code.lower()
        query = """
                select security_id, min(begin_date) as begin_date_min, min(begin_date) as begin_date,
                max(end_date) as end_date_max, max(end_date) as end_date
                from KGR..HISTO_SECURITY_QUANT
                where security_id in (%s)
                and primary_trading_destination_id is not null
                group by security_id
                order by security_id
                """ % sec_str

    df_temp = pd.read_sql(query, con_mis)
    df_temp = df_temp.rename(columns=name_mapping)

    if date_ref != '':
        date_ref = pd.to_datetime(date_ref).date()
        df_temp = df_temp[(df_temp.begin_date <= date_ref) & (df_temp.end_date > date_ref)]
    df_temp.index = df_temp.security_id
    df_temp = df_temp.rename_axis(columns=None, index=None)

    df_temp = df_temp.sort_values(by=['security_id', 'end_date', 'begin_date']) \
        .drop_duplicates(['security_id'], keep='last')
    if 'DATE' in code:
        code = code.lower()
    return df_temp[code]

def mapping_bbg_mtf_from_security(security_id, date_ref=''):
    """
        Mapping from security_id to  'bbg' with mtfs

        Args:
            security_id (list): list of security_id
            date_ref (str): date %Y%m%d, reference date

        Returns:
            DataFrame with security in index and bbg tickers and td_id in columns

        .. exec_code::

            import dbtools.src.get_repository as rep
            df = rep.mapping_bbg_mtf_from_security(security_id=[2, 8, 391, 512])
            print(df)
        """
    sec_str = ','.join([str(int(s)) for s in security_id])
    query = """
                        select distinct r.security_id,r.trading_destination_id, r.reference, r.begin_date, r.end_date, r.last_in_date
                        from KGR..HISTO_SECURITY_QUANT_REFERENCE r
                        where r.security_id in (%s)
                        and reference_type_id = 4
                        order by r.end_date 
                        """ % sec_str
    df_temp = pd.read_sql(query, con_mis)
    df_temp = df_temp.rename(columns=name_mapping)
    if date_ref != '':
        date_ref = pd.to_datetime(date_ref).date()
        df_temp = df_temp[(df_temp.begin_date <= date_ref) & (df_temp.end_date > date_ref)]
    df_temp = df_temp.sort_values(by=['security_id', 'trading_destination_id', 'end_date', 'begin_date']) \
        .drop_duplicates(['security_id', 'trading_destination_id'], keep='last')
    df_temp.set_index('security_id',inplace=True)
    return df_temp[['reference', 'trading_destination_id']]


def mapping_sector_from_security(security_id, code='gics', date_ref=''):
    """
    Mapping sector GICS, ICB from security_id in KGR..HISTO_SECURITY_QUANT_CLASSIFICATION,
    SECURITY_QUANT_CLASSIFICATION_CODE, SECURITY_QUANT_CLASSIFICATION_TYPE

    Args:
        security_id (list): list of security_id
        code (str): type of sector to return in [icb, gics, kc_sector]
        date_ref (str):date %Y%m%d, reference date

    Returns:
         DataFrame with security in index, columns are sectors

    .. exec_code::

        import dbtools.src.get_repository as rep
        sector = ['GICS_SECTOR', 'ICB_INDUSTRY', 'KC_SECTOR']
        df = rep.mapping_sector_from_security(security_id=[2, 8, 391, 512], code = '')
        print(df.columns)
        print(df[sector])
    """
    code = code.upper()
    sec_str = ','.join([str(int(s)) for s in security_id])
    # query for ICB, GICS sector
    query = """
            select distinct security_id, t.name as class_type,
                    o.name as class_name, end_date, begin_date 
            from KGR..HISTO_SECURITY_QUANT_CLASSIFICATION c, 
            KGR..SECURITY_QUANT_CLASSIFICATION_CODE o, 
            KGR..SECURITY_QUANT_CLASSIFICATION_TYPE t 
            where c.classification_code = o.classification_code 
            and c.classification_type_id = t.classification_type_id
            and o.classification_type_id = t.classification_type_id
            and security_id in (%s)
            and (t.name like 'GICS_%%' or t.name like 'ICB_%%')
            order by security_id
            """ % sec_str
    ### query for KC sector
    query_kc = """
                select c.security_id, t.name as class_type, c.classification_code as class_name,
                    end_date, begin_date
                from KGR..HISTO_SECURITY_QUANT_CLASSIFICATION c,
                KGR..SECURITY_QUANT_CLASSIFICATION_TYPE t
                where security_id in (%s)
                and c.classification_type_id = t.classification_type_id
                and c.classification_type_id in (5)
                """ % sec_str
    df_temp = pd.read_sql(query, con_mis)
    df_kc = pd.read_sql(query_kc, con_mis)
    df_temp = pd.concat([df_temp, df_kc])

    if date_ref != '':
        date_ref = pd.to_datetime(date_ref).date()
        df_temp = df_temp[(df_temp.begin_date <= date_ref) & (df_temp.end_date > date_ref)]
    df_temp = df_temp.sort_values(by=['security_id', 'class_type', 'end_date'])
    df_temp = df_temp.drop_duplicates(subset=['security_id', 'class_type'], keep='last')
    df_temp = df_temp.set_index(['security_id', 'class_type']).class_name.unstack()
    df_temp = df_temp.rename(columns={'Kepler Cheuvreux Sector': 'KC_SECTOR'})
    kech_sector = get_kc_sector()
    if not('KC_SECTOR' in df_temp.columns):
        df_temp['KC_SECTOR'] = np.nan
    df_temp.KC_SECTOR = df_temp.KC_SECTOR.fillna(-1).astype(int).map(kech_sector.Name)
    df_temp = df_temp.rename_axis(columns=None, index=None)
    df_temp.columns = df_temp.columns.str.upper()
    if code == 'GICS':
        return df_temp.loc[:, df_temp.columns.str[:4] == code]
    elif code == 'ICB':
        return df_temp.loc[:, df_temp.columns.str[:3] == code]
    elif code == 'KC_SECTOR':
        return df_temp[[code]]
    return df_temp


def mapping_security_id_from(value, code='isin', date_ref=''):
    """
    Get security_id from isin, sedol, ric or bbg
    (security_id is defined by ISIN + market + currency, so 1 ISIN can have more than 1 security_id
    so output includes primary_trading_destination_id)
    * the trailing '1' is ignored in the matching of rics 

    Args:
        value (list): list of value STRING to convert to security_id
        code (str): code to map to security_id
                in [isin, sedol, ric or bbg, KC_COMPANY_ID, FUND_TICKER]
        date_ref (str): yyyymmdd, referential date

    Returns:
        Series ou DataFrame with value in index, security is data.

    .. exec_code::

        import dbtools.src.get_repository as rep
        isin = rep.mapping_from_security(security_id = [2, 8, 391, 512], code = 'isin')
        df = rep.mapping_security_id_from(value = isin ,code = 'isin')
        print(df)
    """
    code = code.upper()
    value1 = [val + '1' for val in value] # prepare values with trailing 1 for RIC patch
    value = "','".join(value)
    value1 = "','".join(value1)
    query = """
            select security_id, %s, primary_trading_destination_id, begin_date, end_date
            from KGR..HISTO_SECURITY_QUANT
            where %s in ('%s')
            """ % (code, code, value)
            
    if code in reference_type_id.keys():
        if code == 'RIC': # ignoring trailing 1 in the RIC code
            query = """
                SELECT DISTINCT r.security_id, r.reference AS %s, r.begin_date, r.end_date
                FROM KGR..HISTO_SECURITY_QUANT_REFERENCE r 
                WHERE r.reference_type_id = %i AND (r.reference IN ('%s') OR r.reference IN ('%s'))
                """ % (code, reference_type_id[code], value, value1) 
        else:
            query = """
                SELECT DISTINCT r.security_id, r.reference AS %s, r.begin_date, r.end_date
                FROM KGR..HISTO_SECURITY_QUANT_REFERENCE r 
                WHERE r.reference_type_id = %i AND r.reference IN ('%s')
                """ % (code, reference_type_id[code], value)        
    # if code in reference_type_id.keys():
    #     query = """
    #         select distinct r.security_id, r.reference as %s, r.begin_date, r.end_date
    #         from KGR..HISTO_SECURITY_QUANT_REFERENCE r 
    #         where r.reference_type_id = %i and r.reference in ('%s')
    #             """ % (code, reference_type_id[code], value)

    df_temp = pd.read_sql(query, con_mis)
    df_temp.index = df_temp[code]
    if date_ref != '':
        date_ref = pd.to_datetime(date_ref).date()
        df_temp = df_temp[(df_temp.begin_date <= date_ref) & (df_temp.end_date > date_ref)]
    df_temp = df_temp.rename_axis(columns=None, index=None)
    if code in reference_type_id.keys():
        df_temp = df_temp.sort_values(by=[code, 'end_date', 'begin_date'])
        df_temp = df_temp.drop_duplicates(subset=[code], keep='last')
        if code == 'RIC': #remove trailing 1 from df_temp.RIC
            df_temp['RIC'] = df_temp['RIC'].str.rstrip('1')
            df_temp.index = df_temp.index.map(lambda x: str(x).rstrip("1"))
            df_temp = df_temp.drop_duplicates(subset=[code], keep='last')
        
    else:
        df_temp = df_temp.sort_values(by=['security_id', 'end_date', 'begin_date'])
        df_temp = df_temp.drop_duplicates(subset=['security_id'], keep='last')
        if code == 'ISIN':
            return df_temp[['security_id', 'primary_trading_destination_id']]
    return df_temp['security_id']


def mapping_from_td_id(td_id, code=''):
    """
    Get code from td_destination_id

    Args:
        td_id (list of int): trading destination_id
        code (str): in ['OPERATINGMIC', 'TRADING_NAME', 'GLOBALZONEID']

    Returns:
        Series or DataFrame, index is trading_destination_id

    .. exec_code::

        import dbtools.src.get_repository as rep
        df = rep.mapping_from_td_id(td_id = [1,2,5,7,17])
        print(df)
    """
    code = code.upper()
    td_id = "','".join([str(i) for i in td_id])
    query = """
            select distinct r.trading_destination_id, r.OPERATINGMIC,
                            q.trading_name as TRADING_NAME, q.GLOBALZONEID
            from KGR..EXCHANGE_REFCOMPL  r
                left join KGR..EXCHANGE_QUANT q
                on r.trading_destination_id=q.trading_destination_id
            where r.trading_destination_id in ('%s')
            """ % td_id
    df_temp = pd.read_sql(query, con_mis, index_col='trading_destination_id')
    df_temp = df_temp.rename_axis(columns=None, index=None)
    if code == '':
        return df_temp
    return df_temp[code]

def get_market_segment_by_security(security_ids, trading_dest_ids, reference_date=None):
    """
    Fetch market segments based on security IDs and trading destination IDs.
    
    Parameters
    ----------
    security_ids : list or np.ndarray
        List of security IDs.
    trading_dest_ids : list or np.ndarray
        List of trading destination IDs.
    reference_date : str or datetime or None, optional
        The reference date for filtering. If provided, it should either be a string in the format 'YYYY-MM-DD'
        or a datetime object. If not provided, the function will return the last available entry for each
        security-trading_destination combination.
        
    Returns
    -------
    DataFrame
        DataFrame containing 'security_id', 'trading_destination_id', and 'SEGMENT_MIC'.
    """
    
    # Assertions to check input types
    assert isinstance(security_ids, (list, np.ndarray)), 'security_ids should be a list or ndarray'
    assert isinstance(trading_dest_ids, (list, np.ndarray)), 'trading_dest_ids should be a list or ndarray'
    assert isinstance(reference_date, (str, datetime, type(None))), 'reference_date should be str, datetime or None'
    
    sec_ids_str = ",".join(map(str, security_ids))
    td_ids_str  = ",".join(map(str, trading_dest_ids))
    
    query = f"""
        SELECT DISTINCT security_id, trading_destination_id, SEGMENT_MIC, begin_date, end_date
        FROM KGR..HISTO_SECURITY_QUANT_MARKET
        WHERE security_id IN ({sec_ids_str})
        AND trading_destination_id IN ({td_ids_str})
        ORDER BY security_id, trading_destination_id, end_date, begin_date
    """
    
    data = pd.read_sql(query, con_mis)  # Assuming read_sql is a defined function elsewhere in your code
    
    if reference_date:
        # Convert to datetime.date if it's a string
        ref_date = datetime.strptime(reference_date, '%Y-%m-%d').date() if isinstance(reference_date, str) else reference_date.date()
        ref_date = min(ref_date, pd.Timestamp.max.date())
        
        flag_filter = (data.begin_date <= ref_date) & (data.end_date > ref_date)
        output_data = data[flag_filter]
    else:
        output_data = data.drop_duplicates(['security_id', 'trading_destination_id'], keep='last')

    return output_data[['security_id', 'trading_destination_id', 'SEGMENT_MIC']]
def get_ticker_quant_from_ticker_bbg(ticker):
    """
    Convert bloomberg ticker to quant ticker

    Args:
        ticker (list): list of tickers

    Returns:
        Series of ticker quant

    .. exec_code::

        import dbtools.src.get_repository as rep
        ticker = ['AF FP', 'ADL GY', 'ADL GR', 'BCJ SW']
        df = rep.get_ticker_quant_from_ticker_bbg(ticker = ticker)
        print(df)
    """
    query = """select BBG_EXCH_CODE, BBG_EXCH_MAP
                from KGR..EXCHANGE_QUANT
                where EXCHANGETYPE = 'M'"""
    df_temp = pd.read_sql(query, con_mis, index_col='BBG_EXCH_CODE').replace({None: np.nan})
    exch = df_temp.BBG_EXCH_MAP.dropna()
    keys = exch.str.split(',').sum()
    values = exch.index.repeat(exch.str.count(',') + 1)

    dict_map = pd.Series(index=keys, data=values)

    ticker = pd.Series(ticker)
    suffix = ticker.str[-2:].replace(dict_map)
    tikcer_quant = ticker.str[:-2] + suffix
    return tikcer_quant


def get_trading_information():
    """
    Get trading information from KGR..EXCHANGE_QUANT

    Returns:
         DataFrame

    .. exec_code::

        import dbtools.src.get_repository as rep
        df = rep.get_trading_information()
        print(df.head())
    """
    query = """select trading_destination_id,trading_name,GLOBALZONEID,EXCHANGETYPE,BBG_EXCH_CODE,BBG_EXCH_MAP,BookingCotationPlace,OPERATING_MIC
                from KGR..EXCHANGE_QUANT
                where EXCHANGETYPE in ('M', 'F')"""
    df_temp = pd.read_sql(query, con_mis).replace({None: np.nan})
    df_temp.index = df_temp.trading_destination_id.values
    df_temp.loc[288, 'BBG_EXCH_CODE'] = 'I2'
    df_temp.loc[288, 'BBG_EXCH_CODE'] = 'QE'
    df_temp.loc[289, 'BBG_EXCH_CODE'] = 'T1'

    df_temp = df_temp.ffill(axis=1, limit=1).dropna()

    exch = df_temp.BBG_EXCH_MAP.dropna()
    bbg_exch_map = exch.str.split(',').sum()
    count = exch.str.count(',') + 1

    trading_info = pd.DataFrame()
    for col in df_temp.columns:
        trading_info[col] = df_temp[col].repeat(count)
    trading_info.BBG_EXCH_MAP = bbg_exch_map
    return trading_info

def get_fx_histo(ccy, start_date, end_date, ccy_ref='EUR'):
    """
    Get historical FX rate against basic curreny from KGR..HISTOCURRENCYTIMESERIES to DataFrame

    Args:
        ccy (list of str): list of foreign currency names
        start_date (str): format %Y%m%d
        end_date (str): format %Y%m%d
        ccy_ref (str):EUR or USD, by default EURO

    Returns:
        DataFrame with date in index, currency in columns

    .. exec_code::

        import dbtools.src.get_repository as rep
        df = rep.get_fx_histo(ccy = ['EUR', 'USD', 'SEK', 'GBX'], start_date = '20220901', end_date = '20220902', ccy_ref='EUR')
        print(df)
    """
    ccy = "','".join([str(i) for i in ccy])
    query = """
            select CCY, CCYREF, DATE, VALUE
            from KGR..HISTOCURRENCYTIMESERIES
            where DATE >='%s' and DATE<='%s'
            and CCY in ('%s')
            and CCYREF='%s'
            order by DATE
            """ % (start_date, end_date, ccy, ccy_ref)

    df_temp = pd.read_sql(query, con_mis, index_col='DATE')
    df_temp = df_temp.rename_axis(columns=None, index=None)
    df_temp = df_temp.set_index('CCY', append=True).VALUE.unstack()
    return df_temp

def get_euronext_mcap(security_id, start_date, end_date):
    """
    Get market cap from Euronext. Only wekkly data on fridays are available
    
    Args:
        security_id (list, array): list of security_id
        start_date (str, %Y%m%d): start date
        end_date (str, %Y%m%d): end date
    
    Return:
        Dataframe with columns 'security_id', 'date', 'ff_mcap_eur', 'nb_shares', 'float_pct'

        .. exec_code::

            import dbtools.src.get_repository as rep
            df = rep.get_euronext_mcap([2, 3, 8], start_date = '20220901', end_date = '20220920')
            print(df)
    """
    sec_str = ','.join(str(x) for x in security_id)
    req = f"""select upd_date as date, security_id, ff_mcap_eur, shares as nb_shares, float_pct
                from KGR..ff_market_cap
                where source = 'EURONEXT'
                and security_id in ({sec_str})
                and upd_date>='{start_date}' and upd_date <='{end_date}'
                order by upd_date"""
    df_mcap = pd.read_sql(req, con_mis)
    df_mcap.date = pd.to_datetime(df_mcap.date)
    
    sec_miss = pd.Index(security_id) .difference(df_mcap.security_id.unique())
    if not sec_miss.empty:
        print('!!!!! Missing data of securities: %s !!!!'%(list(sec_miss)))
    return df_mcap

def get_kc_coverage_histo(date_ref=''):
    """
    Get Data coverage of KC from API by mapping with KGR..HISTO_SECURITY_QUANT_CLASSIFICATION

    Args:
        date_ref (str): date %Y%m%d, reference date

    Returns:
        DataFrame

    .. exec_code::

        import dbtools.src.get_repository as rep
        df = rep.get_kc_coverage_histo()
        print(df)
    """
    query = """select distinct security_id, begin_date, end_date,
                    classification_type_id as type, classification_code as code
                from KGR..HISTO_SECURITY_QUANT_CLASSIFICATION 
                where classification_type_id in (5, 6) 
                order by security_id, classification_type_id , end_date, begin_date"""

    df_temp = pd.read_sql(query, con_mis)

    if date_ref != '':
        date_ref = pd.to_datetime(date_ref).date()
        df_temp = df_temp[(df_temp.begin_date <= date_ref) & (df_temp.end_date > date_ref)]#(df_temp.begin_date <= date_ref) &

    df_temp = df_temp.sort_values(by=['security_id', 'type', 'end_date']) \
        .drop_duplicates(['security_id', 'type'], keep='last')
    df_group_date = df_temp.groupby(['security_id'])
    df_date = pd.concat((df_group_date.begin_date.min(), df_group_date.end_date.max()), axis = 1).reset_index()
    df_temp = df_temp[['security_id', 'type', 'code']]
    df_temp = df_temp.set_index(['security_id', 'type']).code.unstack()
    df_temp.columns = ['sectorID', 'analystID']
    df_temp['COMPANYID'] = mapping_from_security(security_id=df_temp.index, code='KC_COMPANY_ID')
    df_temp = df_temp.astype(int)
    df_temp = df_temp.reset_index()
    df_temp.index = df_temp.security_id

    kech_company = get_kc_company()
    kech_sector = get_kc_sector()
    kech_analyst = get_kc_analyst()
    kech_analyst['ANALYST'] = kech_analyst.FirstName + ' ' + kech_analyst.LastName

    kc_coverage = df_temp.merge(kech_sector.Name, how='left', left_on='sectorID', right_index=True)
    kc_coverage = kc_coverage.merge(kech_analyst.ANALYST, how='left', left_on='analystID', right_index=True)
    kc_coverage = kc_coverage.merge(kech_company, how='left', left_on='COMPANYID', right_index=True)
    kc_coverage = kc_coverage.rename(columns={'Name': 'SECTOR'})
    kc_coverage = kc_coverage.drop(columns=['sectorID', 'analystID'])
    kc_coverage = kc_coverage.rename_axis(index=None, columns=None)
    
    kc_coverage = kc_coverage.merge(df_date, how = 'left')
    kc_coverage.index = kc_coverage.security_id
    return kc_coverage


def get_flag_EEA(security_id):
    """
    Get flag 1 for EEA stocks, 0 for others
    takes a hard-coded ISIN prefix list to determine EEA 

    Args:
        security_id: list of security_id

    Returns:
        DataFrame of flag
            1: EEA, 0: non EEA

    .. exec_code::

        import dbtools.src.get_repository as rep
        df = rep.get_flag_EEA([110, 276])
        print(df.head())
    """
    EEA_prefix = ["AT","BE","BG","CY","CZ","DE","DK","EE","ES","FI",
                      "FR","GR","HR","HU","IE","IS","IT","LI","LT","LU","LV","MT","NL",
                      "NO","PL","PT","RO","SE","SI","SK"]
        
    df = mapping_from_security(security_id, code='isin', date_ref='')
    df = df.to_frame()
    
    def check_EEA_prefix(isin):
        return 1 if isin[:2] in EEA_prefix else 0
    
    df['flag_EEA'] = df['ISIN'].apply(check_EEA_prefix)
    df = df.drop('ISIN',axis=1)
    return df 

def get_flag_lms(start_date=datetime.today().strftime('%Y%m%d'),
                 end_date=datetime.today().strftime('%Y%m%d'),
                 shift = False):
    """
    Get flag LMS from KGR..HISTO_INDEXCOMPONENT_QUANT.
        - Large: 0
        - Mid: 1
        - Small: 2

    Args:
        start_date (str): %Y%m%d, by default today
        end_date (str): %Y%m%d, by default today

    Returns:
        DataFrame of flag
            0: Large, 1: Mid, 2: Small, -1 otherwise

    .. exec_code::

        import dbtools.src.get_repository as rep
        df = rep.get_flag_lms(start_date='20220901', end_date = '20220902')
        print(df.head())
    """
    df_lcxp = get_index_comp('LCXP', start_date, end_date, shift).notna() * 1
    df_mcxp = get_index_comp('MCXP', start_date, end_date, shift).notna() * 2
    df_scxp = get_index_comp('SCXP', start_date, end_date, shift).notna() * 3

    col_lms = df_lcxp.columns.union(df_mcxp.columns).union(df_scxp.columns)
    index_lms = df_lcxp.index.union(df_mcxp.index).union(df_scxp.index)

    ### Extend columns of Dataframes df_lcxp, df_mcxp, df_scxp, fill by 0
    df_lcxp = df_lcxp.reindex(columns=col_lms, index = index_lms).fillna(0)
    df_mcxp = df_mcxp.reindex(columns=col_lms, index = index_lms).fillna(0)
    df_scxp = df_scxp.reindex(columns=col_lms, index = index_lms).fillna(0)

    df_lms = (df_lcxp + df_mcxp + df_scxp) - 1
    return df_lms


def get_growth_value(start_date=datetime.today().strftime('%Y%m%d'),
              end_date=datetime.today().strftime('%Y%m%d'),
              shift = False):
    """
    Get style of investment: Growth (STGP) or Value (STVP)
        - Growth: 1
        - Value: 0
        - Other: -1

    Args:
        start_date (str): %Y%m%d, by default today
        end_date (str): %Y%m%d, by default today

    Returns:
        DataFrame of flag
            1: Growth, 0: Value, -1: Other

    .. exec_code::

        import dbtools.src.get_repository as rep
        df = rep.get_growth_value(start_date='20220901', end_date = '20220902')
        print(df.head())
    """
    
    df_growth = get_index_comp(index_ticker = 'STGP', start_date = start_date, end_date = end_date, shift = shift)
    df_growth = df_growth.notna()*2
    df_value = get_index_comp(index_ticker = 'STVP', start_date = start_date, end_date = end_date, shift = shift)
    df_value = df_value.notna()*1
    
    columns = df_growth.columns.union(df_value.columns)
    index = df_growth.index.union(df_value.index)
    
    df_growth = df_growth.reindex(columns = columns, index = index).fillna(0)
    df_value = df_value.reindex(columns = columns, index = index).fillna(0)
    
    df_style = (df_growth + df_value) - 1
    if df_style.max().max()>1:
        df_check = df_style[df_style>1].stack().reset_index()
        df_check.columns = ['date','security_id', 'style']
        df_check = df_check.drop_duplicates('security_id')
        check_sec = df_check.security_id.values
        check_req = """select DATE, count(security_id) nb_security
                        from KGR..HISTO_INDEXCOMPONENT_QUANT
                        where INDEXCODEBLOOMBERG in ('STGP', 'STVP')
                        and security_id in (%s)
                        and DATE>='%s' and DATE<='%s'
                        group by DATE
                        order by nb_security desc
                        """ %(check_sec[0], start_date, end_date)
        print('!!!!!!!!! Check style of security_id: %s!!!!!!!!!' % df_check.security_id.values)
        print('1 security_id CANNOT in both Growth and Value')
        print('Example of request to check: \n %s' %check_req)
    return df_style

def get_kc_coverage_screening(prod_date=datetime.today().strftime('%Y%m%d'),
                                   index_ticker='SXXP', security_id=[]):
    """
    Get KC coverage screening

    Args:
        prod_date (str): %Y%m%d, date for screening
        index_ticker (str): ticker bbg of bloom
        security_id (list): list of additional securities

    Returns:
         DataFrame, Analysis of KC, sector ICB, GICS, Flag LSM, and their identifiers (ISIN, SEDOL,...)

    .. exec_code::

        import dbtools.src.get_repository as rep
        df = rep.get_kc_coverage_screening(prod_date ='20220901', index_ticker = 'SXXP')
        print(df)
    """
    start_date = pd.to_datetime(prod_date) - pd.to_timedelta(7, unit='D')
    end_date = pd.to_datetime(prod_date) + pd.to_timedelta(7, unit='D')
    start_date = datetime.strftime(start_date, '%Y%m%d')
    end_date = datetime.strftime(end_date, '%Y%m%d')

    kc_coverage = get_kc_coverage_histo(prod_date)[['security_id', 'COMPANYID', 'SECTOR', 'ANALYST']]
    kc_coverage.index.name = None
    df_ticker = get_index_comp(index_ticker, start_date, end_date, shift = True)
    df_lms = get_flag_lms(start_date, end_date, shift = True)
    flag_lms = df_lms.loc[prod_date].dropna()

    sec_ticker = df_ticker.loc[prod_date].dropna().index
    sec_coverage = kc_coverage.index
    sec_id = sec_ticker.union(sec_coverage).union(security_id)

    cols = {'CCY': 'CURRENCYID', 'SECNAME': 'NAME',
            'bbg': 'BLOOMBERGCODE', 'fund_ticker': 'EQY_FUND_TICKER'}

    quant_data = pd.DataFrame(index=sec_id)
    quant_data['security_id'] = sec_id
    for code in ['ISIN', 'PRIM_TD_ID', 'CCY', 'SECNAME', 'begin_date', 'end_date', 'last_in_date', 'CFICODE']:
        quant_data[code] = mapping_from_security(sec_id, code=code)
    quant_reference = mapping_from_security(sec_id, code='bbg')
    coverage_ticker = pd.concat([quant_data, quant_reference], axis=1)

    coverage_ticker['flag_lms'] = flag_lms.reindex(sec_id).fillna(-1)
    coverage_ticker['fund_ticker'] = mapping_from_security(sec_id, code='fund_ticker')
    coverage_ticker['bbg'] = mapping_from_security(sec_id, code='bbg')

    coverage_gics = mapping_sector_from_security(coverage_ticker.index, '')
    
    coverage = coverage_ticker.merge(coverage_gics, left_on='security_id', right_index = True, how ='left')

    
    coverage = coverage.merge(kc_coverage, on='security_id', how ='left')
    coverage['flag_kc'] = coverage.security_id.isin(kc_coverage.index) * 1

    fund_doublon = coverage[coverage['fund_ticker'].duplicated(keep=False)]\
                    .sort_values('fund_ticker')
    fund_keep = pd.DataFrame()
    for fund in fund_doublon.fund_ticker.unique():
        fund_dup = fund_doublon[fund_doublon.fund_ticker == fund]
        fund_in_kc = fund_dup[fund_dup.flag_kc == 1]
        fund_out_kc = fund_dup[fund_dup.flag_kc != 1]
        if len(fund_in_kc) > 1:
            fund_in_kc = fund_in_kc.sort_values(['flag_lms', 'end_date']).drop_duplicates(keep='last')
        elif len(fund_in_kc) == 0:
            fund_in_kc = fund_dup.sort_values(['flag_lms', 'end_date']).drop_duplicates(keep='last')
        else:
            idx = fund_in_kc.index[0]
            fund_in_kc.loc[idx, 'security_id'] = fund_out_kc.security_id.iloc[0]
            fund_in_kc.loc[idx, 'flag_lms'] = fund_out_kc.flag_lms.iloc[0]
            fund_in_kc.index = fund_out_kc.security_id.values
        fund_keep = pd.concat([fund_keep, fund_in_kc])
    coverage = coverage.drop_duplicates(subset=['fund_ticker'], keep=False)
    coverage = pd.concat([coverage, fund_keep])
    coverage = coverage.drop(columns=coverage.columns.intersection(
                ['begin_date', 'end_date', 'last_in_date', 'reference', 'CFICODE']))
    coverage.index = coverage.security_id.values
    coverage = coverage.rename(columns=cols).sort_index()
    coverage = coverage[~coverage.PRIM_TD_ID.isin([309, 310, 312])]# exlude Bucharest, Abu Dhabi, Saudi Stock Exchange
    return coverage

def get_kc_plus_index_comp(index_ticker, start_date, end_date, td_excl = [309, 310, 312]):
    """
    Get union composition of KC coverage and index ticker
    
    Task:
        1. Loading composition index and KC coverage
        2. Union of 2 universes based of fund ticker. If there are duplicated tickers in KC and index member, ticker un index is priority.
        3. Data cleaning: Exclude securities whose primary td_id in parameter td_excl, and delete empty columns
    
    Args:
        start_date (str): %Y%m%d
        end_date (str): %Y%m%d
        index_ticker (str): Bloomberg index ticker, default SXXP
        td_excl (list of int): primary trading to be exclued,
                        default [309, 310, 312]: Bucharest, Abu Dhabi,, Saudi Stock Exchange
    Returns:
        df_union (df): Data Frame in timeseries format, with 1 or 0
    
    .. exec_code::

        import dbtools.src.get_repository as rep
        df = rep.get_kc_plus_index_comp('SXXP', '20240520','20240528')
        print(df)
    """
     # Composition index
    df_idx = get_index_comp(index_ticker, start_date, end_date, shift = True).notna()*1
    df_idx = df_idx[df_idx!=0]
    dates_idx = df_idx.index
    
    # composition KC coverage
    query = """select distinct security_id, begin_date, end_date,
                    classification_type_id as type, classification_code as code
                    from KGR..HISTO_SECURITY_QUANT_CLASSIFICATION 
                    where classification_type_id in (5)
                    order by security_id, end_date, begin_date"""

    df_temp = pd.read_sql(query, con_mis)
    df_temp.begin_date = pd.to_datetime(df_temp.begin_date)
    df_temp.end_date = df_temp.end_date.apply(pd.Timestamp)
    df_temp.index = df_temp.security_id.values

    securities = df_temp.index.unique()
    df_kc = pd.DataFrame(index = dates_idx, columns = securities)
    for date in df_kc.index:
         sec_id = df_temp[(df_temp.begin_date <= date) & (df_temp.end_date > date)].index.drop_duplicates()
         df_kc.loc[date, sec_id] = 1

    # Union 2 universes, priority for index member
    df_union = pd.DataFrame(index = dates_idx, columns = df_kc.columns.union(df_idx.columns))
    ticker_kc = mapping_from_security(df_kc.columns,'fund_ticker')
    ticker_idx = mapping_from_security(df_idx.columns,'fund_ticker')
    for date in df_kc.index:
        sec_id_kc = df_kc.loc[date].dropna().index
        sec_id_idx = df_idx.loc[date].dropna().index

        ticker_union = pd.concat((ticker_kc.loc[sec_id_kc], ticker_idx.loc[sec_id_idx]))
        
        sec_universe = ticker_union.drop_duplicates(keep = 'last').index
        df_union.loc[date, sec_universe] = 1
    
    prim_td = mapping_from_security(securities, 'prim_td_id')
    sec_excl = prim_td[prim_td.isin([309,310,311, 312])].index
    df_union = df_union[df_union.columns.difference(sec_excl)]
    df_union = df_union.dropna(how = 'all', axis = 1)
    return df_union

def mapping_index_from_ticker(ticker, code = 'index_id'):
    """
    From index ticker get index informations:
        Index id
        Name
        Short name
        ISIN
        Currency
        Region
        Version (price, gross)
        
    Args:
        ticker (list, array, series): list of tickers
        code (str): information to retrieve in ['index_id', 'name', 'short_name', 'isin', 'region', 'ccy']
    
    Returns:
        df (serie): ticker in index, value is the given code
    
    .. exec_code::

        import dbtools.src.get_repository as rep
        ticker = ['CAC', 'SBF120', 'SXXP', 'BEL20']

        df = rep.mapping_index_from_ticker(ticker, code = 'index_id').head(3)
        print(df)
    """
    list_ticker = ",".join(["'%s'"%s for s in ticker])
    df = pd.read_sql('''
                select * from QUANT_work..INDEX_REFERENTIAL
                where ticker in (%s)
                ''' %list_ticker,
                con_mis)
    df.columns = df.columns.str.lower()
    df = df.rename(columns= {'id':'index_id'})
    df = df.set_index('ticker')
    df = df[code]
    return df


def mapping_from_index(index_id, code = 'ticker'):
    
    """
    From QUANT index_id get index informations:
        ticker
        Name
        Short name
        ISIN
        Currency
        Region
        Version (price, gross)
        
    Args:
        index_id (list, array, series): list of index_id (int)
        code (str): information to retrieve in ['ticker', 'name', 'short_name', 'isin', 'region', 'ccy']

    
    Returns:
        df (serie): ticker in index, value is the given code
    
    .. exec_code::

        import dbtools.src.get_repository as rep
        index_id = [1, 2 ,3 ,4, 5]

        df = rep.mapping_from_index(index_id, code = 'ticker').head(3)
        print(df)
    
    """
    list_ticker = ",".join([str(int(s)) for s in index_id])
    df = pd.read_sql('''
                select * from QUANT_work..INDEX_REFERENTIAL
                where index_id in (%s)
                ''' %list_ticker,
                con_mis)
    df.columns = df.columns.str.lower()
    df = df.set_index('index_id')
    df = df[code]
    
    return df


#%% mapping names
def mapping_country_from_security_id(security_id):
    """
    Mapping group country from security_id
    
    Args:
        security_id: list of security_id
        code (str): 'tfm': for country group in tfm project
                    'name': for country name
    
    Returns:
        series: index is security_id, value is country name/group
        
    .. exec_code::

        import dbtools.src.get_repository as rep
        security_id = [2,  8, 17, 18, 23, 24, 25, 26, 31, 41]

        df = rep.mapping_country_from_security_id(security_id).head(5)
        print(df)
    """
    security_id = np.unique(security_id)
    df_prim_trading = mapping_from_security(security_id, code = 'prim_td_id').to_frame('td_id')
    df_trading = get_trading_information()[['trading_destination_id', 'trading_name']].drop_duplicates(keep = 'last')
    df_trading.index = df_trading.trading_destination_id
    df_trading = df_trading.trading_name
    df_prim_trading['trading_name'] = df_prim_trading.td_id.map(df_trading)
    df_prim_trading['country'] = df_prim_trading.trading_name.map(country_name)
    df_country = df_prim_trading['country']
    return df_country

def mapping_zone_from_security_id(security_id):
    """
    Mapping country zone from security_id
    
    Args:
        security_id: list of security_id
        code (str): 'tfm': for country group in tfm project
                    'name': for country name
    
    Returns:
        series: index is security_id, value is country name/group
        
    .. exec_code::

        import dbtools.src.get_repository as rep
        security_id = [2,  8, 17, 18, 23, 24, 25, 26, 31, 41]

        df = rep.mapping_zone_from_security_id(security_id).head(5)
        print(df)
    """
    security_id = np.unique(security_id)
    df_prim_trading = mapping_from_security(security_id, code = 'prim_td_id').to_frame('td_id')
    df_trading = get_trading_information()[['trading_destination_id', 'GLOBALZONEID']].drop_duplicates(keep = 'last')
    df_trading.index = df_trading.trading_destination_id
    df_trading = df_trading.GLOBALZONEID
    df_prim_trading['GLOBALZONEID'] = df_prim_trading.td_id.map(df_trading)
    df_prim_trading['ZONE'] = df_prim_trading.GLOBALZONEID.map(zone_name)
    df_zone = df_prim_trading['ZONE']
    return df_zone

def add_dynamic_stock_style(df_data):
    """
    Add dynamic stock style growth/value
    Args:
        df_data (df): df in database format, contains columns 'date', 'security_id'
    
    Returns:
        df with additional column style_gv

    .. exec_code::

        import dbtools.src.get_repository as rep
        import dbtools.src.get_kc_flows as gkf
        execution_close = gkf.run_kech_flows(start_date = '20230616', end_date = '20230619',
                                         mode='execution', perimeter = 'SXXP',
                                         filter_management=True, filter_research=True)

        df = execution_close.drop_duplicates('security_id')[['date', 'security_id', 'amount_eur']]
        df = rep.add_dynamic_stock_style(df)
        df = rep.add_dynamic_stock_capi(df)
        df = rep.add_stock_country(df)
        print(df.sample(10))
    """

    
    start_date = df_data.date.min().strftime('%Y%m%d')
    
    end_date = df_data.date.max().strftime('%Y%m%d')
    end_date = pd.date_range(start=end_date, periods = 7)[-1]
    end_date = end_date.strftime('%Y%m%d')


    
    df_style = get_growth_value(start_date, end_date, shift = True)
    df_style = df_style.stack().reset_index()
    df_style.columns = ['date', 'security_id', 'style_gv']
    
    
    df_ = df_data.merge(df_style, on = ['date', 'security_id'], how = 'left').fillna(-1)
    df_.style_gv = df_.style_gv.replace(style_gv_map)
    return df_.copy()


def add_dynamic_stock_capi(df_data):
    """
    Add dynamic stock capitalisation (Large, Mid, Small, Smallest)
    Args:
        df_data (df): df in database format, contains columns 'date', 'security_id'
    
    Returns:
        df with additional column capi
    
    .. exec_code::

        import dbtools.src.get_repository as rep
        import dbtools.src.get_kc_flows as gkf
        execution_close = gkf.run_kech_flows(start_date = '20230616', end_date = '20230619',
                                         mode='execution', perimeter = 'SXXP',
                                         filter_management=True, filter_research=True)

        df = execution_close.drop_duplicates('security_id')[['date', 'security_id', 'amount_eur']]
        df = rep.add_dynamic_stock_style(df)
        df = rep.add_dynamic_stock_capi(df)
        df = rep.add_stock_country(df)
        print(df.sample(10))
    """

    start_date = df_data.date.min().strftime('%Y%m%d')
    
    end_date = df_data.date.max().strftime('%Y%m%d')
    end_date = pd.date_range(start=end_date, periods = 7)[-1]
    end_date = end_date.strftime('%Y%m%d')

    df_capi = get_flag_lms(start_date, end_date, shift = True)
    df_capi = df_capi.stack().reset_index()
    df_capi.columns = ['date', 'security_id', 'capi']

    df_ = df_data.merge(df_capi, on = ['date', 'security_id'], how = 'left')
    df_.capi = df_.capi.fillna(-1)
    df_.capi = df_.capi.replace(capi_map)

    return df_.copy()

def add_stock_sector(df_data, sector = 'GICS'):
    """
    Add stock GICS_SECTOR / ICB_INDUSTRY
    Args:
        df_data (df): df in database format, contains column 'security_id'
    
    Returns:
        df with additional column sector
    
    .. exec_code::

        import dbtools.src.get_repository as rep
        import dbtools.src.get_kc_flows as gkf
        execution_close = gkf.run_kech_flows(start_date = '20230616', end_date = '20230619',
                                         mode='execution', perimeter = 'SXXP',
                                         filter_management=True, filter_research=True)

        df = execution_close.drop_duplicates('security_id')[['date', 'security_id', 'amount_eur']]
        df = rep.add_dynamic_stock_style(df)
        df = rep.add_dynamic_stock_capi(df)
        df = rep.add_stock_sector(df)
        print(df.sample(10))
    """

    security_id = df_data.security_id.drop_duplicates()

    df_sector = mapping_sector_from_security(security_id, code = sector)
    if sector == 'GICS':
        df_sector = df_sector['GICS_SECTOR'].replace(sector_map)
    elif sector == 'ICB':
        df_sector = df_sector['ICB_INDUSTRY'].replace(sector_map)
    
    df_ = df_data.copy()
    df_['sector'] = df_.security_id.map(df_sector)
    df_['sector'] = df_['sector'].fillna('Unknown')
    
    return df_.copy()



def add_stock_country(df_data):
    """
    Add stock country
    Args:
        df_data (df): df in database format, contains column 'security_id'
    
    Returns:
        df with additional column country
    
    .. exec_code::

        import dbtools.src.get_repository as rep
        import dbtools.src.get_kc_flows as gkf
        execution_close = gkf.run_kech_flows(start_date = '20230616', end_date = '20230619',
                                         mode='execution', perimeter = 'SXXP',
                                         filter_management=True, filter_research=True)

        df = execution_close.drop_duplicates('security_id')[['date', 'security_id', 'amount_eur']]
        df = rep.add_dynamic_stock_style(df)
        df = rep.add_dynamic_stock_capi(df)
        df = rep.add_stock_country(df)
        print(df.sample(10))
    """

    security_id = df_data['security_id'].unique()
    df_country = mapping_country_from_security_id(security_id)

    df_ = df_data.copy()
    df_['country'] = df_.security_id.map(df_country)
    return df_.copy()

def extract_dynamic_compo_data(df, index_ticker='BKXP', df_format = 'db'):
    """
    Extract index dynamic data
    
    Task:
        1. Identify start_date and end_date (transform df to db format if necessary)
        2. Load dynamic index component
    Args:
        df (df): data
        df_format (df) 'ts'/'db'
            if ts: security_id en columns, 'date' en index
            if db: 3 columns named 'security_id', 'date' and 'value'
        index_ticker (str): bloomberg index ticker
    Returns:
        df_extract (df): Dynamic data of index_ticker in given df_format
    
    Example:
        .. code-block:: python
        
        import dbtools.src.get_repository as rep
        import dbtools.src.get_market_data as mkt

        index_ticker='CAC'
        df_compo = rep.get_index_comp('CAC', start_date='20240101', end_date='20240331', shift = True)
        df = mkt.get_trading_daily(security_id=df_compo.columns, start_date='20240101', end_date='20240331', field=['turnover'])['turnover']['MAIN']

        df_extract_ts = rep.extract_dynamic_compo_data(df, 'CAC', 'ts')
    """
    if df_format=='ts':
        df_data = df.stack().reset_index()
        df_data.columns = ['date', 'security_id', 'value']
    else:
        df_data = df.copy()
        df_data.columns = ['date', 'security_id', 'value']
    
    start_date = df_data.date.min()
    end_date = df_data.date.max()

    df_compo = get_index_comp(index_ticker, start_date, end_date, shift = True)
    df_compo_db = df_compo.stack().dropna().reset_index()
    df_compo_db.columns = ['date', 'security_id', 'value']

    df_extract = df_data.merge(df_compo_db[['date', 'security_id']])
    if df_format=='ts':
        df_extract = df_extract.set_index(['date', 'security_id']).value.unstack()
    
    return df_extract

def add_client_country_name(df_data, iso_column = 'IdCountry'):
    if not (iso_column in df_data.columns):
        print(f'!!!! {iso_column} not in df_data !!!!')
        return df_data
    df_data['client_country'] = df_data[iso_column].map(country_iso)
    return df_data







