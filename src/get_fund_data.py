# -*- coding: utf-8 -*-
"""
Created on Mon Nov 18 14:57:29 2024

@author: nle
"""

import os
import pandas as pd
from dbtools.src.db_connexion import SqlConnector
import dbtools.src.get_repository as rep
import dbtools.src.DatabaseManager as db
from pandas.tseries.offsets import BDay
import dbtools.src.dbtools_util as du
import numpy as np
import warnings
from copy import deepcopy
import json
# from copy import deepcopy
connector = SqlConnector()
con_mis = connector.connection()

config_file_name = 'default_fund_data_spec.json'
script_dir = os.path.dirname(os.path.abspath(__file__))
parent_dir = os.path.dirname(script_dir)
config_dir = os.path.join(parent_dir, 'config')
config_path = os.path.join(config_dir, config_file_name)
#%%

def resample_data_ts(df_data_ts, frequency = 'friday', nb_filldays = None):
    if (nb_filldays in [None, 0]) and (frequency in [None, '']):
        return df_data_ts
    # resample daily, including week-end
    df_weekday = pd.date_range(df_data_ts.index.min(), df_data_ts.index.max())
    df_data_wd = df_data_ts.reindex(index = df_weekday, method = None)
    # ffill if nb_filldays is given
    if nb_filldays not in [None, 0]:
        df_data_res = df_data_wd.ffill(limit = nb_filldays)
    # Resample according to the input frequency.
    # Fonction df.resample(freq).last(): auto ffills till the given frequency, no need to give nb_filldays
    if frequency=='weekday':
        df_data_res = df_data_wd.copy()
        df_data_res = df_data_wd[df_data_wd.index.weekday<=4]
    elif frequency=='allday':
        df_data_res = df_data_wd.copy()
    # weekly
    elif frequency in ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']:
        freq = 'W-%s'%(frequency.upper()[:3])
        df_data_res = df_data_ts.resample(freq).last()
        df_data_res = df_data_res[df_data_res.index<=df_data_ts.index.max()]
    # eom
    elif frequency=='eom':
        df_data_res = df_data_ts.resample('BME').last()
    # friday_plus_last_date
    elif frequency=='friday_plus_last_date':
        freq = 'W-FRI'
        df_data_res = df_data_ts.resample(freq).last()
        df_data_res = df_data_res[df_data_res.index<=df_data_ts.index.max()]
        if not (df_data_ts.index.max() in df_data_res.index):
            df_last_date = df_data_ts[df_data_ts.index == df_data_ts.index.max()]
            df_data_res = pd.concat((df_data_res, df_last_date)).sort_index()
    
    return df_data_res.copy()
def check_fiscal_year_missing(df_fy, df_eps=pd.DataFrame()):
    if not df_eps.empty:
        s_first_year = df_eps.stack().reset_index()
        s_first_year.columns = ['Dates', 'ids', 'first_date']
        s_first_year = s_first_year.groupby('ids').Dates.min().dt.year
    else:
        s_first_year = pd.Series(index = df_fy.columns, data = 1998)
    
    common_ids = df_eps.columns.intersection(df_fy.columns)
    df_fy_ts = df_fy[common_ids].stack().reset_index()
    df_fy_ts.columns = ['DATE', 'ids','year']
    #df_fy_ts = df_fy_ts.set_index(['year', 'ids']).DATE.unstack()
    df_fy_ts = df_fy_ts.set_index(['year', 'ids']).DATE.dt.year.unstack()
    max_date = df_eps.index.max()
    common_year = df_fy_ts.index.union([max_date.year])
    df_fy_ts = df_fy_ts.reindex(common_year)
    df_fy_ts.index = df_fy_ts.index.astype(int)
    
    ymin = df_fy_ts.index.min()
    ymax = df_fy_ts.index.max()+1
    index = range(ymin, ymax)
    df_year_fill = pd.DataFrame(index = index, columns = df_fy_ts.columns, data = np.array([index]*len(df_fy_ts.columns)).T)

    df_not_avb = ((df_year_fill>s_first_year-1)*1).replace({0:'not avb', 1:np.nan})
    df_first_year = ((df_year_fill==s_first_year)*1).replace({1:'first year', 0:np.nan})

    df_fy_na = (df_fy_ts.isna()*1).replace({1:'MISSING', 0:np.nan})
    
    df_missing = df_first_year.fillna(df_not_avb).fillna(df_fy_na).fillna(df_fy_ts)
    df_missing = df_missing[df_missing.isin(['MISSING'])]
    df_missing = df_missing.dropna(how = 'all')
    return df_missing, s_first_year

def correct_fiscal_year(df_fy, df_1y, df_2y):
    common_date = df_1y.index.union(df_2y.index).union(df_fy.index)
    common_ids = df_1y.columns
    sec_2y_miss = common_ids.difference(df_2y.columns)
    sec_fy_miss = common_ids.difference(df_fy.columns)
    df_fy_miss, s_first_year = check_fiscal_year_missing(df_fy, df_1y)
    
    actual_year = pd.to_datetime('today').year
    s_actual_year = pd.Series(index = df_fy.columns, data = actual_year)
    
    sec_act_y_miss = df_fy_miss.loc[df_fy_miss.index == actual_year].stack().reset_index()['ids']
    
    if not df_fy_miss.empty:
        print('!!!!!! Missing fiscal year for ids: !!!!!!!\n%s'%df_fy_miss)
    if len(sec_fy_miss)>0:
        print('!!!!!! Missing fiscal data for ids: %s !!!!!!!'%sec_fy_miss)
    if len(sec_2y_miss)>0:
        print('!!!!!! Missing 2Y data for ids: %s !!!!!!!'%sec_2y_miss)

    df_1y_reidx = df_1y.reindex(index = common_date, columns = common_ids, method = None)
    df_2y_reidx = df_2y.reindex(index = common_date, columns = common_ids, method = None)

    df_fy1_fill = df_fy.reindex(common_date, columns = common_ids).ffill(limit=None)
    df_fy2_fill = df_fy.reindex(common_date, columns = common_ids).shift(-1).bfill(limit=None)
    
    df_fy2_fill.loc[df_fy2_fill.index.year == actual_year, sec_act_y_miss] = df_fy2_fill.loc[df_fy2_fill.index.year == actual_year, sec_act_y_miss].fillna(s_actual_year)

    df_year_fill = pd.DataFrame(index = common_date, columns = common_ids, data = np.array([common_date.year]*len(common_ids)).T)
    
    
    #df_first_year = (df_year_fill-s_first_year)==0

    df_fy1_idx = df_fy1_fill[df_fy1_fill==df_year_fill].notna()# + df_first_year
    df_fy2_idx = df_fy2_fill[df_fy2_fill==df_year_fill].notna()

    df_fy1_idx[sec_fy_miss] = False


    df_1y_keep = df_1y_reidx[df_fy1_idx]
    df_2y_keep = df_2y_reidx[df_fy2_idx]

    df_1y_corr = df_1y_keep.fillna(df_2y_keep)
    
    df_1y_corr = df_1y_corr.reindex(index = df_1y.index)
    return df_1y_corr


class GetFundData():
    def __init__(self, ids, start_date, end_date, attribute_name = ['eps_1gy'],
                 security_type = 'index', config_path=config_path, correct_fy = 'default'):
        self.ids = ids
        self.start_date = start_date
        self.end_date = end_date
        self.attribute_name = attribute_name
        self.security_type = security_type
        self.config_path = config_path
        self.config = self.load_config(config_path).get(security_type)
        
        if correct_fy=='default':
            correct_fy= self.config.get('correct_fiscal_year',{})
        else:
            correct_fy= deepcopy(correct_fy)
        common_keys = pd.Index(correct_fy.keys()).intersection(attribute_name)
        self.correct_fy = {k: correct_fy[k] for k in common_keys}
        
        pass
    def load_config(self, config_path=config_path):
        with open(config_path, 'r') as file:
            config = json.load(file)
        return config
    def get_raw_fund_data(self, attribute_name):
        conf = deepcopy(self.config)
        ids = self.ids
        start_date = self.start_date
        end_date = self.end_date
        if attribute_name=='fy_1gy':
            start_date = (pd.offsets.YearBegin().rollback(start_date) - BDay(260)).strftime('%Y%m%d')
            end_date =  pd.offsets.YearEnd().rollforward(end_date).strftime('%Y%m%d')
        table_name = conf.get('table_name')
        table_data_dictionary = conf.get('table_data_dictionary')
        table_date_field = conf.get('table_date_field')
        table_perimeter_field = conf.get('table_perimeter_field')
        df_dict = pd.read_sql(f'select * from {table_data_dictionary}', con_mis)
        attribute_id = df_dict.loc[df_dict['name'] == attribute_name, 'attribute_id']
        
        if attribute_id.empty:
            attribute_id = -1
            raise Exception('Only support attribute_name in %s'%df_dict['name'].unique())
        else:
            attribute_id = attribute_id.iloc[0]
        ids_str = ','.join([str(x) for x in ids])
        request = f"""select * from {table_name} 
                    where attribute_id={attribute_id}
                        and DATE>='{start_date}'
                        and DATE<='{end_date}'
                        and {table_perimeter_field} in ({ids_str})"""

        df_data_db = pd.read_sql(request, con_mis)
        df_data_db[table_date_field]=pd.to_datetime(df_data_db[table_date_field])

        df_data_ts = df_data_db.set_index([table_date_field, table_perimeter_field]).value.unstack()
        return df_data_ts

    def get_fund_data(self):
        d_fund_res = {}
        d_fund_correct_fy = {}
        d_fund_raw_daily = {}
        res_data = self.config['data_resample']
        res_default = self.config.get('default_resample',{})
        attribute_name = self.attribute_name
        correct_fy = self.correct_fy


        for attr in attribute_name:
            df_raw_ts = self.get_raw_fund_data(attribute_name = attr)
            df_raw_daily = resample_data_ts(df_raw_ts, frequency = 'weekday', nb_filldays = None)
            d_fund_raw_daily[attr] = df_raw_daily.copy()

        if correct_fy:
            df_fy = self.get_raw_fund_data(attribute_name = 'fy_1gy')
            for y1 in attribute_name:
                if y1 in correct_fy.keys():
                    y2 = correct_fy[y1]
                    if not (y2 in attribute_name):
                        print(f'!!!!! Data {y1} will not be corrected due to missing {y2} !!!!!!')
                        y2 = y1
                    df_1y = d_fund_raw_daily[y1].copy()
                    df_2y = d_fund_raw_daily[y2].copy()
                    df_1y_corr = correct_fiscal_year(df_fy, df_1y, df_2y)
                    d_fund_correct_fy[y1] = df_1y_corr.copy()
                else:
                    d_fund_correct_fy[y1] = d_fund_raw_daily[y1].copy()
        else:
            d_fund_correct_fy = deepcopy(d_fund_raw_daily)
        
        for attr in attribute_name:
            if attr in res_data.keys():
                frequency = res_data[attr]['frequency']
                nb_filldays = res_data[attr]['ffill_max_daily_nb_day']
            else:
                frequency = res_default.get('frequency', 'friday')
                nb_filldays = res_default.get('ffill_max_daily_nb_day', 0)
            
            df_raw_ts = d_fund_correct_fy[attr]
            df_res = resample_data_ts(df_raw_ts, frequency = frequency, nb_filldays = nb_filldays)
            
            d_fund_res[attr] = df_res.copy()

        return d_fund_res, d_fund_correct_fy, d_fund_raw_daily

#gfd = GetFundData(ids = [1, 2, 8, 15, 998745, 651526], start_date = '20200101', end_date = '20240630',
#                   attribute_name=['eps_2gy'], security_type = 'stock')
#d_fund_res, d_fund_correct_fy, d_fund_raw_daily = gfd.get_fund_data()
#%%
# =============================================================================
# gfd = GetFundData(ids = [1, 2, 8, 15, 998745, 651526], start_date = '20200101', end_date = '20240630',
#                   attribute_name=['eps_2gy'], security_type = 'stock')
# d_fund_res, d_fund_correct_fy, d_fund_raw_daily = gfd.get_fund_data()
# #%%
# 
# def get_raw_fund_data(ids, start_date, end_date, attribute_name='eps_1gy', security_type = 'index'):
#     table_data_dictionary = 'QUANT_work..FUNDAMENTAL_DATA_DICTIONARY'
#     table_date_field = 'DATE'
#     if security_type=='index':
#         table_name = 'QUANT_work..FUNDAMENTAL_DATA_INDEX'
#         table_perimeter_field = "index_id"
#     elif security_type=='stock':
#         table_name = 'QUANT_work..FUNDAMENTAL_DATA_STOCK'
#         table_perimeter_field = "security_id"
# 
#     df_dict = pd.read_sql(f'select * from {table_data_dictionary}', con_mis)
#     attribute_id = df_dict.loc[df_dict['name'] == attribute_name, 'attribute_id']
#     if attribute_id.empty:
#         attribute_id = -1
#         raise Exception('Only support attribute_name in %s'%df_dict['name'].unique())
#     else:
#         attribute_id = attribute_id.iloc[0]
# 
#     ids_tuple = tuple(ids)
#     request = f"""select * from {table_name} 
#                 where attribute_id={attribute_id}
#                     and DATE>='{start_date}'
#                     and DATE<='{end_date}'
#                     and {table_perimeter_field} in {ids_tuple}"""
# 
#     df_data_db = pd.read_sql(request, con_mis)
#     df_data_db[table_date_field]=pd.to_datetime(df_data_db[table_date_field])
# 
#     df_data_ts = df_data_db.set_index([table_date_field, table_perimeter_field]).value.unstack()
#     #df_weekday = pd.bdate_range(start_date, end_date)
#     #df_data_ts = df_data_ts.reindex(index = df_weekday, method = None)
#     return df_data_ts
# 
# def resample_data_ts(df_data_ts, frequency = 'friday', nb_filldays = None):
# 
#     if (nb_filldays in [None, 0]) and (frequency in [None, '']):
#         return df_data_ts
#     # resample daily, including week-end
#     df_weekday = pd.date_range(df_data_ts.index.min(), df_data_ts.index.max())
#     df_data_wd = df_data_ts.reindex(index = df_weekday, method = None)
#     # ffill if nb_filldays is given
#     if nb_filldays not in [None, 0]:
#         df_data_res = df_data_wd.ffill(limit = nb_filldays)
#     # Resample according to the input frequency.
#     # Fonction df.resample(freq).last(): auto ffills till the given frequency, no need to give nb_filldays
#     if frequency=='weekday':
#         df_data_res = df_data_wd.copy()
#         df_data_res = df_data_wd[df_data_wd.index.weekday<=4]
#     elif frequency=='allday':
#         df_data_res = df_data_wd.copy()
#     # weekly
#     elif frequency in ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday']:
#         freq = 'W-%s'%(frequency.upper()[:3])
#         df_data_res = df_data_ts.resample(freq).last()
#     elif frequency=='eom':
#         df_data_res = df_data_ts.resample('BME').last()
# 
#     return df_data_res
# 
# def check_fiscal_year_missing(df_fy):
#     
#     df_tmp = df_fy.stack().reset_index()
#     df_tmp.columns = ['DATE', 'ids','year']
#     df_tmp = df_tmp.set_index(['year', 'ids']).DATE.unstack()
# 
#     df_missing = (df_tmp.isna()*1).replace({0:np.nan}).stack().reset_index()
#     df_missing = df_missing.set_index('ids')[['year']].T
#     return df_missing
# 
# def correct_fiscal_year(df_fy, df_1y, df_2y):
# 
#     common_date = df_1y.index.union(df_2y.index).union(df_fy.index)
#     common_ids = df_1y.columns
#     sec_2y_miss = common_ids.difference(df_2y.columns)
#     sec_fy_miss = common_ids.difference(df_fy.columns)
#     df_fy_miss = check_fiscal_year_missing(df_fy)
# 
#     if not df_fy_miss.empty:
#         print('!!!!!! Missing fiscal year for ids: !!!!!!!\n%s'%df_fy_miss)
#     if len(sec_fy_miss)>0:
#         print('!!!!!! Missing fiscal data for ids: %s !!!!!!!'%sec_fy_miss)
#     if len(sec_2y_miss)>0:
#         print('!!!!!! Missing 2Y data for ids: %s !!!!!!!'%sec_2y_miss)
# 
#     df_1y_reidx = df_1y.reindex(index = common_date, columns = common_ids, method = None)
#     df_2y_reidx = df_2y.reindex(index = common_date, columns = common_ids, method = None)
# 
#     df_fy1_fill = df_fy.reindex(common_date, columns = common_ids).ffill(limit=None)
#     df_fy2_fill = df_fy.reindex(common_date, columns = common_ids).shift(-1).bfill(limit=None)
# 
#     df_year_fill = pd.DataFrame(index = common_date, columns = common_ids, data = np.array([common_date.year]*len(common_ids)).T)
# 
#     df_fy1_idx = df_fy1_fill[df_fy1_fill==df_year_fill].notna()
#     df_fy2_idx = df_fy2_fill[df_fy2_fill==df_year_fill].notna()
# 
#     df_fy1_idx[sec_fy_miss] = False
# 
# 
#     df_1y_keep = df_1y_reidx[df_fy1_idx]
#     df_2y_keep = df_2y_reidx[df_fy2_idx]
# 
#     df_1y_corr = df_1y_keep.fillna(df_2y_keep)
#     
#     df_1y_corr = df_1y_corr.reindex(index = df_1y.index)
#     return df_1y_corr
# 
# def get_fund_data(ids, start_date, end_date, attribute_name=['eps_1gy'], security_type = 'index',
#                   frequency = 'friday', nb_filldays = None, correct_fy={}):
#     d_fund_res = {}
#     d_fund_correct_fy = {}
#     d_fund_raw_daily = {}
#     for attr in attribute_name:
#         df_raw_ts = get_raw_fund_data(ids, start_date, end_date, attribute_name = attr, security_type = security_type)
#         df_raw_daily = resample_data_ts(df_raw_ts, frequency = 'allday', nb_filldays = None)
#         d_fund_raw_daily[attr] = df_raw_daily.copy()
#         
#     if correct_fy:
#         df_fy = get_raw_fund_data(ids, start_date, end_date, attribute_name = 'fy_1gy', security_type=security_type)
#         for y1, y2 in correct_fy.items():
#             df_1y = d_fund_raw_daily[y1].copy()
#             df_2y = d_fund_raw_daily[y2].copy()
#             df_1y_corr = correct_fiscal_year(df_fy, df_1y, df_2y)
#             d_fund_correct_fy[y1] = df_1y_corr.copy()
#     
#     for attr in attribute_name:
#         df_res = resample_data_ts(df_raw_ts, frequency = frequency, nb_filldays = nb_filldays)
#         d_fund_res[attr] = df_res.copy()
# 
#     return d_fund_res, d_fund_correct_fy, d_fund_raw_daily
# 
# 
# d_fund_res, d_fund_correct_fy, d_fund_raw_daily = get_fund_data(ids = [1, 2, 8, 15, 998745, 651526], start_date = '20200101', end_date = '20240630',
#                                      attribute_name=['eps_2gy'], security_type = 'stock')
# 
# #%%
# ids = [1, 2, 8, 15, 998745, 651526]
# start_date = '20240801'
# end_date = '20241031'
# attribute_name = 'eps_1gy'
# security_type='stock'#'stock', 'index'
# table_data_dictionary = 'QUANT_work..FUNDAMENTAL_DATA_DICTIONARY'
# table_date_field = 'DATE'
# if security_type=='index':
#     table_name = 'QUANT_work..FUNDAMENTAL_DATA_INDEX'
#     table_perimeter_field = "index_id"
# elif security_type=='stock':
#     table_name = 'QUANT_work..FUNDAMENTAL_DATA_STOCK'
#     table_perimeter_field = "security_id"
# 
# df_dict = pd.read_sql(f'select * from {table_data_dictionary}', con_mis)
# #%%
# attribute_id = df_dict.loc[df_dict['name'] == attribute_name, 'attribute_id']
# if attribute_id.empty:
#     attribute_id = -1
#     raise Exception('Only support attribute_name in %s'%df_dict['name'].unique())
# else:
#     attribute_id = attribute_id.iloc[0]
# 
# ids_tuple = tuple(ids)
# request = f"""select * from {table_name} 
#             where attribute_id={attribute_id}
#                 and DATE>='{start_date}'
#                 and DATE<='{end_date}'
#                 and {table_perimeter_field} in {ids_tuple}"""
# df_data_db = pd.read_sql(request, con_mis)
# 
# df_data_ts = df_data_db.set_index([table_date_field, table_perimeter_field]).value.unstack()
# 
# #%%
# attribute_fy = df_dict.loc[df_dict['name'] == 'fy_1gy', 'attribute_id'].iloc[0]
# req_fy = f"""select * from {table_name} 
#             where attribute_id={attribute_fy}
#                 --and DATE>='{start_date}'
#                 --and DATE<='{end_date}'
#                 and {table_perimeter_field} in {ids_tuple}"""
# df_fy = pd.read_sql(req_fy, con_mis)
# df_fy['DATE'] = pd.to_datetime(df_fy['DATE'])
# df_fy = df_fy[df_fy.DATE>='20200101']
# 
# df_fy_date = df_fy.set_index([table_date_field, table_perimeter_field])['value'].unstack()
# df_fy_date.iloc[:,0] = np.nan
# 
# index = pd.date_range(df_fy_date.index.min() - pd.offsets.YearBegin(), df_fy_date.index.max())
# df_fy_date_wd = df_fy_date.reindex(index)
# 
# df_fy_date_wd_fill = df_fy_date_wd.bfill(limit=None)
# 
# df_fy_year_fill = df_fy_date_wd_fill.copy()*np.nan
# df_fy_year_fill.iloc[:,0] = df_fy_year_fill.index.year
# df_fy_year_fill = df_fy_year_fill.ffill(axis = 1)
# 
# 
# df_fy_fill = (df_fy_date_wd_fill==df_fy_year_fill)
# #df_1y_corr_bis = df_1y*(~df_fy_fill) + df_2y*df_fy_fill
# 
# #%%
# ids = [1, 2, 3, 4, 15]
# start_date = '20200101'
# end_date = '20241031'
# attribute_name = 'eps_1gy'
# frequency = 'friday'
# 
# nb_filldays = None
# df_fy = get_raw_fund_data(ids, start_date, end_date, attribute_name = 'fy_1gy', security_type='index')
# 
# df_1y = pd.DataFrame(index = pd.bdate_range(start_date, end_date), columns = ids, data = 1.5)
# df_2y = pd.DataFrame(index = pd.bdate_range(start_date, end_date), columns = ids, data = 2)
# df_3y = pd.DataFrame(index = pd.bdate_range(start_date, end_date), columns = ids, data = 3)
# 
# df_2y = df_2y.iloc[:, :-1]
# df_fy = df_fy.iloc[:, 1:]
# df_fy.iloc[0,2]=np.nan
# #df_fy_ts = pd.DataFrame(index = pd.to_datetime(['20240225']))
# #df_1y_corr = correct_fiscal_year(df_fy, df_1y, df_2y)
# #df_2y_corr = correct_fiscal_year(df_fy, df_2y, df_3y)
# 
# 
# 
# df_1y_res = resample_data_ts(df_1y, frequency = frequency, nb_filldays = nb_filldays)
# df_2y_res = resample_data_ts(df_2y, frequency = frequency, nb_filldays = nb_filldays)
# df_3y_res = resample_data_ts(df_3y, frequency = frequency, nb_filldays = nb_filldays)
# 
# df_1y_corr = correct_fiscal_year(df_fy, df_1y_res, df_2y_res)
# df_2y_corr = correct_fiscal_year(df_fy, df_2y_res, df_3y_res)
# 
# #%%
# 
# common_date = df_1y.index.union(df_2y.index).union(df_fy.index)
# common_ids = df_1y.columns
# sec_2y_miss = common_ids.difference(df_2y.columns)
# sec_fy_miss = common_ids.difference(df_fy.columns)
# df_fy_miss = check_fiscal_year_missing(df_fy)
# 
# if not df_fy_miss.empty:
#     print('!!!!!! Missing fiscal year for ids: !!!!!!!\n%s'%df_fy_miss)
# if len(sec_fy_miss)>0:
#     print('!!!!!! Missing fiscal data for ids: %s !!!!!!!'%sec_fy_miss)
# if len(sec_2y_miss)>0:
#     print('!!!!!! Missing 2Y data for ids: %s !!!!!!!'%sec_2y_miss)
# 
# df_1y_reidx = df_1y.reindex(index = common_date, columns = common_ids, method = None)
# df_2y_reidx = df_2y.reindex(index = common_date, columns = common_ids, method = None)#.fillna(0)
# 
# df_fy1_fill = df_fy.reindex(common_date, columns = common_ids).ffill(limit=None)
# df_fy2_fill = df_fy.reindex(common_date, columns = common_ids).shift(-1).bfill(limit=None)
# 
# 
# df_year_fill = pd.DataFrame(index = common_date, columns = common_ids, data = np.array([common_date.year]*5).T)
# 
# df_fy1_idx = df_fy1_fill[df_fy1_fill==df_year_fill].notna()
# df_fy2_idx = df_fy2_fill[df_fy2_fill==df_year_fill].notna()
# 
# df_fy1_idx[sec_fy_miss] = False
# 
# 
# df_1y_keep = df_1y_reidx[df_fy1_idx]
# df_2y_keep = df_2y_reidx[df_fy2_idx]
# 
# 
# df_1y_corr = df_1y_keep.fillna(df_2y_keep)
# 
# 
# #df_1y_corr = df_1y_reidx * df_fy1_idx + df_2y_reidx*df_fy2_idx
# #%%
# 
# 
# 
# 
# df_fy_gap2fill = (df_fy_fill-df_fy_year_fill)==0
# #df_fy_gap2fill = df_fy_gap2fill[df_fy_fill.notna()]
# 
# 
# df_1y_reidx = df_1y.reindex(index = common_date, columns = common_ids, method = None)
# df_2y_reidx = df_2y.reindex(index = common_date, columns = common_ids, method = None)
# df_1y_reidx[common_ids.difference(df_1y.columns)] = 0
# df_2y_reidx[common_ids.difference(df_2y.columns)] = 0
# 
# df_1y_corr = df_1y_reidx*(~df_fy_gap2fill) + df_2y_reidx*df_fy_gap2fill
# df_1y_corr = df_1y_corr.reindex(index = df_1y.index, columns = df_1y.columns)
# 
# 
# =============================================================================





