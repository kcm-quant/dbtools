USE [QUANT_work]
GO

/****** Object:  StoredProcedure [dbo].[get_kc_flows_execution]    Script Date: 03/10/2023 14:53:06 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO


-- USE COMMAND EXAMPLE: 
-- use QUANT_work EXEC dbo.get_kc_flows_execution @startDate = '20230901',  @endDate = '20230905'


CREATE PROCEDURE [dbo].[get_kc_flows_execution]
    (
	@startDate date = '2023-08-01',   
    @endDate date = '2023-08-04',

	@ALLTRADE_condition varchar(max) = ' ', 
	
	@ALLTRADE_add_col BIT = 0, 
	@ALLTRADE_col_names varchar(max) = ' ', 
	@ALLTRADE_col_creation varchar(max) = ' ', 
	
	@FilterManagement_FM BIT = 1, 
	@FM_TechnicalDef varchar(max) = 'client_type in (''Technical'')',
	@FM_CorporateDef varchar(max) = 'SALESID in (''@CORPO_Traders'',''BMC'',''BMC2'',''CGA'',''CGD'',''CGD2'',''CIE'',''DMZ'', ''EPE'',
                        ''JFT2'',''JFT3'',''JSS'',''JSS2'',''LBR'',''PDS'',''PSC'',''PSR'',''PSR-T'',''PSU'',''PSU2'')',
	
	@FilterResearch_FR BIT = 1, 
	@FR_ResearchDef varchar(max) = 'client_type not in (''Hedge funds'',''Funds'',''Banks-Brokers'',''Retail'')'
	)
AS

--------------------------------------------------------------------------------------------------------------------
-- PART 0: define QR perimeter (security_id)
SELECT distinct security_id 
INTO #TempHSQ
FROM KGR..HISTO_SECURITY_QUANT

--------------------------------------------------------------------------------------------------------------------
-- PART 1: extracts from tables into temporary tables for efficiency 
	-- exchange rates
	SELECT QuotedCCy, Date, Rate
	INTO #TempExchangeRate
	FROM INFOCENTRE.dbo.Daily_ExchangeRate 
	WHERE Date >= @startDate AND Date <= @endDate

	-- ALLTRADE
	CREATE TABLE #TempALLTRADE (date [date] not NULL,security_id [int] not NULL,CCY [varchar](3) NULL,SIDE [varchar](4) NOT NULL,
		ORDID [nvarchar](1027) NULL,quantity [float] NOT NULL,amount [float] NOT NULL);
	
		-- add specific columns if asked in input
		IF @ALLTRADE_add_col = 1
		BEGIN
			DECLARE @alt_TempALLTRADE NVARCHAR(MAX);
			SET @alt_TempALLTRADE = N'ALTER TABLE #TempALLTRADE 
				ADD '+ @ALLTRADE_col_creation+';';
			EXEC sp_executesql @alt_TempALLTRADE;
		END

	DECLARE @fo_query NVARCHAR(MAX);
	SET @fo_query = N'INSERT INTO #TempALLTRADE
		SELECT a.TRADEDATE_date AS date,a.security_id,a.CCY,a.SIDE,
		a.ORDID,
		sum(a.MATCHQTY) AS quantity,sum(a.MATCHQTY * a.PRICE_NUM) AS amount
		'+@ALLTRADE_col_names+'
		FROM INFOCENTRE..ALLTRADE_QUANT a
		WHERE a.TRADEDATE_date >= CAST(''' + CONVERT(NVARCHAR(10), @startDate, 120) + ''' AS DATE) AND a.TRADEDATE_date<= CAST(''' + CONVERT(NVARCHAR(10), @endDate, 120) + ''' AS DATE) '
		+ @ALLTRADE_condition + ' and a.security_id in (select security_id from #TempHSQ) 
		GROUP BY a.TRADEDATE_date, a.security_id,a.CCY,a.SIDE,a.ORDID'+ @ALLTRADE_col_names;
	EXEC sp_executesql @fo_query;

	-- ALLORDER
	SELECT o.ORDID,o.SALESID,o.CLNID
	INTO #TempALLORDER
	FROM OMS..ALLORDER o
	WHERE o.CREATETIME >= DATEADD(day, -4*30, @startDate)  AND o.TRADEDATE_date <= @endDate
	--o.ORDID in (select distinct ORDID from #TempALLTRADE)
	

--------------------------------------------------------------------------------------------------------------------
-- PART 2: JOIN TABLES
	CREATE TABLE #TempFlows (date [date] not NULL,security_id [int] not NULL,ccy [varchar](3) NULL,SIDE [varchar](4) NOT NULL,
		SALESID [nvarchar](255) NULL,
		account_id bigint NOT NULL,
		quantity [float] NOT NULL,amount [float] NOT NULL,amount_eur [float] NOT NULL);
		
		-- add specific columns if asked in input
		IF @ALLTRADE_add_col = 1
		BEGIN
			DECLARE @alt_TempFlows NVARCHAR(MAX);
			SET @alt_TempFlows = N'ALTER TABLE #TempFlows 
				ADD '+ @ALLTRADE_col_creation +';';
			EXEC sp_executesql @alt_TempFlows;
		END

	DECLARE @join_query NVARCHAR(MAX);
	SET @join_query = N'INSERT INTO #TempFlows
		SELECT fo.date,fo.security_id,fo.CCY ccy,fo.SIDE,
			o.SALESID,
			case when isnumeric(c.CLNID) = 1 then convert(bigint,c.EXTCLNID) else 0 end account_id,
			sum(fo.quantity) AS quantity,
			sum(fo.amount) AS amount,
			SUM(fo.amount / NULLIF(er.Rate, 0)) AS amount_eur
			'+@ALLTRADE_col_names+'
		FROM #TempALLTRADE fo
		INNER JOIN #TempALLORDER o ON fo.ORDID = o.ORDID
		INNER JOIN #TempExchangeRate er ON fo.CCY = er.QuotedCCy AND fo.date = er.Date
		INNER JOIN OMS..CLIENTIDMAP c ON o.CLNID = c.CLNID
		GROUP BY fo.date, fo.security_id, fo.CCY, fo.SIDE, o.SALESID, case when isnumeric(c.CLNID) = 1 then convert(bigint,c.EXTCLNID) else 0 end'+ @ALLTRADE_col_names;
	EXEC sp_executesql @join_query;

--------------------------------------------------------------------------------------------------------------------
-- PART 3: FINAL OUTPUT
	-- Preparing table
	CREATE TABLE #SelectedFlows (date [date] not NULL, security_id [int] not NULL, ccy [varchar](3) NULL, SIDE [varchar](4) NOT NULL,
		SALESID [nvarchar](255) NULL,
		account_id bigint NOT NULL,
		quantity [float] NOT NULL, amount [float] NOT NULL, amount_eur [float] NOT NULL, 
		client_type [nvarchar](255) NULL, client_region [nvarchar](255) NULL);

		-- add specific columns if asked in input
		IF @ALLTRADE_add_col = 1
		BEGIN
			DECLARE @alt_SelectedFlows NVARCHAR(MAX);
			SET @alt_SelectedFlows = N'ALTER TABLE #SelectedFlows 
				ADD '+ @ALLTRADE_col_creation+';';
			EXEC sp_executesql @alt_SelectedFlows;
		END

	-- Add Client referential: client_type, client_region from Crystal referential
	DECLARE @cmap_query NVARCHAR(MAX);
	SET @cmap_query = N'INSERT INTO #SelectedFlows
		SELECT t.date, t.security_id, t.ccy, t.SIDE, t.SALESID, t.account_id, t.quantity, t.amount, t.amount_eur, c.Type client_type, c.Country client_region
		'+@ALLTRADE_col_names+'
		FROM #TempFlows t
		INNER JOIN KGR..Account_Classification c on c.Id = t.account_id';
	EXEC sp_executesql @cmap_query;
	

	--@FilterManagement_FM // remove Technical (ACCOUNT_TYPE) and Corporate (SALESID list)
	IF @FilterManagement_FM = 1
	BEGIN
		DECLARE @FM_query NVARCHAR(MAX);
		SET @FM_query = N'DELETE FROM #SelectedFlows 
			WHERE '+ @FM_TechnicalDef;
		EXEC sp_executesql @FM_query;
		
		SET @FM_query = N'DELETE FROM #SelectedFlows 
			WHERE '+ @FM_CorporateDef;
		EXEC sp_executesql @FM_query;
	END

	--@FilterResearch_FR // keep ACCOUNT_TYPE list and remove opt-out clients
	IF @FilterResearch_FR = 1
	BEGIN
		--keep ACCOUNT_TYPE list 
		DECLARE @FR_query NVARCHAR(MAX);
		SET @FR_query = N'DELETE FROM #SelectedFlows 
			WHERE '+ @FR_ResearchDef;
		EXEC sp_executesql @FR_query;


		-- remove opt-out clients
		SELECT ID, case when ID in (951480556,2146849257,2146849258,2146859947) then '1990-01-01' else StampDate end StampDate
		INTO #QFDFilters
		FROM [QUANT_FLOW_DATA].[dbo].[DailyClientFilter]
		
		SELECT t1.account_id, t1.date INTO #OOO
		FROM #SelectedFlows t1
			JOIN #QFDFilters t2 ON t1.account_id = t2.ID
			WHERE t1.date >= t2.[StampDate]
		DELETE t1
		FROM #SelectedFlows t1
			JOIN #OOO t2 ON t1.account_id = t2.account_id AND t1.date = t2.date;
	END

	-- **************** FINAL OUTPUT ****************
	DECLARE @final_query NVARCHAR(MAX);
	SET @final_query = N'SELECT f.date,f.security_id,f.ccy,f.SIDE,f.account_id,f.client_type,f.client_region,
		sum(f.quantity) AS quantity,
		sum(f.amount) AS amount,
		SUM(f.amount_eur) AS amount_eur
		'+@ALLTRADE_col_names+'
		FROM #SelectedFlows  f
		GROUP BY  f.date,f.security_id,f.ccy,f.SIDE,f.account_id,f.client_type,f.client_region'+@ALLTRADE_col_names;		
	EXEC sp_executesql @final_query;

GO


