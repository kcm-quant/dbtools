# -*- coding: utf-8 -*-
"""
Created on Wed May 12 16:33:11 2021

@author: abenhassine (abenhassine@keplercheuvreux.com)
"""

import pandas as pd
import os
import subprocess
from sqlalchemy import Table, MetaData, Column
from sqlalchemy import Integer, String, Float, DateTime, Boolean
from sqlalchemy import inspect
# HARD CODED PARAMETERS
bcp = 'bcp'
sep = ';'
bcp_error_file = 'bcp_error'


def _bulk_copy(con, table_name, data):
    """
    Executes a bulk copy as a windows command in order to upload a large dataframe in a faster way
    NB : https://www.microsoft.com/en-us/download/confirmation.aspx?id=53591 needs to be installed to make it work

    Parameters
    ----------
    con : sqlalchemy engine
        the connection to the database created previously using db_connexion.py .
    table_name : str
        name of the table we want to create.
    data : dataframe
        data that we want to upload to the sql server.

    Raises
    ------
    Exception
        tells if there was a problem with the bcp command,if so the subprovess o needs to be invistigated.

    Returns
    -------
    None.

    """
    global bcp_error_file, sep
    if not _is_table(con, table_name):
        _table_creation(con, table_name, data)
    col_order = _get_col_order(con, table_name)
    data = data[col_order]
    if os.path.exists(bcp_error_file):
        os.remove(bcp_error_file)
    bcp_cmd, csv_filename = _create_bcp_cmd(con, table_name)
    data.to_csv(csv_filename, index=False, header=False, sep=sep)
    #print(bcp_cmd)
    o = subprocess.run(bcp_cmd, shell=True, capture_output=True)
    if _iserr(o):
        #raise Exception('bcp error')
        print(o)
        
        

def _iserr(o):
    global bcp_error_file
    if o.stderr:
        return True
    isempty = True
    with open(bcp_error_file, 'r') as f:
        for l in f:
            if isempty:
                print('Il y eut un souci lors du chargement')
                isempty = False
            print(l)
    if not isempty:
        return True
    for l in o.stdout.split(b'\n'):
        if l[:5]==b"Error":
            print(o.stdout)
            return True
    return False


def _create_bcp_cmd(con, table_name):
    global bcp, sep, bcp_error_file
    
    dbname = pd.read_sql('SELECT DB_NAME()',con).values[0][0]
    
    """passwd = con.url.password
    user = con.url.username"""
    server_name = con.url.host
    csv_filename= r'c:\temp\%s.bcp' % table_name
    bcp_cmd = f'{bcp} {dbname}..{table_name} in {csv_filename} ' +\
              f'-S{server_name} -T -c -t"{sep}" ' +\
              f'-e  {bcp_error_file}'
    return (bcp_cmd, csv_filename)


def _get_col_order(con, table_name):
    query = f"""SELECT COLUMN_NAME
                FROM INFORMATION_SCHEMA.COLUMNS
                WHERE TABLE_NAME = N'{table_name}'"""
    # Use pd.read_sql to execute the query and fetch results into a DataFrame
    df = pd.read_sql(query, con)
    
    # Extract the COLUMN_NAME column into a list to get the column order
    col_order = df['COLUMN_NAME'].tolist()
    
    return col_order
    

def _is_table(con, table_name):
    inspector = inspect(con)

    return table_name in inspector.get_table_names()


def _table_creation(con, table_name, dataframe):
    """Dynamically create a table based on the pandas DataFrame schema."""
    """Create a table based on the pandas DataFrame schema using SQLAlchemy."""
    metadata = MetaData()  # Do not bind here
    
    # Dynamically create columns based on DataFrame dtypes
    columns = [
        Column(name, _dtype_to_sqlalchemy_type(dtype)) 
        for name, dtype in dataframe.dtypes.items()
    ]
    
    table = Table(table_name, metadata, *columns)
    
    # Now bind the engine and create the table
    metadata.create_all(con)
    
def _dtype_to_sqlalchemy_type(dtype):
    """Map a pandas dtype to a SQLAlchemy type."""
    if pd.api.types.is_integer_dtype(dtype):
        return Integer
    elif pd.api.types.is_float_dtype(dtype):
        return Float
    elif pd.api.types.is_string_dtype(dtype):
        return String
    elif pd.api.types.is_datetime64_any_dtype(dtype):
        return DateTime
    elif pd.api.types.is_bool_dtype(dtype):
        return Boolean
    # Add other dtype mappings as needed, like JSON for complex structures
    # Example for handling JSON data in pandas, mapping it to PostgreSQL JSONB
    
    else:
        return String # Default fallback

pd.bulk_copy = _bulk_copy
