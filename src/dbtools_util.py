# -*- coding: utf-8 -*-
"""
Created on Tue Jul  9 10:23:59 2024

@author: ssureau
"""

import pandas as pd
import json
import os

from datetime import datetime
import calendar

# Load the configuration from the JSON file
def load_config():
    """
    
    Use example: 
        import dbtools.src.dbtools_util as du
        config = du.load_config()
    """
    script_dir = os.path.dirname(os.path.abspath(__file__))
    parent_dir = os.path.dirname(script_dir)
    config_dir = os.path.join(parent_dir, 'config')
    config_file_name = 'utils.json'
    json_file = os.path.join(config_dir, config_file_name)
    
    with open(json_file, 'r') as file:
        config = json.load(file)
    return config

#%% date sampling tools
#%% date sampling tools / 1. daily monday to sunday
def fill_week_with_nans(df, date_column='date', value_column='value'):
    """
    Fills a DataFrame with NaNs for missing dates within the full weeks
    ranging from the minimum to maximum dates in the specified date column.

    Tasks: 
        1. assures date_colmun is datetime format
        2. determines all dates between min and max date in input dataframe
        3. add nan in value_column for all dates missing (for each combination of values in columns other than date and value if any)
        
    Args:
        df (pd.DataFrame): Input DataFrame containing dates and values.
        date_column (str): Column name in `df` representing dates. Default is 'date'.
        value_column (str): Column name in `df` representing values. Default is 'value'.

    Returns:
        pd.DataFrame: A DataFrame with all dates filled in, including NaNs for missing values.
    
    Use example: 
        import pandas as pd
        import dbtools.src.dbtools_util as du
        data = {
                'date': ['2024-07-15', '2024-07-17', '2024-07-20'],
                'security_id': [110, 276, 276],
                'attribute_id': [1 1 2],
                'value': [10, 20, 30]}
        df = pd.DataFrame(data)       
        filled_df = du.fill_week_with_nans(df, date_column='date')
        print(filled_df)
    """
    # Ensure the date_column is in datetime format
    df.loc[:, date_column] = pd.to_datetime(df[date_column])

    # Get the minimum and maximum dates from the input DataFrame
    min_date = df[date_column].min()
    max_date = df[date_column].max()

    # Generate a date range with all dates from the start of the week of min_date to the end of the week of max_date
    start_date = min_date - pd.Timedelta(days=min_date.weekday())
    end_date = max_date + pd.Timedelta(days=(6 - max_date.weekday()))
    all_dates = pd.date_range(start=start_date, end=end_date, freq='D')

    # Get all columns except date_column and value_column
    other_columns = df.columns.difference([date_column, value_column])

    # Create a DataFrame with all dates
    all_dates_df = pd.DataFrame({date_column: all_dates})

    if len(other_columns) > 0:
        # Get unique combinations of other columns
        unique_combinations = df[other_columns].drop_duplicates()

        # Create a MultiIndex from product of all_dates and unique combinations of other columns
        all_combinations = pd.MultiIndex.from_product(
            [all_dates, *[unique_combinations[col].unique() for col in other_columns]],
            names=[date_column] + list(other_columns)
        ).to_frame(index=False)

        # Merge the all_combinations DataFrame with the dates
        all_combinations = pd.merge(all_combinations, unique_combinations, how='left', on=list(other_columns))
        # Merge the all_dates_df with the complete combinations
        result_df = pd.merge(all_combinations, df, on=[date_column] + list(other_columns), how='left')

    else:
        # If no other columns, simply merge dates with the original DataFrame
        result_df = pd.merge(all_dates_df, df, on=date_column, how='left')

    return result_df

#%% date sampling tools / 2. daily forward fill
def fixed_ffill_limit(df, date_column, value_column, limit):
    """
    Performs a forward fill on a specified column with a limit on the number of consecutive
    NaNs that can be filled.
    
    Tasks: 
        1. assures date_colmun is datetime format
        2. replaces NaN values by previous up to a given number of lines

    Args:
        df (pd.DataFrame): Input DataFrame containing dates and values.
        date_column (str): Column name in `df` representing dates.
        value_column (str): Column name in `df` representing values to be forward filled.
        limit (int): Maximum number of consecutive NaNs to fill.

    Returns:
        pd.DataFrame: DataFrame with forward filled values up to the specified limit.
    
    Example:
        import pandas as pd
        import dbtools.src.dbtools_util as du
        data = {
            'date': ['2024-07-15', '2024-07-16', '2024-07-17', '2024-07-18', '2024-07-19'],
            'value': [10, None, None, 20, None]
        }
        df = pd.DataFrame(data)
        filled_df = du.fixed_ffill_limit(df, date_column='date', value_column='value', limit=1)
        print(filled_df)
    """
    # Ensure the date column is of datetime type
    df.loc[:, date_column] = pd.to_datetime(df[date_column])

    # Sort by date to ensure forward filling works correctly
    df = df.sort_values(by=date_column).reset_index(drop=True)

    df_filled = df.copy()

    # Track the index of the last non-NaN value and number of forward fills
    last_valid_index = None
    fill_count = 0

    for i in range(len(df_filled)):
        if pd.isna(df_filled.loc[i, value_column]):
            if last_valid_index is not None and fill_count < limit:
                df_filled.loc[i, value_column] = df_filled.loc[last_valid_index, value_column]
                fill_count += 1
            else:
                continue
        else:
            last_valid_index = i
            fill_count = 0  # Reset fill count when encountering a new valid value

    return df_filled

def same_year_ffill_limit(df, date_column, value_column, limit):
    """
    Performs a forward fill on a specified column with a limit on the number of consecutive
    NaNs that can be filled, ensuring the fills are within the same year.

    Tasks: 
        1. assures date_colmun is datetime format
        2. replaces NaN values by previous up to a given number of lines, provided that the year of date remains the same as value used to fill

    Args:
        df (pd.DataFrame): Input DataFrame containing dates and values.
        date_column (str): Column name in `df` representing dates.
        value_column (str): Column name in `df` representing values to be forward filled.
        limit (int): Maximum number of consecutive NaNs to fill within the same year.

    Returns:
        pd.DataFrame: DataFrame with forward filled values up to the specified limit within the same year.
    
    Example:
        import pandas as pd
        import dbtools.src.dbtools_util as du
        data = {
            'date': ['2023-12-30', '2023-12-31', '2024-01-01', '2024-01-02', '2024-01-03'],
            'value': [10, None, None, 20, None]
        }
        df = pd.DataFrame(data)
        filled_df = du.same_year_ffill_limit(df, date_column='date', value_column='value', limit=1)
        print(filled_df)
    """
    # Ensure the date column is of datetime type
    df.loc[:, date_column] = pd.to_datetime(df[date_column])

    # Sort by date to ensure forward filling works correctly
    df = df.sort_values(by=date_column).reset_index(drop=True)

    df_filled = df.copy()

    # Track the index of the last non-NaN value and number of forward fills
    last_valid_index = None
    fill_count = 0

    for i in range(len(df_filled)):
        if pd.isna(df_filled.loc[i, value_column]):
            current_year = df_filled.loc[i, date_column].year
            if last_valid_index is not None and fill_count < limit:
                last_valid_year = df_filled.loc[last_valid_index, date_column].year
                if last_valid_year == current_year:
                    df_filled.loc[i, value_column] = df_filled.loc[last_valid_index, value_column]
                    fill_count += 1
            else:
                continue
        else:
            last_valid_index = i
            fill_count = 0  # Reset fill count when encountering a new valid value

    return df_filled


def custom_forward_fill(df, date_column, field, value_column='value', group_by_columns=None):
    """
    Applies a custom forward fill to a DataFrame based on field-specific configurations.
    
    This function uses a configuration file to determine the appropriate forward fill function
    and parameters for the specified field. It then applies the forward fill function within
    the specified groups.

    Tasks: 
        1. calls forward-fill column dedicated to the field specified in input (for each combinations of values in columns given in group_by_columns if any)        

    Args:
        df (pd.DataFrame): Input DataFrame containing dates, values, and optional grouping columns.
        date_column (str): Column name in `df` representing dates.
        field (str): Field name used to look up the forward fill function and parameters from the configuration.
        value_column (str): Column name in `df` representing values to be forward filled. Default is 'value'.
        group_by_columns (list or str, optional): Column name(s) to group by before applying forward fill. Default is None.
    
    Returns:
        pd.DataFrame: DataFrame with forward filled values based on the specified field configuration.
   
    Use example: 
        import pandas as pd
        import numpy as np
        import dbtools.src.dbtools_util as du
        data = {'date': ['2024-07-15', '2024-07-17', '2024-07-20', '2025-01-01', '2024-07-15', '2024-07-17', '2024-07-20', '2025-01-01'],
            'value': [10, np.nan, 30, np.nan, 20, np.nan, 40, np.nan],
            'security_id': [1, 1, 1, 1, 2, 2, 2, 2]}
        df = pd.DataFrame(data)
        filled_df_1gy = du.custom_forward_fill(df, date_column='date', field='eps_1gy', group_by_columns='security_id')
        filled_df_bf = du.custom_forward_fill(df, date_column='date', field='eps_bf', group_by_columns='security_id')
    """    
    config= load_config()
    if field not in config['ffill_specs']:
        raise ValueError(f"Field {field} is not in the configuration.")
    
    ffill_function_name = config['ffill_specs'][field]['ffill_function']
    ffill_params = int(config['ffill_specs'][field]['ffill_max_nb'])
    
    if ffill_function_name not in globals():
        raise ValueError(f"Unknown forward fill function {ffill_function_name}")

    # Retrieve the function using globals()
    ffill_function = globals()[ffill_function_name]
    
    # Apply the forward fill function within each group
    if group_by_columns:
        # Ensure group_by_columns is a list
        if not isinstance(group_by_columns, list):
            group_by_columns = [group_by_columns]
            
        # Check if all group_by_columns exist in the DataFrame
        missing_cols = [col for col in group_by_columns if col not in df.columns]
        if missing_cols:
            raise ValueError(f"Columns {missing_cols} specified for grouping are not in the DataFrame.")

        df = df.groupby(group_by_columns, as_index=False).apply(
            lambda group: ffill_function(group, date_column, value_column, ffill_params)
        ).reset_index(drop=True)
    else: 
        df = ffill_function(df, date_column, value_column, ffill_params)

    return df

#%% date sampling tools / 3. sampling
def resample_data(df,  date_column='date', mode='daily', calendar_type='weekdays', bespoke_calendar=None):
    """
    Resamples the input DataFrame based on the specified mode and calendar type.
    
    Tasks: 
        1. assures date_colmun is datetime format
        2. select dates according to input arguments: 
            - monday to friday for mode="daily" and calendar_type='weekdays'
            - dates given in bespoke_calendar for mode='daily' and calendar_type='bespoke'
            - fridays for mode="weekly" and calendar_type='friday'
        3. gives error when wrong configuration of arguments is given
        
    Args:
        df (pd.DataFrame): The input DataFrame.
        date_column (str): The name of the column containing date information. Defaults to 'date'.
        mode (str): The resampling mode. Can be 'daily' (default) or 'weekly'.
        calendar_type (str): 
            For "daily" mode: 'weekdays' (default) or 'bespoke'.
            For "weekly" mode: 'friday'.
        bespoke_calendar (pd.Series): A Series of dates to include if calendar_type is set to "bespoke". Defaults to None.
    
    Returns:
        pd.DataFrame: The resampled DataFrame.
        
    Use example: 
        import pandas as pd
        import dbtools.src.dbtools_util as du
        data = {'date': pd.date_range(start='2023-01-01', end='2023-01-31', freq='D'),
            'attribute_id': range(1, 32),
            'value': range(1, 32)}  # Just some example values       
        df = pd.DataFrame(data)
        
        # Display the original DataFrame
        print("Original DataFrame:")
        print(df)
        # Use the resample_data function to get only weekdays
        resampled_df_weekdays = du.resample_data(df, date_column='date', mode='daily', calendar_type='weekdays')
        # Display the resampled DataFrame with only weekdays
        print("\nResampled DataFrame (Daily, Weekdays):")
        print(resampled_df_weekdays)
        
        # Use the resample_data function to get only Fridays
        resampled_df_fridays = du.resample_data(df, date_column='date', mode='weekly', calendar_type='friday')
        # Display the resampled DataFrame with only Fridays
        print("\nResampled DataFrame (Weekly, Fridays):")
        print(resampled_df_fridays)
        
        # Define a bespoke calendar with specific dates
        bespoke_dates = pd.Series(['2023-01-03', '2023-01-10', '2023-01-17'])
        # Use the resample_data function to get only bespoke dates
        resampled_df_bespoke = du.resample_data(df, date_column='date', mode='daily', calendar_type='bespoke', bespoke_calendar=bespoke_dates)
        # Display the resampled DataFrame with bespoke dates
        print("\nResampled DataFrame (Daily, Bespoke):")
        print(resampled_df_bespoke)
    """
    
    # Ensure the date column is in datetime format
    df.loc[:, date_column] = pd.to_datetime(df[date_column])

    if mode == 'daily':
        if calendar_type == 'weekdays':
            # Filter for weekdays (Monday to Friday)
            df_resampled = df[df[date_column].dt.weekday < 5]
        elif calendar_type == 'bespoke' and bespoke_calendar is not None:
            # Filter for bespoke calendar dates
            bespoke_dates = pd.to_datetime(bespoke_calendar)
            df_resampled = df[df[date_column].isin(bespoke_dates)]
        else:
            raise ValueError("For 'daily' mode, 'calendar_type' must be 'weekdays' or 'bespoke' with a provided 'bespoke_calendar'.")
    
    elif mode == 'weekly':
        if calendar_type == 'friday':
            # Filter for Fridays
            df_resampled = df[df[date_column].dt.weekday == 4]
        elif calendar_type == 'sunday':
            # Filter for Sunday
            df_resampled = df[df[date_column].dt.weekday == 6]
        elif calendar_type == 'monday':
            # Filter for Monday
            df_resampled = df[df[date_column].dt.weekday == 0]
        else:
            raise ValueError("For 'weekly' mode, 'calendar_type' must be 'friday'.")
    
    else:
        raise ValueError("Mode must be 'daily' or 'weekly'.")
    
    return df_resampled

#%% date tuples tools
def apply_function_using_date_tuples(func, start_date, end_date, mode='month', *args, **kwargs):
    if mode=='month':
        date_tuples = generate_monhtly_start_end_dates(start_date,end_date)
    elif mode=='semester': 
        date_tuples = generate_semester_start_end_dates(start_date,end_date)
    
    results = []
    for start_date_m, end_date_m in date_tuples:
        result = func(start_date=start_date_m, end_date=end_date_m, *args, **kwargs)
        results.append(result)
        
    df = pd.concat(results)
    return df


def end_of_month(year, month):
    """ 
    Task 1: give the number of the latest day of the month 
        
    Args:
        year (Integer)
        month (Integer)
    
    Returns:
        last_day (Integer)
    
    Use example:
        import dbtools.src.dbtools_util as du
        du.end_of_month(2024, 5)
    """
    _, last_day = calendar.monthrange(year, month)
    return last_day

def get_month_name(month):
    """
    Task 1: gives month as string with two characters 
        
    Args:
        month (Integer)
    
    Returns:
        String
    
    Use example:
        import dbtools.src.dbtools_util as du
        du.get_month_name(2)
    """
    return f"{month:02d}"

def generate_monhtly_start_end_dates(start_date,end_date):
    """
    Generates monthly start and end dates for a given date range.
    
    Use example: 
        import dbtools.src.dbtools_util as du
        start_date = '20180101'
        end_date = '20240331'
        du.generate_monhtly_start_end_dates(start_date,end_date)
    """
    start_date = datetime.strptime(start_date, '%Y%m%d')
    end_date = datetime.strptime(end_date, '%Y%m%d')

    start_end_tuples = []
    
    # Initialize the current date to the first day of the start month
    current_date = start_date.replace(day=1)
    
    while current_date <= end_date:
        # Get the first day of the current month
        month_start = current_date.strftime('%Y%m%d')
        
        # Get the last day of the current month
        last_day = end_of_month(current_date.year, current_date.month)
        month_end = current_date.replace(day=last_day).strftime('%Y%m%d')
        
        # Add the tuple (month_start, month_end) to the list
        start_end_tuples.append((month_start, month_end))
        
        # Move to the first day of the next month
        if current_date.month == 12:
            current_date = current_date.replace(year=current_date.year + 1, month=1, day=1)
        else:
            current_date = current_date.replace(month=current_date.month + 1, day=1)
    
    return start_end_tuples


def generate_semester_start_end_dates(start_date, end_date):
    """
    Generates semester start and end dates (January-June, July-December) for a given date range.
    
    Use example: 
        import dbtools.src.dbtools_util as du
        start_date = '20180101'
        end_date = '20240331'
        du.generate_semester_start_end_dates(start_date,end_date)
    """
    start_date = datetime.strptime(start_date, '%Y%m%d')
    end_date = datetime.strptime(end_date, '%Y%m%d')

    start_end_tuples = []
    
    # Initialize the current year to the year of the start date
    current_year = start_date.year
    
    while True:
        # Generate semester 1 (January 1 to June 30)
        sem1_start = datetime(current_year, 1, 1)
        sem1_end = datetime(current_year, 6, 30)
        if sem1_start <= end_date and sem1_end >= start_date:
            sem1_start_str = sem1_start.strftime('%Y%m%d')
            sem1_end_str = sem1_end.strftime('%Y%m%d')
            start_end_tuples.append((sem1_start_str, sem1_end_str))
        
        # Generate semester 2 (July 1 to December 31)
        sem2_start = datetime(current_year, 7, 1)
        sem2_end = datetime(current_year, 12, 31)
        if sem2_start <= end_date and sem2_end >= start_date:
            sem2_start_str = sem2_start.strftime('%Y%m%d')
            sem2_end_str = sem2_end.strftime('%Y%m%d')
            start_end_tuples.append((sem2_start_str, sem2_end_str))
        
        # Move to the next year
        current_year += 1
        
        # Break the loop if both semesters are beyond the end date
        if sem1_start > end_date and sem2_end > end_date:
            break
    
    return start_end_tuples