# -*- coding: utf-8 -*-
"""
Created on Mon Aug  8 16:43:56 2022

@author: nle
"""
from datetime import datetime  
import pandas as pd
import numpy as np
from openpyxl import Workbook
from dbtools.src.db_connexion import SqlConnector
import dbtools.src.get_repository as rep
import dbtools.src.get_market_data as mkt
from dbtools.src.QuantWorkUpdater import *
from dbtools.src import bcp_import_to_sql

connector = SqlConnector()
con_mis = connector.connection()
#%% Date to update data: 
update_date = '20250124' # friday before LBP update.
sql_table_fund = 'level2_fundvar_prod'
sql_table_gics = 'additional_stock_GICS_%s' % update_date
sql_table_adjustment = 'QUANT_ADJUSTMENT_FACTOR_add_%s' % update_date
sql_table_index_level = 'Kch_sxxp_index_level'
#%% Create macro excel and Load data from Bloomberg

fund = FundamentalLoader(perimeter = 2, end_date = update_date)
index_level = IndexLevelLoader(perimeter = 1, end_date = update_date)
adjustment = AdjustmentLoader(perimeter = 1, end_date = update_date)
gics = GicsLoader(perimeter = 1, end_date = update_date)


fund.create_macro()
adjustment.create_macro()
gics.create_macro()
index_level.create_macro()
#%% Read data in excel and import to QUANT_work

gics.get_from_excel()
gics.import_to_sql(sql_table_name=sql_table_gics)
print('GICS imported to %s'%sql_table_gics)
#%%
index_level.get_from_excel()
index_level.add_ffama_data()
index_level.import_to_sql(sql_table_name=sql_table_index_level)
print('Index Level imported to %s'%sql_table_index_level)
#%% Read data in excel and import to QUANT_work

fund.get_from_excel()
fund.correct_fundccy()
df_fund = fund.df_db.copy()
fund.correct_EXO()
fund.correct_DSFIR()
fund.import_to_sql(sql_table_name=sql_table_fund)
print('Fundamental data imported to %s'%sql_table_fund)
#%%
adjustment.get_from_excel()
adjustment.compute_adjustment_factor()
adjustment.import_to_sql(sql_table_name=sql_table_adjustment)
print('Adjustment data imported to %s'%sql_table_adjustment)

#%%
sales_europe = SalesEuropeLoader(perimeter=['SXXP', 'SBF120'], start_date = '20240923', end_date = '20250124')
#%%
sales_europe.get_from_excel()
sales_europe.import_to_sql(sql_table_name='sbf_sxxp_sales', drop_existing=False)


