# -*- coding: utf-8 -*-
"""
Created on Fri Feb  3 13:15:13 2023

@author: nle
"""
import pandas as pd
from dbtools.src.QuantWorkUpdater import QuantHolidaysRebal
qhr = QuantHolidaysRebal()
import dbtools.src.DatabaseManager as db

#%% Parameters

file_holidays = 'dissemination_calendar_2024.csv'
msci_rebalancing=[]
threshold = 0.2
#%% Quant holidays

flag_foliday = qhr.weigh_stoxx_holidays(filename = file_holidays,
                                        subfolder = 'stoxx hoidays/',
                                        folder = 'W:/Global_Research/Quant_research/projets/quant_note/liquidity_factor_portfolios/txt_file/')

holidays = flag_foliday[flag_foliday>=threshold].index
holidays = pd.DataFrame(holidays)
holidays.columns = ['date']
print('Check list of holidays:\n%s'%holidays)

#%% MSCI rebalancing
msci_rebalancing = pd.DataFrame(data = msci_rebalancing, columns = ['date'])
print('Check list of msci rebalancing:\n%s'%msci_rebalancing)
#%% Import to sql
sql_table_name = 'quant_holidays'#msci_rebalancing
db_quant_holidays = db.DatabaseManager(sql_table_name)
db_quant_holidays.append_table(df = holidays.copy())


sql_table_name_msci = 'msci_rebalancing'#msci_rebalancing
db_quant_msci = db.DatabaseManager(sql_table_name_msci)
db_quant_msci.append_table(df = msci_rebalancing.copy())








