# -*- coding: utf-8 -*-
"""
Created on Tue Jan  2 16:45:17 2024

@author: krulik
"""


from dbtools.src.DatasetManager import DatasetManager

############## CHECK ###########################

manager = DatasetManager(config_file_name = 'datasets_manager.json')

manager.check_if_dataset_up_to_date('MARKET')

manager.check_if_dataset_up_to_date('BETA_PROFILES_BPE')

manager.check_if_dataset_up_to_date('FAMILY_BUSINESS')

manager.check_if_dataset_up_to_date('BETA_PROFILES_KECH')

manager.check_if_dataset_up_to_date('ERPM_STOCKS')

manager.check_if_dataset_up_to_date('ERPM_INDICES')

manager.check_if_dataset_up_to_date('FACTOR_SENTIMENT')

############### CLIENT OUTPUT ##########################

your_instance = DatasetManager()
dataset_name = 'FACTOR_SENTIMENT'
begin_date = '2023-05-01'
end_date= '2024-05-31'
df = your_instance.fetch_client_output(dataset_name, begin_date, end_date)





############### TESTS ##########################


due_dates = manager.get_date_update_due('BETA_PROFILES_KECH')
expected_dates = manager.get_date_update_due('BETA_PROFILES_KECH')

update_status = manager.check_if_dataset_up_to_date('BETA_PROFILES_KECH')


