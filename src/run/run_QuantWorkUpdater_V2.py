# -*- coding: utf-8 -*-
"""
Created on Tue Jan  2 16:45:17 2024

@author: krulik
"""

from datetime import datetime
from dbtools.src.QuantWorkUpdater_v2 import QuantWorkUpdater
from pandas.tseries.offsets import BDay
from copy import deepcopy
from dbtools.src.check_referential import check_referential
import dbtools.src.update_quant_portfolio as qidx
from dbtools.config.quant_portfolio import QuantIndexConfig as Conf
from dbtools.config.quant_portfolio import QuantIndexLevel as Conf_idx
from dbtools.config.quant_portfolio import QuantIndexPatch as Conf_patch
#%% Check referential (Fundamental ticker and SEDOL)
df_check = check_referential(['KECH', 'BKXP'])

#%% ################## Index_level ###############

loader = QuantWorkUpdater(dataset_name = "LEVEL_INDEX", config_file_name = 'quant_work_updater.json')
config = loader.load_config()

last_date = datetime.date(loader.get_table_last_date() + BDay(1))
due_date = loader.get_date_update_due()

#%% Load data
df_idx_lvl = loader.get_data(last_date, due_date)
df_idx_lvl_ts = df_idx_lvl.set_index(['DATE', 'index_id'])['value'].unstack()
#%% import to INDEX_LEVEL
loader.import_to_sql(df_idx_lvl)

#%% ###################### Fundamental data stock #####################

loader = QuantWorkUpdater(dataset_name = "FUNDAMENTAL_DATA_STOCK", config_file_name = 'quant_work_updater.json')
config = loader.load_config()
start_date = datetime.date(loader.get_table_last_date() + BDay(1)).strftime('%Y%m%d')
end_date = loader.get_date_update_due().strftime('%Y%m%d')
#%%
print('RETRIEVE FUNDAMENTAL DATA FROM %s TO %s'%(start_date, end_date))
df_fund = loader.get_data(start_date, end_date)
df_fund_ts = df_fund.set_index(['DATE', 'attribute_id', 'security_id'])['value'].unstack()
#%%
loader.import_to_sql(df_fund)

#%%################## KC index composition###############
d_config = {
    'idx_fb' : deepcopy(Conf.spec_fb),
    'idx_kech' : deepcopy(Conf.spec_kech),
    'idx_cov' : deepcopy(Conf.spec_cov),
    'idx_lbp_v3' : deepcopy(Conf.spec_lbp_v3),
    'idx_lbp_v4' : deepcopy(Conf.spec_lbp_v4),
    'factor_sentiment' : deepcopy(Conf.spec_factor_sentiment)
    }
d_compo = {}
for k, config_idx in d_config.items():
    print('---------- %s --------'%k)
    if k == 'idx_fb':
        qidx_updater = qidx.FBCompoUpdater(config_idx)
    elif k == 'idx_cov':
        qidx_updater = qidx.CoverageCompoUpdater(config_idx)
    elif k == 'idx_kech':
        qidx_updater = qidx.KechCompoUpdater(config_idx)
    elif k in ['idx_lbp_v3', 'idx_lbp_v4']:
        qidx_updater = qidx.LBPCompoUpdater(config_idx)
    elif k in ['factor_sentiment']:
        qidx_updater = qidx.FactorSentimentUpdater(config_idx)
    df = qidx_updater.run_update_compo(flag_to_sql = True)
    d_compo[k] = df.copy()

#%% ################## KC index level ###############

config_idx = deepcopy(Conf_idx.config)
qiu = qidx.QuantIndexUpdater(config_idx)

table_name = config_idx['table_level']
index_id = config_idx['index_id']
previous_rebal = qiu.get_previous_rebalance()
s_start_date = qiu.df_index_ref.date_level
end_date = qiu.get_end_date(config_idx['update_freq'])
df_compo = qidx.get_index_compo(index_id, previous_rebal, end_date=end_date-BDay(1))

params = qiu.get_param_backtest(df_compo, end_date)
data_bkt = qiu.get_backtest_data(params)
df_perf_db, df_perf_ts = qiu.backtest(data_bkt)
df_perf_ts = df_perf_ts[df_perf_ts.columns.sort_values()]
df_perf_recomp = qiu.recompute_index_level(df_perf_db)
df_perf_recomp_ts = df_perf_recomp.set_index(['DATE', 'index_id']).value.unstack()
df_to_import = qiu.filter_existing_data(df_perf_recomp, s_start_date)
#%% import to sql
qidx.import_to_sql(df_to_import, table_name)

#%% ################## KC index composition patch ###############
df_to_import_ts = qidx.patch_compo()
#%% ################## KC index level patch ###############

config_patch = deepcopy(Conf_patch.config)
qiu = qidx.QuantIndexUpdater(config_patch)

table_name = config_patch['table_level']
index_id = config_patch['index_id']
previous_rebal = qiu.get_previous_rebalance()
s_start_date = qiu.df_index_ref.date_level
end_date = qiu.get_end_date(config_patch['update_freq'])
df_compo = qidx.get_index_compo(index_id, previous_rebal, end_date=end_date-BDay(1), patch = True)

params = qiu.get_param_backtest(df_compo, end_date)
data_bkt = qiu.get_backtest_data(params)
df_perf_db, df_perf_ts = qiu.backtest(data_bkt)
df_perf_ts = df_perf_ts[df_perf_ts.columns.sort_values()]
df_perf_recomp = qiu.recompute_index_level(df_perf_db)
df_perf_recomp_ts = df_perf_recomp.set_index(['DATE', 'index_id']).value.unstack()
df_to_import = qiu.filter_existing_data(df_perf_recomp, s_start_date)
#%% import to sql
qidx.import_to_sql(df_to_import, table_name)









