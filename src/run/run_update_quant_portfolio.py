# -*- coding: utf-8 -*-
"""
Created on Fri Apr 19 13:54:33 2024

@author: nle
"""

import dbtools.src.update_quant_portfolio as qidx
from copy import deepcopy
import pandas as pd
import sys, os
sys.path.insert(0, os.path.join(qidx.__file__, '../..'))
from config.quant_portfolio import QuantIndexConfig as Conf

d_config = {
    'idx_fb' : deepcopy(Conf.spec_fb),
    'idx_kech' : deepcopy(Conf.spec_kech),
    'idx_cov' : deepcopy(Conf.spec_cov),
    'idx_lbp_v3' : deepcopy(Conf.spec_lbp_v3),
    'idx_lbp_v4' : deepcopy(Conf.spec_lbp_v4),
    'factor_sentiment' : deepcopy(Conf.spec_factor_sentiment)
    }
#%% COMPO updater
d_compo = {}
for k, config_idx in d_config.items():
    print('---------- %s --------'%k)
    if k == 'idx_fb':
        qidx_updater = qidx.FBCompoUpdater(config_idx)
    elif k == 'idx_cov':
        qidx_updater = qidx.CoverageCompoUpdater(config_idx)
    elif k == 'idx_kech':
        qidx_updater = qidx.KechCompoUpdater(config_idx)
    elif k in ['idx_lbp_v3', 'idx_lbp_v4']:
        qidx_updater = qidx.LBPCompoUpdater(config_idx)
    elif k in ['factor_sentiment']:
        qidx_updater = qidx.FactorSentimentUpdater(config_idx)
    df = qidx_updater.run_update_compo(flag_to_sql = True)
    d_compo[k] = df.copy()

#%% index updater

d_idx = {}
df_idx_level = pd.DataFrame()
for k in ['idx_fb', 'idx_kech', 'idx_cov', 'idx_lbp_v3', 'idx_lbp_v4']:
    config_idx = d_config[k]
    qidx_updater = qidx.QuantIndexUpdater(config_idx)
    df = qidx_updater.run_update_index(flag_to_sql = False)
    df_idx_level = pd.concat((df_idx_level, df), axis = 1)
    d_idx[k] = df.copy()
#%%

# =============================================================================
# config_idx = deepcopy(Conf.spec_fb)
# index_id = config_idx['index_id']
# table_name = config_idx['table_level']
# previous_rebal = fb_updater.get_previous_rebalance()
# end_date = fb_updater.get_end_date(config_idx['update_freq'])
# s_start_date = fb_updater.df_index_ref.date_level
# 
# df_compo = qidx.get_index_compo(index_id, previous_rebal, end_date=end_date)
# params=fb_updater.get_param_backtest(df_compo, end_date)
# data_bkt = fb_updater.get_backtest_data(params)
# df_perf_db, df_perf_ts = fb_updater.backtest(data_bkt)
# df_perf_recomp = fb_updater.recompute_index_level(df_perf_db)
# df_perf_recomp_ts = df_perf_recomp.set_index(['DATE', 'index_id']).value.unstack()
# 
# df_to_import = fb_updater.filter_existing_data(df_perf_recomp, s_start_date)
# #qidx.import_to_sql(df_to_import, table_name)
# =============================================================================

# =============================================================================
# df_idx_imp = pd.DataFrame()
# for k in d_idx.keys():
#     df = d_idx[k].iloc[1:].stack().to_frame('value').reset_index()
#     df_idx_imp = pd.concat((df_idx_imp, df)).drop_duplicates()
# df_idx_imp.columns= ['DATE','index_id', 'value']
# df_idx_imp = df_idx_imp.sort_values(['index_id', 'DATE'])
# 
# qidx.import_to_sql(df_idx_imp, 'INDEX_LEVEL')
# =============================================================================









