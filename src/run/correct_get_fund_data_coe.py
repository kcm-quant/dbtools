# -*- coding: utf-8 -*-
"""
Created on Tue Dec  3 12:01:20 2024

@author: nle
"""

import os
import pandas as pd
from dbtools.src.db_connexion import SqlConnector
import dbtools.src.get_repository as rep
import dbtools.src.get_fund_data as gfd
import dbtools.src.DatabaseManager as db
from pandas.tseries.offsets import BDay
import dbtools.src.dbtools_util as du
from openpyxl import load_workbook
import numpy as np
import warnings
from copy import deepcopy
import json
from tqdm import tqdm
import matplotlib.pyplot as plt
# from copy import deepcopy
connector = SqlConnector()
con_mis = connector.connection()


#%% input
start_date = '19980101'
end_date = '20250131'
exclude_sheets = ["info&inputs", "KCEUDEF", "KCEUCYC"]
folder_indices = 'W:/Global_Research/Quant_research/projets/market_watch/Cost of Equity/'
file_path_init = folder_indices + f'CoE - Data_{end_date}_init.xlsm'
file_path_corr = folder_indices + f'CoE - Data_{end_date}.xlsm'
file_path_corr_ex = folder_indices + f'CoE - Data_{end_date}.xlsx'
#%%
df_ref = pd.read_sql('select index_id, ticker, currency, short_name from QUANT_work..INDEX_REFERENTIAL', con_mis).set_index('index_id')
wb_new = load_workbook(file_path_init, data_only=True)

worksheet = pd.Index(wb_new.sheetnames).difference(exclude_sheets, sort = False)
wb_new.close()
index_name = 'Dates'

usecols=pd.Index(["Dates", 'Last Price', "EPS1", "EPS2", "EPS3", 'Risk Free', 'FF_MCAP'])
cols_corr = ["EPS1", "EPS2", "EPS3"]
cols_other = ['Last Price', 'Risk Free', 'FF_MCAP']


ids = df_ref[df_ref.ticker.isin(worksheet)].index

ticker = worksheet.intersection(df_ref.loc[ids].ticker)

worksheet_del = worksheet.difference(df_ref.loc[ids].ticker)
#%%


df_fy_all = gfd.GetFundData(ids = ids, start_date = start_date, end_date = end_date, security_type = 'index').get_raw_fund_data('fy_1gy')
df_fy_all = df_fy_all.rename(columns = df_ref.ticker)


df_1y = gfd.GetFundData(ids = ids, start_date = start_date, end_date = end_date, security_type = 'index').get_raw_fund_data('eps_1gy')
df_1y = df_1y.rename(columns = df_ref.ticker)
df_2y = gfd.GetFundData(ids = ids, start_date = start_date, end_date = end_date, security_type = 'index').get_raw_fund_data('eps_2gy')
df_2y = df_2y.rename(columns = df_ref.ticker)
df_3y = gfd.GetFundData(ids = ids, start_date = start_date, end_date = end_date, security_type = 'index').get_raw_fund_data('eps_3gy')
df_3y = df_3y.rename(columns = df_ref.ticker)
df_4y = gfd.GetFundData(ids = ids, start_date = start_date, end_date = end_date, security_type = 'index').get_raw_fund_data('eps_4gy')
df_4y = df_4y.rename(columns = df_ref.ticker)

df_fy_all.columns.difference(df_1y.columns)

#%% correction

df_1y_corr = gfd.correct_fiscal_year(df_fy_all, df_1y, df_2y)
df_2y_corr = gfd.correct_fiscal_year(df_fy_all, df_2y, df_3y)
df_3y_corr = gfd.correct_fiscal_year(df_fy_all, df_3y, df_4y)

#%% resample
df_1y_res = gfd.resample_data_ts(df_1y_corr, frequency = 'friday_plus_last_date', nb_filldays = None)
df_2y_res = gfd.resample_data_ts(df_2y_corr, frequency = 'friday_plus_last_date', nb_filldays = None)
df_3y_res = gfd.resample_data_ts(df_3y_corr, frequency = 'friday_plus_last_date', nb_filldays = None)

#%% intervention excel
tickers_corr = worksheet.intersection(df_1y_res.columns, sort = False)

usecols=pd.Index(['Last Price', "EPS1", "EPS2", "EPS3", 'Risk Free', 'FF_MCAP'])
cols_corr = ["EPS1", "EPS2", "EPS3"]
cols_other = ['Last Price', 'Risk Free', 'FF_MCAP']

df_input = pd.read_excel(file_path_init, sheet_name = 'info&inputs')
df_input.iloc[0,0] = df_input.iloc[0,0].strftime('%d/%m/%Y')
d_data_corr = {}
for sheet_name in tqdm(tickers_corr):
    df_init = pd.read_excel(file_path_init, sheet_name=sheet_name, header=1).set_index(index_name).sort_index(ascending = False)
    df_init = df_init[usecols]
    
    df_corr = df_init.copy()
    df_corr['EPS1'] = df_1y_res[sheet_name]
    df_corr['EPS2'] = df_2y_res[sheet_name]
    df_corr['EPS3'] = df_3y_res[sheet_name]
    d_data_corr[sheet_name] = df_corr.copy()
    
    
    fig, ax = plt.subplots(1, 1)
    df = df_corr[cols_corr].dropna(how ='all')
    df_i = df_init[cols_corr].dropna(how ='all')
    df.plot(ax = ax, figsize = (12,5), title = sheet_name)
    df_i.plot(ax = ax, ls = '--')
    
    date_range = pd.date_range(start=df.index.min(), end=df.index.max(), freq='MS')
    xticks = date_range[date_range.month.isin([1,7])]
    ax.set_xticks(xticks)
    plt.grid()
    plt.show()
#%%
with pd.ExcelWriter(file_path_corr_ex) as writer:
    df_input.to_excel(writer, sheet_name = 'info&inputs', index = False)
    for sheet_name in tqdm(tickers_corr):
        df_tmp = d_data_corr.get(sheet_name).reset_index()
        df_tmp.T.reset_index().T.to_excel(writer, sheet_name=sheet_name, index = False)
#%%
with pd.ExcelWriter(file_path_corr) as writer:
    df_input.to_excel(writer, sheet_name = 'info&inputs', index = False)
    for sheet_name in tqdm(tickers_corr):
        df_tmp = d_data_corr.get(sheet_name).reset_index()
        df_tmp.T.reset_index().T.to_excel(writer, sheet_name=sheet_name, index = False)







