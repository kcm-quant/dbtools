# -*- coding: utf-8 -*-
"""
Created on Mon Nov  6 16:47:12 2023

@author: ssureau
"""

from sqlalchemy import  MetaData, Table, Column, Integer, Float, String, PrimaryKeyConstraint, Sequence, UniqueConstraint, Date
import dbtools.src.DatabaseManager as db
from dbtools.src.db_connexion import SqlConnector
from datetime import datetime

import dbtools.src.get_repository as rep
import dbtools.src.get_market_data as mkt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

connector = SqlConnector()
con_mis = connector.connection()

db_manager = db.DatabaseManager("FUNDAMENTAL_DATA_DICTIONARY")

#%%
column_formats = {
    'attribute_id': Integer,
    'name': String(25),
    'readable_name' :  String(30),
    'description' : String(500)}
df = pd.read_excel(r'W:\Global_Research\Quant_research\.shared files\fundamental_data_dictionary.xlsx')
df = df.drop('level2_fundvar_prod_name', axis=1)

db_manager.create_table(df = df.copy(),column_types=column_formats, primary_keys = ['attribute_id'], auto_incremental_index='attribute_id')

#%% append
df = pd.DataFrame({
    'name' : ['ff_mcap'],
    'readable_name': ['Free-float market cap'],
    'description': ['Free-float market capitalisation (field: FREE_FLOAT_MARKET_CAP)']})

db_manager = db.DatabaseManager("FUNDAMENTAL_DATA_DICTIONARY")
db_manager.append_table(df)

#%% modify fiscal year end to fiscal year 1gy
updates = {
    'name': 'fy_1gy', #'fy_end'
    'readable_name': 'Fiscal year 1GY', #Fiscal year end
    'description': 'Year corresponding to 1GY estimate' #Date of the end of fiscal year
    }

db_manager.modify_record('attribute_id',10, updates=updates)