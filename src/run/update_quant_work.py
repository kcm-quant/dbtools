# -*- coding: utf-8 -*-
"""
Created on Fri Apr  1 16:50:44 2022

@author: nle
"""
# %% Part 1: Function

import string
from datetime import datetime
import pandas as pd
import numpy as np
from openpyxl import Workbook
from backtest import dynamic_security
from src.db_connexion import SqlConnector
import src.get_repository as rep
import src.get_market_data as mkt
from src import bcp_import_to_sql

connector = SqlConnector()
con_mis = connector.connection()


def get_fund_ticker(level=2):
    """
    Load fundamental ticker of given level
    if fundamental ticker not found, fill with
        W:/Global_Research/Quant_research/team/tle/bpe_screening/
            portfolios_factor/source_file/old_fund_for_missing.csv
    if fundamental tiker not found, drop corresponding security_id
    -----------------------
    Return:
        Series: index is security_id, values is fundamental tickers
        file saved in W:/Global_Research/Quant_research/projets/backtest/
                        Excel/Level2_yyyymmdd.xlsx
    """

    level_id = rep.get_quant_perimeter(level=level)
    fund_ticker = rep.mapping_from_security(level_id, code='fund_ticker')
    path = 'W:/Global_Research/Quant_research/projets/backtest/Excel/'
    ticker_old = pd.read_csv(path + 'source_file/old_fund_for_missing.csv', index_col=0)
    ticker_old = ticker_old.FUNDAMENTAL_TICKER
    level_na = level_id.difference(fund_ticker.index)
    fund_missing = ticker_old.loc[level_na.intersection(ticker_old.index)]
    fund_ticker_full = pd.concat([fund_ticker, fund_missing]).sort_index()
    df_temp = pd.DataFrame(fund_ticker_full)
    df_temp.columns = ['FUNDAMENTAL_TICKER']
    df_temp = df_temp.rename_axis(index='security_id')
    filedate = datetime.today().strftime('%Y%m%d')
    filename = '%slevel2/Level2_%s.xlsx' % (path, filedate)
    df_temp.to_excel(filename, sheet_name='Level2')
    print('File fund_ticker_full is saved in %s' % filename)
    return fund_ticker_full

def get_gics_ticker(level):
    """
    get missing GICS tticker top update in Referential
    ------------------------
    level: int, 1 or 2, perimeter of gics data to control
    ---------------------
    Return: Series of missing ticker, security in index, tickers in value
    """
    level1 = rep.get_quant_perimeter(level)
    sector = rep.mapping_sector_from_security(level1, code='GICS')
    sector = sector.dropna(subset=['GICS_INDUSTRY', 'GICS_SECTOR', 'GICS_INDUSTRY_GROUP'])
    gics_missing = level1.difference(sector.index)
    tickers = rep.mapping_from_security(gics_missing, code='bbg')
    return tickers

def get_adj_ticker(level, start_date, end_date):
    """
    get adjustment ticker updated
    ---------------------------
    level: int, quant perimeter (1 or 2)
    start_date, end_date: str yyyymmdd
        start_date is often the previous end_date
    -----------------------
    Return: Serie, security_id in index, and value is ticker bbg
    """
    security_id = rep.get_quant_perimeter(level, start_date, end_date)
    df_adj_fac = mkt.get_corp_action_histo(security_id, start_date, end_date)['adj_prc']
    sec_id_existing = df_adj_fac.columns
    sec_id_missing = security_id.difference(df_adj_fac.columns)
    # check late listing
    # sec_id_confirm  = sec_id_existing[df_adj_fac.isna().sum()==0]
    sec_id_check = sec_id_existing[df_adj_fac.isna().sum() > 0]
    sec_id_check = sec_id_missing.union(sec_id_check)
    sec_add_adj_fac = rep.mapping_from_security(sec_id_check, code='bbg')
    return sec_add_adj_fac

def get_ticker(level, code, start_date, end_date):
    if code=='level2':
        tickers = get_fund_ticker(level)
    elif code =='adjustment':
        tickers = get_adj_ticker(level, start_date, end_date)
    elif code=='gics':
        tickers = get_gics_ticker(level)
    elif code == 'index_level':
        tickers = ['Dates', 'SV2P Index', 'SXSQEP Index', 'SD3P Index',
                   'STVP Index', 'STGP Index', 'M9EUEV Index',
                   'MXEUQU Index', 'MXEUHDVD Index', 'MXEU000G Index',
                   'SXXEWP Index', 'SXXP Index', 'CAC Index', 'CACEW Index']
    return tickers


def parse_macro_gics(tickers, path_to_save):
    """
    Create macro excel file to load data GICS and (or) IBC from Bloomberg
    ---------------
    tickers: Series of tickers (security_id in index, ticker in value)
    path_to_save: save macro excel in path_to_save
    -----------------------
    Return:
        str: path of macro excel
        """
    filename = path_to_save + 'GICS_to_import_%s_formula.xlsx' \
               % datetime.today().strftime('%Y%m%d')
    colnames = pd.Series({'A': 'security_id', 'B': 'ticker',
                          'C': 'GICS_SECTOR', 'D': 'GICS_INDUSTRY_GROUP',
                          'E': 'GICS_INDUSTRY',
                          'F': 'ICB_SECTOR_NUM', 'G': 'ICB_SECTOR_NAME',
                          'H': 'ICB_SUPERSECTOR_NUM', 'I': 'ICB_SUPERSECTOR_NAME',
                          'J': 'ICB_INDUSTRY_NUM', 'K': 'ICB_INDUSTRY_NAME'})
    gics_book = Workbook()
    sheet_temp = gics_book.active
    for (key, value) in colnames.items():
        for (i, (ind, tick)) in enumerate(tickers.items()):
            if i == 0:
                sheet_temp[key + str(i + 1)] = value

            if key == 'A':
                sheet_temp[key + str(i + 2)] = ind
            elif key == 'B':
                sheet_temp[key + str(i + 2)] = tick
            else:
                sheet_temp[key + str(i + 2)] = '=BDP(B%d&" Equity",%s$1)' % (i + 2, key)
    sheet_temp.title = 'BBG formula'
    gics_book.save(filename)
    print('GICS to import saved in %s' % filename)
    return filename

def parse_macro_index_level(tickers, start_date='20100101',
                            end_date=datetime.today().strftime('%Y%m%d'),
                            path_to_save='W:/Global_Research/Quant_research/projets/' \
                                         'backtest/Excel/index_level/'):
    """
    Create index level formular in excel
    """
    START_DATE_ADJ = datetime(2006,1,1)
    columns = list(string.ascii_uppercase)

    index_level = Workbook()
    sheet_temp = index_level.active
    sheet_temp['A1'] = 'PX_LAST'
    sheet_temp['A2'] = 'Start Date'
    sheet_temp['A3'] = 'End Date'
    sheet_temp['B2'] = START_DATE_ADJ
    sheet_temp['B3'] = datetime.strptime(end_date, '%Y%m%d')
    for (i, ind) in enumerate(tickers):
        sheet_temp[columns[i] + '4'] = ind
        if i == 0:
            formula = '=BDH(B$4,$A$1,$B$2,$B$3,"Dir=V","CDR=5D","Days=A","Fill=NA",' \
                      '"UseDPDF=N","Dts=S","Sort=R","cols=2;rows=3110")'
            sheet_temp['A5'] = formula
        elif i == 1:
            continue
        else:
            formula = '=BDH(%s$4,$A$1,$B2,$B3,"Dir=V","CDR=5D","Days=A","Fill=NA",' \
                      '"UseDPDF=N","Dts=H","Sort=R","cols=1;rows=3110")' % columns[i]
            sheet_temp[columns[i] + '5'] = formula

    filename = '%sindex_level_%s.xlsx' % (path_to_save, end_date)
    sheet_temp.title = 'BBG formula'
    index_level.save(filename)
    print('index_level to import saved in %s' % filename)
    return filename

def parse_macro(tickers, code='level2', start_date='', end_date='',
                path_to_save='W:/Global_Research/Quant_research/' \
                             'projets/backtest/Excel/'):
    """
    Create macro excel file to load data from Bloomberg
    ---------------
    tickers: series of tickers with security_id in index,
            or list of security_id
    start_date, start_date: str, '%yyyy%mm%dd'
    code: str, level2, adjustment, gics, index_level
        index_level: parse_macro_index_level,
                save file in path_to_save + index_level_yyyymmdd_formula.xlsx
        gics: call function parse_macro_gics,
                save file in path_to_save + GICS_to_import_yyyymmdd_formula.xlsx
        level2: parse formule for level 2 in
            path_to_save + 'source_file/FIELD_client_macro_level2.xlsx'
        adjustment: parse formule for adjustment in
            path_to_save + 'source_file/for_macro_adjustement_factor.xlsx
    path_to_save: save macro excel,
            default: W:/Global_Research/Quant_research/projets/backtest/Excel/
    -----------------------
    Return:
        str: path of macro excel
    """

    if start_date == '':
        start_date = datetime(2010, 1, 1).strftime('%Y%m%d')
    if end_date == '':
        end_date = pd.date_range(end=datetime.today(), periods=7).normalize()
        end_date = end_date[end_date.weekday == 4][-1].strftime('%Y%m%d')
    if code == 'level2':
        field_client = pd.read_excel(path_to_save +
                                     'source_file/FIELD_client_macro_level2.xlsx')
        filename = 'macro/Raw_data_bbg_level2_%s_weekly_formula.xlsx' % end_date
    elif code == 'adjustment':
        field_client = pd.read_excel(path_to_save +
                                     'source_file/for_macro_adjustement_factor.xlsx')
        filename = 'macro/Raw_data_for_adjustment_%s_formula.xlsx' % end_date
    elif code in ['gics', 'GICS']:
        filename = parse_macro_gics(tickers, path_to_save)
        return filename
    elif code in ['index_level']:
        filename = parse_macro_index_level(tickers, start_date, end_date, path_to_save)
        return filename
    else:
        assert False, 'Only support  code in [level2, gics, adjustment, index_level]'
        return None
    if not isinstance(tickers, pd.Series):
        tickers = rep.mapping_from_security(tickers, code='bbg')
    tickers = pd.Series(tickers)
    start_date = datetime.strptime(start_date, '%Y%m%d')
    end_date = datetime.strptime(end_date, '%Y%m%d')

    fund_level2_new = Workbook()
    nb_stocks = len(tickers)
    sheet_temp = fund_level2_new.active
    sheet_temp['A1'] = 'security_id'
    sheet_temp['B1'] = 'FUNDAMENTAL_TICKER'
    for i in range(nb_stocks):
        sheet_temp['A%s' % (i + 2)] = tickers.index[i]
        sheet_temp['B%s' % (i + 2)] = tickers.values[i]

    for index in range(len(field_client)):
        sheet_temp = fund_level2_new.create_sheet(field_client.sheet_name[index])
        for cell in ['A1', 'B1', 'C1', 'D1']:
            sheet_temp[cell] = field_client[cell][index]
        sheet_temp['B2'] = start_date
        sheet_temp['B3'] = end_date
        sheet_temp['A2'] = 'start_date'
        sheet_temp['A3'] = 'end_date'
        sheet_temp['A4'] = 'Date'

        for i in range(nb_stocks):
            ticker_row = sheet_temp.cell(row=4, column=i + 2, value=tickers.iloc[i])
            coor = ticker_row.coordinate
            if i == 0:
                formula = field_client.formula1[index]
                formula_row = sheet_temp.cell(row=5, column=1, value=formula)

            else:
                if field_client.sheet_name[index] == 'PX_LAST_N_N_Y_fundccy':
                    formula = field_client.formula2[index] % (coor[:-1], coor[-1],
                                                              coor[:-1], coor[-1])
                else:
                    formula = field_client.formula2[index] % (coor[:-1], coor[-1])
                formula_row = sheet_temp.cell(row=5, column=i + 2, value=formula)
    path_to_save = path_to_save + filename
    fund_level2_new.save(path_to_save)
    print('File macro %s is saved in %s' % (code, path_to_save))

    return path_to_save

def get_from_excel_and_stack(path_to_load, sheetname):
    """"
    Regulate Data
        unstack DataFrames with security in columns and date in index
        to a DataFrame with date, security and ticker bbg in index.
    -------------------------
    path_to_load: path excel file (adjustment, or level 2) to load
    sheetname: list of sheetnames in excel file
    ---------------------
    Return Dataframe,
        columns are sheetname, added security_id, ticker and date
    """
    # load security_id and ticker in sheetname 'Sheet' in excel file to map
    sec_map_ticker = pd.read_excel(path_to_load, sheet_name = 'Sheet', index_col=0).squeeze()
    # Read sheets in the workbook
    df_db_stack = pd.DataFrame()
    for j in range(len(sheetname)):
        sheet = sheetname[j]
        print('No.%d - %s'%(j, sheet))
        df_table = pd.read_excel(path_to_load, sheet_name = sheet,
                           header=3, index_col = 0, na_values=['#N/A Invalid Security'])
        df_table = df_table.rename_axis(columns = ['ticker'])
        # mapping ticker in columns to find security_id
        df_transp = df_table.T
        df_transp['security_id'] = sec_map_ticker.index
        df_transp = df_transp.set_index(['security_id'], append = True)
        df_transp = df_transp.rename_axis(index = ['ticker', 'security_id'], columns = 'date')
        # Transform each sheet format table to formar database sql
        df_stack = df_transp.stack(dropna=False).to_frame(sheet)
        df_db_stack = pd.concat((df_db_stack, df_stack), axis = 1)
        print(df_db_stack.shape)

    df_db_stack = df_db_stack.sort_values(by = ['date', 'security_id']).reset_index()
    return df_db_stack


def clean_adjustment(df_stack, flag_volume=False):
    """
    Forward fill weekly (limit=5)
    Delete data on holiday
    Pre-traitement for volume
    ---------------------
    df_stack: DataFrame in format database.
    """
    sec_map_ticker = df_stack[['security_id', 'ticker']].drop_duplicates()
    sec_map_ticker = sec_map_ticker.set_index('security_id').squeeze()

    df_stack = df_stack.set_index(['date', 'security_id']).drop(columns = 'ticker')
    df_unstack = df_stack.unstack()#.fillna(method = 'ffill', limit = 5)
    # delete holidays: nb of na > seuil
    seuil_holiday = 0.05 * df_unstack.shape[1]
    df_unstack = df_unstack[df_unstack.notna().sum(axis = 1) >= seuil_holiday]
    # Only for PX_VOLUME, for the purpose of computing pct_chg
    if flag_volume:
        df_unstack = df_unstack.replace({0:np.nan})
        df_unstack = df_unstack.pct_change(1, fill_method=None)*100
    # clean to adapt to output format database
    df_db_stack = df_unstack.stack(dropna = False).reset_index()
    df_db_stack['ticker'] = df_db_stack.security_id.map(sec_map_ticker)
    df_db_stack  = df_db_stack.set_index(['ticker', 'security_id', 'date'])
    df_db_stack = df_db_stack.reset_index().sort_values(['date', 'security_id'])

    return df_db_stack

def compute_adjustment_factor(path_to_load_adj):
    """
    Recompute adjustment factor
    """

    sheetname_prc = ['CHG_PCT_1D_N_N_N', 'CHG_PCT_1D_N_N_Y', 'CHG_PCT_1D_N_Y_N', 'CHG_PCT_1D_Y_N_N']
    col = ['return_ref', 'return_cap_chg', 'return_cash_abnormal', 'return_cash_normal']
    df_value_reg_chg_pct = get_from_excel_and_stack(path_to_load_adj, sheetname_prc)
    df_value_reg_chg_pct = df_value_reg_chg_pct.rename(columns=dict(zip(sheetname_prc, col)))
    df_value_reg_chg_pct = clean_adjustment(df_value_reg_chg_pct, flag_volume = False)

    sheetname_vlm = ['PX_VOLUME_N_N_N', 'PX_VOLUME_N_N_Y']
    col = ['pct_volume_ref', 'pct_volume_cap_chg']
    df_value_reg_vlm_pct = get_from_excel_and_stack(path_to_load_adj, sheetname_vlm)
    # force NaN volume if price change is Nan for a given day
    df_value_reg_vlm_pct[df_value_reg_chg_pct.iloc[:,:5].isna()] = np.nan
    df_value_reg_vlm_pct = df_value_reg_vlm_pct.rename(columns=dict(zip(sheetname_vlm, col)))
    df_value_reg_vlm_pct = clean_adjustment(df_value_reg_vlm_pct, flag_volume = True)

    df_adj_reg = pd.merge(df_value_reg_chg_pct, df_value_reg_vlm_pct,
                          on=['ticker', 'security_id', 'date'])
    df_adj_reg = df_adj_reg.sort_values(['security_id', 'date']).reset_index(drop=True)
    # trucate the adjustment periode between begin_date_min and end_date_max
    nb_repeat = df_adj_reg.groupby('security_id').count()['ticker']
    date_agg = rep.mapping_from_security(df_adj_reg.security_id, 'date')
    begin_date_min = date_agg.begin_date_min.repeat(nb_repeat).reset_index(drop=True)
    end_date_max = date_agg.end_date_max.repeat(nb_repeat).reset_index(drop=True)

    df_adj_reg_trunc = df_adj_reg[(df_adj_reg.date >= begin_date_min)
                                  & (df_adj_reg.date <= end_date_max)]

    #
    df_adj_reg_trunc.loc[:,'F1_agg'] = ((df_adj_reg_trunc.return_ref / 100 + 1) /
                                  (df_adj_reg_trunc.return_cap_chg / 100 + 1)).round(5)
    df_adj_reg_trunc['F2'] = ((df_adj_reg_trunc.return_ref / 100 + 1) /
                              (df_adj_reg_trunc.return_cash_abnormal / 100 + 1)).round(5)
    df_adj_reg_trunc['F3'] = ((df_adj_reg_trunc.return_ref / 100 + 1) /
                              (df_adj_reg_trunc.return_cash_normal / 100 + 1)).round(5)
    df_adj_reg_trunc['F0'] = ((df_adj_reg_trunc.pct_volume_cap_chg / 100 + 1) /
                              (df_adj_reg_trunc.pct_volume_ref / 100 + 1)).round(2)

    # ############## additional processing for F0 ####################
    # "F0" Processing 1: F0 is forced to 1 if NaN And at the same time F1_agg is not nan
    flag_bad = (df_adj_reg_trunc.F0.isna()) & (df_adj_reg_trunc.F1_agg.notna())
    df_adj_reg_trunc['F0'][flag_bad] = 1

    # "F0" Processing 2: F0 is forced to 1 if F1_agg = 1 (rectifying rounding issues in F0)
    df_adj_reg_trunc['F0'][df_adj_reg_trunc.F1_agg == 1] = 1

    # create F1
    df_adj_reg_trunc['F1'] = df_adj_reg_trunc['F1_agg'] / df_adj_reg_trunc['F0']
    df_adj_reg_trunc['F1'] = df_adj_reg_trunc['F1'].round(5)

    df_adj_reg_trunc = df_adj_reg_trunc.fillna(1)
    df_adj_reg_trunc = df_adj_reg_trunc.sort_values(['date', 'security_id']) \
        .reset_index(drop=True)
    return df_adj_reg_trunc

def create_sales_europe_excel(start_date, end_date,
                              path_to_save='W:/Global_Research/Quant_research/projets/' \
                                           'screener/Factor_client/Publication/'):
    """
    Generation excel file to send to ASTOR Sylvie to update QUANT_work..sbf_sxxp_sales
    ------------------------------------------------------
    prod_date: str, yyyymmdd
    path_to_save: str, path to save file
    -----------------------------------
    Return: filename
    """
    member = dynamic_security(start_date, end_date, index_ticker=['SXXP', 'SBF120'])
    security_id = member.columns

    univers = rep.mapping_from_security(security_id, code="")
    bbg = rep.mapping_from_security(security_id, code="bbg")
    univers['ticker'] = bbg
    univers['member'] = 1

    sector = rep.mapping_sector_from_security(security_id, code='gics')
    univers = pd.concat([univers, sector], axis=1)

    cols = {'SECNAME': 'name', 'ticker': 'ticker', 'GICS_SECTOR': 'sector',
            'GICS_INDUSTRY_GROUP': 'sector_sub', 'member': 'member', 'ISIN': 'ISIN'}
    univers = univers.rename(columns=cols)[cols.values()]

    filename = path_to_save + '%s_Sales_Europe.xlsx' % end_date
    univers.to_excel(filename)
    print('File sales Europe save in %s' % filename)
    return filename


# %% Part 2.1: Formula to update level 2
fund_ticker_full = get_ticker(level=2, code='level2', start_date=None, end_date=None)

path_to_load_level2 = parse_macro(fund_ticker_full, code='level2',
                                  path_to_save='W:/Global_Research/Quant_research/' \
                                               'projets/backtest/Excel/')
# W:/Global_Research/Quant_research/projets/backtest/Excel/
# TODO: connect to BBG post, open file saved in path_to_load_level2
# and replace '@@' by '' and save only values
# guided in https://www.extendoffice.com/documents/excel/4140-excel-save-workbook-as-values.html,
# chnage name to xxx_weekly_value.xlsx

# %% Part 2.2 Load level 2 in dataframe to import
sheetname = ['BEST_BPS_BF', 'BEST_EPS_1GY', 'BEST_EPS_2GY', 'BEST_EPS_3GY',
             'BEST_EPS_BF', 'BEST_EPS_2BF', 'BEST_EPS_STDDEV_BF', 'BEST_PE_RATIO_BF',
             'BEST_PX_BPS_RATIO_BF', 'BEST_ROE_BF', 'EARN_YLD', 'PX_TO_BOOK_RATIO',
             'PX_TO_SALES_RATIO', 'TOT_COMMON_EQY', 'TOT_DEBT_TO_COM_EQY',
             'EARN_YLD_yearly', 'PX_TO_BOOK_RATIO_yearly', 'PX_TO_SALES_RATIO_yearly',
             'TOT_COMMON_EQY_yearly', 'TOT_DEBT_TO_COM_EQY_yearly', 'PX_LAST_N_N_N',
             'PX_LAST_N_N_Y', 'PX_LAST_N_Y_Y', 'PX_LAST_Y_Y_Y', 'PX_LAST_N_N_N_EUR',
             'PX_LAST_N_N_Y_EUR', 'PX_LAST_N_Y_Y_EUR', 'PX_LAST_Y_Y_Y_EUR',
             'PX_LAST_N_N_Y_fundccy', 'CUR_MKT_CAP_Y_Y_Y_EUR', 'EQY_FREE_FLOAT_PCT']

path_to_load_level2_value = path_to_load_level2.split('_formula')[0] + '_value.xlsx'
df_level2_reg = get_from_excel_and_stack(path_to_load_level2_value, sheetname)

# for the PX_LAST_N_N_Y_fundccy there are no values available for three tickers (#N/A Invalid value),
# stocks IAP LN, RGU LN, HBMN SW -> before definite resolution, replace with the local currency prices (PX_LAST_N_N_Y).
index_to_correct = df_level2_reg.ticker.isin(['IAP LN', 'RGU LN', 'HBMN SW'])
fundccy_to_correct = df_level2_reg.loc[index_to_correct,'PX_LAST_N_N_Y']
df_level2_reg.loc[index_to_correct, 'PX_LAST_N_N_Y_fundccy'] = fundccy_to_correct

# %% Part 2.3: import to SQL Server, replace if table exists

try:
    pd.read_sql('drop Table QUANT_work..level2_fundvar_prod', con_mis)
except:
    print('Drop table if existing')

con_dev = SqlConnector().connection()
con_dev.execute('USE QUANT_work')
pd.bulk_copy(con_dev, 'level2_fundvar_prod', df_level2_reg)

# %% Part 3.1: adjustment: Formula to update adjustement level 1

start_date_adj = '20090101'
end_date = '20220729'
sec_add_adj_fac = get_ticker(level=1, code='adjustment', start_date=start_date_adj, end_date=end_date)

path_to_load_adj = parse_macro(sec_add_adj_fac.index.values, 'adjustment', start_date_adj, end_date,
                               path_to_save='W:/Global_Research/Quant_research/projets/data/' \
                                            'dynamic universe/adjustment_factor/Excel/')
# 'W:/Global_Research/Quant_research/projets/data/dynamic universe/adjustment_factor/Excel/'
# TODO: connect to BBG post, open file saved in path_to_load_adj
# replace '@@' by '' and save only values
# guided in https://www.extendoffice.com/documents/excel/4140-excel-save-workbook-as-values.html,
# change name to xxx_value.xlsx

# %% Part 3.2: Load adjustment factors in dataframe to import
path_to_load_adj_value = path_to_load_adj.split('_formula')[0] + '_value.xlsx'
df_adj_reg = compute_adjustment_factor(path_to_load_adj_value)
# %% Part 3.3: Import to SQL Server


con_dev = SqlConnector().connection()

## Conenxion à la base qui nous intéresse
con_dev.execute('USE QUANT_work')
sql_table_import = 'QUANT_ADJUSTMENT_FACTOR_add_%s' % end_date
try:
   pd.read_sql('drop Table QUANT_work..%s'%sql_table_import, con_mis)
except:
   print('Drop table if existing')
## création de la table et écriture des données
df_agg = df_adj_reg.copy()

pd.bulk_copy(con_dev, sql_table_import, df_agg)

# %% Part 4.1: GICS: Load missing GICS to update

gics_missing = get_ticker(level=1, code='gics', start_date=None, end_date=None)

path_to_save_gics = parse_macro(gics_missing, code='gics',
                                path_to_save='W:/Global_Research/Quant_research/' \
                                             'projets/referentiel_KGR/Initialisation_GICS/Excel/')
# W:/Global_Research/Quant_research/projets/referentiel_KGR/Initialisation_GICS/Excel/
# TODO: connect to BBG post, open file saved in path_to_save_gics and replace '@@' by ''
# save only values in a new sheet named Sheet1 or as guided in below link
# guided in https://www.extendoffice.com/documents/excel/4140-excel-save-workbook-as-values.html,
# %% Parth 4.2: Load GICS in dataframe to import
df_gics = pd.read_excel(path_to_save_gics, sheet_name='Sheet1')
df_gics = df_gics[['security_id', 'ticker', 'GICS_SECTOR',
                   'GICS_INDUSTRY_GROUP', 'GICS_INDUSTRY']].dropna()

# %% Part 4.3: Import to SQL Server

sql_table_import = 'additional_stock_GICS_%s' % datetime.today().strftime('%Y%m%d')

con_dev = SqlConnector().connection()
con_dev.execute('USE QUANT_work')

pd.bulk_copy(con_dev, sql_table_import, df_gics)

# %% Parth 5.1: Update index_level
index_tickers = get_ticker(1, code='index_level', start_date=None, end_date=None)
path_index_level = parse_macro(index_tickers, code = 'index_level', start_date='20100101', end_date=end_date,
                               path_to_save='W:/Global_Research/Quant_research/' \
                                            'projets/backtest/Excel/index_level/')
ffama = mkt.get_ffama_data()
# 'W:/Global_Research/Quant_research/projets/backtest/Excel/index_level/'
# TODO: connect to BBG post, open file saved in path_index_level
# save only values in a new sheet named Sheet1 or as guided in below link
# guided in https://www.extendoffice.com/documents/excel/4140-excel-save-workbook-as-values.html,
# %% Part 5.2: Import to SQL Server
try:
    index_level = pd.read_excel(path_index_level, sheet_name='Sheet1', header=3,
                                index_col=0, na_values=['#N/A', '#N/A Invalid Security']).dropna(how='all')
except:
    index_level = pd.read_excel(path_index_level, sheet_name='BBG formula', header=3,
                                index_col=0, na_values=['#N/A']).dropna(how='all')

ffama = ffama.reindex(index_level.index)

index_level = pd.concat([index_level, ffama], axis=1)
index_level = index_level.rename_axis(index=['date'], columns=None)
index_level.to_excel(path_index_level[:-5] + '_ffama.xlsx')
try:
    pd.read_sql('drop table QUANT_work..Kch_sxxp_index_level', con_dev)
except:
    print("Update QUANT_work..Kch_sxxp_index_level")

con_mis.execute('USE QUANT_work')
pd.bulk_copy(con_mis, 'Kch_sxxp_index_level', index_level.reset_index())

# %% Part 6: Generation excel file to send to ASTOR Sylvie to update QUANT_work..sbf_sxxp_sales

start_date_sale = datetime.today().strftime('%Y%m%d')
end_date_sale = datetime.today().strftime('%Y%m%d')
sales_europe = create_sales_europe_excel(start_date_sale, end_date_sale)

# %% update Sale europe
