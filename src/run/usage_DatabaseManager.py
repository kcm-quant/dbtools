# -*- coding: utf-8 -*-
"""
Created on Fri Sep 22 11:32:16 2023

@author: krulik
"""
import pandas as pd
from sqlalchemy import  MetaData, Table, Column, Integer, String, PrimaryKeyConstraint, Sequence, UniqueConstraint
import dbtools.src.DatabaseManager as db
#Usage example

file_path = r"W:\Global_Research\Quant_research\.shared files\data_dictionary_erp.xlsx"

# Read the Excel file into a pandas DataFrame
df = pd.read_excel(file_path)

column_formats = {
    'name': String(50),
    'readable_name': String(100),
    'description':String(200)
    
}
# Create an instance of DatabaseManager for our DataFrame and table
db_manager = db.DatabaseManager( "TEST_usage")

db_manager.create_table(df = df.copy(),column_types=column_formats, 
                        auto_incremental_index='indicator_id' 
                        ,primary_keys = ['name'],
                        unique_columns = ['name'])

# Replace Table



db_manager.replace_table(df = df.copy(),column_types=column_formats, auto_incremental_index='indicator_id',primary_keys = ['name'],unique_columns = ['name'])



data = {
    
    'name': ['pippo1'],
    'readable_name': [None],
    'description': ['My description']
}

# Create DataFrame
df = pd.DataFrame(data)



db_manager.append_table(df = df.copy())
# Error Table 'data_dictionary_erp' does not exist. Cannot append.

#Modify line
updates = {
    'name': 'risk_free',
    'readable_name': 'my_name'
}

db_manager.modify_record(record_id=4, updates=updates)



db_manager.delete_record(record_id=6)
        

#