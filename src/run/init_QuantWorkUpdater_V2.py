# -*- coding: utf-8 -*-
"""
Created on Tue Jan  2 16:45:17 2024

@author: krulik
"""


from dbtools.src.QuantWorkUpdater_v2 import QuantWorkUpdater
import dbtools.src.DatabaseManager as db
from dbtools.src.db_connexion import SqlConnector
import pandas as pd
from apitools.src.apitools import EndOfDayBbg


connector = SqlConnector()
con_mis = connector.connection()

#%% ############ INDEX_LEVEL ################

dataset_name = "LEVEL_INDEX"
loader = QuantWorkUpdater(dataset_name = dataset_name, config_file_name = 'quant_work_updater.json')
config = loader.load_config()
table_name = config[dataset_name]['table_name']

start_date = '19980101'
end_date = '20231229'

#%% load index level from start_date to end_date
# df_index_lvl = loader.get_data(start_date, end_date)# historique trop longue, découpe en plusieurs périodes
df_index_lvl = pd.DataFrame()
years = [1998, 2002, 2006, 2010, 2014, 2018, 2022, 2024]
for i in range(len(years)-1):
    start = '%s0101'%years[i]
    end = '%s1231'%(years[i+1]-1)
    df_tmp = loader.get_data(start, end)
    df_index_lvl = pd.concat((df_index_lvl, df_tmp))

df_ts = df_index_lvl.set_index(['DATE', 'index_id'])['value'].unstack()

#%% delete existing data of index_ids in data base

index_ids = df_index_lvl.index_id.unique()

ids_str = ','.join([str(s) for s in index_ids])
req = f"DELETE FROM {table_name} WHERE index_id in ({ids_str})"
# pd.read_sql(req, con_mis)

#%% import table to INDEX_LEVEL

db_idx = db.DatabaseManager('INDEX_LEVEL')
db_idx.append_table(df_index_lvl)

#%% ###################  FUNDAMENTAL DATA STOCK #######################















