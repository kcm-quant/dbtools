# -*- coding: utf-8 -*-
"""
Created on Fri Sep 27 10:11:45 2024

@author: nle
"""

import pandas as pd
from dbtools.src.db_connexion import SqlConnector
from sqlalchemy import  Integer, Float, Date
import dbtools.src.DatabaseManager as db
connector = SqlConnector()
con_mis = connector.connection()

column_formats = {
    'DATE' : Date,
    'attribute_id' : Integer,
    'security_id': Integer,
    'value': Float,
    'insert_date' : Date
    }
df = pd.DataFrame(columns = column_formats.keys())

db_manager = db.DatabaseManager("FUNDAMENTAL_DATA_STOCK")
db_manager.create_table(df = df.copy(),column_types=column_formats, primary_keys = ['DATE', 'attribute_id', 'security_id'])