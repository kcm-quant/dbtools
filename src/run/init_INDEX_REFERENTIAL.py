# -*- coding: utf-8 -*-
"""
Created on Mon Sep 25 11:19:00 2023

@author: krulik
"""

import pandas as pd
import numpy as np
from sqlalchemy import  MetaData, Table, Column, Integer, String, PrimaryKeyConstraint, Sequence, UniqueConstraint
import dbtools.src.DatabaseManager as db

# Create an instance of DatabaseManager for our DataFrame and table
db_manager = db.DatabaseManager( "INDEX_REFERENTIAL")

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%    Initialisation table INDEX_REFERENTIAL   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
column_formats = {
    'name': String(100),
    'short_name': String(25),
    'ticker' : String(30),
    'INDEXID' : String(50),
    'region' : String(30),
    'category' : String(30),
    'version' : String(30), 
    'currency' : String(3)}

file_path = r"W:\Global_Research\Quant_research\projets\KC Datasets\INDEX_REFERENTIAL\index_referential_init_no_id.xlsx"

# Read the Excel file into a pandas DataFrame
df = pd.read_excel(file_path)


db_manager.create_table(df = df.copy(),column_types=column_formats, 
                        auto_incremental_index='index_id' 
                        ,primary_keys = ['index_id'],
                        unique_columns = ['ticker'])

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%    Replace table INDEX_REFERENTIAL  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
db_manager.replace_table(df = df.copy(),column_types=column_formats, 
                        auto_incremental_index='index_id' 
                        ,primary_keys = ['index_id'],
                        unique_columns = ['ticker'])

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%    Modify table INDEX_REFERENTIAL  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
updates = {'INDEXID': pd.NA}
db_manager.modify_record('ticker', 'GVSK10YR', updates=updates)

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%    Append table INDEX_REFERENTIAL   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data = {'name': ['ERPM long short EPS relative RP reversion strategy'],
    'short_name': ['ERPM long short'],
    'ticker': ['KCERPM'],
    'INDEXID': [''],
    'region': ['Europe'],
    'category': [''],
    'version': ['']}
df = pd.DataFrame(data)
db_manager.append_table(df = df.copy())

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%    Append table INDEX_REFERENTIAL   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data = {'name': ['Europe Defensives using STOXX sectorial indices', 'Europe Cyclicals using STOXX sectorial indices'],
    'short_name': ['Europe Defensives', 'Europe Cyclicals'],
    'ticker': ['KCEUDEF', 'KCEUCYC'],
    'INDEXID': ['',''],
    'region': ['Europe','Europe'],
    'category': ['Sector_Style','Sector_Style'],
    'version': ['STRD','STRD']}
df = pd.DataFrame(data)
db_manager.append_table(df = df.copy())

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%    Append table INDEX_REFERENTIAL   %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
data = {'name': ['MSCI WORLD', 'MSCI KOREA', 'KOREA 10Y GOVT BOND'],
    'short_name': ['MSCI World', 'MSCI Korea', 'KOR 10Y'],
    'ticker': ['MXWO', 'MXKR', 'GVSK10YR'],
    'region': ['World','Korea','Korea'],
    'category': ['General','Country','Interest_rate'],
    'version': ['STRD','STRD','YLD'],
    'currency': ['USD','KRW','KRW']}
df = pd.DataFrame(data)
db_manager.append_table(df = df.copy())

db_manager.delete_record('ticker','MXWO') 
db_manager.delete_record('ticker','MXKR') 
db_manager.delete_record('ticker','GVSK10YR') 