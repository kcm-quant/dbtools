# -*- coding: utf-8 -*-
"""
Created on Thu May 30 14:23:59 2024

@author: nle
"""

from sqlalchemy import  MetaData, Table, Column, Integer, Float, String, PrimaryKeyConstraint, Sequence, UniqueConstraint, Date
import dbtools.src.DatabaseManager as db
from dbtools.src.db_connexion import SqlConnector
from datetime import datetime

import dbtools.src.get_repository as rep
import dbtools.src.get_market_data as mkt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

connector = SqlConnector()
con_mis = connector.connection()


#%% init "factor_data_dictionary"
db_manager = db.DatabaseManager("factor_data_dictionary")
column_formats = {
    'indicator_id': Integer,
    'name': String(25),
    'readable_name' :  String(30),
    'description' : String(500)}
df = pd.read_csv(r'C:\python_tools\dbtools\data\clean\factor_data_dictionary.csv')
df = df.drop(columns=['attribute_id'])
db_manager.create_table(df = df.copy(),column_types=column_formats, primary_keys = ['indicator_id'], auto_incremental_index='indicator_id')
#%% init "factor_stock_indicator"
db_manager = db.DatabaseManager("factor_stock_indicator")
column_formats = {
    'date': Date,
    'security_id': Integer,
    'indicator_id': Integer,
    'value': Float,
    'insert_date': Date,}
df = pd.DataFrame(columns = column_formats.keys())
db_manager.create_table(df = df.copy(),column_types=column_formats, primary_keys = ['date', 'indicator_id', 'security_id'])
#%%
map_attr_id = pd.read_sql('select indicator_id, name from QUANT_work..factor_data_dictionary', con_mis)
map_attr_id = map_attr_id.set_index('name').indicator_id

prod_dates = pd.to_datetime(['20230224', '20230526', '20230825', '20231124', '20240223', '20240524'])
df_db = pd.DataFrame(columns= ['date', 'indicator_id', 'security_id', 'value', 'insert_date'])
d_score = {}
for d in prod_dates:
    date_str = d.strftime('%Y%m%d')
    eom = pd.bdate_range(start= d, periods = 10)
    eom = eom[eom.month.diff(-1)<0].date[0]
    year = d.year
    filepath = 'W:/Global_Research/Quant_research/projets/screener/Factor_kch_coverage/Publication/%s/%s/Breakdown_%s.xlsx'%(year, date_str, date_str)
    df_score = pd.read_excel(filepath, sheet_name='Score_init')
    df_score = df_score.rename(columns = {'high_dvd':'dividend'})
    df_score = df_score.set_index(['security_id'])
    df_score = df_score[['value', 'growth', 'lowvol', 'highvol', 'quality','momentum', 'dividend']].sort_index().round(4)
    df_score.columns.name = 'indicator_id'
    df_score = df_score.rename(columns = map_attr_id)
    df_score_db = df_score.T.stack(dropna= False).to_frame('value').reset_index()
    df_score_db['date'] = eom
    df_score_db['insert_date'] = datetime.today().date()
    df_db = pd.concat((df_db, df_score_db))
    d_score[eom] = df_score_db.copy()
#%%
db_manager = db.DatabaseManager("factor_stock_indicator")
db_manager.append_table(df_db)





