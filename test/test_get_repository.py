# -*- coding: utf-8 -*-
"""
This module test functions in module get_repository.py

@author: tle (tle@keplercheuvreux.com)

Parameter of test:

    #. Past 2 years from today
    #. 1000 random securities from level 1
"""

from datetime import datetime, timedelta
import pandas as pd
import numpy as np
from dbtools.src.get_repository import *

today = datetime.today()
past_2y = today - timedelta(2 * 365)
end_date = (today- timedelta(30)).strftime('%Y%m%d')
start_date = past_2y.strftime('%Y%m%d')

quant = get_quant_perimeter()
np.random.shuffle(quant.values)
security_id = quant[:1000]
cols = ['cond', 'error']


def test_get_kc_analyst():
    """
    output get_kc_analyst is not an empty DataFrame
    """
    df = get_kc_company()
    error_df = 'output get_kc_analyst is not DataFrame'
    error_emp = 'output is empty'
    cond_df = isinstance(df, pd.DataFrame)
    cond_emp = len(df) != 0
    cond = ['type', 'empty']
    error = [[cond_df, error_df], [cond_emp, error_emp]]
    error = pd.DataFrame(error, index=cond, columns=cols)

    cond = ~error.cond
    temp = error['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp
    return None


def test_get_kc_sector():
    """
    output get_kc_analyst is not an empty DataFrame
    """
    df = get_kc_sector()
    error_df = 'output get_kc_sector is not DataFrame'
    error_emp = 'output is empty'
    cond_df = isinstance(df, pd.DataFrame)
    cond_emp = len(df) != 0
    cond = ['type', 'empty']
    error = [[cond_df, error_df], [cond_emp, error_emp]]
    error = pd.DataFrame(error, index=cond, columns=cols)

    cond = ~error.cond
    temp = error['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp
    return None


def test_get_kc_company():
    """
    output get_kc_analyst is not an empty DataFrame
    """
    df = get_kc_sector()
    error_df = 'output get_kc_company is not DataFrame'
    error_emp = 'output is empty'
    cond_df = isinstance(df, pd.DataFrame)
    cond_emp = len(df) != 0
    cond = ['type', 'empty']
    error = [[cond_df, error_df], [cond_emp, error_emp]]
    error = pd.DataFrame(error, index=cond, columns=cols)

    cond = ~error.cond
    temp = error['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp
    return None


def test_quant_perimeter():
    """
    output quant_perimeter is >= 2000 elements
    """
    quant = get_quant_perimeter()
    cond = not (len(quant) < 2000)
    assert cond, 'output quant_perimeter is <= 2000 elements'
    return None


def test_mapping_from_index():
    """
    Output mapping_from_index is not an empty DataFrame or Series
    """
    df = mapping_from_index([1, 2, 3, 4], code='ticker')
    error_df = 'output mapping_from_index is not DataFrame nor Series'
    error_emp = 'output is empty'
    cond_df = isinstance(df, pd.Series)
    cond_emp = len(df) != 0
    cond = ['type', 'empty']
    error = [[cond_df, error_df], [cond_emp, error_emp]]
    error = pd.DataFrame(error, index=cond, columns=cols)
    cond = ~error.cond
    temp = error['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp
    return None


def test_index_comp():
    """
    Output mapping_from_index is not an empty DataFrame or Series
    output SXXP has less than 600 compositions
    """
    df = get_index_comp('SXXP', start_date, end_date)

    error_df = 'output index_comp is not DataFrame'
    error_emp = 'output is empty'
    error_size = 'output has %d less than 600 compositions' % len(df.columns)
    cond_df = isinstance(df, pd.DataFrame)
    cond_emp = len(df) != 0
    cond_size = len(df.columns) >= 599
    cond = ['type', 'empty', 'size']
    error = [[cond_df, error_df], [cond_emp, error_emp], [cond_size, error_size]]
    error = pd.DataFrame(error, index=cond, columns=cols)
    cond = ~error.cond
    temp = error['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp
    return None


def test_mapping_from_security_when_code_not_empty():
    """
    output mapping_from_security is a Series without missing security_id
    """
    df = mapping_from_security(security_id, code='ISIN')
    error_df = 'output mapping_from_security is not Series'
    error_size = 'mapping_from_security wrong for KGR..HISTO_SECURITY_QUANT (isin, sedol, security_id)'
    cond_df = isinstance(df, pd.Series)
    cond_size = len(df) > len(security_id) * 0.9
    cond = ['type', 'size']
    error = [[cond_df, error_df], [cond_size, error_size]]
    error = pd.DataFrame(error, index=cond, columns=cols)
    cond = ~error.cond
    temp = error['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp
    return None


def test_mapping_from_security_when_code_bbg():
    """
    output mapping_from_security is a DataFrame without missing security_id
    """
    ticker = mapping_from_security(security_id, code='bbg')
    error_df = 'output mapping_from_security is not Series'
    error_size = 'mapping_from_security wrong for ticker KGR..HISTO_SECURITY_QUANT_REFERENCE'
    cond_df = isinstance(ticker, pd.Series)
    cond_size = len(ticker) > len(security_id) * 0.9
    cond = ['type', 'size']
    error = [[cond_df, error_df], [cond_size, error_size]]
    error = pd.DataFrame(error, index=cond, columns=cols)
    cond = ~error.cond
    temp = error['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp
    return None


def test_mapping_from_security_when_code_fund_ticker():
    """
    output mapping_from_security is a DataFrame without missing security_id
    """
    fund_ticker = mapping_from_security(security_id, code='fund_ticker')
    error_df = 'output mapping_from_security is not Series'
    error_size = 'mapping_from_security wrong for fund ticker KGR..HISTO_SECURITY_QUANT_REFERENCE'
    cond_df = isinstance(fund_ticker, pd.Series)
    cond_size = len(fund_ticker) > len(security_id) * 0.75
    cond = ['type', 'size']
    error = [[cond_df, error_df], [cond_size, error_size]]
    error = pd.DataFrame(error, index=cond, columns=cols)
    cond = ~error.cond
    temp = error['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp
    return None


def test_mapping_sector_from_security():
    """
    #. output mapping_from_security is a DataFrame without missing security_id
    #. All sectors GICS and ICB in columns of dataframe
    """
    df = mapping_sector_from_security(security_id, code='')
    error_df = 'output mapping_sector_from_security is not DataFrame'
    error_size = 'mapping_sector_from_security wrong for size (<50% security_id)'
    error_col = """missing columns among ['GICS_INDUSTRY', 'GICS_INDUSTRY_GROUP', 'GICS_SECTOR', 'ICB_INDUSTRY',
                 'ICB_SECTOR', 'ICB_SUBSECTOR', 'ICB_SUPERSECTOR', 'KC_SECTOR']"""
    cond_df = isinstance(df, pd.DataFrame)
    cond_size = len(df) > len(security_id) * 0.5
    cond_col = len(df.columns.intersection(
        ['GICS_INDUSTRY', 'GICS_INDUSTRY_GROUP', 'GICS_SECTOR', 'ICB_INDUSTRY',
         'ICB_SECTOR', 'ICB_SUBSECTOR', 'ICB_SUPERSECTOR', 'KC_SECTOR'])) == 8
    error = [[cond_df, error_df], [cond_size, error_size], [cond_col, error_col]]
    cond = ['type', 'size', 'columns']
    error = pd.DataFrame(error, index=cond, columns=cols)
    cond = ~error.cond
    temp = error['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp
    return None


def test_mapping_security_id_from_when_code_isin():
    """
    output mapping_from_security is a DataFrame without missing security_id

    """
    isin = mapping_from_security(security_id, 'isin')
    sec_from_isin = mapping_security_id_from(isin, 'isin')

    error_df = 'output mapping_from_security is not DataFrame or Series'
    error_size = 'test_mapping_security_id_from wrong for isin'
    cond_df = isinstance(sec_from_isin, (pd.DataFrame, pd.Series))
    cond_size = len(sec_from_isin) > len(security_id) * 0.98
    cond = ['type', 'size']
    error = [[cond_df, error_df], [cond_size, error_size]]
    error = pd.DataFrame(error, index=cond, columns=cols)
    cond = ~error.cond
    temp = error['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp
    return None


def test_mapping_security_id_from_when_code_sedol():
    """
    output mapping_from_security is a DataFrame without missing security_id
    """
    sedol = mapping_from_security(security_id, 'sedol').dropna()
    sec_from_sedol = mapping_security_id_from(sedol, 'sedol')

    error_df = 'output mapping_from_security is not DataFrame or Series'
    error_size = 'test_mapping_security_id_from wrong for sedol'
    cond_df = isinstance(sec_from_sedol, (pd.DataFrame, pd.Series))
    cond_size = len(sec_from_sedol) > len(security_id) * 0.95
    cond = ['type', 'size']
    error = [[cond_df, error_df], [cond_size, error_size]]
    error = pd.DataFrame(error, index=cond, columns=cols)
    cond = ~error.cond
    temp = error['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp
    return None


def test_mapping_security_id_from_when_code_bbg():
    """
    output mapping_from_security is a DataFrame without missing security_id
    """
    ticker = mapping_from_security(security_id, 'bbg').dropna()
    sec_from_ticker = mapping_security_id_from(ticker, 'bbg')
    error_df = 'output mapping_from_security is not DataFrame or Series'
    error_size = 'output mapping_security_id_from wrong for ticker'
    cond_df = isinstance(sec_from_ticker, (pd.DataFrame, pd.Series))
    cond_size = len(sec_from_ticker) > len(security_id) * 0.95
    cond = ['type', 'size']
    error = [[cond_df, error_df], [cond_size, error_size]]
    error = pd.DataFrame(error, index=cond, columns=cols)
    cond = ~error.cond
    temp = error['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp
    return None


def test_mapping_from_td_id():
    """
    #. output mapping_from_security is a DataFrame without missing trading_destination_id
    #. 'OPERATINGMIC', 'TRADING_NAME' in columns
    """
    td = mapping_from_security(security_id, 'prim_td_id').sort_values().drop_duplicates()
    td = td[td != 8]  # exclude market HongKong
    df = mapping_from_td_id(td)
    error_df = 'output mapping_from_td_id is not DataFrame'
    error_size = 'output mapping_from_td_id does not equal to len(td)'
    error_col = "output mapping_from_td_id missing columns ['OPERATINGMIC', 'TRADING_NAME']"
    cond_df = isinstance(df, pd.DataFrame)
    cond_size = len(df) == len(td)
    cond_col = len(df.columns.intersection(['OPERATINGMIC', 'TRADING_NAME'])) == 2
    cond = ['type', 'size', 'columns']
    error = [[cond_df, error_df], [cond_size, error_size], [cond_col, error_col]]
    error = pd.DataFrame(error, index=cond, columns=cols)
    cond = ~error.cond
    temp = error['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp
    return None


def test_get_fx_histo():
    """
    #. output get_fx_histo is a DataFrame with length equal to len(ccy)
    """
    ccy = mapping_from_security(security_id, 'ccy').unique()
    df = get_fx_histo(ccy, start_date, end_date)
    error_df = 'output get_fx_histo is not DataFrame'
    error_size = 'output get_fx_histo is not equal to len(ccy)'
    cond_df = isinstance(df, pd.DataFrame)
    cond_size = len(df.columns.intersection(ccy)) == len(ccy)
    cond = ['type', 'size']
    error = [[cond_df, error_df], [cond_size, error_size]]
    error = pd.DataFrame(error, index=cond, columns=cols)
    cond = ~error.cond
    temp = error['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp
    return None


def test_get_flag_lms():
    """
    #. output get_flag_lms is not DataFrame
    #. output get_flag_lms is >=600 elements
    #. output get_flag_lms flag L, M is >=200 elements
    #. get_flag_lms error: values  in [-1, 0, 1, 2]
    """
    df = get_flag_lms(start_date, end_date)
    error_df = 'output get_flag_lms is not DataFrame'
    error_size = 'output get_flag_lms is <600 elements'
    error_l = 'output get_flag_lms flag L is <200 elements'
    error_m = 'output get_flag_lms flag M is <200 elements'
    error_s = 'output get_flag_lms flag S is <200 elements'
    error_flag = 'get_flag_lms error: values not in [-1, 0, 1, 2]'
    cond_df = isinstance(df, pd.DataFrame)
    cond_size = len(df.columns) >= 598
    cond_s = ((df == 2).sum(axis=1) < 190).sum() == 0
    cond_m = ((df == 1).sum(axis=1) < 190).sum() == 0
    cond_l = ((df == 0).sum(axis=1) < 190).sum() == 0
    cond_flag = (df.max().max() == 2) or df.min().min() == -1
    cond = ['type', 'size', 'cond_l', 'cond_m', 'cond_s', 'cond_flag']
    error = [[cond_df, error_df], [cond_size, error_size], [cond_l, error_l],
             [cond_m, error_m], [cond_s, error_s], [cond_flag, error_flag]]
    error = pd.DataFrame(error, index=cond, columns=cols)
    cond = ~error.cond
    temp = error['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp
    return None
