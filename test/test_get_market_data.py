# -*- coding: utf-8 -*-
"""
This module test functions in module get_market_data.py

@author: tle (tle@keplercheuvreux.com)

Parameter du test:

    #. Past 2 years from today
    #. 250 random securities from level 1
    #. reference currency: EUR
"""

from datetime import datetime, timedelta
import pandas as pd
import numpy as np
import os
from dbtools.src.get_repository import *
from dbtools.src.get_market_data import *

today = datetime.today()
past_2y = today - timedelta(2 * 365)
end_date = (today- timedelta(30)).strftime('%Y%m%d')
start_date = past_2y.strftime('%Y%m%d')

quant = get_quant_perimeter(level=1)
np.random.shuffle(quant.values)
security_id = quant[:250].union([351446]).sort_values()
cols = ['cond', 'error']
field = ['close_prc']
fx_adj = ['turnover', 'prc', 'qty_1er_limit', 'tick_size']
ccy_ref = 'EUR'


def test_fx_adjustment():
    """
    #. output is a dataframe with the same index and columns with input
    #. application to a serie in EUR doesn t change the result"""
    sec_str = ",".join(map(str, security_id))
    fld_str = ",".join(field)

    prim_td = rep.mapping_from_security(security_id, 'prim_td_id')
    query = """
            select distinct security_id, date, trading_destination_id, %s
            from MARKET_DATA..trading_daily
            where security_id in (%s)
            and date between '%s' and '%s'
            """ % (fld_str, sec_str, start_date, end_date)
    ### Load data, prim market, and delete trading destination NaN
    df = pd.read_sql(query, con_mis)
    df['is_prim'] = df.security_id.map(prim_td) == df.trading_destination_id
    df = df.set_index(['date', 'security_id'])
    df = df.dropna(subset=['trading_destination_id'])
    df = df[df.is_prim].close_prc
    df_local = df.unstack()
    df_fx_adj = adjust_fx(df_local, ccy_ref=ccy_ref)

    ccy = rep.mapping_from_security(security_id, code='ccy')
    sec_eur = ccy[ccy == ccy_ref].index
    df_eur = df_local[df_local.columns.intersection(sec_eur)]

    err_df = 'output is not a dataframe'
    err_size = 'output size does not match input size'
    err_eur = 'test with EUR securities, output before and after adjusting fx is different'
    cond_df = isinstance(df_fx_adj, pd.DataFrame)
    cond_size = ((df_local.shape == df_fx_adj.shape)
                 and (len(df_local.columns.difference(df_fx_adj.columns)) == 0)
                 and (len(df_local.index.difference(df_fx_adj.index)) == 0))
    cond_eur = (df_eur.fillna(0) - df_fx_adj[df_fx_adj.columns.intersection(sec_eur)].fillna(0)).sum().sum() < 1e-4

    cond = ['type', 'size', 'sec EUR']
    err = [[cond_df, err_df], [cond_size, err_size], [cond_eur, err_eur]]
    err = pd.DataFrame(err, index=cond, columns=cols)
    cond = ~err.cond
    temp = err['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp.values
    return None


def test_get_split_histo():
    """
    #. output is a dict with ['adj_volume', 'adj_prc', 'adj_prc_special', 'adj_prc_regular']
    #. 4 df have same date, sec_id, order
    """

    dict_split = get_corp_action_histo(security_id, start_date, end_date)
    volume = dict_split['adj_volume']
    cond_shape = True
    cond_index = True
    cond_columns = True
    cond_df = True
    cond_dict = isinstance(dict_split, dict)
    for key in dict_split.keys():
        cond_df = cond_df and isinstance(dict_split[key], pd.DataFrame)
        cond_shape = cond_shape and (dict_split[key].shape == volume.shape)
        try:
            cond_index = cond_index and ((dict_split[key].index != volume.index).sum() == 0)
        except:
            cond_index = False
        try:
            cond_columns = cond_columns and ((dict_split[key].columns != volume.columns).sum() == 0)
        except:
            cond_columns = False

    err_dict = 'ouptut is not a dict'
    err_df = 'elements in dict output are not dataframe'
    err_shape = 'elements shape are different'
    err_index = 'elements index are different, not in the same order'
    err_columns = 'elements columns are different, not in the same order'

    cond = ['output', 'element in oputput', 'shape', 'index', 'columns']
    err = [[cond_dict, err_dict], [cond_df, err_df], [cond_shape, err_shape],
           [cond_index, err_index], [cond_columns, err_columns]]
    err = pd.DataFrame(err, index=cond, columns=cols)
    cond = ~err.cond
    temp = err['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp.values
    return None


def test_split_adjustment():
    """
    #. output is a dataframe with the same index and columns with input
    #. when code = "is_volume", ignore 2 params "special_adj", "regular_adj"
        If
            - code = is_volume
            - special_adj = True
            - regular_adj = True

        The results of volume is the same with
            - code = is_volume
            - special_adj = False
            - regular_adj = False
    """
    field = ['close_prc', 'volume']
    sec_str = ",".join(map(str, security_id))
    fld_str = ",".join(field)

    prim_td = rep.mapping_from_security(security_id, 'prim_td_id')
    query = """
            select distinct security_id, date, trading_destination_id, %s
            from MARKET_DATA..trading_daily
            where security_id in (%s)
            and date between '%s' and '%s'
            """ % (fld_str, sec_str, start_date, end_date)
    ### Load data, prim market, and delete trading destination NaN
    df = pd.read_sql(query, con_mis)
    df['is_prim'] = df.security_id.map(prim_td) == df.trading_destination_id
    df = df.set_index(['date', 'security_id'])
    df = df.dropna(subset=['trading_destination_id'])
    prc = df[df.is_prim].close_prc.unstack()
    volume = df[df.is_prim].volume.unstack()

    volume_prc_false = adjust_corp_action(volume, code='is_volume',
                                          split_adj=False, special_adj=False, regular_adj=False)

    volume_prc_true = adjust_corp_action(volume, code='is_volume',
                                         split_adj=False, special_adj=True, regular_adj=True)

    volume_diff = (abs((volume_prc_true.fillna(0) - volume_prc_false.fillna(0)) > 1e-1)).sum().sum() == 0

    prc_all_adj = adjust_corp_action(prc, code='is_prc', split_adj=True, special_adj=True, regular_adj=True)
    prc_split = adjust_corp_action(prc, code='is_prc', split_adj=True, special_adj=False, regular_adj=False)

    cond = ['output', 'shape', 'index', 'columns']
    err_df = 'ouput is not a dataframe'
    err_shape = 'ouput shape are different with input'
    err_index = 'ouput index are different, not in the same order with input'
    err_columns = 'ouput columns are different, not in the same order with input'

    err_is_volume = 'when code = "is_volume", ignore param "split_adj", "special_adj", "regular_adj"'
    cond_is_volume = 'code = "is_volume"'

    df_err = pd.DataFrame([[volume_diff, err_is_volume]], index=[cond_is_volume], columns=cols)

    for (i, temp) in enumerate([prc_all_adj, volume_prc_false]):
        cond_df = isinstance(temp, pd.DataFrame)
        cond_shape = temp.shape == volume.shape
        cond_index = False
        cond_columns = False
        if cond_shape:
            cond_index = (temp.index != volume.index).sum() == 0
            cond_columns = (temp.columns != volume.columns).sum() == 0

        err = [[cond_df, err_df], [cond_shape, err_shape],
               [cond_index, err_index], [cond_columns, err_columns]]
        index = pd.Series(cond)
        index = '(prc) ' + index if i == 0 else '(volume) ' + index
        err = pd.DataFrame(err, index=index, columns=cols)
        df_err = pd.concat((df_err, err))

    # prc_volume_split = prc_split.fillna(0) * volume_prc_false.fillna(0)
    # prc_volume = prc.fillna(0) * volume.fillna(0)
    # prc_volume_diff = (prc_volume - prc_volume_split)
    # temp =  prc_volume_diff.replace({0: np.nan}).stack()
    # temp1 = temp.unstack()
    # split2check = temp1.columns
    # temp2 = prc_volume_split[split2check] / prc_volume[split2check]
    # temp3 = temp2[temp1.notna()].unstack().dropna().round(7).drop_duplicates(keep = 'last')
    # temp3 = temp3.reset_index()
    # temp3.columns = ['security_id', 'date', 'ratio']
    # temp3.to_excel(r'W:\Global_Research\Quant_research\team\tle\test_market_data\adjustment_to_check_level1.xlsx')

    cond = ~df_err.cond
    temp = df_err['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp.values
    return None


def test_read_trading_daily():
    """
    Check if:
        #. Fields are not in output.keys()
        #. trading_destinations are not in output[key1].keys()
        #. Main market: index is not unique
        #. Consolidated market: index is not the same with MAIN
        #. Consolidated market: columns is not the same with MAIN
        #. Consolidated market: recomputed consolidated market is not equal to read_trading_daily consolidated market
        #. Local data without adjustment factor are not NA, but converted to EUR data with adjustment factor are NA
    """
    field = ['close_prc', 'volume']
    trading_destinations = ['MAIN', 159, 183, 189, 288, 289]
    split_adj = True
    special_adj = False
    regular_adj = False
    flag_consolidated  = True
    ccy_ref = 'EUR'

    global dict_rtd
    dict_rtd = get_trading_daily(security_id, start_date, end_date, ccy_ref=ccy_ref, field=field,
                                 trading_destinations=trading_destinations, flag_consolidated =flag_consolidated ,
                                 split_adj=split_adj, special_adj=special_adj, regular_adj=regular_adj)

    key1 = pd.Index(dict_rtd.keys())
    i = np.random.randint(0, len(key1))
    j = np.random.randint(0, len(trading_destinations))
    key2 = pd.Index(dict_rtd[key1[i]].keys())
    ## check condition on trading_destination_id and fields
    df_rtd = dict_rtd[key1[i]]['MAIN']
    cond_dict1 = (len(key1.difference(field)) == 0) and (len(key1) == len(field))
    cond_dict2 = key2.difference(trading_destinations)
    cond_dict2 = (cond_dict2 != 'CONS').sum() == 0
    err_dict1 = 'fields are not in output.keys()'
    err_dict2 = 'trading_destinations are not in output[key1].keys()'

    ## check condition if compute null = True
    ### on columns(security_id) and index (date)
    err_null_index = 'Consolidated market: index is not the same with MAIN'
    err_null_columns = 'Consolidated market: columns is not the same with MAIN'
    err_null_recomp = '''Consolidated market: recomputed consolidated market 
                        is not equal to read_trading_daily consolidated market'''
    cond_null_index = True
    cond_null_columns = True
    cond_null_recomp = True

    if flag_consolidated :
        cond_null_index = (dict_rtd[key1[i]]['CONS'].index != df_rtd.index).sum() == 0
        cond_null_columns = (dict_rtd[key1[i]]['CONS'].columns != df_rtd.columns).sum() == 0

        ### condition on trading consolidated market aggregation ####
        aggr_filename = 'config/consolidated_market_aggregation.txt'
        path_config = os.path.join(os.path.dirname(__file__), '..')
        try:
            f = open(os.path.join(path_config, aggr_filename), "r")
        except:

            f = open(os.path.join(path_config, 'dbtools', aggr_filename), "r")
        aggr = eval(f.read())
        # recompute condolidated market from given trading_destination_id
        df_temp = pd.DataFrame()
        for k1 in key1:
            df = pd.DataFrame()
            for k2 in key2.difference(['CONS']):
                temp = dict_rtd[k1][k2].unstack().to_frame(k1)
                temp['td_id'] = k2
                temp.index.set_names(['security_id', 'date'], inplace=True)
                df = pd.concat((df, temp))
            df = df.reset_index()
            columns = df.columns.difference(df_temp.columns)
            df_temp = pd.concat((df_temp, df[columns]), axis=1)
        df_temp = df_temp.set_index(['date', 'security_id'])
        df_temp['is_prim'] = df_temp.td_id == 'MAIN'

        df_cons = pd.DataFrame(index=df_temp.index.drop_duplicates().sort_values(), columns=df_temp.columns)
        for key, value in aggr.items():
            colunms = df_temp.columns.intersection(value)
            if len(colunms) == 0:
                continue
            if key == 'sum':
                numeric_cols = df_temp.select_dtypes(include=[np.number]).columns
                columns = pd.Index(numeric_cols).intersection(value)
                df_cons[columns] = df_temp.groupby(['date', 'security_id'])[columns].sum()
            elif key == 'is_prim':
                colunms = df_temp.columns.intersection(value)
                df_cons[colunms] = df_temp[df_temp.is_prim][colunms]
            elif key == 'nasum':
                colunms = df_temp.columns.intersection(value)
                df_cons[colunms] = df_temp.fillna(0).groupby(['date', 'security_id']).sum()[colunms]
            elif key == 'min':
                colunms = df_temp.columns.intersection(value)
                df_cons[colunms] = df_temp.groupby(['date', 'security_id']).min()[colunms]
            elif key == 'max':
                colunms = df_temp.columns.intersection(value)
                df_cons[cols] = df_temp.groupby(['date', 'security_id']).max()[colunms]
            elif key == 'sumweightedby':
                colunms = df_temp.columns.intersection(value.keys())
                val = df_temp.columns.intersection(value.values())
                denom = df_temp.groupby(['date', 'security_id']).sum()[val]
                num = df_temp[colunms].mul(df_temp[val].values).groupby(['date', 'security_id']).sum()[colunms]
                df_cons[colunms] = num.div(denom.values)
        # compare recomputed consolidated market et read_trading_daily consolidated market
        for k1 in key1:
            cons_recomp = df_cons[k1].unstack().fillna(0)
            cons_rdt = dict_rtd[k1]['CONS'].fillna(0)
            cons_diff = (abs(cons_recomp / cons_rdt - 1)) > 1e-4
            cons_diff = cons_diff.replace({False: np.nan}).stack().unstack()
            cond_null_recomp = cond_null_recomp and (len(cons_diff) == 0)
            if not cond_null_recomp:
                err_null_recomp = err_null_recomp + '\n(%s: date:%s, \nsecurity: %s)' \
                                  % (k1, cons_diff.index.astype(str).to_list(), cons_diff.columns.to_list())

    # check unique date in main market
    err_main_unique = 'Main market: index is not unique'
    cond_main_unique = True
    for k1 in key1:
        cond_main_unique = cond_main_unique and (dict_rtd[k1]['MAIN'].index.duplicated().sum() == 0)

    # check NA value on local data without adjustment factor vs EUR data with adjustment factor
    cond_adj = True
    err_adj = 'local data without adjustment factor are not NA, but converted to EUR data with adjustment factor are NA'
    if split_adj or special_adj or regular_adj:
        dict_rtd_local_not_adj = get_trading_daily(security_id, start_date, end_date, ccy_ref='', field=field,
                                                   trading_destinations=trading_destinations, flag_consolidated=flag_consolidated,
                                                   split_adj=False, special_adj=False, regular_adj=False)
        for k1 in key1:
            for k2 in key2:
                temp_rtd_local = dict_rtd_local_not_adj[k1][k2].stack().sort_index()
                temp_rtd = dict_rtd[k1][k2].stack().sort_index()

                temp = temp_rtd[temp_rtd_local.index != temp_rtd.index]
                temp = temp.unstack()
                cond_temp = len(temp) == 0
                if not cond_temp:
                    err_adj = err_adj + '\n (%s-%s: date: %s \n security: %s) ' \
                              % (k1, k2, temp.index.to_list(), temp.columns.to_list())
                cond_adj = cond_adj and cond_temp

    cond = ['output key 1', 'output key 2', 'cons mkt index',
            'cons mkt columns', 'cons recomputed', 'main mkt index', 'adj presence']
    err = [
        [cond_dict1, err_dict1], [cond_dict2, err_dict2],
        [cond_null_index, err_null_index], [cond_null_columns, err_null_columns],
        [cond_null_recomp, err_null_recomp], [cond_main_unique, err_main_unique],
        [cond_adj, err_adj]
    ]
    err = pd.DataFrame(err, index=cond, columns=cols)
    cond = ~err.cond
    temp = err['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp.values

    return None


def test_get_index_prices():
    """
    index_ticker in columns of output
    """
    index_ticker = pd.Index(['CAC', 'SXXP', 'SBF120', 'LCXP', 'MCXP', 'SCXP'])
    df_index = get_index_prices(index_ticker, start_date, end_date)
    err_columns = 'Missing ticker in output'
    cond_columns = len(df_index.columns) == len(index_ticker) == len(df_index.columns.intersection(index_ticker))
    if not cond_columns:
        err_columns = err_columns + ': %s' % index_ticker.difference(df_index.columns).to_list()
    err = pd.DataFrame([[cond_columns, err_columns]], index=['Missing columns'], columns=cols)
    cond = ~err.cond
    temp = err['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp.values
    return None


def test_get_ffama_data():
    """
    DataFrame with 4  columns 'FF_Mkt_RF', 'FF_Size', 'FF_Value', 'RF'
    """
    try:
        folder = r'W:/Global_Research/Quant_research/projets/backtest/Excel/'
        files = pd.Index(os.listdir(folder))
        ffama_file = files[files.str.contains('fama_french_data')].sort_values()
        df_ffama = pd.read_excel(folder + ffama_file[-1], index_col=0)
    except:
        df_ffama = pd.read_csv('data/fama_french_data.csv', index_col=0)
    columns = pd.Index(['FF_Mkt_RF', 'FF_Size', 'FF_Value', 'RF'])
    err_columns = 'Missing columns in output'
    cond_columns = len(df_ffama.columns) == len(columns) == len(df_ffama.columns.intersection(columns))
    if not cond_columns:
        err_columns = err_columns + ': %s' % columns.difference(df_ffama.columns).to_list()
    err = pd.DataFrame([[cond_columns, err_columns]], index=['Missing columns'], columns=cols)
    cond = ~err.cond
    temp = err['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp.values
    return None


def test_get_region_sales():
    """
    Test if columns 'security_id', 'date', 'sales', 'region', 'flag' are in output
    """
    df_sale = get_region_sales(security_id)
    columns = pd.Index(['security_id', 'date', 'sales', 'region', 'flag'])

    err_columns = 'Missing columns in output'
    cond_columns = len(columns) == len(df_sale.columns.intersection(columns))
    if not cond_columns:
        err_columns = err_columns + ': %s' % columns.difference(df_sale.columns).to_list()
    err = pd.DataFrame([[cond_columns, err_columns]], index=['Missing columns'], columns=cols)
    cond = ~err.cond
    temp = err['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp.values
    return None


def test_get_fund_data():
    """
    Check if:
        - dict output has all keys of fundamental data, each key is a Dataframe
        - All Dataframes have the save columns
        - Rebuilt data is not negative
    """
    dict_fund = get_fund_data(security_id, start_date, end_date)
    keys = list(dict_fund.keys())
    columns = ['PX_LAST_no_adj_loc', 'PX_LAST_no_adj', 'PX_LAST_capchg_loc', 'PX_LAST_capchg',
               'PX_LAST_adj_loc', 'PX_LAST_adj', 'PX_LAST_tot_ret_loc', 'PX_LAST_tot_ret',
               'PX_TO_BOOK_RATIO', 'PX_TO_SALES_RATIO', 'BEST_PE_rebuilt', 'BEST_PB_rebuilt',
               'TRAIL_12M_EPS_rebuilt', 'DEBT_TO_EQUITY_rebuilt', 'BEST_ROE_BF', 'BEST_EPS_1GY',
               'BEST_EPS_2GY', 'BEST_EPS_3GY', 'BEST_EPS_BF', 'BEST_EPS_2BF', 'BEST_EPS_STDDEV_BF',
               'BEST_EPS_STDDEV_BF_rebuilt', 'BEST_EPS_STDDEV_BF_rebuilt_old',
               'CUR_MKT_CAP', 'EQY_FREE_FLOAT_PCT']
    columns = pd.Index(columns)
    ## dict with all keys
    err_columns = 'Missing DataFrame in output'
    cond_columns = len(columns) == len(keys) == len(columns.intersection(keys))
    if not cond_columns:
        err_columns = err_columns + ': %s' % columns.difference(keys).to_list()

    # All security_id in DataFrame columns
    sec_id = dict_fund[keys[0]].columns.intersection(security_id)
    err_sec = "Missing security"
    err_rebuilt = "Rebuilt negative"
    cond_sec = True
    cond_rebuilt = True
    for k in keys:
        temp = dict_fund[k]
        cond_temp = (len(sec_id) == len(temp.columns.intersection(sec_id)))
        cond_sec = cond_sec and cond_temp
        if not cond_temp:
            err_sec = err_sec + '\n%s: %s' % (k, sec_id.difference(temp.columns).to_list())
        # xxx_rebuilt is not negative
        if ('rebuilt' in k) and (k != 'TRAIL_12M_EPS_rebuilt'):
            print(k)
            temp = temp.fillna(0)
            temp = temp[temp < 0].stack().unstack()
            cond_temp = len(temp.columns) == 0
            cond_rebuilt = cond_rebuilt and cond_temp
            if not cond_temp:
                err_rebuilt = err_rebuilt + '\n%s: %s' % (k, temp.columns.to_list())

    err = [[cond_columns, err_columns], [cond_sec, err_sec], [cond_rebuilt, err_rebuilt]]
    cond = ['columns', 'security', 'rebuilt']
    err = pd.DataFrame(err, index=cond, columns=cols)
    cond = ~err.cond
    temp = err['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp.values
    return None


def test_transform_fund_data():
    """
    Check if:
        - dict output has all keys of fundamental data, each key is a Dataframe
        - All Dataframes have the save columns
        - Rebuilt data is not negative
    """
    dict_fund_raw = get_fund_data(security_id, start_date, end_date)
    global price_eur
    price_eur = dict_rtd['close_prc']['MAIN']

    dict_fund = transform_fund_data(dict_fund_raw.copy(), price_eur)
    keys = list(dict_fund.keys())
    columns = ['EPS_GROWTH_organic', 'EQY_DVD_YLD_12M_rebuilt',
               'EQY_DVD_YLD_daily_rebuilt', 'close_prc',
               'PX_LAST_no_adj_loc', 'PX_LAST_no_adj', 'PX_LAST_capchg_loc', 'PX_LAST_capchg',
               'PX_LAST_adj_loc', 'PX_LAST_adj', 'PX_LAST_tot_ret_loc', 'PX_LAST_tot_ret',
               'PX_TO_BOOK_RATIO', 'PX_TO_SALES_RATIO', 'BEST_PE_rebuilt', 'BEST_PB_rebuilt',
               'TRAIL_12M_EPS_rebuilt', 'DEBT_TO_EQUITY_rebuilt', 'BEST_ROE_BF', 'BEST_EPS_1GY',
               'BEST_EPS_2GY', 'BEST_EPS_3GY', 'BEST_EPS_BF', 'BEST_EPS_2BF', 'BEST_EPS_STDDEV_BF',
               'BEST_EPS_STDDEV_BF_rebuilt', 'BEST_EPS_STDDEV_BF_rebuilt_old',
               'CUR_MKT_CAP', 'EQY_FREE_FLOAT_PCT']
    columns = pd.Index(columns)
    ## dict with all keys
    err_columns = 'Missing DataFrame in output'
    cond_columns = len(columns) == len(keys) == len(columns.intersection(keys))

    if not cond_columns:
        err_columns = err_columns + ': %s' % columns.difference(keys).to_list()

    err_index = 'Missing date in output'
    cond_index = True
    for k in keys:
        temp = price_eur.index.difference(dict_fund[k].index).to_list()
        cond_temp = len(temp) == 0
        cond_index = cond_index and cond_temp
        if not cond_temp:
            err_index = err_index + '\n %s: %s' \
                        % (k, temp)
    # All security_id in DataFrame columns
    sec_id = dict_fund[keys[0]].columns.intersection(security_id)
    err_sec = "Missing security"
    err_rebuilt = "Rebuilt negative"
    cond_sec = True
    cond_rebuilt = True
    for k in keys:
        temp = dict_fund[k]
        cond_temp = (len(sec_id) == len(temp.columns.intersection(sec_id)))
        cond_sec = cond_sec and cond_temp
        if not cond_temp:
            err_sec = err_sec + '\n%s: %s' % (k, sec_id.difference(temp.columns).to_list())
        # xxx_rebuilt is not negative
        if ('rebuilt' in k) and (k != 'TRAIL_12M_EPS_rebuilt'):
            print(k)
            temp = temp.fillna(0)
            temp = temp[temp.round(4) < 0].stack().unstack()
            cond_temp = len(temp.columns) == 0
            cond_rebuilt = cond_rebuilt and cond_temp
            if not cond_temp:
                err_rebuilt = err_rebuilt + '\n%s: %s' % (k, temp.columns.to_list())

    err = [[cond_columns, err_columns], [cond_sec, err_sec], [cond_rebuilt, err_rebuilt]]
    cond = ['columns', 'security', 'rebuilt']
    err = pd.DataFrame(err, index=cond, columns=cols)
    cond = ~err.cond
    temp = err['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp.values
    return None


def test_fill_fund_data():
    """
    Check if:
        - dict output has all keys of fundamental data, each key is a Dataframe
        - All Dataframes have the save columns
        - Rebuilt data is not negative
    """
    dict_fund_raw = get_fund_data(security_id, start_date, end_date)
    dict_rtd = get_trading_daily(security_id, start_date, end_date, ccy_ref='EUR', field=['close_prc'],
                                 trading_destinations=['MAIN'], flag_consolidated=False,
                                 split_adj=True, special_adj=False, regular_adj=False)
    price_eur = dict_rtd['close_prc']['MAIN']

    dict_fund = fill_fund_data(dict_fund_raw.copy(), price_eur)
    keys = list(dict_fund.keys())
    columns = ['close_prc',
               'PX_LAST_no_adj_loc', 'PX_LAST_no_adj', 'PX_LAST_capchg_loc', 'PX_LAST_capchg',
               'PX_LAST_adj_loc', 'PX_LAST_adj', 'PX_LAST_tot_ret_loc', 'PX_LAST_tot_ret',
               'PX_TO_BOOK_RATIO', 'PX_TO_SALES_RATIO', 'BEST_PE_rebuilt', 'BEST_PB_rebuilt',
               'TRAIL_12M_EPS_rebuilt', 'DEBT_TO_EQUITY_rebuilt', 'BEST_ROE_BF', 'BEST_EPS_1GY',
               'BEST_EPS_2GY', 'BEST_EPS_3GY', 'BEST_EPS_BF', 'BEST_EPS_2BF', 'BEST_EPS_STDDEV_BF',
               'BEST_EPS_STDDEV_BF_rebuilt', 'BEST_EPS_STDDEV_BF_rebuilt_old',
               'CUR_MKT_CAP', 'EQY_FREE_FLOAT_PCT']
    columns = pd.Index(columns)
    ## dict with all keys
    err_columns = 'Missing DataFrame in output'
    cond_columns = len(columns) == len(keys) == len(columns.intersection(keys))

    if not cond_columns:
        err_columns = err_columns + ': %s' % columns.difference(keys).to_list()

    err_index = 'Missing date in output'
    cond_index = True
    for k in keys:
        temp = price_eur.index.difference(dict_fund[k].index).to_list()
        cond_temp = len(temp) == 0
        cond_index = cond_index and cond_temp
        if not cond_temp:
            err_index = err_index + '\n %s: %s' \
                        % (k, temp)
    # All security_id in DataFrame columns
    sec_id = dict_fund[keys[0]].columns.intersection(security_id)
    err_sec = "Missing security"
    err_rebuilt = "Rebuilt negative"
    cond_sec = True
    cond_rebuilt = True
    for k in keys:
        temp = dict_fund[k]
        cond_temp = (len(sec_id) == len(temp.columns.intersection(sec_id)))
        cond_sec = cond_sec and cond_temp
        if not cond_temp:
            err_sec = err_sec + '\n%s: %s' % (k, sec_id.difference(temp.columns).to_list())
        # xxx_rebuilt is not negative
        if ('rebuilt' in k) and (k != 'TRAIL_12M_EPS_rebuilt'):
            print(k)
            temp = temp.fillna(0)
            temp = temp[temp < 0].stack().unstack()
            cond_temp = len(temp.columns) == 0
            cond_rebuilt = cond_rebuilt and cond_temp
            if not cond_temp:
                err_rebuilt = err_rebuilt + '\n%s: %s' % (k, temp.columns.to_list())

    err = [[cond_columns, err_columns], [cond_sec, err_sec], [cond_rebuilt, err_rebuilt]]
    cond = ['columns', 'security', 'rebuilt']
    err = pd.DataFrame(err, index=cond, columns=cols)
    cond = ~err.cond
    temp = err['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp.values
    return None


def test_compute_dividend():
    """
    Check if:
        - dict output has all keys of fundamental data, each key is a Dataframe
        - All Dataframes have the save columns
        - Rebuilt data is not negative
    """
    dict_fund_raw = get_fund_data(security_id, start_date, end_date)
    dict_fund = compute_dividend(dict_fund_raw.copy(), price_eur)
    keys = list(dict_fund.keys())
    columns = ['EQY_DVD_YLD_12M_rebuilt', 'EQY_DVD_YLD_daily_rebuilt',
               'PX_LAST_no_adj_loc', 'PX_LAST_no_adj', 'PX_LAST_capchg_loc', 'PX_LAST_capchg',
               'PX_LAST_adj_loc', 'PX_LAST_adj', 'PX_LAST_tot_ret_loc', 'PX_LAST_tot_ret',
               'PX_TO_BOOK_RATIO', 'PX_TO_SALES_RATIO', 'BEST_PE_rebuilt', 'BEST_PB_rebuilt',
               'TRAIL_12M_EPS_rebuilt', 'DEBT_TO_EQUITY_rebuilt', 'BEST_ROE_BF', 'BEST_EPS_1GY',
               'BEST_EPS_2GY', 'BEST_EPS_3GY', 'BEST_EPS_BF', 'BEST_EPS_2BF', 'BEST_EPS_STDDEV_BF',
               'BEST_EPS_STDDEV_BF_rebuilt', 'BEST_EPS_STDDEV_BF_rebuilt_old',
               'CUR_MKT_CAP', 'EQY_FREE_FLOAT_PCT']
    columns = pd.Index(columns)
    ## dict with all keys
    err_columns = 'Missing DataFrame in output'
    cond_columns = len(columns) == len(keys) == len(columns.intersection(keys))

    if not cond_columns:
        err_columns = err_columns + ': %s' % columns.difference(keys).to_list()

    err_index = 'Missing date in output'
    cond_index = True
    for k in keys:
        temp = price_eur.index.difference(dict_fund[k].index).to_list()
        cond_temp = len(temp) == 0
        cond_index = cond_index and cond_temp
        if not cond_temp:
            err_index = err_index + '\n %s: %s' \
                        % (k, temp)
    # All security_id in DataFrame columns
    sec_id = dict_fund[keys[0]].columns.intersection(security_id)
    err_sec = "Missing security"
    err_rebuilt = "Rebuilt negative"
    cond_sec = True
    cond_rebuilt = True
    for k in keys:
        temp = dict_fund[k]
        cond_temp = (len(sec_id) == len(temp.columns.intersection(sec_id)))
        cond_sec = cond_sec and cond_temp
        if not cond_temp:
            err_sec = err_sec + '\n%s: %s' % (k, sec_id.difference(temp.columns).to_list())
        # xxx_rebuilt is not negative
        if ('rebuilt' in k) and (k != 'TRAIL_12M_EPS_rebuilt'):
            temp = temp.fillna(0)
            temp = temp[temp.round(4) < 0].stack().unstack()
            cond_temp = len(temp.columns) == 0
            cond_rebuilt = cond_rebuilt and cond_temp
            if not cond_temp:
                err_rebuilt = err_rebuilt + '\n%s: %s' % (k, temp.columns.to_list())

    err = [[cond_columns, err_columns], [cond_sec, err_sec], [cond_rebuilt, err_rebuilt]]
    cond = ['columns', 'security', 'rebuilt']
    err = pd.DataFrame(err, index=cond, columns=cols)
    cond = ~err.cond
    temp = err['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp.values
    return None


def test_compute_organic_growth():
    """
    Check if:
        - dict output has all keys of fundamental data, each key is a Dataframe
        - All Dataframes have the save columns
        - Rebuilt data is not negative
    """
    dict_fund_raw = get_fund_data(security_id, start_date, end_date)

    dict_fund = compute_organic_growth(dict_fund_raw.copy())
    keys = list(dict_fund.keys())
    columns = ['EPS_GROWTH_organic',
               'PX_LAST_no_adj_loc', 'PX_LAST_no_adj', 'PX_LAST_capchg_loc', 'PX_LAST_capchg',
               'PX_LAST_adj_loc', 'PX_LAST_adj', 'PX_LAST_tot_ret_loc', 'PX_LAST_tot_ret',
               'PX_TO_BOOK_RATIO', 'PX_TO_SALES_RATIO', 'BEST_PE_rebuilt', 'BEST_PB_rebuilt',
               'TRAIL_12M_EPS_rebuilt', 'DEBT_TO_EQUITY_rebuilt', 'BEST_ROE_BF', 'BEST_EPS_1GY',
               'BEST_EPS_2GY', 'BEST_EPS_3GY', 'BEST_EPS_BF', 'BEST_EPS_2BF', 'BEST_EPS_STDDEV_BF',
               'BEST_EPS_STDDEV_BF_rebuilt', 'BEST_EPS_STDDEV_BF_rebuilt_old',
               'CUR_MKT_CAP', 'EQY_FREE_FLOAT_PCT']
    columns = pd.Index(columns)
    ## dict with all keys
    err_columns = 'Missing DataFrame in output'
    cond_columns = len(columns) == len(keys) == len(columns.intersection(keys))

    if not cond_columns:
        err_columns = err_columns + ': %s' % columns.difference(keys).to_list()

    err_index = 'Missing date in output'
    cond_index = True
    for k in keys:
        temp = price_eur.index.difference(dict_fund[k].index).to_list()
        cond_temp = len(temp) == 0
        cond_index = cond_index and cond_temp
        if not cond_temp:
            err_index = err_index + '\n %s: %s' \
                        % (k, temp)
    # All security_id in DataFrame columns
    sec_id = dict_fund[keys[0]].columns.intersection(security_id)
    err_sec = "Missing security"
    err_rebuilt = "Rebuilt negative"
    cond_sec = True
    cond_rebuilt = True
    for k in keys:
        temp = dict_fund[k]
        cond_temp = (len(sec_id) == len(temp.columns.intersection(sec_id)))
        cond_sec = cond_sec and cond_temp
        if not cond_temp:
            err_sec = err_sec + '\n%s: %s' % (k, sec_id.difference(temp.columns).to_list())
        # xxx_rebuilt is not negative
        if ('rebuilt' in k) and (k != 'TRAIL_12M_EPS_rebuilt'):
            print(k)
            temp = temp.fillna(0)
            temp = temp[temp < 0].stack().unstack()
            cond_temp = len(temp.columns) == 0
            cond_rebuilt = cond_rebuilt and cond_temp
            if not cond_temp:
                err_rebuilt = err_rebuilt + '\n%s: %s' % (k, temp.columns.to_list())

    err = [[cond_columns, err_columns], [cond_sec, err_sec], [cond_rebuilt, err_rebuilt]]
    cond = ['columns', 'security', 'rebuilt']
    err = pd.DataFrame(err, index=cond, columns=cols)
    cond = ~err.cond
    temp = err['error'][cond]
    assert cond.sum() == 0, '\n%s' % temp.values
    return None
