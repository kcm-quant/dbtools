# -*- coding: utf-8 -*-
"""
Created on Mon Apr 15 12:27:35 2024

@author: nle
"""

from datetime import datetime
from dbtools.src.QuantWorkUpdater_v2 import QuantWorkUpdater
from pandas.tseries.offsets import BDay

from dbtools.src.db_connexion import SqlConnector
# import pandas as pd
connector = SqlConnector()
con_mis = connector.connection()

#%% ################## Index_level ###############

loader = QuantWorkUpdater(dataset_name = "LEVEL_INDEX", config_file_name = 'quant_work_updater.json')
config = loader.load_config()
loader.config['table_name']='QUANT_work..INDEX_LEVEL_test_tle'

due_date = loader.get_date_update_due()
last_date = due_date - BDay(10)#datetime.date(loader.get_table_last_date() + BDay(1))

#%% Load data
df_idx_lvl = loader.get_data(last_date, due_date)
# df_ts = df_idx_lvl.set_index(['DATE', 'index_id'])['value'].unstack()
#%% import to INDEX_LEVEL
loader.import_to_sql(df_idx_lvl)
