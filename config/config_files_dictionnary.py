"""
**Bitbucket:** https://bitbucket.org/kcm-quant/dbtools/src/master/

 #. *adjustment.json*: bloomberg formula to load historical data adjustment factors
 #. *consolidated_market_aggregation.txt*: formula to aggregate daily data in consolidated market
 #. *fundamental.json*: bloomberg formula to load historical data fundamental data
 #. *index_level.json*: bloomberg formula to load historical data index level
 #. *account_ref.json*: config used for get_account_ref.py
 #. *kc_flows.json*: config used for get_kc_flows.py
 #. *mapping_name.json*: country, sector, capi, style name mappings
 #. *old_fund_for_missing_fund.json*: List of fundamental of delisted securities

"""