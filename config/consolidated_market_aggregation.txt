{
	'sum': ['volume', 'turnover', 'nb_deal', 'intraday_volume', 'intraday_turnover',
			'cross_volume', 'cross_turnover', 'open_nb_deal', 'intraday_nb_deal',
			'auction_volume', 'auction_turnover', 'auction_nb_deal', 'dark_volume',
			'dark_turnover', 'dark_nb_deal', 'midclose_volume', 'midclose_turnover',
			'midclose_nb_deal', 'end_volume', 'end_turnover', 'off_volume',
			'off_turnover', 'overbid_volume', 'overask_volume', 'off_nb_deal',
			'spmatch_nb_deals', 'imbalance_denom', 'replenishment_denom',
			'sev_lim_replenishment_denom', 'ext_overbid_volume', 'ext_overask_volume',
			'periodic_volume', 'periodic_turnover', 'periodic_nb_deal'],
	
	'nansum': ['average_spread_numer', 'average_spread_denom'],
	
	'min':['low_prc'],
	'max':['high_prc'],
	'is_prim':['tick_size', 'open_prc', 'close_prc', 'open_volume', 'open_turnover',
			   'close_volume', 'close_turnover', 'close_nb_deal', 'last_before_close_prc'],
	'sumweightedby': {'qty_1er_limit' : 'spmatch_nb_deals',
					  'orderbook_imbalance' : 'imbalance_denom'},
	'undefined':['deal_amount_var_log', 'deal_amount_avg_log', 'replenishment',
				 'line_id', 'resilience', 'sev_lim_replenishment'] 
}