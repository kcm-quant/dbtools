# -*- coding: utf-8 -*-
'''
Created on Wed May 17 11:11:52 2023

@author: nle
'''
class Config:
    def __init__(self):
        self.dict_fund = {'BEST_BPS_BF':{'fields':['BEST_BPS'],
                                         'overrides':{'BEST_FPERIOD_OVERRIDE':'BF'},
                        'setParams':{
                                     'adjustmentNormal': False,
                                     'adjustmentAbnormal': False,
                                     'adjustmentSplit': True,
                                     'periodicitySelection':'WEEKLY'}
                        },
         'BEST_EPS_1GY':{'fields':['BEST_EPS'],
                         'overrides':{'BEST_FPERIOD_OVERRIDE':'1GY'},
                         'setParams':{
                                      'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'BEST_EPS_2GY':{'fields':['BEST_EPS'],
                         'overrides':{'BEST_FPERIOD_OVERRIDE':'2GY'},
                         'setParams':{
                                      'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'BEST_EPS_3GY':{'fields':['BEST_EPS'],
                         'overrides':{'BEST_FPERIOD_OVERRIDE':'3GY'},
                         'setParams':{
                                      'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'BEST_EPS_BF':{'fields':['BEST_EPS'],
                        'overrides':{'BEST_FPERIOD_OVERRIDE':'BF'},
                         'setParams':{
                                      'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'BEST_EPS_2BF':{'fields':['BEST_EPS'],
                         'overrides':{'BEST_FPERIOD_OVERRIDE':'2BF'},
                         'setParams':{
                                      'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'BEST_EPS_STDDEV_BF':{'fields':['BEST_EPS_STDDEV'],
                               'overrides':{'BEST_FPERIOD_OVERRIDE':'BF'},
                         'setParams':{
                                      'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'BEST_PE_RATIO_BF':{'fields':['BEST_PE_RATIO'],
                             'overrides':{'BEST_FPERIOD_OVERRIDE':'BF'},
                         'setParams':{
                                      'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'BEST_PX_BPS_RATIO_BF':{'fields':['BEST_PX_BPS_RATIO'],
                                 'overrides':{'BEST_FPERIOD_OVERRIDE':'BF'},
                         'setParams':{
                                      'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'BEST_ROE_BF':{'fields':['BEST_ROE'],
                        'overrides':{'BEST_FPERIOD_OVERRIDE':'BF'},
                         'setParams':{
                                      'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'EARN_YLD':{'fields':['EARN_YLD'],
                     'overrides':None,
                         'setParams':{'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'EARN_YLD_yearly':{'fields':['EARN_YLD'],
                            'overrides':{'FUND_PER':'Y'},
                         'setParams':{
                                      'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         
         'PX_TO_BOOK_RATIO':{'fields':['PX_TO_BOOK_RATIO'],
                             'overrides':None,
                         'setParams':{'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'PX_TO_BOOK_RATIO_yearly':{'fields':['PX_TO_BOOK_RATIO'],
                                    'overrides':{'FUND_PER':'Y'},
                         'setParams':{
                                      'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         
         'PX_TO_SALES_RATIO':{'fields':['PX_TO_SALES_RATIO'],
                              'overrides':None,
                         'setParams':{'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'PX_TO_SALES_RATIO_yearly':{'fields':['PX_TO_SALES_RATIO'],
                                     'overrides':{'FUND_PER':'Y'},
                         'setParams':{
                                      'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         
         'TOT_COMMON_EQY':{'fields':['TOT_COMMON_EQY'],
                           'overrides':None,
                         'setParams':{'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'TOT_COMMON_EQY_yearly':{'fields':['TOT_COMMON_EQY'],
                                  'overrides':{'FUND_PER':'Y'},
                         'setParams':{
                                      'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         
         'TOT_DEBT_TO_COM_EQY':{'fields':['TOT_DEBT_TO_COM_EQY'],
                                'overrides':None,
                         'setParams':{'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'TOT_DEBT_TO_COM_EQY_yearly':{'fields':['TOT_DEBT_TO_COM_EQY'],
                                       'overrides':{'FUND_PER':'Y'},
                         'setParams':{
                                      'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         
         'EQY_FREE_FLOAT_PCT':{'fields':['EQY_FREE_FLOAT_PCT'],
                               'overrides':None,
                         'setParams':{'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         
         'PX_LAST_N_N_N':{'fields':['PX_LAST'],
                          'overrides':None,
                         'setParams':{'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': False,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'PX_LAST_N_N_Y':{'fields':['PX_LAST'],
                          'overrides':None,
                         'setParams':{'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'PX_LAST_N_Y_Y':{'fields':['PX_LAST'],
                          'overrides':None,
                         'setParams':{'adjustmentNormal': False,
                                      'adjustmentAbnormal': True,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'PX_LAST_Y_Y_Y':{'fields':['PX_LAST'],
                          'overrides':None,
                         'setParams':{'adjustmentNormal': True,
                                      'adjustmentAbnormal': True,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         
         'PX_LAST_N_N_N_EUR':{'fields':['PX_LAST'],
                              'overrides':None,
                         'setParams':{'currency': 'EUR',
                                      'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': False,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'PX_LAST_N_N_Y_EUR':{'fields':['PX_LAST'],
                              'overrides':None,
                         'setParams':{'currency': 'EUR',
                                      'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'PX_LAST_N_Y_Y_EUR':{'fields':['PX_LAST'],
                              'overrides':None,
                         'setParams':{'currency': 'EUR',
                                      'adjustmentNormal': False,
                                      'adjustmentAbnormal': True,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         'PX_LAST_Y_Y_Y_EUR':{'fields':['PX_LAST'],
                              'overrides':None,
                         'setParams':{'currency': 'EUR',
                                      'adjustmentNormal': True,
                                      'adjustmentAbnormal': True,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         
         'CUR_MKT_CAP_Y_Y_Y_EUR':{'fields':['CUR_MKT_CAP'],
                                  'overrides':None,
                         'setParams':{'currency': 'EUR',
                                      'adjustmentNormal': True,
                                      'adjustmentAbnormal': True,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         },
         
         'PX_LAST_N_N_Y_fundccy':{'fields':['PX_LAST'],
                                  'overrides':None,
                         'setParams':{'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'}
                         }
        
        }
        
        self.dict_adj = {
            'PX_VOLUME_N_N_N':{'fields':['PX_VOLUME'],
                            'setParams':{'adjustmentNormal': False,
                                         'adjustmentAbnormal': False,
                                         'adjustmentSplit': False,
                                         'CDR':'5D'}
                            },
            'PX_VOLUME_N_N_Y':{'fields':['PX_VOLUME'],
                            'setParams':{'adjustmentNormal': False,
                                         'adjustmentAbnormal': False,
                                         'adjustmentSplit': True,
                                         'CDR':'5D'}
                            },
            'PX_LAST_N_N_N':{'fields':['PX_LAST'],
                            'setParams':{'adjustmentNormal': False,
                                         'adjustmentAbnormal': False,
                                         'adjustmentSplit': False,
                                         'CDR':'5D'}
                            },
            'PX_LAST_N_Y_N':{'fields':['PX_LAST'],
                            'setParams':{'adjustmentNormal': False,
                                         'adjustmentAbnormal': True,
                                         'adjustmentSplit': False,
                                         'CDR':'5D'}
                            },
            'PX_LAST_N_N_Y':{'fields':['PX_LAST'],
                            'setParams':{'adjustmentNormal': False,
                                         'adjustmentAbnormal': False,
                                         'adjustmentSplit': True,
                                         'CDR':'5D'}
                            },
            'PX_LAST_Y_N_N':{'fields':['PX_LAST'],
                            'setParams':{'adjustmentNormal': True,
                                         'adjustmentAbnormal': False,
                                         'adjustmentSplit': False,
                                         'CDR':'5D'}
                            }
            }
    pass










