config package
==============

Submodules
----------

.. toctree::
   :maxdepth: 4

   config.config_files_dictionnary
   config.fundamental_config

Module contents
---------------

.. automodule:: config
   :members:
   :undoc-members:
   :show-inheritance:
