
# -*- coding: utf-8 -*-
"""
Created on Fri Sep 27 09:46:34 2024

@author: nle
"""

import os
import pandas as pd
from dbtools.src.db_connexion import SqlConnector
import dbtools.src.get_repository as rep
from datetime import datetime
from copy import deepcopy
from dbtools.src.QuantWorkUpdater_v2 import QuantWorkUpdater, get_mapping_from_dictionnary, transform_dict_to_df
connector = SqlConnector()
con_mis = connector.connection()

loader = QuantWorkUpdater(dataset_name = "FUNDAMENTAL_DATA_INDEX", config_file_name = 'quant_work_updater.json')
config = loader.load_config()

#%% get 1 attribute for data quality check
end_date = '20240926'
start_date = '20240901'
start_date_default = '20240601'
#ids = loader.get_perimeter(start_date, end_date) #[2, 8, 283812,292445,292446,10174308,499,13647,2735258, 45519, 12813]# loader.get_perimeter(start_date, end_date)#[2, 8, 283812,292445,292446,10174308,499,13647,2735258, 45519, 12813]
ids = [1, 2, 4, 5]
table_field = loader.config.get('table_field')
table_data_dictionary = loader.config.get('table_data_dictionary', None)
df_symbols = loader.get_tickers_from_referential(ids)
# df_symbols = df_symbols[df_symbols['delisted_date']>pd.to_datetime(start_date).date()]
df_date_ref = loader.get_security_date_ref(attribute_id=1)

df_symbols = df_symbols.merge(df_date_ref, left_index =True, right_index =True, how = 'left')
df_symbols['data_date_min'] = df_symbols['data_date_min'].fillna(start_date_default)
df_symbols['data_date_max'] = df_symbols['data_date_max'].fillna(datetime(2999,12,31))
d_data = {}
data_name = 'eps_1gy'

param = {'data_frequence': 'DAILY',
 'function': 'EndOfDayBbg',
 'field': ['BEST_EPS'],
 'currency': 'local',
 'overrides': {'BEST_FPERIOD_OVERRIDE': '1GY'},
 'setParams': {'adjustmentNormal': False,
  'adjustmentAbnormal': False,
  'adjustmentSplit': False}}

df_eps_1gy = loader.get_data_bloomberg(df_symbols, param, start_date, end_date, start_date_default)

#%%%%%%%%%%%%%%%%%% script prod %%%%%%%%%%%%%%%%%%%


from dbtools.src.QuantWorkUpdater_v2 import QuantWorkUpdater

loader = QuantWorkUpdater(dataset_name = "FUNDAMENTAL_DATA_INDEX", config_file_name = 'quant_work_updater.json')
config = loader.load_config()
#%%
start_date = '20240801'
end_date = '20240831'
df_fund = loader.get_data(start_date, end_date)
df_fund_ts = df_fund.set_index(['DATE', 'attribute_id', 'index_id'])['value'].unstack()

df_fund.to_pickle(r'C:\python_tools\dbtools\scratch\df_fund_indices.pkl')

#%% import to sql
loader.import_to_sql(df_fund)











