# -*- coding: utf-8 -*-
"""
Created on Thu May 16 17:22:14 2024

@author: nle
"""

import pandas as pd
import numpy as np
from dbtools.src.db_connexion import SqlConnector
from dbtools.src.QuantWorkUpdater_v2 import QuantWorkUpdater
import dbtools.src.get_repository as rep
import dbtools.src.DatabaseManager as db
from sqlalchemy import  MetaData, Table, Column, Integer, Float, String, PrimaryKeyConstraint, Sequence, UniqueConstraint, Date
from copy import deepcopy
from openpyxl import Workbook
connector = SqlConnector()
con_mis = connector.connection()
import dbtools.src.bcp_import_to_sql as bcp_import_to_sql
import json
from tqdm import tqdm
from datetime import datetime

#%%
START_DATE = '20211201'
END_DATE = '20240131'
security_id = [25, 31, 89, 210, 258, 286, 359, 436, 448, 6254, 6607, 6672, 8087, 10527, 11701, 11874, 11963, 12057, 12594, 12658, 13373, 13439, 13541,15210,
                15726, 15730, 17821, 18584, 18590, 18678, 18890, 19072, 19556,34195, 34280, 34348, 38718, 38760, 38763, 41814, 45391, 47044, 76654,
                76761, 80918, 132988, 172387, 219147, 283813, 284818, 289927, 289969,295511, 328915, 359146, 363239, 392166, 395770, 658971, 673005,
                1012954,1037829, 1270162, 1298078, 1978337, 2168769, 2196618, 2632714, 2732114,2735260, 2742838, 2743386, 2743482, 2743973, 2752161,
                2753232, 2756821,2757656, 2761676, 2764505, 2764531, 2765679, 2783174, 2825493, 2833331,2852007, 2886184, 2905367, 2907194, 2921870,
                 2923786, 2925627, 2962159,3008458, 3010803, 3012537, 3018908, 3031213, 10172537, 10173378]

fund_ticker = rep.mapping_from_security(security_id, 'fund_ticker')
sec_from_fund = rep.mapping_security_id_from(fund_ticker, code = 'fund_ticker')
print(sec_from_fund)
#%% %%%%%%%%%%%%%%%%%%%%%%%%  API %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
loader = QuantWorkUpdater(dataset_name = "FUNDAMENTAL_DATA_STOCK_daily", config_file_name = 'fundamental_data_stock.json')
#%% Initialize FUNDAMENTAL_DATA_STOCK
# =============================================================================
# column_formats = {
#     'DATE' : Date,
#     'attribute_id' : Integer,
#     'security_id': Integer,
#     'value': Float
#     }
# df = pd.DataFrame(columns = column_formats.keys())
# 
# db_manager = db.DatabaseManager( "FUNDAMENTAL_DATA_STOCK_week_ly_excel_test_100_stocks_v1")
# db_manager.create_table(df = df.copy(),column_types=column_formats, primary_keys = ['DATE', 'attribute_id', 'security_id'])
# =============================================================================
#%% Change config to adapt to 100 stocks
config = loader.load_config()
conf_mod = deepcopy(config['FUNDAMENTAL_DATA_STOCK_daily'])
conf_mod['table_perimeter_type'] = 'fixed'
conf_mod['table_perimeter'] = security_id#[8087]#security_id

loader.config = deepcopy(conf_mod)
#%% Load data
df_fund_api = pd.DataFrame()
df_fund_api = loader.get_data(START_DATE, END_DATE)

#%% import to sql
db_manager = db.DatabaseManager("FUNDAMENTAL_DATA_STOCK_daily_api_test_100_stocks_v1")
db_manager.append_table(df_fund_api.drop_duplicates(keep='first'))
# df_fund_api.to_csv(r'C:\Users\nle\Downloads\df_fund_api.csv')

# =============================================================================
# update QUANT_work..FUNDAMENTAL_DATA_STOCK_daily_api_test_100_stocks_v1 set security_id = 8087 where security_id = 15070
# update QUANT_work..FUNDAMENTAL_DATA_STOCK_daily_api_test_100_stocks_v1 set security_id = 41814 where security_id = 10175152
# update QUANT_work..FUNDAMENTAL_DATA_STOCK_daily_api_test_100_stocks_v1 set security_id = 132988 where security_id = 10175242
# update QUANT_work..FUNDAMENTAL_DATA_STOCK_daily_api_test_100_stocks_v1 set security_id = 359146 where security_id = 10175065
# update QUANT_work..FUNDAMENTAL_DATA_STOCK_daily_api_test_100_stocks_v1 set security_id = 2196618 where security_id = 10175155
# =============================================================================


#%% %%%%%%%%%%%%%%%%%%%%%%%% EXCEL %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
# init table
# =============================================================================
# column_formats = {
#     'DATE' : Date,
#     'attribute_id' : Integer,
#     'security_id': Integer,
#     'value': Float
#     }
# df = pd.DataFrame(columns = column_formats.keys())
# 
# db_manager = db.DatabaseManager( "FUNDAMENTAL_DATA_STOCK_daily_excel_test_100_stocks_v1")
# db_manager.create_table(df = df.copy(),column_types=column_formats, primary_keys = ['DATE', 'attribute_id', 'security_id'])
# =============================================================================
#%% init excel
# =============================================================================
# ticker = rep.mapping_from_security(security_id, 'fund_ticker')
# start_date = datetime.strptime(START_DATE, '%Y%m%d')
# end_date = datetime.strptime(END_DATE, '%Y%m%d')
# 
# path_to_save = 'W:/Global_Research/Quant_research/projets/data/dynamic universe/quant_work/'
# path_config = 'C:/python_tools/dbtools/scratch/'
# config_file = 'config_to_fund_data_to_test_v2.json'
# 
# df_config = pd.read_json(path_config + config_file, orient = 'split')
# 
# fund_level2_new = Workbook()
# nb_stocks = len(ticker)
# sheet_temp = fund_level2_new.active
# sheet_temp['A1'] = 'security_id'
# sheet_temp['B1'] = 'FUNDAMENTAL_TICKER'
# for i in range(nb_stocks):
#     sheet_temp['A%s' % (i + 2)] = ticker.index[i]
#     sheet_temp['B%s' % (i + 2)] = ticker.values[i]
# 
# for index in range(len(df_config)):
#     sheet_temp = fund_level2_new.create_sheet(df_config.sheet_name[index])
#     for cell in ['A1', 'B1', 'C1', 'D1']:
#         sheet_temp[cell] = df_config[cell][index]
#     sheet_temp['B2'] = start_date
#     sheet_temp['B3'] = end_date
#     sheet_temp['A2'] = 'start_date'
#     sheet_temp['A3'] = 'end_date'
#     sheet_temp['A4'] = 'Date'
# 
#     for i in range(nb_stocks):
#         ticker_row = sheet_temp.cell(row=4, column=i + 2, value=ticker.iloc[i])
#         coor = ticker_row.coordinate
#         if i == 0:
#             formula = df_config.formula1[index]
#             formula_row = sheet_temp.cell(row=5, column=1, value=formula)
# 
#         else:
#             if df_config.sheet_name[index] == 'PX_LAST_N_N_Y_fundccy':
#                 formula = df_config.formula2[index] % (coor[:-1], coor[-1],
#                                                           coor[:-1], coor[-1])
#             else:
#                 formula = df_config.formula2[index] % (coor[:-1], coor[-1])
#             formula_row = sheet_temp.cell(row=5, column=i + 2, value=formula)
# file_to_save = path_to_save + 'fundamental/fundamental_formula_test_api_%s.xlsx' %end_date.strftime('%Y%m%d')
# file_to_save_value = file_to_save.replace('formula', 'value')
# fund_level2_new.save(file_to_save)
# fund_level2_new.save(file_to_save_value)
# print('File macro fundamental is saved in %s' % (file_to_save_value))
# =============================================================================
#%% load data from excel
def load_excel(path_to_load):
    sheetname = pd.ExcelFile(path_to_load).sheet_names
    sheetname = pd.Index(sheetname)
    sheetname = sheetname[sheetname!='Sheet']
    # load security_id and ticker in sheetname 'Sheet' in excel file to map
    sec_map_ticker = pd.read_excel(path_to_load, sheet_name = 'Sheet', index_col=0).squeeze()
    sec_map_ticker = pd.Series(index = sec_map_ticker.values, data = sec_map_ticker.index)
    
    df_attrib = pd.read_sql('select * from QUANT_work..FUNDAMENTAL_DATA_DICTIONARY', con_mis)
    s_attr_name_id = df_attrib.set_index('name').attribute_id
    # Read sheets in the workbook
    df_db_stack = pd.DataFrame()
    for j in range(len(sheetname)):
        sheet = sheetname[j]
        print('No.%d - %s'%(j, sheet))
        df_table = pd.read_excel(path_to_load, sheet_name = sheet,
                           header=3, index_col = 0, na_values=['#N/A Invalid Security'])
        
        df_table = df_table.rename(columns = sec_map_ticker)
        df_table.index.name = 'DATE'
        df_table.columns.name = 'security_id'
        df_table_db = df_table.stack().to_frame('value').reset_index()
        df_table_db['attribute_id'] = sheet
    
        df_db_stack = pd.concat((df_db_stack, df_table_db))
    df_db_stack['attribute_id'] = df_db_stack['attribute_id'].replace(s_attr_name_id)
    return df_db_stack
#%%
path_to_load_day = 'W:/Global_Research/Quant_research/projets/data/dynamic universe/quant_work/fundamental/fundamental_value_daily_test_api_20240131_v1.xlsx'
path_to_load_month = 'W:/Global_Research/Quant_research/projets/data/dynamic universe/quant_work/fundamental/fundamental_value_test_api_monthly_20231231.xlsx'
path_to_load_week = 'W:/Global_Research/Quant_research/projets/data/dynamic universe/quant_work/fundamental/fundamental_value_test_api_weekly_20231231.xlsx'
df_db_week = load_excel(path_to_load_day)
df_db_month = load_excel(path_to_load_month)
df_db_week['freq'] = 'weekly'
df_db_month['freq'] = 'monthly'
df_db_stack = pd.concat((df_db_week, df_db_month)).drop_duplicates(subset = ['DATE', 'security_id', 'attribute_id', 'value'], keep = 'first')
# df_db_stack = load_excel(path_to_load_day)

#%%
# j'ai téléchargé les données bloom via excel weekly (PER=cw), monthly (PER=cm).Pour la même date, les valeurs sont différentes
# check variable (df_diff)
df_diff = df_db_stack[df_db_stack[['DATE', 'security_id', 'attribute_id']].duplicated(keep  = False)].sort_values(['DATE', 'attribute_id', 'security_id'])

###### 
df_db_stack = df_db_stack.drop_duplicates(subset = ['DATE', 'attribute_id', 'security_id'], keep = 'first')
df_db_stack = df_db_stack.sort_values(['DATE', 'attribute_id', 'security_id'])[['DATE', 'attribute_id', 'security_id', 'value']]
#%% import to sql
# df_db_stack = df_db_stack[df_db_stack.DATE.dt.weekday!=4]
db_manager = db.DatabaseManager("FUNDAMENTAL_DATA_STOCK_daily_excel_test_100_stocks_v1")
db_manager.append_table(df_db_stack)

con_mis.execute('USE QUANT_work')
try:
    pd.bulk_copy(con_mis, 'FUNDAMENTAL_DATA_STOCK_excel_weekly_monthly_test_100_stocks', df_diff)
except:
    pass






