# -*- coding: utf-8 -*-
"""
Created on Wed May 17 09:26:02 2023

@author: nle
"""

from kc_bbg_api.connection import ApiSettings,KCBBGClient
from kc_bbg_api import historical_data,intraday_tick_data,reference_data,indexcomposition_data,intraday_bar_data
from datetime import datetime
import pandas as pd

import numpy as np
import os, sys

import dbtools.src.get_repository as rep
import dbtools.src.get_market_data as mkt
import dbtools.config.fundamental_config as config
import dbtools.src.bcp_import_to_sql as bcp_import_to_sql

from dbtools.src.db_connexion import SqlConnector
connector = SqlConnector()
con_mis = connector.connection()


#ApiSettings.api_url = 'http://uatifcweb903.keplercm.lan:8082'
ApiSettings.user_name = "bbg.quant-research.user"
ApiSettings.user_password = "pZ_B5LJ-QgfOO0Vpk_Yq"

conf = config.Config()
dict_adj_conf = conf.dict_adj.copy()
dict_fund_conf = conf.dict_fund.copy()

level2 = rep.get_quant_perimeter(2)


start_date = '20100101'# '20221003'#20160614
end_date = '20230519' #'20221015'#20160620


#%%
perimeter = [34097,      257,  1313623,  2769415,  2743387, 10174313,
              309900,       84,   183255,     5479,  3004012,    45449,
             2911271,  2836511,  2975580,    19556,    13600,      461,
                 843,  2758985,    76649,  1233395,  1507291,      951,
               38808,      494,      532,    12483,     7000,    19459,
              539877,  2744144,      503,    38847,    13474,   985013,
               15391,       42,    11814,    39028,   249722,   318055,
               76321,    79227,   290244,    38900,   290351,   289938,
              287574,    17872]

fund_ticker_l2 = rep.mapping_from_security(level2, code = 'fund_ticker')
fund_ticker_l2 = fund_ticker_l2 + ' Equity'

fund_ticker = fund_ticker_l2.loc[perimeter]#sample(200)
security_id = fund_ticker.index

#%%

def _get_histo_data(tickers, fields, start_date, end_date, overrides, params):
    df = historical_data(securities=list(tickers),
                                fields=fields,
                                startDate=start_date, endDate=end_date,
                                overrides=overrides,
                                setParams=params)
    df = df.drop_duplicates(keep = 'last').set_index(['date', 'security'])[fields[0]].unstack()
    
    tickers_fix = tickers[tickers.str.contains('/')]
    tickers_fix = dict(zip(tickers_fix.str.replace('/',''), tickers_fix))
    df = df.rename(columns = tickers_fix)
    
    df = df.reindex(columns = tickers)
    df.columns = tickers.index
    return df

def get_histo_data(dict_config, tickers, start_date, end_date):
    t_s = datetime.now()
    dict_data = {}
    for k in dict_config.keys():
        conf_tmp = dict_config[k]
        ts = datetime.now()
        if k == 'PX_LAST_N_N_Y_fundccy':
            fund_ccy = reference_data(securities=list(tickers), fields="EQY_FUND_CRNCY").drop_duplicates()
            fund_ccy = pd.Series(index = fund_ccy.security.values, data = fund_ccy.EQY_FUND_CRNCY.values)
            tickers_fix = tickers[tickers.str.contains('/')]
            tickers_fix = dict(zip(tickers_fix.str.replace('/',''), tickers_fix))
            fund_ccy = fund_ccy.rename(index = tickers_fix)
            fund_ccy = fund_ccy.reindex(tickers).to_frame('ccy').reset_index()
            fund_ccy.index = tickers.index
            fund_ccy.columns = ['fund_ticker', 'ccy']
            df = pd.DataFrame()
            for ccy in fund_ccy['ccy'].unique():
                ticker_tmp = fund_ccy.loc[fund_ccy['ccy']==ccy, 'fund_ticker']
                conf_tmp['overrides'] = {'EQY_FUND_CRNCY':ccy}

                df_ = _get_histo_data(tickers = ticker_tmp,
                                     start_date = start_date,
                                     end_date = end_date,
                                     fields = conf_tmp.get('fields'),
                                     overrides = conf_tmp.get('overrides'),
                                     params = conf_tmp.get('setParams'))
                df = pd.concat((df, df_), axis = 1)
            df = df.reindex(columns = tickers.index)
        else:
            df = _get_histo_data(tickers = tickers,
                             start_date = start_date,
                             end_date = end_date,
                             fields = conf_tmp.get('fields'),
                             overrides = conf_tmp.get('overrides'),
                             params = conf_tmp.get('setParams'))
        te = datetime.now()
        t_exec = te-ts
        print('--------- %s: \t %s -------' %(k, t_exec))
        dict_data[k] = df.sort_index(axis = 1).copy()
    t_e = datetime.now()
    t_total = t_e - t_s
    print('------------- Load Fundamental data in %s ----------------' %t_total)
    return dict_data.copy()

def import_to_sql(df, sql_table_name, drop_existing = True):
    con_mis.execute('USE QUANT_work')
    if drop_existing:
        try:
            pd.read_sql("drop table QUANT_work..%s" %sql_table_name, con = con_mis)
        except:
            print('Drop existing table: QUANT_work..%s'%sql_table_name)
    
    pd.bulk_copy(con_mis, sql_table_name, df)
    pass

def get_fund_excel(security_id, start_date, end_date, table = 'QUANT_work..level2_fundvar_prod_test_api'):
    sec_str = ",".join(map(str, security_id))

    query = """select *
    from %s
    where security_id in (%s)
    and date between '%s' and '%s'
    order by date, security_id""" % (table, sec_str, start_date, end_date)
    df_fund = pd.read_sql_query(query, con_mis)
    
    df_fund = df_fund.set_index(['date', 'security_id'])
    if 'ticker' in df_fund.columns:
        df_fund = df_fund.drop('ticker', axis=1)
    dict_fund = {}
    for key in df_fund.columns:
        temp = df_fund[key].unstack().sort_index().astype(float)
        dict_fund[key] = temp.rename_axis(index=None, columns=None)
    return dict_fund
#%%
tickers = fund_ticker.iloc[:50]
#%%
# =============================================================================
# fund_api = get_histo_data(dict_fund_conf, tickers, start_date, end_date)
# sql_table_name_api='level2_fundvar_prod_api_20230519'
# df_api = pd.DataFrame()
# for k,v in fund_api.items():
#     df_api = pd.concat((df_api, v.stack().to_frame(k)), axis = 1)
# df_api = df_api.reset_index().rename(columns={'level_1':'security_id'}).sort_values(by = ['date', 'security_id'])
# import_to_sql(df_api, sql_table_name_api, drop_existing = True)
# =============================================================================

#%%
sql_table_name_api='QUANT_work..level2_fundvar_prod_api_20230519'
sql_table_name_excel='QUANT_work..level2_fundvar_prod_excel_20230519'
fund_api = get_fund_excel(tickers.index, start_date, end_date, sql_table_name_api)
fund_excel = get_fund_excel(tickers.index, start_date, end_date, sql_table_name_excel)
#%%
fund_diff = {}
fund_diff_rel = {}
for k in fund_api.keys():

    df_api = fund_api[k].copy().fillna(-1).reindex(columns= tickers.index).stack().to_frame('api')
    df_excel = fund_excel[k].copy().fillna(-1).reindex(columns= tickers.index).stack().to_frame('excel')
    df = pd.concat((df_api, df_excel), axis = 1)
    df['diff'] = df.api - df.excel
    
    df['diff'] = df['diff'].replace({0:np.nan, np.inf:np.nan, -np.inf:np.nan})
    df = df.dropna(subset = 'diff')
    df_diff = df.reset_index().rename(columns = {'level_0': 'date', 'level_1':'security_id'})
    df_diff['ticker'] = df_diff.security_id.map(tickers)
    fund_diff[k] = df_diff.replace({-1:np.nan})
    
    df_diff_rel = df_diff.copy()
    df_diff_rel['diff'] = (df_diff['diff']/df_diff['api']).replace({0:np.nan, np.inf:np.nan, -np.inf:np.nan})*100
    df_diff_rel = df_diff_rel[df_diff_rel['diff'].abs()>1]
    
    fund_diff_rel[k] = df_diff_rel.replace({-1:np.nan})


#%%
check = pd.Series()
for k in fund_diff_rel.keys():
    if not fund_diff_rel[k].empty:
        check.loc[k] = len(fund_diff_rel[k])
check = check.sort_values()
#%%
k = 'BEST_PX_BPS_RATIO_BF'
df = fund_diff_rel[k].copy()
df.security_id.value_counts()
df.drop_duplicates(['security_id'])


df[df.security_id == 503]

#%%
df_ = historical_data(securities=list(['SWMA SS Equity']),
                             fields=['BEST_PX_BPS_RATIO'],
                             startDate=start_date, endDate=end_date,
                             overrides={'BEST_FPERIOD_OVERRIDE':'BF'},
                             setParams={
                                      'adjustmentNormal': False,
                                      'adjustmentAbnormal': False,
                                      'adjustmentSplit': True,
                                      'periodicitySelection':'WEEKLY'})

#%%
def get_fund_data(security_id, start_date, end_date, sql_table_name = 'QUANT_work..level2_fundvar_prod_excel_20230519'):
    """
    Load fundamental data from QUANT_work..level2_fundvar_prod
    !!!!!! Frequency weekly (every Friday) !!!!!!

    Args:
        security_id (list): list of security_id
        start_date (str): yyyymmdd
        end_date (str): yyyymmdd

    Returns:
        Dict of DataFrame, security_in column, date in index

    .. exec_code::

        import dbtools.src.get_market_data as mkt
        security_id = [2, 8, 391, 512]
        dict_fund = mkt.get_fund_data(security_id = security_id, start_date = '20180805', end_date = '20220826')
        print(list(dict_fund.keys()))
        print(dict_fund['CUR_MKT_CAP'])
    """
    sec_str = ",".join(map(str, security_id))

    query = """select date, security_id,

            PX_LAST_N_N_N as PX_LAST_no_adj_loc,
            PX_LAST_N_N_N_EUR as PX_LAST_no_adj,
    
            PX_LAST_N_N_Y as PX_LAST_capchg_loc,
            PX_LAST_N_N_Y_EUR as PX_LAST_capchg,

            PX_LAST_N_Y_Y as PX_LAST_adj_loc,
            PX_LAST_N_Y_Y_EUR as PX_LAST_adj,

            PX_LAST_Y_Y_Y as PX_LAST_tot_ret_loc,
            PX_LAST_Y_Y_Y_EUR as PX_LAST_tot_ret,

            isnull(PX_TO_BOOK_RATIO, PX_TO_BOOK_RATIO_yearly) as PX_TO_BOOK_RATIO,
            isnull(PX_TO_SALES_RATIO, PX_TO_SALES_RATIO_yearly) as PX_TO_SALES_RATIO,
                
            CASE WHEN BEST_EPS_BF<=0 THEN NULL ELSE BEST_PE_RATIO_BF end as BEST_PE_rebuilt,
            CASE WHEN BEST_BPS_BF<=0 THEN NULL ELSE BEST_PX_BPS_RATIO_BF end as BEST_PB_rebuilt,
            
            PX_LAST_N_Y_Y * isnull(EARN_YLD, EARN_YLD_yearly) as TRAIL_12M_EPS_rebuilt,
            
            CASE WHEN isnull(TOT_COMMON_EQY, TOT_COMMON_EQY_yearly)<=0 THEN NULL
            ELSE isnull(TOT_DEBT_TO_COM_EQY, TOT_DEBT_TO_COM_EQY_yearly) END as DEBT_TO_EQUITY_rebuilt,

            
            BEST_ROE_BF, BEST_EPS_1GY, BEST_EPS_2GY, BEST_EPS_3GY,
            BEST_EPS_BF, BEST_EPS_2BF,
            BEST_EPS_STDDEV_BF,
            
            BEST_EPS_STDDEV_BF / PX_LAST_N_N_Y_fundccy as BEST_EPS_STDDEV_BF_rebuilt,
            BEST_EPS_STDDEV_BF / PX_LAST_N_N_Y as BEST_EPS_STDDEV_BF_rebuilt_old,
    
            CUR_MKT_CAP_Y_Y_Y_EUR as CUR_MKT_CAP, EQY_FREE_FLOAT_PCT
            from %s
            where security_id in (%s)
            and date between '%s' and '%s'
            order by date, security_id""" % (sql_table_name, sec_str, start_date, end_date)

    df_fund = pd.read_sql_query(query, con_mis)

    df_fund = df_fund.set_index(['date', 'security_id'])
    dict_fund = {}
    for key in df_fund.columns:
        temp = df_fund[key].unstack().sort_index()#.dropna(how = 'all')
        dict_fund[key] = temp.rename_axis(index=None, columns=None)
    return dict_fund

dict_fund_api_ = get_fund_data(security_id, start_date='20130101', end_date='20230519', sql_table_name = 'QUANT_work..level2_fundvar_prod_api_20230519')
dict_fund_excel_ = get_fund_data(security_id, start_date='20130101', end_date='20230519', sql_table_name = 'QUANT_work..level2_fundvar_prod_excel_20230519')
dict_fund_api = dict_fund_api_.copy()
dict_fund_excel = dict_fund_excel_.copy()
#%%
price_eur = mkt.get_trading_daily(security_id, start_date='20130101', end_date='20230519', field = ['close_prc'])['close_prc']['MAIN']

dict_fund_api = mkt.transform_fund_data(dict_fund_api_.copy(), price_eur.copy())
dict_fund_excel = mkt.transform_fund_data(dict_fund_excel_.copy(), price_eur.copy())
#%%




dict_diff = {}
dict_diff_rel = {}
for k in dict_fund_api.keys():

    df_api = dict_fund_api[k].copy().fillna(-1).reindex(columns= security_id).stack().to_frame('api')
    df_excel = dict_fund_excel[k].copy().fillna(-1).reindex(columns= security_id).stack().to_frame('excel')
    df = pd.concat((df_api, df_excel), axis = 1)
    df['diff'] = df.api - df.excel
    
    df['diff'] = df['diff'].replace({0:np.nan, np.inf:np.nan, -np.inf:np.nan})
    df = df.dropna(subset = 'diff')
    df_diff = df.reset_index().rename(columns = {'level_0': 'date', 'level_1':'security_id'})
    df_diff['ticker'] = df_diff.security_id.map(tickers)
    df_diff.loc[df_diff.api.isna() | df_diff.excel.isna(),'diff'] = np.nan
    dict_diff[k] = df_diff.copy()
    
    df_diff_rel = df_diff.copy()
    df_diff_rel['diff'] = (df_diff['diff']/df_diff['api']).replace({0:np.nan, np.inf:np.nan, -np.inf:np.nan})*100
    df_diff_rel = df_diff_rel[df_diff_rel['diff'].abs()>0.5]
    df_diff_rel = df_diff_rel.replace({-1:np.nan})
    df_diff_rel.loc[df_diff_rel.api.isna() | df_diff_rel.excel.isna(),'diff'] = np.nan
    
    dict_diff_rel[k] = df_diff_rel.copy()









