# -*- coding: utf-8 -*-
"""
Created on Thu Apr 25 11:38:30 2024

@author: nle
"""

from dbtools.src.QuantWorkUpdater_v2 import QuantWorkUpdater
from pandas.tseries.offsets import BDay
import pandas as pd
from apitools.src.apitools import EndOfDayBbg
loader = QuantWorkUpdater(dataset_name = "FUNDAMENTAL_DATA_STOCK", config_file_name = 'quant_work_updater.json')
config = loader.load_config()

last_date = pd.to_datetime('20220101').date()
due_date = pd.to_datetime('20240229').date()
perimeter = loader.get_perimeter()
tickers = loader.get_tickers_from_referential(perimeter)
ids = loader.get_referential_from_tickers(tickers)
df_idx_lvl = loader.get_data(last_date, due_date)

df_tmp = df_idx_lvl[(df_idx_lvl.security_id==2) & (df_idx_lvl.attribute_id.isin(['debt/common_eqy','common_eqy', 'eps_1gy']))]#debt/common_eqy
df_tmp = df_tmp.set_index(['DATE', 'attribute_id']).value.unstack()
print(df_tmp)
#%%
index = pd.bdate_range(start = last_date, end = due_date)
index = index[index.weekday==4]
a = df_tmp.reindex(index, method = 'nearest')
a = df_tmp.interpolate(method='nearest', limit = 1)
#%%
overrides={"BEST_FPERIOD_OVERRIDE":"1GY"}
setParams = {
"adjustmentNormal": False,
"adjustmentAbnormal": False,
"adjustmentSplit": False,
"periodicitySelection":"DAILY"
}
df = EndOfDayBbg(['AC FP Equity', 'AI FP Equity'], '20240229', '20240229', setParams = setParams, overrides=overrides)
df = df.get(['BEST_BPS']).get('BEST_BPS')

print(df)

