# -*- coding: utf-8 -*-
"""
Created on Tue Sep 10 11:53:31 2024

@author: nle
"""

import pandas as pd
import numpy as np
from dbtools.src.db_connexion import SqlConnector
from dbtools.src.QuantWorkUpdater_v2 import QuantWorkUpdater
import dbtools.src.get_repository as rep
import dbtools.src.DatabaseManager as db
from sqlalchemy import  MetaData, Table, Column, Integer, Float, String, PrimaryKeyConstraint, Sequence, UniqueConstraint, Date
from copy import deepcopy
from openpyxl import Workbook
from apitools.src.apitools import EndOfDayBbg
import warnings
warnings.filterwarnings("ignore")
connector = SqlConnector()
con_mis = connector.connection()

df_lcxp = rep.get_index_comp('LCXP', start_date='20240910', end_date='20240910')

sec_id = df_lcxp.columns

bbg_ticker = rep.mapping_from_security(sec_id, 'bbg')
ccy = rep.mapping_from_security(sec_id, 'ccy')
fund_ticker = rep.mapping_from_security(sec_id, 'fund_ticker')


df_ticker = pd.concat((bbg_ticker, fund_ticker), axis = 1)
df_ticker.columns = ['bbg', 'fund_ticker']
df_ticker['bbg_bis'] = df_ticker.bbg.str.replace(' GY',' GR')
df_ticker['idem'] = df_ticker.bbg_bis == df_ticker.fund_ticker
df_ticker['ccy'] = ccy

df_check = df_ticker[~df_ticker.idem]


rep.mapping_security_id_from(df_check['fund_ticker'], 'fund_ticker')
#%%
symbols = np.unique(df_check[['bbg', 'bbg_bis', 'fund_ticker']].values.flatten())

startDate = '20211201'
endDate = '20240131'
field = "BEST_EPS"
#%% sans preciser ccy
params = {
    "symbols":list(symbols + ' equity'),
    "startDate": startDate,
    "endDate":endDate,
    "overrides": {
        "BEST_FPERIOD_OVERRIDE": "1GY"
        },
    "setParams": {
        "currency": None,
        "adjustmentNormal": False,
        "adjustmentAbnormal": False,
        "adjustmentSplit": False
        }
    }
df_eps = EndOfDayBbg(**params).get(field)[field]
df_eps.columns = df_eps.columns.str[:-7]
#%% EUR
params_eur = {
    "symbols":list(symbols + ' equity'),
    "startDate": startDate,
    "endDate":endDate,
    "overrides": {
        "BEST_FPERIOD_OVERRIDE": "1GY"
        },
    "setParams": {
        "currency": "EUR",
        "adjustmentNormal": False,
        "adjustmentAbnormal": False,
        "adjustmentSplit": False
        }
    }
df_eps_eur = EndOfDayBbg(**params_eur).get(field)[field]
df_eps_eur.columns = df_eps_eur.columns.str[:-7]

#%% local
params_loc = {
    "symbols":[],
    "startDate": startDate,
    "endDate":endDate,
    "overrides": {
        "BEST_FPERIOD_OVERRIDE": "1GY"
        },
    "setParams": {
        "adjustmentNormal": False,
        "adjustmentAbnormal": False,
        "adjustmentSplit": False
        }
    }

s_ccy = pd.Series(index = symbols +' Equity', data = pd.Index(symbols).str[-2:])
s_ccy = s_ccy.replace({'SW':'CHF', 'FH':'EUR', 'SS':'EUR', 'IM':'EUR', 'US':'USD', 'LN':'GBP', 'NA':'EUR', 'FP':'EUR', 'GR':'EUR', 'GY':'EUR'})

df_eps_loc = pd.DataFrame()

for c in s_ccy.unique():
    params_loc['symbols'] = list(s_ccy[s_ccy==c].index)
    params_loc['setParams']['currency'] = c
    df_tmp = EndOfDayBbg(**params_loc).get(field)[field]
    df_eps_loc = pd.concat((df_eps_loc, df_tmp), axis =1)

df_eps_loc.columns = df_eps_loc.columns.str[:-7]
df_eps_loc = df_eps_loc[df_eps_loc.columns.sort_values()]
#%%
dict_eps = {}
for i in df_check.index:
    dict_eps[i] = df_eps_eur[df_eps_eur.columns.intersection(df_check.loc[i])]

#%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



