# -*- coding: utf-8 -*-
"""
Created on Thu May 16 14:30:38 2024

@author: nle
"""

import os
import pandas as pd
import numpy as np
from dbtools.src.db_connexion import SqlConnector
import dbtools.src.get_repository as rep
import dbtools.src.DatabaseManager as db
from copy import deepcopy
connector = SqlConnector()
con_mis = connector.connection()
import json
from tqdm import tqdm
from datetime import datetime

end_date = '20231229'
level2 = rep.get_quant_perimeter(2)
level2 = pd.Series(data = level2, index = level2, name = 'sec_id')
flag_lms= rep.get_flag_lms(end_date, end_date).iloc[0].to_frame('capi')
flag_lms = flag_lms.reindex(level2, fill_value = -1)
flag_lms = flag_lms.replace({-1:'XS', 0:'L', 1:'M', 2:'S'})
last_in_date = pd.to_datetime(rep.mapping_from_security(level2, code  ='last_in_date'))
begin_date = pd.to_datetime(rep.mapping_from_security(level2, code  ='begin_date_min'))
country = rep.mapping_country_from_security_id(level2)
ccy = rep.mapping_from_security(level2, code = 'ccy').to_frame('ccy')
fund_ticker = rep.mapping_from_security(level2, code = 'fund_ticker').to_frame('fund_ticker')

df_sec = pd.concat([level2, flag_lms, country, ccy, fund_ticker, last_in_date, begin_date], axis =1)
df_sec = df_sec[(df_sec.begin_date_min<'20220101') & (df_sec.last_in_date>'20231231')]

df_sec = df_sec.drop_duplicates(subset = 'fund_ticker', keep = 'first')

n_values = df_sec.nunique().loc[['capi', 'country', 'ccy']]
#%% Random with criteria: all country, all ccy and all market cap
n_values_sample = n_values*0
(n_values_sample==n_values).sum()!=1
i= 0
while (n_values_sample==n_values).sum()!=len(n_values_sample):
    df_sample = df_sec.sample(100).sort_index()
    n_values_sample = df_sample.nunique().loc[n_values_sample.index]

#%% statistics od sample compare to population
df_stat = pd.DataFrame()
for c in n_values_sample.index:
    s1 = df_sample[c].value_counts(normalize=True).to_frame('sample')*100
    s2 = df_sec[c].value_counts(normalize=True).to_frame('level2')*100
    df = pd.concat([s1, s2], axis =1)
    df['category'] = df.index
    df['group'] = c
    df = df.set_index(['group', 'category'])
    df_stat = pd.concat((df_stat, df))
print(df_stat)
#%%
print('---------- Check: sec_ids ----------')
sec_ids = df_sample.sec_id.values
print('list of security_id: %s'% ', '.join([str(s) for s in sec_ids]))











