# -*- coding: utf-8 -*-
"""
Created on Fri Sep 27 09:46:34 2024

@author: nle
"""

import os
import pandas as pd
from dbtools.src.db_connexion import SqlConnector
import dbtools.src.get_repository as rep
import dbtools.src.DatabaseManager as db
from copy import deepcopy
from dbtools.src.QuantWorkUpdater_v2 import QuantWorkUpdater, get_mapping_from_dictionnary, transform_dict_to_df
connector = SqlConnector()
con_mis = connector.connection()
from datetime import datetime
from tqdm import tqdm

import warnings
warnings.filterwarnings("ignore")



end_date = '20031231'
start_date = '19980101'
start_date_default = start_date

loader = QuantWorkUpdater(dataset_name = "FUNDAMENTAL_DATA_STOCK", config_file_name = 'quant_work_updater.json')
config = loader.load_config()


df_perim = pd.read_sql(f"select distinct security_id from QUANT_work..FUNDAMENTAL_DATA_STOCK", con_mis).security_id.sort_values()
ids = df_perim
table_field = loader.config.get('table_field')
table_data_dictionary = loader.config.get('table_data_dictionary', None)

if table_data_dictionary:
    s_mapping_name = get_mapping_from_dictionnary(table_data_dictionary, key='name', value='attribute_id')
else:
    s_mapping_name = {}
d_data = {}
source_name = loader.config.get('data_source',{}).get("source_name",'')
#%%
if source_name == "Bloomberg":
    data_spec = loader.config.get('data_source',{}).get("data_spec",{})
    start_date_default = loader.config.get('start_date_default','19980101')
    print('-------------- Loading from %s ------------------'%source_name)

    for name, param in tqdm(data_spec.items()):
        df_symbols = loader.get_tickers_from_referential(ids)
        df_symbols = df_symbols[df_symbols['delisted_date']>pd.to_datetime(start_date).date()]
        if param.get('currency')!='local':
            df_symbols['currency'] = param.get('currency')
        loader.df_symbols = df_symbols.copy()

        # get start date default per attribute_id. if df_date_ref, get start_date_default from config, else min(date in table, start_date)
        attribute_id = s_mapping_name.get(name)
        df_date_ref = loader.get_security_date_ref(attribute_id)
        df_date_ref.data_date_max = pd.to_datetime('20230101')
        if df_date_ref.empty:
            start_date_default = loader.config.get('start_date_default','19980101')
        else:
            start_date_default = min(pd.to_datetime(start_date), 
                                     df_date_ref.data_date_min.min())
            start_date_default = start_date_default.strftime('%Y%m%d')
        df_symbols = loader.df_symbols.merge(df_date_ref, left_index =True, right_index =True, how = 'left')
        df_symbols['flag_exist'] = df_symbols['data_date_min'].notna()
        df_symbols['data_date_min'] = df_symbols['data_date_min'].fillna(start_date_default)
        df_symbols['data_date_max'] = df_symbols['data_date_max'].fillna(datetime(2999,12,31))
        # Load data using get_data_bloomberg
        df_tmp = loader.get_data_bloomberg(df_symbols, param, start_date, end_date, start_date_default)
        d_data[name] = df_tmp.copy()


    # transform data into format (index_id, DATE, value)
    df_data = transform_dict_to_df(d_data, loader.config.get('table_date_field'))
    df_data = df_data.rename(index = s_mapping_name)
    df_data.columns.name = loader.config.get("table_perimeter_field")
#%%
if df_data.empty:
    df_db = pd.DataFrame(columns = loader.config.get('table_columns'))
else:
    df_db = df_data.stack().to_frame(table_field).reset_index()

df_db['insert_date'] = pd.to_datetime(datetime.now().strftime('%Y%m%d'))
df_db = df_db[loader.config.get('table_columns')].drop_duplicates(keep = 'first')

sec_missing = df_symbols.index.difference(df_db[loader.config.get("table_perimeter_field")])
loader.df_symbols_check = pd.DataFrame()
if len(sec_missing)>0:
    print('!!!!!!!!!!!!! Check if below securities are delisted !!!!!!!!!!!!!')
    print(df_symbols.loc[sec_missing])
    loader.df_symbols_check = df_symbols.loc[sec_missing].copy()
#%%

loader.import_to_sql(df_db)

#%%




df_date_ref = loader.get_security_date_ref(attribute_id=1)

param = config['FUNDAMENTAL_DATA_STOCK']['data_source']

df_data = loader.get_data(start_date, end_date)



df_symbols = df_symbols.merge(df_date_ref, left_index =True, right_index =True, how = 'left')
d_data = {}
data_name = 'eps_1gy'

param = {'data_frequence': 'DAILY',
 'function': 'EndOfDayBbg',
 'field': ['BEST_EPS'],
 'currency': 'local',
 'overrides': {'BEST_FPERIOD_OVERRIDE': '1GY'},
 'setParams': {'adjustmentNormal': False,
  'adjustmentAbnormal': False,
  'adjustmentSplit': False}}

df_eps_1gy = loader.get_data_bloomberg(df_symbols, param, start_date, end_date, start_date_default)











