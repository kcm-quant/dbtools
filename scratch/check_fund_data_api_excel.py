# -*- coding: utf-8 -*-
"""
Created on Thu Nov  2 13:16:11 2023

@author: nle
"""
from kc_bbg_api.connection import ApiSettings,KCBBGClient
from kc_bbg_api import historical_data,intraday_tick_data,reference_data,indexcomposition_data,intraday_bar_data
from datetime import datetime
import pandas as pd

import numpy as np
import os, sys

import dbtools.src.get_repository as rep
import dbtools.src.get_market_data as mkt
import dbtools.config.fundamental_config as config
import dbtools.src.bcp_import_to_sql as bcp_import_to_sql

from dbtools.src.db_connexion import SqlConnector
connector = SqlConnector()
con_mis = connector.connection()


#ApiSettings.api_url = 'http://uatifcweb903.keplercm.lan:8082'
ApiSettings.user_name = "bbg.quant-research.user"
ApiSettings.user_password = "pZ_B5LJ-QgfOO0Vpk_Yq"

conf = config.Config()
dict_adj_conf = conf.dict_adj.copy()
dict_fund_conf = conf.dict_fund.copy()

perimeter = [34097,      257,  1313623,  2769415,  2743387, 10174313,
              309900,       84,   183255,     5479,  3004012,    45449,
             2911271,  2836511,  2975580,    19556,    13600,      461,
                 843,  2758985,    76649,  1233395,  1507291,      951,
               38808,      494,      532,    12483,     7000,    19459,
              539877,  2744144,      503,    38847,    13474,   985013,
               15391,       42,    11814,    39028,   249722,   318055,
               76321,    79227,   290244,    38900,   290351,   289938,
              287574,    17872]

level2 = rep.get_quant_perimeter(2)
fund_ticker_l2 = rep.mapping_from_security(level2, code = 'fund_ticker')
fund_ticker_l2 = fund_ticker_l2 + ' Equity'

fund_ticker = fund_ticker_l2.loc[perimeter]#sample(200)
security_id = fund_ticker.index

start_date = '20100101'# '20221003'#20160614
end_date = '20230519' #'20221015'#20160620

def get_fund_data(security_id, start_date, end_date, table = 'QUANT_work..level2_fundvar_prod_test_api'):
    sec_str = ",".join(map(str, security_id))

    query = """select *
    from %s
    where security_id in (%s)
    and date between '%s' and '%s'
    order by date, security_id""" % (table, sec_str, start_date, end_date)
    df_fund = pd.read_sql_query(query, con_mis)
    
    df_fund = df_fund.set_index(['date', 'security_id'])
    if 'ticker' in df_fund.columns:
        df_fund = df_fund.drop('ticker', axis=1)
    dict_fund = {}
    for key in df_fund.columns:
        temp = df_fund[key].unstack().sort_index().astype(float)
        dict_fund[key] = temp.rename_axis(index=None, columns=None)
    return dict_fund

sql_table_name_api='QUANT_work..level2_fundvar_prod_api_20230519'
sql_table_name_excel='QUANT_work..level2_fundvar_prod_excel_20230519'
fund_api = get_fund_data(fund_ticker.index, start_date, end_date, sql_table_name_api)
fund_excel = get_fund_data(fund_ticker.index, start_date, end_date, sql_table_name_excel)

#%%

fund_diff = {}
d_stat = {}
for k in fund_api.keys():
    df_api = fund_api[k].copy().dropna(how = 'all').fillna(-1).reindex(columns= fund_ticker.index).stack().to_frame('api')
    df_excel = fund_excel[k].copy().dropna(how = 'all').fillna(-1).reindex(columns= fund_ticker.index).stack().to_frame('excel')
    df = pd.concat((df_api, df_excel), axis = 1).fillna(-1)
    df['diff'] = df.api - df.excel
    
    df = df.reset_index().rename(columns = {'level_0': 'date', 'level_1':'security_id'})
    
    df['diff'] = df['diff'].replace({0:np.nan, np.inf:np.nan, -np.inf:np.nan})
    df_diff = df.dropna(subset = ['diff'])
    df_diff = df_diff.replace({-1:np.nan})
    df_diff.loc[(df_diff.excel.isna() | df_diff.api.isna()),'diff'] = np.nan
    df_diff['diff_rel(%)'] = (df_diff['diff']/df_diff['api']).replace({0:np.nan, np.inf:np.nan, -np.inf:np.nan})*100
    df_diff['ticker'] = df_diff.security_id.map(fund_ticker)
    df_diff = df_diff.replace({-1:np.nan})
    df_diff = df_diff[(df_diff['diff_rel(%)']>0.5) | df_diff['diff_rel(%)'].isna()]
    fund_diff[k] = df_diff.copy()

    s_tot = df[['security_id','date']].nunique()
    s_tot.loc['nb_lines'] = len(df)
    
    s_diff = df_diff[['security_id','date']].nunique()
    s_diff.loc['nb_lines'] = len(df_diff)
    df_stat = pd.concat((s_tot, s_diff), axis = 1)
    df_stat.columns = ['nb total', 'nb difference']
    df_stat['pct'] = (df_stat['nb difference'] / df_stat['nb total']*100).round(2)
    df_stat = df_stat.loc[['nb_lines', 'security_id', 'date']]
    d_stat[k] = df_stat.copy()

#%%
for k in d_stat.keys():
    df = fund_diff[k].copy()
    if df.empty:
        print('\n------------- %s : OK------------'%k)
    else:
        print('\n------------- %s : KO, check fund_diff["%s"]------------'%(k,k))
        print(d_stat[k].to_string())
    




#%%
k = 'BEST_BPS_BF'


df = fund_diff[k].copy()
df.groupby('security_id')['date'].nunique()

df.security_id.nunique()
s_diff = df[['security_id','date']].nunique()
s_diff.loc['nb_lines'] = len(df)





























