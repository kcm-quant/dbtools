# -*- coding: utf-8 -*-
"""
Created on Wed Jun 14 17:35:57 2023

@author: nle
"""
from dbtools import *
import pandas as pd
#%%
def _correct_EXO(security_id, field, start_date, end_date):
    """
    Concatenate data  of EXO IM (795445) and EXO NA (10174467)
    As if EXO IM (795445) changed listing from Italy to Amsterdam (EXO NA (10174467))
    """
    if (795445 in security_id) & (10174467 in security_id):
        sec_id = pd.Index(security_id)
    elif (795445 in security_id):
        sec_id = pd.Index(security_id).union([10174467])
    elif (10174467 in security_id):
        sec_id = pd.Index(security_id).union([795445])
    else:
        sec_id = pd.Index(security_id)
        
    sec_str = ",".join(map(str, sec_id))
    fld_str = ",".join(field)
    
    prim_td = rep.mapping_from_security(security_id, 'prim_td_id')
    query = """
            select distinct security_id, date, trading_destination_id, %s
            from MARKET_DATA..trading_daily
            where security_id in (%s)
            and date between '%s' and '%s'
            """ % (fld_str, sec_str, start_date, end_date)
    # Load data, prim market, and delete trading destination NaN
    df_temp = pd.read_sql(query, con_mis)
    
    df_tmp = df_temp[df_temp.security_id.isin([10174467, 795445])]
    
    df_temp = pd.concat((df_temp, df_tmp)).drop_duplicates(keep = False)
    
    df_ = df_tmp.set_index(['date', 'trading_destination_id', 'security_id']).unstack()
    df_exo = pd.DataFrame()
    if not df_.empty:
        for f in field:
            df = df_[f].fillna(axis = 1, method = 'ffill').fillna(axis = 1, method = 'bfill').stack().to_frame(f)
            df_exo = pd.concat((df_exo, df), axis = 1)
        df_exo = df_exo.reset_index()[df_temp.columns]
    
    df_temp = pd.concat((df_temp, df_exo))
    df_temp = df_temp[df_temp.security_id.isin(security_id)]
    return df_temp

def _correct_EXO(df):
    """
    Concatenate data  of EXO IM (795445) and EXO NA (10174467)
    As if EXO IM (795445) changed listing from Italy to Amsterdam (EXO NA (10174467))
    """
    
    df_temp = df.copy()
    df_tmp = df_temp[df_temp.security_id.isin([10174467, 795445])]
    
    df_temp = pd.concat((df_temp, df_tmp)).drop_duplicates(keep = False)
    
    df_ = df_tmp.set_index(['date', 'trading_destination_id', 'security_id']).unstack()
    df_exo = pd.DataFrame()
    if not df_.empty:
        for f in field:
            df = df_[f].fillna(axis = 1, method = 'ffill').fillna(axis = 1, method = 'bfill').stack().to_frame(f)
            df_exo = pd.concat((df_exo, df), axis = 1)
        df_exo = df_exo.reset_index()[df_temp.columns]
    
    df_temp = pd.concat((df_temp, df_exo))
    df_temp = df_temp[df_temp.security_id.isin(security_id)]
    return df_temp
#%%
start_date = '20220801'
end_date='20221015'
field = ['close_prc', 'turnover']
security_id = [2, 10174467]

df = correct_EXO(security_id, field, start_date, end_date)
