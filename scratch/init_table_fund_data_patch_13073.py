# -*- coding: utf-8 -*-
"""
Created on Mon Aug 19 16:18:37 2024

@author: nle
"""


import pandas as pd
import numpy as np
from dbtools.src.db_connexion import SqlConnector
from dbtools.src.QuantWorkUpdater_v2 import QuantWorkUpdater
import dbtools.src.get_repository as rep
import dbtools.src.DatabaseManager as db
from sqlalchemy import  MetaData, Table, Column, Integer, Float, String, PrimaryKeyConstraint, Sequence, UniqueConstraint, Date
from copy import deepcopy
from openpyxl import Workbook
connector = SqlConnector()
con_mis = connector.connection()
import dbtools.src.bcp_import_to_sql as bcp_import_to_sql
import json
from tqdm import tqdm
from datetime import datetime

folder = 'W:/Global_Research/Quant_research/projets/data/dynamic universe/quant_work/fundamental/old/'



def load_excel(path_to_load):
    sheetname = pd.ExcelFile(path_to_load).sheet_names
    sheetname = pd.Index(sheetname)
    sheetname = sheetname[sheetname!='Sheet']
    # load security_id and ticker in sheetname 'Sheet' in excel file to map
    sec_map_ticker = pd.read_excel(path_to_load, sheet_name = 'Sheet', index_col=0).squeeze()
    sec_map_ticker = pd.Series(index = sec_map_ticker.values, data = sec_map_ticker.index)
    
    df_db_stack = pd.DataFrame()
    for j in range(len(sheetname)):
        sheet = sheetname[j]
        print('No.%d - %s'%(j, sheet))
        df_table = pd.read_excel(path_to_load, sheet_name = sheet,
                           header=3, index_col = 0, na_values=['#N/A Invalid Security'])
        df_table = df_table.rename(columns = sec_map_ticker.to_dict())
        df_table.index.name = 'date'
        df_table.columns.name = 'security_id'
        df_table_db = df_table.stack().to_frame(sheet)
        df_db_stack = pd.concat((df_db_stack, df_table_db), axis = 1)
    return df_db_stack



# =============================================================================
# file1 = 'fundamental_value_20230331.xlsx'
# file2 = 'fundamental_value_20230428.xlsx'
# df1 = load_excel(folder+file1).reset_index()
# df2 = load_excel(folder+file2).reset_index()
# df2_filter = df2[(df2['date']>df1.date.max()) & (df2['date']<'20230418')]
# df_dsm_old = pd.concat((df1[df1.security_id==13073], df2_filter[df2_filter.security_id==13073]))
# df_dsm_old.index = df_dsm_old.date
# =============================================================================

filename = 'fundamental_value_20230428_dsm.xlsx'
df_dsm = load_excel(folder+filename).reset_index()
df_dsm.PX_LAST_N_N_Y_fundccy = df_dsm.PX_LAST_N_N_Y
df_dsm.index = df_dsm.date
#con_mis.execute('USE QUANT_work')
#pd.bulk_copy(con_mis, 'level2_fundvar_patch_13073', df_dsm)





