# -*- coding: utf-8 -*-
"""
Created on Tue Nov 21 09:58:47 2023

@author: nle
"""

from kc_bbg_api.connection import ApiSettings,KCBBGClient
from kc_bbg_api import historical_data,intraday_tick_data,reference_data,indexcomposition_data,intraday_bar_data
from datetime import datetime
import pandas as pd

import numpy as np
import os, sys

import dbtools.src.get_repository as rep
import dbtools.src.get_market_data as mkt
import dbtools.config.fundamental_config as config
import dbtools.src.bcp_import_to_sql as bcp_import_to_sql
import dbtools.src.DatabaseManager as db
from dbtools.src.db_connexion import SqlConnector
connector = SqlConnector()
con_mis = connector.connection()


#ApiSettings.api_url = 'http://uatifcweb903.keplercm.lan:8082'
ApiSettings.user_name = "bbg.quant-research.user"
ApiSettings.user_password = "pZ_B5LJ-QgfOO0Vpk_Yq"

conf = config.Config()
dict_adj_conf = conf.dict_adj.copy()
dict_fund_conf = conf.dict_fund.copy()

map_attr_id = pd.read_sql("""select * from QUANT_work..FUNDAMENTAL_DATA_DICTIONARY""",
                          con_mis)

map_attr_id = map_attr_id.set_index('name').attribute_id
# %% param
start_date = '20180101'# '20221003'#20160614
end_date = '20231019'


perimeter = [2, 8, 951, 34097, 287574, 17872]
#%%
table_name = 'FUNDAMENTAL_DATA_STOCK'
fund_ticker = rep.mapping_from_security(perimeter, code = 'fund_ticker')
fund_ticker = fund_ticker + ' Equity'

security_id = fund_ticker.index

#%%

def _get_histo_data(tickers, fields, start_date, end_date, overrides, params):
    df = historical_data(securities=list(tickers),
                                fields=fields,
                                startDate=start_date, endDate=end_date,
                                overrides=overrides,
                                setParams=params)
    df = df.drop_duplicates(keep = 'last').set_index(['date', 'security'])[fields[0]].unstack()
    
    tickers_fix = tickers[tickers.str.contains('/')]
    tickers_fix = dict(zip(tickers_fix.str.replace('/',''), tickers_fix))
    df = df.rename(columns = tickers_fix)
    
    df = df.reindex(columns = tickers)
    df.columns = tickers.index
    return df

def get_histo_data(dict_config, tickers, start_date, end_date):
    t_s = datetime.now()
    dict_data = {}
    for k in dict_config.keys():
        conf_tmp = dict_config[k]
        ts = datetime.now()
        if k == 'PX_LAST_N_N_Y_fundccy':
            fund_ccy = reference_data(securities=list(tickers), fields="EQY_FUND_CRNCY").drop_duplicates()
            fund_ccy = pd.Series(index = fund_ccy.security.values, data = fund_ccy.EQY_FUND_CRNCY.values)
            tickers_fix = tickers[tickers.str.contains('/')]
            tickers_fix = dict(zip(tickers_fix.str.replace('/',''), tickers_fix))
            fund_ccy = fund_ccy.rename(index = tickers_fix)
            fund_ccy = fund_ccy.reindex(tickers).to_frame('ccy').reset_index()
            fund_ccy.index = tickers.index
            fund_ccy.columns = ['fund_ticker', 'ccy']
            df = pd.DataFrame()
            for ccy in fund_ccy['ccy'].unique():
                ticker_tmp = fund_ccy.loc[fund_ccy['ccy']==ccy, 'fund_ticker']
                conf_tmp['overrides'] = {'EQY_FUND_CRNCY':ccy}

                df_ = _get_histo_data(tickers = ticker_tmp,
                                     start_date = start_date,
                                     end_date = end_date,
                                     fields = conf_tmp.get('fields'),
                                     overrides = conf_tmp.get('overrides'),
                                     params = conf_tmp.get('setParams'))
                df = pd.concat((df, df_), axis = 1)
            df = df.reindex(columns = tickers.index)
        else:
            df = _get_histo_data(tickers = tickers,
                             start_date = start_date,
                             end_date = end_date,
                             fields = conf_tmp.get('fields'),
                             overrides = conf_tmp.get('overrides'),
                             params = conf_tmp.get('setParams'))
        te = datetime.now()
        t_exec = te-ts
        print('--------- %s: \t %s -------' %(k, t_exec))
        dict_data[k] = df.sort_index(axis = 1).copy()
    t_e = datetime.now()
    t_total = t_e - t_s
    print('------------- Load Fundamental data in %s ----------------' %t_total)
    return dict_data.copy()

def import_to_sql(df, table_name):

    db_quant_index = db.DatabaseManager(table_name)
    db_quant_index.append_table(df = df.copy())
    pass
#%% subset fundamental config for loading load data
dict_conf = {}
for k in dict_fund_conf.keys():
    if k[:3]=='eps':
        dict_conf[k] = dict_fund_conf[k].copy()

fund_api = get_histo_data(dict_conf, fund_ticker, start_date, end_date)
#%% transform to database format
df_db = pd.DataFrame()

for k in fund_api.keys():
    df = fund_api[k].copy().stack().to_frame('value')
    df['attribute_id'] = map_attr_id.loc[k]
    df = df.reset_index()
    df = df.rename(columns = {'date':'DATE', 'level_1':'security_id'})
    df_db = pd.concat((df_db, df))

#%% import to sql
import_to_sql(df_db, table_name = table_name)






















