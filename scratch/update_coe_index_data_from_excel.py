# -*- coding: utf-8 -*-
"""
Created on Mon Oct 23 11:32:40 2023

@author: nle
"""

from sqlalchemy import  MetaData, Table, Column, Integer, Float, String, PrimaryKeyConstraint, Sequence, UniqueConstraint, Date
import dbtools.src.DatabaseManager as db
from dbtools.src.db_connexion import SqlConnector
from datetime import datetime

import dbtools.src.get_repository as rep
import dbtools.src.get_market_data as mkt
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np

connector = SqlConnector()
con_mis = connector.connection()


#%% Initialize INDEX_DATA
# =============================================================================
# column_formats = {
#     'index_id': Integer,
#     'DATE' : Date,
#     'attribute_id' : Integer,
#     'value': Float
#     }
# df = pd.DataFrame(columns = column_formats.keys())
# 
# db_manager = db.DatabaseManager( "INDEX_DATA")
# db_manager.create_table(df = df.copy(),column_types=column_formats, primary_keys = ['index_id', 'DATE', 'attribute_id'])
# 
# =============================================================================
#%% Initialize INDEX_DATA_DICTIONARY

# =============================================================================
# column_formats = {
#     'attribute_id': Integer,
#     'name': String(25),
#     'readable_name' :  String(30),
#     'description' : String(500)}
# df = pd.read_excel('W:\Global_Research\Quant_research\.shared files\index_data_dictionary.xlsx')
# 
# db_manager = db.DatabaseManager("INDEX_DATA_DICTIONARY")
# db_manager.create_table(df = df.copy(),column_types=column_formats, primary_keys = ['attribute_id'], auto_incremental_index='attribute_id')
# 
# =============================================================================
#%%%%%%%%%%%%%% Update INDEX_DATA for ERPM %%%%%%%%%%%%%%%%%%%%
def load_coe_from_excel(path_coe):
    # Load df_info from sheet info&inputs, only keep group 'US_ref','country'
    df_info = pd.read_excel(path_coe, sheet_name = 'info&inputs')
    
    df_info = df_info[df_info.Group.isin(['US_ref','country'])]
    df_info = df_info[df_info.columns.difference(['DATES'])]
    df_info['Risk Free'] = df_info['Risk Free'].str[:-6]
    
    
    list_ticker = pd.concat((df_info.Ticker, df_info['Risk Free']))
    list_ticker = pd.Index(list_ticker)
    
    #% Load max_date in actual database
    s_max_date = pd.read_sql('''select index_id, max(DATE) max_date from QUANT_work..INDEX_DATA group by index_id''', con_mis)
    s_max_date = s_max_date.set_index('index_id')
    s_max_date = s_max_date.reindex(list_ticker).fillna(pd.to_datetime('19900101'))
    
    # Create mapping for attribute_id-attribute name, index_ticker-index_id, risk free corresponding to index
    s_map_riskfree = pd.Series(index = df_info.Ticker.values, data = df_info['Risk Free'].values)
    s_map_attribute_name = pd.Series({'Last Price':'level', 'EPS1':'eps_1gy', 'EPS2':'eps_2gy', 'EPS3':'eps_3gy','FF_MCAP':'ffmcap'})
    s_map_index_id = rep.mapping_index_from_ticker(list_ticker)
    
    df_attribute = pd.read_sql("""select * from QUANT_work..INDEX_DATA_DICTIONARY""", con_mis)
    s_map_attribute_id = pd.Series(index = df_attribute.name.values, data = df_attribute.attribute_id.values)
    
    # Load data from excel
    df_db = pd.DataFrame()
    for index_ticker in df_info['Ticker']:
        # index ticker, index_id, risk free ticker and risk free id
        index_id = s_map_index_id.loc[index_ticker]
    
        rf = s_map_riskfree.loc[index_ticker]
        if not isinstance(rf, str):
            print('Missing risk free index for index %s' %index_ticker)
        rf_id = s_map_index_id.loc[rf]
        
        print('-------------- %s, %s ---------------' %(index_ticker, rf))
        
        # Max date in actual database
        max_date_index = s_max_date.loc[index_ticker].iloc[0]
        max_date_rf = s_max_date.loc[rf].iloc[0]
        
        # load data from excel
        # keep columns 'Dates', 'Last Price', 'EPS1', 'EPS2', 'EPS3', 'Risk Free', 'FF_MCAP'
        # keep only friday
        df_tmp = pd.read_excel(path_coe, sheet_name = index_ticker, header = 1)
        df_tmp = df_tmp.drop_duplicates()
        df_tmp = df_tmp[df_tmp.columns.intersection(['Dates', 'Last Price', 'EPS1', 'EPS2', 'EPS3', 'Risk Free', 'FF_MCAP'])]
        df_tmp = df_tmp.rename(columns={'Dates':'DATE'})
        df_tmp.DATE = pd.to_datetime(df_tmp.DATE)
        df_tmp.index = df_tmp.DATE
        df_tmp = df_tmp[df_tmp.DATE.dt.weekday==4]
        df_tmp.columns.name = 'attribute_id'
        
        # Retrieve data of index, transform to database format
        df_index = df_tmp[s_map_attribute_name.keys()]
        df_index = df_index[df_index.index>=max_date_index]
        df_index['index_id'] = index_id
        df_index = df_index.set_index(['index_id'], append = True).stack().to_frame('value')
        
        # Retrieve data for risk free, transform to database format
        df_rf = df_tmp['Risk Free'].to_frame('level')
        df_rf.columns.name = 'attribute_id'
        df_rf = df_rf[df_rf.index>=max_date_rf]
        df_rf['index_id'] = rf_id
        df_rf = df_rf.set_index(['index_id'], append = True).stack().to_frame('value')
        
        # concatenate data
        df_db = pd.concat((df_db, df_index, df_rf))
    # rename attribute name as of attribute id
    df_db = df_db.reset_index()
    df_db['attribute_id'] = df_db['attribute_id'].replace(s_map_attribute_name)
    df_db['attribute_id'] = df_db['attribute_id'].replace(s_map_attribute_id)
    
    return df_db.drop_duplicates().copy()

#%% Load from excel
path_coe = r'W:\Global_Research\Quant_research\projets\market_watch\Cost of Equity\CoE - Data_20230929.xlsm'
df_db = load_coe_from_excel(path_coe)
#%% append to INDEX_DATA
db_index_data = db.DatabaseManager( "INDEX_DATA")
db_index_data.append_table(df_db)


















