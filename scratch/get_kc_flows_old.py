# -*- coding: utf-8 -*-
"""
Created on Wed Oct 19 08:58:21 2022

@author: nle
"""
import pandas as pd
import numpy as np
from datetime import datetime, time, timedelta

import os
import json

import dbtools.src.get_repository as rep
import dbtools.src.get_market_data as mkt
from dbtools.src.db_connexion import SqlConnector
connector = SqlConnector()
con_mis = connector.connection()


def get_account_classification():
    """Get account classification in KGR"""
    df_acc = pd.read_sql("""select Id account_id, Name client_name, Type client_type, Country client_region 
                         from KGR..Account_Classification""", con_mis)
    df_acc = df_acc.copy()
    return df_acc

def get_fx2eur(ccy_list, start_date, end_date):
    """
    Get data Execution or Channel
    """
    ccy_list_txt = ", ".join(["'" + text + "'" for text in ccy_list])

    req = """SELECT Date date, QuotedCCy ccy, (Rate+0) fx_rate 
            FROM INFOCENTRE.dbo.Daily_ExchangeRate  
            WHERE Date >= '%s 00:00:00' and Date<='%s 23:59:59' and BaseCCy='EUR'
            and QuotedCCy in (%s)
            """ %(start_date, end_date, ccy_list_txt)
    
    df_rate = pd.read_sql(req, con_mis)
    return df_rate


class KechFlows:
    """
    Get Kech flows from INFOCENTRE..ALLTRADE_QUANT or QUANT_work..TradingPerChannel or FLEX_STATS (algo) orders
    
    Args:
        start_date (str): yyyymmdd, start date of data
        end_date (str): yyyymmdd, end date of data
        

    Attributes:
        df_data (DataFrame): Dataframe of KC flows 
    """

    def __init__(self, start_date, end_date):
        self._get_account_classification()
        self._start_date = start_date
        self._end_date = end_date
        self.df_data = pd.DataFrame()
        self._load_config()
    
    def _load_config(self):
        """Load flow configurations from 'kc_flows.json' file.

        The method reads the configuration data from the 'kc_flows.json' file
        and updates the `analytics` attribute with the loaded data.
        """
        config_folder_path = os.path.join(os.path.dirname(__file__), '..', 'config')# r'C:\python_tools\dbtools\config'
        config_file_name = "kc_flows.json"
        config_file_path = os.path.join(config_folder_path, config_file_name)
        with open(config_file_path, 'r') as config_file:
            config_data = json.load(config_file)
            config_data = config_data['KechFlows']
            self.corpo_sales_ids =  config_data.get('corpo_sales_ids')
            self.account_patch =  config_data.get('account_patch',{})

    def _get_crystal_code_from_clnid(self):
        """
        Get crystal code from execution CLNID
        """
        
        df_cln_map = pd.read_sql("""select CLNID, 
            case when isnumeric(CLNID) = 1 then convert(bigint,EXTCLNID) else 0 end account_id 
            from OMS..CLIENTIDMAP""", con_mis)
        
        df_data = self.df_data.copy()
        df_data = df_data.merge(df_cln_map, on='CLNID', how='left')
        df_data = df_data.drop('CLNID', axis=1)
        
        self.df_data = df_data.copy()
        pass


    def _get_account_classification(self):
        """Get account classification in KGR"""
        df_acc = get_account_classification()
        self.df_acc = df_acc.copy()
        pass
    
    def _add_account_classification_to_data(self):
        df_init = self.df_data
        df_init = df_init.merge(self.df_acc, on = 'account_id')
        
        # patch for 1 client which changed type from Hedge funds to Funds as of October 2023 --> to be put into json config
        #df_init.loc[(df_init['account_id']==2146856223)&(df_init['date']<='2023-09-28'), 'client_type']='Hedge funds'
        acc_patch = self.account_patch
        for account_id_str, patch_data in acc_patch.items():
            account_id = int(account_id_str)  # Convert account_id from string to integer
            if 'account_id' in df_init.columns and account_id in df_init['account_id'].values:
                from_date = patch_data.get('from')
                to_date = patch_data.get('to')
                client_type = patch_data.get('client_type')
        
                mask = (df_init['account_id'] == account_id) & (df_init['date'] >= from_date) & (df_init['date'] <= to_date)
                df_init.loc[mask, 'client_type'] = client_type
            #else:
                #print(f"Account ID {account_id} not found or 'account_id' column not present in the DataFrame.")
        
        self.df_data  = df_init.copy()

    def get_data(self):
        """
        Get data Execution or Channel
        """
        self.df_data = pd.DataFrame()
        pass
    
    def _apply_fx2eur(self):
        """
        Get data Execution or Channel
        """
        df_data = self.df_data.copy()
        
        dts = df_data['date'].dt.date

        df_rate = get_fx2eur(df_data['ccy'].dropna().unique(), dts.min(), dts.max())
        
        df_data = df_data.merge(df_rate, on=['date','ccy'], how='left')
        df_data['amount_eur'] = df_data['amount'].div(df_data['fx_rate'])
        
        df_data = df_data.drop('fx_rate', axis=1)
        
        self.df_data = df_data.copy()
        return

    def _filter_management(self):
        """
        Filter management (salesapp): Eliminate technical and corporate clients
        """
        df_data = self.df_data.copy()
        df_data = df_data[~df_data.client_type.isin(['Technical'])]
        
        #corpo_sales_ids = ['@CORPO_Traders','BMC','BMC2','CGA','CGD','CGD2','CIE','DMZ', 'EPE',
                        #'JFT2','JFT3','JSS','JSS2','LBR','PDS','PSC','PSR','PSR-T','PSU','PSU2']
        df_data = df_data[~df_data.SALESID.isin(self.corpo_sales_ids)]
        self.df_data = df_data.copy()
        pass
    
    def _filter_research(self):
        """
        Filter research: 
            
            - Client type: Hedge funds,Funds,Banks-Brokers, Retail
            - Eliminate QFD exclusion: QUANT_FLOW_DATA..DailyClientFilter
        """
        client_type = ['Hedge funds','Funds','Banks-Brokers','Retail']
        df_data = self.df_data.copy()

        # QFD exclusion
        PERMANANT_FILTER = [951480556,2146849257,2146849258,2146859947]
        df_excl = pd.read_sql('select ID account_id,convert(DATE,StampDate) date from QUANT_FLOW_DATA..DailyClientFilter', con_mis)
        df_excl.date = pd.to_datetime(df_excl.date)
        df_excl = df_excl[~df_excl.account_id.isin(PERMANANT_FILTER)]
        

        df_data = df_data[~df_data.account_id.isin(PERMANANT_FILTER)]
        for i in range(len(df_excl)):
            s_tmp = df_excl.iloc[i]
            df_data = df_data[~(df_data.account_id.isin([s_tmp.account_id]) 
                              & (df_data.date>=s_tmp.date))]
        # client type
        df_data = df_data[df_data.client_type.isin(client_type)]
        
        self.df_data = df_data.copy()
        pass
    

    def _filter_perimeter(self, perimeter='BKXP'):
        """
        Filter by index ticker dynamic if dynamic = True, static otherwise
        Args:
            perimeter (str or list): index ticker bloomberg (str) or list of security_id, or quant perimeter (int)
                if index ticker given, STATICS composition will be applied to filter
        Returns:
            df: flows in perimeter
        """
        df_data = self.df_data
        if perimeter =='':
            security_id = df_data.security_id.unique()

        elif isinstance(perimeter, str) and (perimeter !=''):
            start_date = df_data.date.min().strftime('%Y%m%d')
            end_date = df_data.date.max().strftime('%Y%m%d')
            end_date = pd.date_range(start=end_date, periods = 7)[-1]
            end_date = end_date.strftime('%Y%m%d')
            index_comp = rep.get_index_comp(index_ticker = perimeter, start_date = start_date, end_date = end_date)
            security_id = index_comp.columns
        elif isinstance(perimeter, int):
            security_id = rep.get_quant_perimeter(perimeter)
        else:
            security_id = perimeter

        df_index = df_data[df_data.security_id.isin(security_id)]
        self.df_data = df_index.copy()
        pass


class ExecutionFlows(KechFlows):
    """
    Default output fields for category='ALL': 
        - date
        - security_id
        - ccy
        - SIDE, side
        - SALESID
        - quantity
        - amount, amount_eur
        - account_id, client_name, client_type
        
    
    """
    def __init__(self, start_date, end_date, perimeter, 
                 trade_category='ALL', additional_fields=None):
        """Initialize an instance of the ExecutionFlows class."""

        super().__init__(start_date, end_date)
        self._trade_category = trade_category
        self._additional_fields = additional_fields
        self.perimeter = perimeter
        self._load_config()
        pass
    
    def _load_config(self):
        """Load flow configurations from 'kc_flows.json' file.

        The method reads the configuration data from the 'kc_flows.json' file
        and updates the `analytics` attribute with the loaded data.
        """
        super()._load_config()
        config_folder_path = os.path.join(os.path.dirname(__file__), '..', 'config')# r'C:\python_tools\dbtools\config'
        config_file_name = "kc_flows.json"
        config_file_path = os.path.join(config_folder_path, config_file_name)
        with open(config_file_path, 'r') as config_file:
            config_data = json.load(config_file)
            config_data = config_data['ExecutionFlows']
            self.category = config_data.get('category', {})
            self.alltrade_specs = config_data.get('ALLTRADE_col_specs', {})
            self.alltrade_renaming = config_data.get('ALLTRADE_col_renaming', {})
            #self.filter = config_data.get('filter', {})
            self.close_hours = config_data.get('close_hours_by_td', {})
    
    def get_data(self):
        """
        Get data in mode execution, OMS..ALLTRADE_QUANT
        """
        start_date = self._start_date
        end_date = self._end_date
        perimeter = self.perimeter
       
        category = self._trade_category
        
        sql_condition = self.category[category]['ALLTRADE_condition']
        sql_cols = self.category[category]['ALLTRADE_specific_cols']
        
        # if additional fields > add to specific cols: 
        add_fields = self._additional_fields        
        if add_fields: 
            add_fields_list  = [field.strip() for field in add_fields.split(',') if field.strip()]
            
            if sql_cols:
                sql_cols_list  = [field.strip() for field in sql_cols.split(',')[1:] if field.strip()]
                sql_cols_list = sql_cols_list + add_fields_list
            else: 
                sql_cols_list = add_fields_list
            
            sql_cols  = ',' + ", ".join(list(set(sql_cols_list)))

            
        if sql_cols: 
            sql_add_option = 1
            
            specs = self.alltrade_specs
            sql_cols_list  = [field.strip() for field in sql_cols.split(',')[1:] if field.strip()]
            extracted_specs = [specs[key] for key in sql_cols_list if key in specs]
            sql_cols_type  = ',' + ", ".join(extracted_specs)
        else: 
            sql_add_option = 0
            sql_cols_type = ''

        
        # Split the input strings into lists and remove the leading commas
        ALLTRADE_col_names = sql_cols
        ALLTRADE_col_creation = sql_cols_type
        col_names_list = ALLTRADE_col_names.split(',')[1:]
        col_creation_list = ALLTRADE_col_creation.split(',')[1:]
        # Use list comprehension to concatenate and add spaces between elements
        col_creation = ', '.join([f'{name.strip()} {creation.strip()}' for name, creation in zip(col_names_list, col_creation_list)])
       
        req = """use QUANT_work EXEC dbo.get_kc_flows_execution_raw 
         @startDate = '%s',  
             @endDate = '%s' , 
             @ALLTRADE_condition='%s',
             @ALLTRADE_add_col=%d, 
             @ALLTRADE_col_names='%s', 
             @ALLTRADE_col_creation='%s'
             """%(start_date,end_date,
             sql_condition, 
             sql_add_option, sql_cols, col_creation)
        self.req = req
        df_init = pd.read_sql(req, con_mis)
        
        #"testing"
        #self.df_data = df_init.copy()   
        #pass
        
        df_init['date'] = pd.to_datetime(df_init['date'])
       
        #create side from SIDE with 1/-1 instead of BUY/SELL
        df_init['SIDE'] = df_init['SIDE'].apply(lambda x: 1 if x == 'BUY' else -1)
        
        # Rename cols
        df_init.rename(columns=self.alltrade_renaming, inplace=True)
        
        # additional function to treat data from sql procedure, if needed
        add_function = self.category[category]['treatment_before_output']
        if add_function!="":
            method_to_call = getattr(self, add_function)
            df_init = method_to_call(df_init)
       
        
        # first version of df_data    
        self.df_data = df_init.copy()
       
        # get crystal codes and classifications
        self._get_crystal_code_from_clnid()
        self._add_account_classification_to_data()
                
        # restrict perimeter
        if perimeter:
            self._filter_perimeter(perimeter = perimeter)
        
        # conversion to eur -> amount_eur
        self._apply_fx2eur()
        
        # pass
    
    
    def extract_close_trades(self, df_data):
        """
        Specific extraction of closing auction trades
        See Also: https://kch-trackit.atlassian.net/wiki/spaces/QAR/pages/986841089/KECH+trades+database#CLOSE
            1. Gets each stock's primary market
            2. Selects trades from close hours to close hours + 15 min
            3. Gets close price for each stock x date
            4. From 2. selects trades with LIQUIDITYINDICATOR=Auction OR price=close_price
        """

        config_folder_path = os.path.join(os.path.dirname(__file__), '..', 'config')
        config_file_name = "kc_flows.json"
        config_file_path = os.path.join(config_folder_path, config_file_name)
        with open(config_file_path, 'r') as config_file:
            config_data = json.load(config_file)
            config_data = config_data["ExecutionFlows"]
            close_hours = config_data.get('close_hours_by_td', {})
        
        start_date = df_data.date.min()
        end_date = df_data.date.max()
        
        #1. Gets each stock's primary market
        security_id = df_data.security_id.unique()
        df_prim_trading = rep.mapping_from_security(security_id, code = 'prim_td_id').to_frame('td_id').reset_index()
        df_prim_trading.rename(columns={'index':'security_id'}, inplace=True)
        df_data = df_data.merge(df_prim_trading, on='security_id', how='left')
        
        #2. Selects trades from close hours to close hours + 15 min
        close_h = {
            key: f"time(hour={value['hour']}, minute={value['minute']})"
            for key, value in close_hours.items()
            }
        td_list = [int(x) for x in close_h.keys()]
        df_data = df_data[df_data['td_id'].isin(td_list)]
        
        # timezones shift (summer/winter)
        req = """select *
            from KGR..TIMEZONE where TIMEZONE ='Europe/Paris' 
            and ENDDATE>='%s' and BEGINDATE<='%s'
            order by BEGINDATE, ENDDATE"""%(start_date, end_date)
        tshift = pd.read_sql(req, con_mis)
        df_data.sort_values('date', inplace=True)
        df_data = pd.merge_asof(df_data, tshift[['BEGINDATE','OFFSET']], left_on='date', right_on='BEGINDATE', direction='backward')
        df_data['EXECTIME_shifted'] = df_data['EXECTIME'] + pd.to_timedelta(df_data['OFFSET'], unit='s')
        
        df_close_hour = pd.DataFrame()
        for c in close_h.keys():
            df = df_data[df_data['td_id']==int(c)]
            # Define the time range
            start_time = eval(close_h[c])
            # Convert it to a datetime.datetime object
            start_datetime = datetime.combine(datetime.today(), start_time)
            # Add a timedelta of 15 minutes
            end_datetime = start_datetime + timedelta(minutes=15)
            # Convert it back to a datetime.time object if needed
            end_time = end_datetime.time()
            # Filter the DataFrame
            filtered_df = df[(df['EXECTIME_shifted'].dt.time >= start_time) & (df['EXECTIME_shifted'].dt.time <= end_time)]
            df_close_hour = pd.concat([df_close_hour, filtered_df]) 
        
        #3. Gets close price for each stock x date
        # primary market, no adjustment
        cp = mkt.get_trading_daily(security_id=security_id, start_date=start_date, end_date=end_date, ccy_ref='EUR',
                              field=['close_prc'],trading_destinations=['MAIN'], flag_consolidated=False,
                              split_adj=False, special_adj=False, regular_adj=False)
        cp = cp['close_prc']['MAIN']
        cp = cp.reset_index()
        # reformat from timeseries to database
        cp = pd.melt(cp, id_vars=['index'], var_name='security_id', value_name='close_prc')
        cp = cp.dropna(subset=['close_prc'])
        cp.rename(columns={'index':'date'}, inplace=True)
        # merge
        df_close_hour = pd.merge(df_close_hour, cp, on=['date','security_id'])
        
        #4. From 2. selects trades with LIQUIDITYINDICATOR=Auction OR price=close_price
        df_close_hour['trade_prc'] = df_close_hour['amount']/df_close_hour['quantity']
        df_close_hour['prc_diff'] = (df_close_hour['trade_prc']-df_close_hour['close_prc'])
        df_close = df_close_hour.copy()
        df_close = df_close[(df_close['LIQUIDITYINDICATOR']=='Auction')
                            | (df_close['prc_diff'].abs()<10-6)]
        
        df_close.loc[:, 'EXECTIME'] = df_close['EXECTIME_shifted']
        cols2drop = ['td_id','BEGINDATE','OFFSET','EXECTIME_shifted','close_prc','trade_prc','prc_diff']
        df_close = df_close.drop(cols2drop, axis=1)
        
        return df_close

    def reclassify_qfd_otc(self, df_data, disclosure_rule=False):
        """
        Specific reclassification of OTC trades based on Quant Flow Data definition
            1. Apply Quant Flow Data OTC definition and replaces TRADINGSYSTEM accordingly
            2. if disclosure_rule is set to True: 
                    2.a For stocks and dates in OTC trades, gets stock's ADT
                    2.b. Big clients OTC trades on small stocks are not disclosed as OTC -> artificially flagged as Other
                    2.c Returns concatenated DataFrame nonOTC + OTC as disclosed in Quant Flow Data process
        """
        df = df_data.copy()
        flag_OTC1 = (df['SOURCE']=='CROSS') & ~(df['MIC_VENUE']=='XOFF')
        flag_OTC2 = df['MIC_VENUE']=='XOFF'
        df.loc[flag_OTC1, 'TRADINGSYSTEM']='OTC'
        df.loc[flag_OTC2, 'TRADINGSYSTEM']='OTC'

        return df



class ChannelFlows(KechFlows):
    def __init__(self, start_date, end_date):
        super().__init__(start_date, end_date)
        pass
    
    def get_data(self):
        """
        Get channel data in  QUANT_work..TradePerChannel
        """
        start_date = self._start_date
        end_date = self._end_date

        req = """SELECT DealDate date, 
                ISIN isin, CotationPlace place, TRADINGCURRENCY ccy, 
                CrystalCustomer account_id, 
                ExecutionChannel channel, 
                ServiceCode service,
                case when SIDE= 'BUY' then 1 else -1 end SIDE, 
                sum(VOLUME) quantity, 
                sum(TURNOVER) amount
                FROM QUANT_work..TradePerChannel T 
                WHERE DealDate >= '%s 00:00:00' and DealDate<='%s 23:59:59' 
                group by DealDate, ISIN, CotationPlace, TRADINGCURRENCY, CrystalCustomer,
                ExecutionChannel, ServiceCode, case when SIDE= 'BUY' then 1 else -1 end""" %(start_date, end_date)

        df_init = pd.read_sql(req, con_mis)
        df_init.date = pd.to_datetime(df_init.date)
                
        self.df_data = df_init.copy()
        self._add_account_classification_to_data()
        self._apply_fx2eur()
        pass
    
    def _filter_management(self):
        """
        Filter management (salesapp): Eliminate technical and corporate clients
        """
        df_data = self.df_data.copy()
        df_data = df_data[~df_data.client_type.isin(['Technical'])]
        df_data = df_data[~df_data.service.isin(['TECH', 'CORP'])]
        self.df_data = df_data.copy()
        pass


class AlgoFlows(KechFlows) : 
    """
    AlgoFlows is a subclass of KechFlows, and is used to process financial trading data.

    Parameters
    ----------
    start_date : datetime
        The starting date for the data to be processed.
    end_date : datetime
        The ending date for the data to be processed.

    filter_missing_info : bool, default False
        If True, apply filters to remove missing information from the data.

    Attributes
    ----------
    col_list : list
        A list of column names to be used in the data processing.
    strat_dict : dict
        A dictionary mapping strategy codes to their corresponding names.
    filter_strategy : list
        A list of strategies to filter the data by.
    filter_perimeter : bool
        If True, apply perimeter filters to the data.
    filter_missing_info : bool
        If True, apply filters to remove missing information from the data.
    """
    
    col_list = ['CLIENT_ID', 'SECID', 'EXEC_DATE', 'SIDE', 'STRATEGY','RESILIENCE','BUY_EXEC',
                'LIMIT', 'FIRST_FILL_TIME', 'LAST_FILL_TIME',
                'ARRIVAL_PRICE', 'EXEC_QTY', 'EXEC_PRICE', 'TARGET_SHARES',
                'MODIFIED', 'END_TIME', 'START_TIME','GMT_OFFSET','FINAL_PRICE',
                'MARKET_VOLUME', 'FIRST_PRICE','PRIMARY_EXCHANGE_ID',
                'ISIN', 'CURRENCY', 'TICK_SIZE', 'ORDER_PERC', 'ORDER_PERC_MAX',
                'ORDER_PERC_MIN','EXECUTION_STYLE','WOD_EXEC','MARKET_IMBALANCE','MARKET_TURNOVER','USUAL_CLOSING_VOLUME',
                'USUAL_CONTINUOUS_DAILY_VOLUME','USUAL_CONTINUOUS_NB_DEALS','USUAL_DAILY_AMOUNT',
                'USUAL_DAILY_SPREAD','USUAL_DAILY_VOLATILITY','USUAL_DAILY_VOLUME','HIGH','LOW','AVERAGE_SPREAD','TRADER_ID']
    
    
    
    def __init__(self, start_date, end_date,filter_strategy = [],filter_perimeter = False , filter_missing_info=False,filter_quantity = True,additional_fields = None):
        """
        Initialize the AlgoFlows class.
        """
        super().__init__(start_date, end_date)
        self.filter_strategy = filter_strategy
        self.filter_perimeter = filter_perimeter
        self.filter_missing_info = filter_missing_info
        self.filter_quantity = filter_quantity
        self._load_config()
        if additional_fields :
            for element in additional_fields:
                if element not in self.col_list:
                    self.col_list.append(element)
        
        
    def _load_config(self):
        """Load flow configurations from 'kc_flows.json' file.

        The method reads the configuration data from the 'kc_flows.json' file
        and updates the `analytics` attribute with the loaded data.
        """
        super()._load_config()
        config_folder_path = os.path.join(os.path.dirname(__file__), '..', 'config')# r'C:\python_tools\dbtools\config'
        config_file_name = "kc_flows.json"
        config_file_path = os.path.join(config_folder_path, config_file_name)
        with open(config_file_path, 'r') as config_file:
            config_data = json.load(config_file)
            config_data = config_data['AlgoFlows']
            self.strat_dict = config_data.get('strat_dict', {})
            self.exchange_dict = config_data.get('exchange_dict', {})
            self.exchange_dict = {int(key): value for key, value in self.exchange_dict.items()}
    
    def get_data(self):
        """
        Main method to process the inforeach and flex export orders data.Loads the data ,enriches it with security id, converts to euro and applies the filters
        """
        
        self.df_data = pd.concat([self._flex_stats(), self._inforeach_stats()]).reset_index(drop=True) #.dropna(axis=1, how='all') #.dropna(axis=1, how='all')
        self.df_data.ID = pd.to_numeric(self.df_data.ID)
        self.df_data.EXEC_QTY = pd.to_numeric(self.df_data.EXEC_QTY)
        if 'GMT_OFFSET' in self.df_data.columns :
            self.df_data.GMT_OFFSET = pd.to_numeric(self.df_data.GMT_OFFSET)
        self.df_data.TARGET_SHARES = pd.to_numeric(self.df_data.TARGET_SHARES)
        self.df_data.EXEC_DATE = pd.to_datetime(self.df_data.EXEC_DATE, format='%Y-%m-%d', errors='coerce')
        self.df_data.FIRST_FILL_TIME = pd.to_datetime(self.df_data.FIRST_FILL_TIME,
                                          format='%Y-%m-%d %H:%M:%S',
                                          errors='coerce')
        self.df_data.LAST_FILL_TIME = pd.to_datetime(self.df_data.LAST_FILL_TIME,
                                          format='%Y-%m-%d %H:%M:%S',
                                          errors='coerce')
        self.df_data.START_TIME = pd.to_datetime(self.df_data.START_TIME,
                                      format='%Y-%m-%d %H:%M:%S',
                                      errors='coerce')
        self.df_data.END_TIME = pd.to_datetime(self.df_data.END_TIME,
                                    format='%Y-%m-%d %H:%M:%S',
                                    errors='coerce')
        self.df_data['duration_seconds'] = (self.df_data.END_TIME-self.df_data.START_TIME).dt.seconds
        self.df_data['duration_fill'] = (self.df_data.LAST_FILL_TIME-self.df_data.FIRST_FILL_TIME).dt.seconds
        self.df_data.replace({'SIDE':{'B':1, 'S':-1, '1':1, '2':-1}}, inplace=True)
        self.df_data.loc[~self.df_data.SIDE.isin([-1, 1]), 'SIDE'] = np.nan
        self.df_data.SIDE = self.df_data.SIDE.astype(float)
        self.df_data['fill_rate'] = self.df_data.EXEC_QTY/self.df_data.TARGET_SHARES
        self.df_data['strat'] = self.df_data.STRATEGY.replace(self.strat_dict)
        self.df_data['primary_mic'] = self.df_data.PRIMARY_EXCHANGE_ID.map(self.exchange_dict)
        self._correct_exchange_id()
        self._get_crystal_code_from_clnid()
        
        self.df_data.loc[self._is_vwap_ap(self.df_data.crystal_code, self.df_data.EXEC_DATE)&(self.df_data.strat=='VWAP'), 'strat'] = 'VWAP-AP'
        self.df_data['rd'] = self.df_data.SIDE*(self.df_data.LIMIT/self.df_data.ARRIVAL_PRICE-1)
        self.df_data['mi'] = self.df_data.SIDE*(self.df_data.EXEC_PRICE/self.df_data.ARRIVAL_PRICE-1)
        self.df_data.EXEC_QTY = self.df_data.EXEC_QTY.map(lambda x: np.nan if (x==0) else x)
        self.df_data['Aggressivity']= self.df_data.BUY_EXEC/ self.df_data.EXEC_QTY
        self.df_data['amount'] = self.df_data['EXEC_QTY'] * self.df_data['EXEC_PRICE']
        self.df_data['date'] = self.df_data['EXEC_DATE']
        self.df_data['ccy'] = self.df_data['CURRENCY']
        #self.df_data.loc[self.df_data["PRIMARY_EXCHANGE_ID"] == 10, "PRIMARY_EXCHANGE_ID"] = 39
                   
        self._add_security_id()
        self._apply_fx2eur()
        if len(self.filter_strategy) > 0 : 
            self._filter_strat(self.filter_strategy)
        if self.filter_perimeter  : 
            self._filter_perim()
        if self.filter_missing_info : 
            self._filter_missing()
        if self.filter_quantity : 
            self._filter_quantity()
        self.df_data['account_id'] =self.df_data['crystal_code']
         
        self._add_account_classification_to_data()
        self.df_data = self.df_data.rename({'TRADER_ID' : 'SALESID'},axis = 1)
           
        self.df_data = self.df_data.drop(columns=['ccy', 'EXEC_DATE', 'crystal_code'])

        
        
        
        
        
        
    def _flex_stats(self):
        """
        Retrieve orders data from the FLEX_STATS table.
        
        Returns
        -------
        pd.DataFrame
            A dataframe containing the retrieved data.
        """
        con = SqlConnector().connection('FLEXPORT')
        colnames = self._get_cols(con, 'FLEX_STATS')
        col_str = ",".join([x for x in self.col_list if x in colnames])
        return pd.read_sql_query(f"""select ID,[table]='FLEX_STATS',{col_str}
                        from FLEX_STATS
                        where EXEC_DATE between '{self._start_date}'
                        and '{self._end_date}'
                        """,con, dtype={'PRIMARY_EXCHANGE_ID': np.float64,
                                        'ORDER_PERC':np.float64})


    def _inforeach_stats(self):
        """
        Retrieve orders data from the INFOREACH_STATS table.
        
        Returns
        -------
        pd.DataFrame
            A dataframe containing the retrieved data.
        """
        con = SqlConnector().connection('FLEXPORT')
        colnames = self._get_cols(con, 'INFOREACH_STATS')
        col_str = ",".join([x for x in self.col_list if x in colnames])
        return pd.read_sql_query(f"""select ID,[table]='INFOREACH_STATS',{col_str}
                        from INFOREACH_STATS
                        where EXEC_DATE between '{self._start_date}'
                        and '{self._end_date}'
                        """, con, dtype={'PRIMARY_EXCHANGE_ID': np.float64,
                                       'ORDER_PERC':np.float64})
    
                                 
    def _get_cols(self, con, table_name):
        """
        Retrieve column names from a table in the database.
        
        Parameters
        ----------
        con : SQLAlchemy engine
            The database connection engine.
        table_name : str
            The name of the table from which to retrieve column names.
        
        Returns
        -------
        list
            A list of column names from the specified table.
        """
        q = f"""SELECT COLUMN_NAME
            FROM INFORMATION_SCHEMA.COLUMNS
            WHERE TABLE_NAME = N'{table_name}'
            """
        df = pd.read_sql(q, con)
        return df['COLUMN_NAME'].tolist()

    def _get_crystal_code_from_clnid(self):
        """
        Retrieve crystal codes from client IDs.
        """
        con = SqlConnector().connection()
        q= """select c.Name, c.Type, c.Country,
                a.CrystalCode, OmsCLNID=convert(int, a.OmsCLNID)
                from KGR..Account_Classification c, OMS..CrystalAccount a
                where c.Id = a.CrystalCode
                """
        client_data = pd.read_sql(q, con)
        self.df_data['crystal_code'] = self.df_data.merge(client_data, how='left', left_on='CLIENT_ID',
                                    right_on='OmsCLNID')['CrystalCode']
        idx = self.df_data.CLIENT_ID.isin(client_data.CrystalCode.to_list())
        self.df_data.loc[idx, 'crystal_code'] = self.df_data.CLIENT_ID[idx]
        self.df_data.loc[self.df_data.crystal_code.isna(), 'CLIENT_ID'] = np.nan
        #client_data = client_data.CrystalCode.isin(self.df_data.crystal_code)
        #return d, client_data
    def _is_vwap_ap(self,crystal_id, date):
        """
        Check if a client is eligible for VWAP-AP on a specific date.
        
        Parameters
        ----------
        crystal_id : int
            The crystal ID of the client.
        date : datetime
            The date on which to check eligibility.
        
        Returns
        -------
        bool
            True if the client is eligible for VWAP-AP on the specified date, False otherwise.
        """
        CRYSTALID4AP = pd.DataFrame(
            [[298488936,	'25/02/2020'], [2146866168,	'13/11/2019'],
            [2146860164,	'13/02/2020'],[2146852278, '04/02/2019'],
            [196657129,	'11/10/2018'],[2146855087,	'24/01/2019'],
            [2146852257,	'13/07/2020'],[172,	'27/09/2018'],
            [2146862979,	'24/07/2020'],[2146850277,	'31/08/2018']],
            columns=['id', 'date'])
        CRYSTALID4AP['date'] = pd.to_datetime(CRYSTALID4AP['date'], dayfirst=True, format='%d/%m/%Y')

        crystal_id.name = 'crystal_id'
        b = pd.DataFrame(crystal_id).merge(CRYSTALID4AP, how='left',
                                           left_on='crystal_id',
                                           right_on='id')['date']<=\
            date.reset_index(drop=True)
        return b
    def _correct_exchange_id(self) : 
        con = SqlConnector().connection()
        q = """SELECT  OPERATINGMIC,trading_destination_id
            FROM KGR..EXCHANGE_REFCOMPL"""
        mapping = pd.read_sql_query(q, con)
        mapping = mapping.set_index('OPERATINGMIC')['trading_destination_id'].to_dict()
        self.df_data['PRIMARY_EXCHANGE_ID_corr'] = self.df_data.primary_mic.map(mapping)
        self.df_data['PRIMARY_EXCHANGE_ID_corr'] = self.df_data['PRIMARY_EXCHANGE_ID_corr'].fillna(self.df_data['PRIMARY_EXCHANGE_ID'])
        self.df_data['PRIMARY_EXCHANGE_ID'] = self.df_data['PRIMARY_EXCHANGE_ID_corr']
        
        self.df_data.drop('PRIMARY_EXCHANGE_ID_corr'  ,axis = 1 ,inplace=True)
        self.df_data['PRIMARY_EXCHANGE_ID'] = self.df_data['PRIMARY_EXCHANGE_ID'].astype(float)
    def _add_security_id(self):
        """
        Add security IDs to the data.
        """
        con = SqlConnector().connection()
        # d['security_id'] = np.nan
        self.df_data.sort_values('EXEC_DATE', inplace=True)
        idx = self.df_data.EXEC_DATE<=datetime(2021,3,29) # Avant cette date pas de code ISIN dans INFOREACH_STATS
        if idx.any():
            secid_str = "'"+"','".join(self.df_data[idx].SECID.dropna().unique())+"'"
            # Appel à SECURITY_LEGACY pour les données antérieures au 30/4/2021
            q = f"""select SECID, security_id=convert(int, SYMBOL6)
                    from KGR..SECURITY_LEGACY
                    where SECID in ({secid_str})
                    and SYMBOL6 is not null
                    union
                    select distinct s1.SECID, security_id=convert(int, s2.SYMBOL6)
                    from KGR..SECURITY_LEGACY s1, KGR..SECURITY_LEGACY s2
                    where s1.SECID in ({secid_str})
                    and s1.SYMBOL1 = s2.SYMBOL1
                    and isnull(s1.SYMBOL11, s1.SYMBOL2) = isnull(s2.SYMBOL11, s2.SYMBOL2)
                    and s2.CCY = s1.CCY
                    and s2.SYMBOL6 is not NULL
                    and s1.SYMBOL6 is null
                    """
            #debug['q'] = q
            kgr = pd.read_sql_query(q, con)
            self.df_data.loc[idx, 'security_id'] = \
                self.df_data[idx].merge(kgr, on='SECID', how='left', validate='many_to_one')['security_id'].values
        if (~idx).any():
            
            isin_str = "'"+"','".join(self.df_data.loc[~idx&self.df_data.ISIN.notnull(), 'ISIN'])+"'"
            q= f"""
                    select ISIN,primary_trading_destination_id,CCY,security_id,
                    begin_date,end_date
                    from KGR..HISTO_SECURITY_QUANT
                    where ISIN in ({isin_str})
                    order by begin_date
                """
            kgr = pd.read_sql_query(q, con, dtype={'primary_trading_destination_id':np.float64})
            kgr.begin_date = pd.to_datetime(kgr.begin_date,format='%Y-%m-%d',
                                            errors='coerce')
            kgr.end_date = pd.to_datetime(kgr.end_date,format='%Y-%m-%d',
                                          errors='coerce')
            kgr.end_date = kgr.end_date.fillna(np.datetime64('2100-12-31', 'D'))
            
            self.df_data.loc[~idx, 'security_id'] = (
                pd.merge_asof(self.df_data.loc[~idx, self.df_data.columns!='security_id'].reset_index(),
                              kgr, left_on='EXEC_DATE', right_on='begin_date',
                              left_by=['PRIMARY_EXCHANGE_ID', 'ISIN', 'CURRENCY'],
                              right_by=['primary_trading_destination_id','ISIN','CCY'])
                .set_index('index')).security_id
        
    def _filter_strat(self,strats = ['DYNVOL', 'VOL', 'VWAP', 'IS', 'VWAP-AP']) : 
        """
        Filter data by strategies.
        
        Parameters
        ----------
        strats : list
            A list of strategies by which to filter the data.
        """
        self.df_data = self.df_data[self.df_data['strat'].isin(strats)]
    
    def _filter_perim(self) : 
        """
        Apply perimeter filter to the data.
        """
        con = SqlConnector().connection()
        perim = pd.read_sql("""select distinct security_id
                                   from KGR..HISTO_SECURITY_QUANT
                                   """, con)
        self.df_data.security_id.isin(perim.security_id)
        
    def _filter_missing(self) :
        """
        Apply filters to remove missing information from the data.
        """
        self.df_data = self.df_data.dropna(subset=['crystal_code', 'SIDE'])
    def _filter_quantity(self) : 
        """
        Apply filters to remove orders with missing or <= 0 executed quantity.
        """
        self.df_data = self.df_data[(self.df_data['EXEC_QTY'] > 0) & (~pd.isna(self.df_data['EXEC_QTY']))]
        
def _run_kech_flows(start_date, end_date,
                    mode='execution', trade_category='ALL', additional_fields=None,
                    perimeter='', filter_management=True, filter_research=True,
                    filter_strategy=[], filter_perimeter=False, filter_missing_info=False,filter_quantity = True):
    """filter_management (bool): filter management or not. 

        - Filter client: Technical
        - Filter service: Corporate, Technical

    filter_research (bool): filter filter_research or not.

        - Filter QFD exclusion
        - Filter client type, only keep [Hedge funds, Funds, Banks Brokers, Retail]"""
    print('mode: %s, start date: %s, end_date: %s' % (mode, start_date, end_date))
    ts = datetime.now()
    
    if mode == 'algo':
        flows = AlgoFlows(start_date=start_date, end_date=end_date, 
                            filter_strategy=filter_strategy, filter_perimeter=filter_perimeter,
                            filter_missing_info=filter_missing_info,filter_quantity = filter_quantity,additional_fields=additional_fields)
        flows.get_data()
        
        
    elif mode == 'execution':
        flows = ExecutionFlows(start_date=start_date, end_date=end_date, perimeter=perimeter,
                               trade_category=trade_category, additional_fields=additional_fields
                               )
        flows.get_data()
        
    elif mode == 'channel':
        flows = ChannelFlows(start_date=start_date, end_date=end_date
                             )
        flows.get_data()
        
    else:
        raise ValueError(f"Invalid mode: {mode}. Mode should be 'algo', 'execution' or 'channel'.")
        
    
    flows.df_management = pd.DataFrame()
    flows.df_research = pd.DataFrame()
    flows.df_index = pd.DataFrame()
    flows.df_tfm = pd.DataFrame()

    if filter_management:
        flows._filter_management()
    if filter_research:
        flows._filter_research()

    df_data = flows.df_data.copy()

    te = datetime.now()
    texec = te - ts
    print('run time: %s' % texec)
    return df_data


def run_kech_flows(start_date, end_date,
                   mode = 'execution', trade_category='ALL', additional_fields=None,
                   perimeter = '', filter_management=True, filter_research=True,filter_strategy = [],filter_perimeter = False , filter_missing_info=False,filter_quantity = True
                   ):
    """
    Args:
        start_date (str): yyyymmdd, start date of data
        end_date (str): yyyymmdd, end date of data
        mode (str): in ['execution', 'channel']
    
            - execution : data from INFOCENTRE..ALLTRADE_QUANT for security_id in quant perimeter
            - channel: data from QUANT_work TradingPerChannel
    
        filter_management (bool): filter management or not. 
    
            - Filter client: Technical
            - Filter service: Corporate, Technical
            
        filter_research (bool): filter research or not.

            - Filter QFD exclusion
            - Keep client type Hedge funds, Funds, Banks-Brokers, Retail
    
    
        perimeter (str, list, int): index ticker bloomberg (str) or list of security_id or quant perimeter (int)
            if index ticker given, STATICS composition will be applied to filter


    .. exec_code::

        import dbtools.src.get_kc_flows as gkf
        print('--------- Example with mode execution -------------')
        print('----------execution_close------------')
        execution = gkf.run_kech_flows(start_date = '20230501', end_date = '20230505',
                                         mode='execution', perimeter = '',
                                         filter_management=True, filter_research=True, 
                                         additional_fields='LIQUIDITYINDICATOR, TRADINGSYSTEM')
        execution_close = gkf.run_kech_flows(start_date = '20230501', end_date = '20230505',
                                         mode='execution', trade_category='CLOSE', perimeter = '',
                                         filter_management=True, filter_research=True)
        print(execution_close.head())
        
        print('----------execution_otc------------')
        execution_otc = gkf.run_kech_flows(start_date = '20230501', end_date = '20230505',
                                       mode='execution', trade_category='OTC', perimeter = '',
                                       filter_management=True, filter_research=True)
        print(execution_otc.head(5))

        print('--------- Example with mode channel -------------')
        channel = gkf.run_kech_flows(start_date = '20220601', end_date = '20220601', mode='channel',
                                     filter_management=True, filter_research=True)
        print(channel.head(5))
        print('--------- Example with mode algo -------------')
        algo_orders = gkf.run_kech_flows(start_date = '20210901', end_date = '20220906',mode='algo',
                                     filter_research=True,filter_strategy = ['DYNVOL', 'VOL', 'VWAP', 'IS', 'VWAP-AP'],filter_perimeter = False , filter_missing_info=False)
        print(algo_orders.head(5))

    """
    df_data = pd.DataFrame()
    year_start = int(start_date[:4])
    year_end = int(end_date[:4])

    for year in range(year_start, year_end+1):
        if year == year_start:
            start = start_date
        else:
            start = '%d0101'%year
        if year == year_end:
            end = end_date
        else:
            end = '%d1231'%year

        df_tmp = _run_kech_flows(start_date = start, end_date = end, mode = mode, 
                                 trade_category = trade_category, additional_fields = additional_fields,
                                 perimeter = perimeter, filter_management=filter_management, filter_research=filter_research,filter_strategy = filter_strategy,filter_perimeter = filter_perimeter , filter_missing_info=filter_missing_info,filter_quantity = filter_quantity)
        df_data = pd.concat((df_data, df_tmp))

    return df_data


def run_example(start_date='20210101', end_date='20210104'):
    """Run example.
        mode execution used in tfm
        mode channel used in tfm
    """
    print('--------- Example with mode execution -------------')
    execution = run_kech_flows(start_date = start_date, end_date = end_date,
                               mode='execution', perimeter = '',
                               filter_management=True, filter_research=True)
    
        
    execution_close = run_kech_flows(start_date = '20230501', end_date = '20230505',
                                     mode='execution', trade_category='CLOSE', perimeter = '',
                                     filter_management=True, filter_research=True)

    import dbtools.src.get_kc_flows as gkf
    execution_fields = gkf.run_kech_flows(start_date = '20230501', end_date = '20230505',
                                 mode='execution', trade_category='ALL', perimeter = '',
                                 filter_management=True, filter_research=True, additional_fields='LIQUIDITYINDICATOR, TRADINGSYSTEM')

    execution_fields = gkf.run_kech_flows(start_date = '20230501', end_date = '20230505',
                                 mode='execution', trade_category='TRADINGSYSTEM', perimeter = '',
                                 filter_management=True, filter_research=True, additional_fields='LIQUIDITYINDICATOR, TRADINGSYSTEM')

    execution_fields = gkf.run_kech_flows(start_date = '20230501', end_date = '20230505',
                                 mode='execution', trade_category='TRADINGSYSTEM', perimeter = '',
                                 filter_management=True, filter_research=True, additional_fields='LIQUIDITYINDICATOR, SOR')


    execution_otc = gkf.run_kech_flows(start_date = '20230501', end_date = '20230505',
                                   mode='execution', trade_category='OTC', perimeter = '',
                                   filter_management=True, filter_research=True, additional_fields='TRADEDATE')

    print('--------- Example with mode channel -------------')
    channel = run_kech_flows(start_date = start_date, end_date = end_date, mode='channel',
                             filter_management=True, filter_research=True)

    print('--------- Example with mode orders -------------')
    algo_orders = run_kech_flows(start_date = start_date, end_date = end_date, mode='algo',
                             filter_management=True, filter_research=True,filter_strategy = ['DYNVOL', 'VOL', 'VWAP', 'IS', 'VWAP-AP'],filter_perimeter = True , filter_missing_info=True,filter_quantity = True)
    
    return {'execution':execution, 'execution_close':execution_close, 'execution_otc':execution_otc,
            'channel':channel,'algo_orders':algo_orders}

    
