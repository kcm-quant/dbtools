# -*- coding: utf-8 -*-
"""
Created on Thu Nov 21 15:18:34 2024

@author: nle
"""

import os
import pandas as pd
from dbtools.src.db_connexion import SqlConnector
import dbtools.src.get_repository as rep
import dbtools.src.get_fund_data as gfd
import dbtools.src.DatabaseManager as db
from pandas.tseries.offsets import BDay
import dbtools.src.dbtools_util as du
import numpy as np
import warnings
from copy import deepcopy
from tqdm import tqdm
import json
import matplotlib.pyplot as plt
# from copy import deepcopy
connector = SqlConnector()
con_mis = connector.connection()

#%% input
start_date = '20050701'
end_date = '20241129'
folder_init = 'W:/Global_Research/Quant_research/projets/screener/Equity risk premiums/supports_screener/Screener_Matlab_Outputs/DATA/INPUTS/EUR_Index_CSV_20241129_init/'
folder_corr = 'W:/Global_Research/Quant_research/projets/screener/Equity risk premiums/supports_screener/Screener_Matlab_Outputs/DATA/INPUTS/EUR_Index_CSV_20241129/'
date_format = '%d/%m/%Y'
#%% Load data from QUANT_work
df_ref = pd.read_sql('select index_id, ticker, currency, short_name from QUANT_work..INDEX_REFERENTIAL', con_mis).set_index('index_id')
index_file = pd.Index(os.listdir(folder_init)).difference(['info.csv'])
# index_file = index_file.difference(['SX3P_Index.csv', 'SXEP_Index.csv', 'SXQP_Index.csv'])
ticker = index_file.str[:-10]
ids = df_ref[df_ref.ticker.isin(ticker)].index


df_ref = pd.read_sql('select index_id, ticker, currency, short_name from QUANT_work..INDEX_REFERENTIAL', con_mis).set_index('index_id')
df_fy_all = gfd.GetFundData(ids = ids, start_date = start_date, end_date = end_date, security_type = 'index').get_raw_fund_data('fy_1gy')
df_fy_all = df_fy_all.rename(columns = df_ref.ticker)


df_1y = gfd.GetFundData(ids = ids, start_date = start_date, end_date = end_date, security_type = 'index').get_raw_fund_data('eps_1gy')
df_1y = df_1y.rename(columns = df_ref.ticker)
df_2y = gfd.GetFundData(ids = ids, start_date = start_date, end_date = end_date, security_type = 'index').get_raw_fund_data('eps_2gy')
df_2y = df_2y.rename(columns = df_ref.ticker)
df_3y = gfd.GetFundData(ids = ids, start_date = start_date, end_date = end_date, security_type = 'index').get_raw_fund_data('eps_3gy')
df_3y = df_3y.rename(columns = df_ref.ticker)
df_4y = gfd.GetFundData(ids = ids, start_date = start_date, end_date = end_date, security_type = 'index').get_raw_fund_data('eps_4gy')
df_4y = df_4y.rename(columns = df_ref.ticker)

#%% correction

df_1y_corr = gfd.correct_fiscal_year(df_fy_all, df_1y, df_2y)
df_2y_corr = gfd.correct_fiscal_year(df_fy_all, df_2y, df_3y)
df_3y_corr = gfd.correct_fiscal_year(df_fy_all, df_3y, df_4y)

#%% resample
df_1y_res = gfd.resample_data_ts(df_1y_corr, frequency = 'friday', nb_filldays = None)
df_2y_res = gfd.resample_data_ts(df_2y_corr, frequency = 'friday', nb_filldays = None)
df_3y_res = gfd.resample_data_ts(df_3y_corr, frequency = 'friday', nb_filldays = None)

#%% patch for csv
d_init = {}
d_corr = {}
for sheet in tqdm(index_file):
    
    df_idx = pd.read_csv(folder_init+sheet, sep = ';', index_col = 0)
    df_idx.index = pd.to_datetime(df_idx.index, format = date_format)
    idx = sheet[:-10]
    dates = df_idx.index
    
    df_corr = df_idx.copy()
    df_corr['Best EPS 1GY'] = df_1y_res.loc[dates, idx]
    df_corr['Best EPS 2GY'] = df_2y_res.loc[dates, idx]
    df_corr['Best EPS 3GY'] = df_3y_res.loc[dates, idx]
    
    df_corr['Best PE 1GY'] = df_corr['Last_PX']/df_corr['Best EPS 1GY']
    df_corr['Best PE 2GY'] = df_corr['Last_PX']/df_corr['Best EPS 2GY']
    df_corr['Best PE 3GY'] = df_corr['Last_PX']/df_corr['Best EPS 3GY']
    df_corr.index = df_corr.index.strftime(date_format)
    df_corr.to_csv(folder_corr+sheet, sep = ';', index = True)
    
    d_init[idx] = df_idx.copy()
    d_corr[idx] = df_corr.copy()
    
    fig, ax = plt.subplots(1, 1)
    df = df_corr[['Best EPS 1GY', 'Best EPS 2GY', 'Best EPS 3GY']].dropna(how ='all')
    df.index = pd.to_datetime(df.index, format = date_format)
    df.plot(ax = ax, figsize = (12,5), title = sheet)
    df_idx[['Best EPS 1GY', 'Best EPS 2GY', 'Best EPS 3GY']].dropna(how ='all').plot(ax = ax, ls = '--')
    
    date_range = pd.date_range(start=df.index.min(), end=df.index.max(), freq='MS')
    xticks = pd.Index([k for i, k in enumerate(date_range) if (i % 3 == 0)])
    ax.set_xticks(xticks)
    plt.grid()
    plt.show()

#%%


df = df_1y_corr.div(df_1y_corr.iloc[-1])
df.plot(figsize = (12,5))
plt.grid()
plt.show()

df_1y.plot(title = 'initial')

df_1y_corr.plot(title = 'correction')


df = df_1y.merge(df_1y_corr, left_index = True, right_index = True, suffixes = ('_int','_corr'))
df[['S600PDP_int', 'S600PDP_corr']].plot(figsize = (12,5))
plt.grid()
plt.show()
