# -*- coding: utf-8 -*-
"""
Created on Fri Sep 22 11:32:48 2023

@author: krulik
"""
import unittest
import pandas as pd
from unittest.mock import MagicMock, patch
from dbtools.src.DatabaseManager import DatabaseManager  # Replace `your_module` with the actual name of your module
class TestDatabaseManager(unittest.TestCase):

    def setUp(self):
        # Sample dataframe for testing
        self.sample_data = {
            'name': ['test'],
            'short_name': ['short'],
            'ticker': ['CAC'],
            'ISIN': ['ISIN1234'],
            'INDEXID': ['IND_1234'],
            'region': ['EU'],
            'category': ['CAT1'],
            'version': ['Price'],
            'currency':['EUR']
        }
        self.df = pd.DataFrame(self.sample_data)
        
        # Mocking the inspector's get_table_names method
        self.mocked_inspector = MagicMock()
        self.mocked_inspector.get_table_names.return_value = []
        
       

    @patch('dbtools.src.DatabaseManager.inspector', side_effect=lambda self: self.mocked_inspector)  
    def test_table_exists(self, mocked_inspector):
        # Case 1: Table does not exist
        db_manager = DatabaseManager("TEST_TABLE")
        self.assertFalse(db_manager.table_exists())
        
        # Case 2: Table exists
        #self.mocked_inspector.get_table_names.return_value = ["TEST_TABLE"]
        mocked_inspector.get_table_names.return_value = ["TEST_TABLE"]
        print(self.mocked_inspector.get_table_names.return_value)
        #pdb.set_trace()  # Set a breakpoint here
        self.assertTrue(db_manager.table_exists())

    @patch('dbtools.src.DatabaseManager.con_mis')
    def test_create_table(self, mocked_conn):
        db_manager = DatabaseManager("TEST_TABLE")
        db_manager.create_table(self.df)
        print("Number of times connect method called:", mocked_conn.connect.call_count)
        # Assert that the table creation method was called
        mocked_conn.connect.assert_called_once()
        
    @patch('dbtools.src.DatabaseManager.con_mis')
    def test_create_table_with_auto_increment(self, mocked_conn):
        db_manager = DatabaseManager("TEST_TABLE")
    
        # Define the expected table structure
        expected_table_structure = {
            "auto_incremental_column": "unique_id_column_name",
            "other_columns": ["col1", "col2"]
            }
    
        # Enhance the mocked inspector to simulate the table structure
        self.mocked_inspector.get_columns.return_value = [
            {"name": "unique_id_column_name", "type": "Integer", "autoincrement": "auto", "primary_key": True},
            {"name": "col1", "type": "String"},
            {"name": "col2", "type": "String"}
            ]
    
        # Call the create_table method
        db_manager.create_table(self.df, auto_incremental_index="unique_id_column_name")
    
        # Now, get the table structure using the inspector
        created_table_structure = self.mocked_inspector.get_columns("TEST_TABLE")
    
        # Verify the auto_incremental column
        auto_incremental_column = next((col for col in created_table_structure if col["name"] == expected_table_structure["auto_incremental_column"]), None)
    
        self.assertIsNotNone(auto_incremental_column)
        self.assertEqual(auto_incremental_column["type"], "Integer")
        self.assertTrue(auto_incremental_column["autoincrement"])
        self.assertTrue(auto_incremental_column["primary_key"])

    @patch('dbtools.src.DatabaseManager.con_mis')
    def test_replace_table(self, mocked_conn):
        db_manager = DatabaseManager("TEST_TABLE")
        db_manager.create_table(self.df)
        db_manager.replace_table(self.df)
        # Assert that the table creation method was called twice (once to drop, once to create)
        self.assertEqual(mocked_conn.connect.call_count, 2)
    
    
    @patch('dbtools.src.DatabaseManager.con_mis')
    def test_append_table(self, mocked_conn):
        db_manager = DatabaseManager("TEST_TABLE")
        
        
        mocked_table_exists = MagicMock()
        
        mocked_table_exists.return_value = True
        # Replace the table_exists method on db_manager with the mocked one
        setattr(db_manager, 'table_exists', mocked_table_exists)
        with patch.object(self.df, 'to_sql') as mocked_to_sql:
            db_manager.append_table(self.df)
            # Assert that the data appending method was called
            mocked_to_sql.assert_called_once()
        
    @patch('dbtools.src.DatabaseManager.con_mis')
    def test_delete_record(self, mocked_conn):
        db_manager = DatabaseManager("TEST_TABLE")
        
        mocked_table_exists = MagicMock()
        mocked_table_exists.return_value = True
        # Replace the table_exists method on db_manager with the mocked one
        setattr(db_manager, 'table_exists', mocked_table_exists)
        db_manager.delete_record('name','exemple')
        # Assert that the delete method was called
        self.assertEqual(mocked_conn.execute.call_count, 1)
    @patch('dbtools.src.DatabaseManager.con_mis')
    def test_modify_record(self, mocked_conn):
        db_manager = DatabaseManager("TEST_TABLE")
        mocked_table_exists = MagicMock()
        mocked_table_exists.return_value = True
        # Replace the table_exists method on db_manager with the mocked one
        setattr(db_manager, 'table_exists', mocked_table_exists)
        updates = {
            'ticker': 'abc',
            'short_name': 'tst'
        }
        db_manager.modify_record('name','exemple', updates)
        # Assert that the modify method was called
        self.assertEqual(mocked_conn.execute.call_count, 1)
        
if __name__ == "__main__":
    unittest.main()
