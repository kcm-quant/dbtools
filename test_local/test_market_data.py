# -*- coding: utf-8 -*-
"""
Created on Thu Apr 21 16:06:37 2022

@author: nle
"""
import sys
sys.path.append('W:/Global_Research/Quant_research/team/tle')
from src.db_connexion import SqlConnector
from datetime import datetime, timedelta
import pandas as pd
import numpy as np
connector = SqlConnector()
con_mis = connector.connection()

import src.get_market_data as mkt1
import bin.libs.dbtools.read_trading_daily as mkt2

import src.get_repository as rep1
import bin.libs.dbtools.get_repository as rep2

start_date1 = '20161201'
end_date1 = '20220101'
today1 = datetime.today().strftime('%Y%m%d')

start_date2 = datetime.strptime(start_date1, '%Y%m%d')
end_date2 = datetime.strptime(end_date1, '%Y%m%d')
today2 = datetime.today()

## quant_perimeter(level, start_date, end_date)
level11 = rep1.get_quant_perimeter(1)[:1000].union([351446])
level12 = np.array(level11)
# %%
# read_trading_daily: TODO: difference sur les stocks UK
trading_destinations =['MAIN', 17, 34, 159, 183, 189, 288, 289]
field=['close_prc', 'volume', 'turnover']
read_trading_daily1_adj_eur = mkt1.get_trading_daily(level11, start_date1, end_date1, ccy_ref = 'EUR',
                                               field=field, compute_null=True, 
                                               trading_destinations = trading_destinations,
                                               split_adj=True, special_adj=True, regular_adj=True)

read_trading_daily12_adj_eur = mkt2.read_trading_daily(level12, start_date2, end_date2,
                                            field=field, compute_null=True, 
                                            trading_destinations=trading_destinations,
                                            convert_in_euro=True,
                                            split_adj=True, special_ret_adj=True,regular_ret_adj=True)
#%%
close_prc1 = read_trading_daily1_adj_eur['close_prc']['CONS'].fillna(0)
turnover1 = read_trading_daily1_adj_eur['turnover']['CONS'].fillna(0)
volume1 = read_trading_daily1_adj_eur['volume']['CONS'].fillna(0)
#trading_destination_id.isna()
close_prc2 = read_trading_daily12_adj_eur[read_trading_daily12_adj_eur.trading_destination_id.isna()].set_index(['security_id'], append = True).close_prc.unstack().fillna(0)
turnover2 = read_trading_daily12_adj_eur[read_trading_daily12_adj_eur.trading_destination_id.isna()].set_index(['security_id'], append = True).turnover.unstack().fillna(0)
volume2 = read_trading_daily12_adj_eur[read_trading_daily12_adj_eur.trading_destination_id.isna()].set_index(['security_id'], append = True).volume.unstack().fillna(0)

close_prc_compa = pd.concat([close_prc1[abs(close_prc1-close_prc2)>1e-2].stack(),
                                  close_prc2[abs(close_prc1-close_prc2)>1e-2].stack()], axis = 1)
close_prc_compa = close_prc_compa.rename_axis(index =['date', 'security_id'])
close_prc_compa.columns = ['close_prc2', 'close_prc1']
close_prc_compa = close_prc_compa.reset_index('security_id')
close_prc_compa['diff'] = abs(close_prc_compa.close_prc1/close_prc_compa.close_prc2-1)

turnover_compa = pd.concat([turnover1[abs(turnover1-turnover2)>1e3].stack(),
                                  turnover2[abs(turnover1-turnover2)>1e3].stack()], axis = 1)
turnover_compa = turnover_compa.rename_axis(index =['date', 'security_id'])
turnover_compa.columns = ['turnover2', 'turnover1']
turnover_compa = turnover_compa.reset_index('security_id')


volume_compa = pd.concat([volume1[abs(volume1-volume2)>1e-2].stack(),
                                  volume2[abs(volume1-volume2)>1e-2].stack()], axis = 1)
volume_compa = volume_compa.rename_axis(index =['date', 'security_id'])
volume_compa.columns = ['volume2', 'volume1']
volume_compa = volume_compa.reset_index('security_id')

ccy = rep1.mapping_from_security(level11,'ccy')
close_prc_compa['ccy'] = close_prc_compa.security_id.map(ccy)
volume_compa['ccy'] = volume_compa.security_id.map(ccy)
#%%
with pd.ExcelWriter(r'W:\Global_Research\Quant_research\team\tle\test_market_data\close_prc_compa_all_adj_eur_cons_null.xlsx') as writer:
    volume_compa.to_excel(writer, sheet_name = 'volume')
    close_prc_compa.to_excel(writer, sheet_name = 'close_prc')
    turnover_compa.to_excel(writer, sheet_name = 'turnover')
# %%
## get_index_prices: OK
indices = ['SXXP', 'CAC','SBF120', 'SBF250', 'BKXP']
get_index_prices1 = mkt1.get_index_prices(indices, start_date1, end_date1)
get_index_prices2 = pd.DataFrame()
for ind in indices:
    get_index_prices2[ind] = mkt2.get_index_prices(ind, start_date2, end_date2).CLOSE_PRC

get_index_prices_compa = (get_index_prices1.fillna(0) - get_index_prices2.fillna(0))
get_index_prices_compa.replace({0:np.nan}).dropna().stack()


