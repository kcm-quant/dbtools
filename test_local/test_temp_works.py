# -*- coding: utf-8 -*-
"""
Created on Fri Apr 29 10:08:25 2022

@author: nle
"""

import sys
sys.path.append('W:/Global_Research/Quant_research/team/tle')
from dbtools.db_connexion import sql_connector
from datetime import datetime, timedelta
import pandas as pd
import numpy as np
connector = sql_connector()
con_mis = connector.connection()

import dbtools.get_market_data as mkt1
import bin.libs.dbtools.read_trading_daily as mkt2

import dbtools.get_repository as rep1
import bin.libs.dbtools.get_repository as rep2

import bin.libs.dbtools.get_tempworks as tmp

start_date1 = '20140101'
end_date1 = '20220101'
today1 = datetime.today().strftime('%Y%m%d')

start_date2 = datetime.strptime(start_date1, '%Y%m%d')
end_date2 = datetime.strptime(end_date1, '%Y%m%d')
today2 = datetime.today()

## quant_perimeter(level, start_date, end_date)
level11 = rep1.quant_perimeter(1)
level12 = rep2.quant_perimeter(1)

level21 = rep1.quant_perimeter(2)
level22 = rep2.quant_perimeter(2)
#%%
# get_kech_analyst: OK
get_kech_analyst1 = rep1.get_kc_analyst()
get_kech_analyst2 = tmp.get_kech_analyst()
get_kech_analyst_compa = (get_kech_analyst1.fillna(0) !=get_kech_analyst2.fillna(0)).replace({False:np.nan}).dropna().stack()

# get_kech_sector: OK
get_kech_sector1 = rep1.get_kc_sector()
get_kech_sector2 = tmp.get_kech_sector()
get_kech_sector_compa = (get_kech_sector1.fillna(0) !=get_kech_sector2.fillna(0)).replace({False:np.nan}).dropna().stack()

# get_kech_company: OK
get_kech_company1 = rep1.get_kc_company()
get_kech_company2 = tmp.get_kech_company()
get_kech_company_compa = (get_kech_company1.fillna(0) !=get_kech_company2.fillna(0)).replace({False:np.nan}).dropna().stack()

#get_fundvar_sxxp: OK
get_fundvar_sxxp1 = mkt1.get_fund_data(level21, start_date1, end_date1)
get_fundvar_sxxp2 = tmp.get_fundvar_sxxp(level22, start_date2, end_date2)
get_fundvar_sxxp22 = {}
get_fundvar_sxxp_compa = {}
for key in get_fundvar_sxxp1:
    df = get_fundvar_sxxp2[['date', 'security_id', key]]
    df = df.set_index(['date', 'security_id'])[key].unstack()
    get_fundvar_sxxp22[key] = df
    compa = (get_fundvar_sxxp1[key].fillna(0) !=get_fundvar_sxxp22[key].fillna(0)).replace({False:np.nan}).dropna().stack()
    get_fundvar_sxxp_compa[key] = compa
    
# get_region_sales: OK
get_region_sales1 = mkt1.get_region_sales(level11)
get_region_sales2 = tmp.get_region_sales(level12)
get_region_sales_compa = (get_region_sales1.fillna(0) !=get_region_sales2.fillna(0)).replace({False:np.nan}).dropna().stack()














