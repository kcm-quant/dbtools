# -*- coding: utf-8 -*-
"""
Created on Thu Apr 14 16:57:14 2022

@author: nle
"""
import sys
sys.path.append('W:/Global_Research/Quant_research/team/tle')

from src.db_connexion import SqlConnector
from datetime import datetime, timedelta
import pandas as pd
import numpy as np
connector = SqlConnector()
con_mis = connector.connection()

import src.get_repository as rep1
import bin.libs.dbtools.get_repository as rep2

start_date1 = '20140101'
end_date1 = '20220101'
today1 = datetime.today().strftime('%Y%m%d')

start_date2 = datetime.strptime(start_date1, '%Y%m%d')
end_date2 = datetime.strptime(end_date1, '%Y%m%d')
today2 = datetime.today()

## quant_perimeter(level, start_date, end_date)
level11 = rep1.get_quant_perimeter(1)
level12 = rep2.quant_perimeter(1)

# index_comp, index_comp_histo
###### TODO ERROR To CHECK: : 10609, 2909460 pour 1 ticker UNA NA sans SXXp le 2019-07-01
index_comp1 = rep1.get_index_comp('SXXP', today1, today1)
index_comp2 = rep2.index_comp('SXXP')

index_comp_compa = index_comp1.isin(index_comp2).sum().sum()

index_comp_histo1 = rep1.get_index_comp('SXXP', start_date1, end_date1)
index_comp_histo2 = rep2.index_comp_histo('SXXP', start_date2, end_date2)
index_comp_histo2_unstack = index_comp_histo2.set_index('security_id', append = True).weight.unstack()

index_comp_histo_compa = (index_comp_histo1.fillna(-1) - index_comp_histo2_unstack.fillna(-1)).replace({0:np.nan}).stack().unstack()

index_comp_histo1.sum(axis = 1).replace({0:np.nan}).dropna().sort_values()
index_comp_histo2_unstack.sum(axis = 1).replace({0:np.nan}).dropna().sort_values()

# get_histo_primary_mkt, get_primary_mkt: OK
get_histo_primary_mkt2 = rep2.get_histo_primary_mkt(level12)
get_primary_mkt2 = rep2.get_primary_mkt(level12)

get_primary_mkt1 = rep1.mapping_from_security(level11, code = 'prim_td_id')

get_primary_mkt_compa = (get_primary_mkt1 != get_primary_mkt2).sum()

# date_from_security_id: OK
date_from_security_id1 = rep1.mapping_from_security(level11, code = 'date')
date_from_security_id2 = rep2.date_from_security_id(level12).set_index('security_id')
date_from_security_id_compa = (date_from_security_id1!=date_from_security_id2).sum()

# tdid_from_security_id: OK
tdid_from_security_id1 = rep1.mapping_from_security(level11, code='prim_td_id')
tdid_from_security_id2 = rep2.tdid_from_security_id(level12)
tdid_from_security_id_compa = (tdid_from_security_id1 != tdid_from_security_id2).sum()

# isin_from_security_id: OK
isin_from_security_id1 = rep1.mapping_from_security(level11, code='isin')
isin_from_security_id2 = rep2.isin_from_security_id(level12)
isin_from_security_id_compa = (isin_from_security_id1 != isin_from_security_id2).sum()

# secname_from_security_id: OK
secname_from_security_id1 = rep1.mapping_from_security(level11, code='secname')
secname_from_security_id2 = rep2.secname_from_security_id(level12)
secname_from_security_id_compa = (secname_from_security_id1 != secname_from_security_id2).sum()


# ccy_from_security_id: difference dur to conversion GBX to GBP in old version: OK
ccy_from_security_id1 = rep1.mapping_from_security(level11, code='ccy')
ccy_from_security_id2 = rep2.ccy_from_security_id(level12)
ccy_from_security_id_compa = (ccy_from_security_id1!=ccy_from_security_id2.CCY).sum()
ccy_from_security_id_diff = ccy_from_security_id1[ccy_from_security_id1!=ccy_from_security_id2.CCY].sort_values()

# OK: fundticker_from_security_id_latest: difference because old version use KGR..TICKERREF, the new one use KGR..HISTO_SECURITY_QUANT_REFERENCE
fundticker_from_security_id_latest1 = rep1.mapping_from_security(level11, code='fund_ticker')
fundticker_from_security_id_latest2 = rep2.fundticker_from_security_id_latest(level12)
fundticker_from_security_id_latest_compa = fundticker_from_security_id_latest1.index.difference(fundticker_from_security_id_latest2.index)\
                                            .union(fundticker_from_security_id_latest2.index.difference(fundticker_from_security_id_latest1.index))
                                            
# OK: ticker_from_security_id: To fix start_date and end_date of 19092
ticker_from_security_id1 = rep1.mapping_from_security(level11, code='bbg')
ticker_from_security_id2 = rep2.ticker_from_security_id(level12)
ticker_from_security_id_compa = (ticker_from_security_id1 != ticker_from_security_id2).sum()

# sector_from_security_id_latest, sector_from_security_id_gics: 
# ICB Difference due to old version use TICKERREF, new version use HISTO_SECURITY_QUANT and HISTO_SECURITY_QUANT_REFERENCE 
sector_from_security_id_latest2 = rep2.sector_from_security_id_latest(level12)
sector_from_security_id_gics2 = rep2.sector_from_security_id_gics(level12)

sector_from_security_id1 = rep1.mapping_sector_from_security(level11, code = '').reindex(level11).fillna('NA')
sector_from_security_id2 = pd.concat([sector_from_security_id_gics2, sector_from_security_id_latest2],axis = 1).reindex(level12).fillna('NA')
for col in sector_from_security_id2.columns[:-2]:
    temp2 = sector_from_security_id2[col].astype(str)
    if col[:3]=='ICB':
        col = col[:-5]
    temp1 = sector_from_security_id1[col]
    temp =  temp1!=temp2
    if sum(temp)!=0:
        print(col)
        print(sector_from_security_id2.index[temp])

# OK: security_id_from_SEDOL 2 security 63423 and 2780773 has no SEDOL.
sedol1 = rep1.mapping_from_security(level11, 'sedol')
sedol1 = sedol1.astype(str)
security_id_from_SEDOL1 = rep1.mapping_security_id_from(sedol1, 'sedol')
security_id_from_SEDOL2 = rep2.security_id_from_SEDOL(sedol1.values).dropna()
security_id_from_SEDOL1 = security_id_from_SEDOL1.reindex(security_id_from_SEDOL2.index)
security_id_from_SEDOL_compa = (security_id_from_SEDOL1!=security_id_from_SEDOL2).replace({False:np.nan}).dropna()

# OK security_id_from_RIC: 10174309, 2769410 have the same ric
ric1 = rep1.mapping_from_security(level11, 'ric')
security_id_from_RIC2 = rep2.security_id_from_RIC(ric1.values).drop_duplicates()
security_id_from_RIC1 = rep1.mapping_security_id_from(ric1, code = 'ric')
security_id_from_RIC1 = security_id_from_RIC1.reindex(security_id_from_RIC2.index)
security_id_from_RIC_compa = (security_id_from_RIC2!=security_id_from_RIC1).replace({False:np.nan}).dropna()

# security_id_from_ISIN: OK
security_id_from_ISIN1 = rep1.mapping_security_id_from(isin_from_security_id1, code = 'isin')
security_id_from_ISIN2 = rep2.security_id_from_ISIN(isin_from_security_id2.values).drop_duplicates(['security_id'], keep='last').set_index('security_id')
security_id_from_ISIN2 = security_id_from_ISIN2.ticker
security_id_from_ISIN1 = pd.Series(security_id_from_ISIN1.index, security_id_from_ISIN1.security_id)
security_id_from_ISIN_compa = (security_id_from_ISIN1!=security_id_from_ISIN2).sum()

# security_id_from_ticker: difference 2 couples 10609 vs 2909460 (UNA NA) et 285671 vs 3028592 (SDRL NO): checké et loggé
security_id_from_ticker1 = rep1.mapping_security_id_from(ticker_from_security_id1, code = 'bbg')
security_id_from_ticker2 = rep2.security_id_from_ticker(ticker_from_security_id2.values)
security_id_from_ticker_compa = security_id_from_ticker2[security_id_from_ticker2.index.duplicated(keep =False)].sort_index()

# get_fx_histo: la difference vient de queleques dates sur GBX, mais petite (10e-5): OK
get_fx_histo1 = rep1.get_fx_histo(ccy_from_security_id1.drop_duplicates(), start_date1, end_date1)
get_fx_histo2 = rep2.get_fx_histo(ccy_from_security_id2.CCY.drop_duplicates().values, start_date2, end_date2)
get_fx_histo1['GBP'] = get_fx_histo1.GBX*100
get_fx_histo2['GBX'] = get_fx_histo2.GBP/100
temp = (get_fx_histo1[get_fx_histo2.columns].fillna(-1)!=get_fx_histo2.fillna(-1))#.replace({False: np.nan}).stack()  
get_fx_histo_compa = get_fx_histo2[temp].dropna(how = 'all')['GBX'] - get_fx_histo1[temp].dropna(how = 'all')['GBX']

# get_split_histo: OK
get_split_histo1  = rep1.get_split_histo(level11, start_date1, end_date1)
get_split_histo2  = rep2.get_split_histo(level12, start_date2, end_date2)

get_split_histo2 = get_split_histo2.set_index('security_id', append = True)
volume2 = get_split_histo2.volume_ratio.unstack().replace({1:np.nan}).dropna(how = 'all').dropna(how = 'all', axis =1).fillna(1)
volume1 = get_split_histo1['volume'].replace({1:np.nan}).dropna(how = 'all').dropna(how = 'all', axis =1).fillna(1)

prc2 = get_split_histo2.price_ratio.unstack().replace({1:np.nan}).dropna(how = 'all').dropna(how = 'all', axis =1).fillna(1)
prc1 = get_split_histo1['prc'].replace({1:np.nan}).dropna(how = 'all').dropna(how = 'all', axis =1).fillna(1)

special2 = get_split_histo2.special_ratio.unstack().replace({1:np.nan}).dropna(how = 'all').dropna(how = 'all', axis =1).fillna(1)
special1 = get_split_histo1['prc_special'].replace({1:np.nan}).dropna(how = 'all').dropna(how = 'all', axis =1).fillna(1)

regular2 = get_split_histo2.regular_ratio.unstack().replace({1:np.nan}).dropna(how = 'all').dropna(how = 'all', axis =1).fillna(1)
regular1 = get_split_histo1['prc_regular'].replace({1:np.nan}).dropna(how = 'all').dropna(how = 'all', axis =1).fillna(1)


get_split_histo_compa = pd.Series([(volume1!=volume2).sum().sum(), (prc1!=prc2).sum().sum(), (special1!=special2).sum().sum(),
           (regular1!=regular2).sum().sum()], index = get_split_histo1.keys())
# kc_coverage_histo: OK
kch_coverage_histo1 = rep1.kc_coverage_histo().fillna(-1)
kch_coverage_histo2 = rep2.kch_coverage_histo().fillna(-1)   
temp = kch_coverage_histo1 !=kch_coverage_histo2[kch_coverage_histo1.columns]
kch_coverage_histo_compa = temp.sum()

# get_dvd_histo: OK
get_dvd_histo1 = rep1.get_dvd_histo(level11, start_date1, end_date1)
get_dvd_histo2 = rep2.get_dvd_histo(level12, start_date2, end_date2)
get_dvd_histo_compa = (get_dvd_histo1.fillna(0)!= get_dvd_histo2.fillna(0)).replace({False:np.nan}).stack()

