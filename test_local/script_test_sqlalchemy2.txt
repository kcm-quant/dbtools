from dbtools.src.db_connexion import SqlConnector
from dbtools.src.QuantWorkUpdater import FundamentalLoader
from dbtools.src import bcp_import_to_sql

connector = SqlConnector()
con_mis = connector.connection()

update_date = '20230526'
sql_table_fund = 'level2_fundvar_test_sqlalchemy'
fund = FundamentalLoader(perimeter = 2, end_date = update_date)
fund.path_to_load = 'W:/Global_Research/Quant_research/projets/data/dynamic universe/quant_work/fundamental/fundamental_value_20230526.xlsx'

fund.get_from_excel()
fund.correct_fundccy()
fund.correct_EXO()
fund.import_to_sql(sql_table_name=sql_table_fund)
print('Fundamental data imported to %s'%sql_table_fund)