
# -*- coding: utf-8 -*-
"""
Created on Sat Mar 27 08:18:18 2021

@author: bcui
"""

import pickle
from bin.libs.dbtools.st_version import sql_connector
import bin.libs.dbtools.get_repository as rep

from datetime import datetime
import pandas as pd
import numpy as np
import time
import matplotlib.pyplot as plt
import os

def convert_excel_time(excel_time):
    return pd.to_datetime('1899-12-30') + pd.to_timedelta(excel_time,'D');

# %% import reference 

filename   = "Raw_data_bbg_level2_20220429_weekly_excel_21"
cwd = r"W:\Global_Research\Quant_research\team\tle\test_update_fundamental\macro"
os.chdir(cwd)

s = cwd + "\\%s.xlsx" % filename;
print(s);

sheetname_ref = 'REF_all'
df_ref = pd.read_excel(s, sheet_name = sheetname_ref, header=0, index_col  = False);
df_ref = df_ref.loc[df_ref.security_id.notna(),:]

# %% import fundamental data

sheetname = ['BEST_BPS_BF','BEST_EPS_1GY','BEST_EPS_2GY','BEST_EPS_3GY',
             'BEST_EPS_BF','BEST_EPS_2BF','BEST_EPS_STDDEV_BF','BEST_PE_RATIO_BF',
             'BEST_PX_BPS_RATIO_BF','BEST_ROE_BF','EARN_YLD','PX_TO_BOOK_RATIO',
             'PX_TO_SALES_RATIO','TOT_COMMON_EQY','TOT_DEBT_TO_COM_EQY',
             'EARN_YLD_yearly','PX_TO_BOOK_RATIO_yearly','PX_TO_SALES_RATIO_yearly',
             'TOT_COMMON_EQY_yearly','TOT_DEBT_TO_COM_EQY_yearly','PX_LAST_N_N_N',
             'PX_LAST_N_N_Y','PX_LAST_N_Y_Y','PX_LAST_Y_Y_Y','PX_LAST_N_N_N_EUR',
             'PX_LAST_N_N_Y_EUR','PX_LAST_N_Y_Y_EUR','PX_LAST_Y_Y_Y_EUR',
             'PX_LAST_N_N_Y_fundccy', 'CUR_MKT_CAP_Y_Y_Y_EUR','EQY_FREE_FLOAT_PCT']

sheetname = ['BEST_EPS_BF', 'BEST_PX_BPS_RATIO_BF',
        'BEST_ROE_BF', 'EARN_YLD', 'PX_TO_BOOK_RATIO', 'PX_TO_SALES_RATIO',
        'TOT_COMMON_EQY', 'TOT_DEBT_TO_COM_EQY', 'PX_LAST_N_N_Y_EUR',
        'PX_LAST_N_N_Y_fundccy', 'CUR_MKT_CAP_Y_Y_Y_EUR']

for j in range(len(sheetname)):
    
    print('No.%d - %s'%(j, sheetname[j]))
    df = pd.read_excel(open(s,'rb'), sheet_name = sheetname[j], header=3, index_col  = False, 
                       na_values=['#N/A Invalid Security'], usecols=np.arange(df_ref.shape[0]+1));
    df_index = df.iloc[:,0].values;
    df = df.iloc[:,1:];
    df.index = convert_excel_time(df_index);
    df.index.name = None;
    df.sort_index(inplace = True);
    
    df = df.T.set_index(df_ref.security_id.values, append=True).T

    df_s = df.astype(float).unstack().to_frame(sheetname[j])
    print(df_s.shape)
    if j == 0:
        df_value_reg = df_s;
    else:
        df_value_reg[sheetname[j]] = df_s.values

df_FA_data_reg = df_value_reg.reset_index().copy()
df_FA_data_reg.columns.values[0] = 'ticker'
df_FA_data_reg.columns.values[1] = 'security_id'
df_FA_data_reg.columns.values[2] = 'date'
df_FA_data_reg = df_FA_data_reg.rename(columns = {'level_1': 'date', 'level_0': 'ticker'})

#%%
df_FA_data_reg = df_FA_data_reg.sort_values(['date','security_id', 'ticker']).set_index(['date','security_id', 'ticker'])
df_level2_reg = df_level2_reg.sort_values(['date','security_id', 'ticker']).set_index(['date','security_id', 'ticker'])

columns = df_FA_data_reg.columns.intersection(df_level2_reg.columns)
fund_compa = (df_FA_data_reg[columns].fillna(0) - df_level2_reg[columns].fillna(0)).replace({0:np.nan}).dropna(how = 'all', axis = 1)
fund_compa = fund_compa[abs(fund_compa)>1e-2].dropna(how = 'all', axis = 1).reset_index()
with pd.ExcelWriter('W:/Global_Research/Quant_research/team/tle/test_update_fundamental/compa_fund_excel_python_2.xlsx') as writer:
    df_FA_data_reg.to_excel(writer, 'excel')
    df_level2_reg.to_excel(writer, 'python')
    fund_compa.to_excel(writer, 'compa')
# %% import to SQL Server

con_dev = sql_connector().connection()

## Conenxion à la base qui nous intéresse
con_dev.execute('USE QUANT_work')

## création de la table et écriture des données
from bin.core.signal import enhanced_pandas
pd.bulk_copy(con_dev, 'level2_fundvar_prod_test_excel_2', df_FA_data_reg)