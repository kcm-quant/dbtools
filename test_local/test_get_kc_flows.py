# -*- coding: utf-8 -*-
"""
Created on Thu Sep  7 14:52:56 2023

@author: ssureau
"""

import dbtools.src.get_kc_flows as gkf
from datetime import datetime
from bin.libs.orderdatabase.data_loading import (xxx_stats, add_crystal_code, add_security_id, is_vwap_ap, enrich_fx_rate)
import numpy as np
start_date = '20230901'
end_date = '20230906'

#%%
execution = gkf.run_kech_flows(start_date = start_date, end_date = end_date, mode='execution', client_ids='v2',
                             filter_management=True, filter_research=True, filter_index = 'BKXP', dynamic = True, add_classification = False,
                             filter_tfm = True, sector = 'GICS',
                             client_type = ['Banks-Brokers','Funds','Hedge funds','Retail'])

execution['BKXP'] = execution.pop('index')

#%%
flows = gkf.ExecutionFlows(start_date = start_date, end_date = end_date,
                       filter_management = True, filter_research = True,
                       client_ids = 'v2', project='tfm')
flows._get_data()
df_data = flows.df_data

import dbtools.src.get_repository as rep
index_comp = rep.get_index_comp(index_ticker = 'BKXP', start_date = start_date, end_date = end_date)

index_comp = index_comp.shift(-1)
index_comp.iloc[-1] = index_comp.iloc[-2]
index_comp = index_comp.stack().reset_index()

index_comp.columns = ['date', 'security_id', 'weight']
index_comp = index_comp[['date', 'security_id']]

df_index = index_comp.merge(df_data, how = 'inner', on = ['date', 'security_id'])

#%% Test order flows 
orders = gkf.OrderFlows(start_date=start_date, end_date=end_date,filter_research=False)
orders.get_data()

data = orders.df_data
data = data.sort_values(by='ID')

start_date_legacy=datetime(2023, 9, 1)
end_date_legacy =datetime(2023, 9, 6) 

data_legacy = xxx_stats(beg_date=start_date_legacy, end_date=end_date_legacy).grab_data()
data_legacy= add_security_id(data_legacy)
data_legacy = enrich_fx_rate(data_legacy)
data_legacy = data_legacy.sort_values(by='ID')
data_legacy['amount'] = data_legacy['EXEC_QTY'] * data_legacy['EXEC_PRICE']
data_legacy['amount_eur'] = data_legacy['amount'] * data_legacy['fx_rate']

assert len(data) == len(data_legacy), "Length of data is not equal to length of data_legacy"

assert np.array_equal(data['amount'].dropna(), data_legacy['amount'].dropna()), "The 'amount' columns are not equal"

assert np.array_equal(data['security_id'].dropna(), data_legacy['security_id'].dropna()), "The 'security_id' columns are not equal"

assert np.array_equal(data['account_id'].dropna(), data_legacy['crystal_code'].dropna()), "The 'crystal code' columns are not equal"

#assert np.array_equal(data['amount_eur'].dropna(), data_legacy['amount_eur'].dropna()), "The 'amount_eur' columns are not equal"





#%%

