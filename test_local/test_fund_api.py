# -*- coding: utf-8 -*-
"""
Created on Fri May 17 13:54:59 2024

@author: nle
"""

import pandas as pd
import numpy as np
from dbtools.src.db_connexion import SqlConnector
from dbtools.src.QuantWorkUpdater_v2 import QuantWorkUpdater
import dbtools.src.get_repository as rep
import dbtools.src.DatabaseManager as db
from sqlalchemy import  MetaData, Table, Column, Integer, Float, String, PrimaryKeyConstraint, Sequence, UniqueConstraint, Date
from copy import deepcopy
from openpyxl import Workbook
connector = SqlConnector()
con_mis = connector.connection()
import json
from tqdm import tqdm
from datetime import datetime

df_data_dict = pd.read_sql("""select attribute_id, name
                               from QUANT_work..FUNDAMENTAL_DATA_DICTIONARY""", con_mis)
df_data_dict = df_data_dict.set_index('attribute_id')['name']

df_api_db = pd.read_sql('select * from QUANT_work..FUNDAMENTAL_DATA_STOCK_api_test_100_stocks', con_mis)
df_excel_db = pd.read_sql('select * from QUANT_work..FUNDAMENTAL_DATA_STOCK_excel_test_100_stocks', con_mis)

df_api_db.DATE = pd.to_datetime(df_api_db.DATE)
df_excel_db.DATE = pd.to_datetime(df_excel_db.DATE)

df_api_db.attribute_id = df_api_db.attribute_id.replace(df_data_dict)
df_excel_db.attribute_id = df_excel_db.attribute_id.replace(df_data_dict)

sec_id = pd.Index(df_api_db.security_id.unique()).union(df_excel_db.security_id.unique())
m_sec_ticker = rep.mapping_from_security(sec_id, code ='fund_ticker')
s_dup_ticker = m_sec_ticker[m_sec_ticker.duplicated(keep = False)].sort_values()
print('Duplicated ticker:\n', s_dup_ticker)
#%% PATCH: rename security_id due to duplicated fund_ticker
s_dup_sec = {8087:15070, 41814:10175152, 132988:10175242, 359146:10175065, 2196618:10175155}
df_excel_db['security_id'] = df_excel_db['security_id'].replace(s_dup_sec)
#%%%
df_api = df_api_db.set_index(['DATE', 'attribute_id', 'security_id'])
df_excel = df_excel_db.set_index(['DATE', 'attribute_id', 'security_id'])
#%% number of data per field
df_count_field = pd.concat((df_api_db.groupby('attribute_id').value.count().to_frame('api'),
                            df_excel_db.groupby('attribute_id').value.count().to_frame('excel')), axis =1)
df_count_field.loc['nb_data_points'] = df_count_field.sum()
df_count_field['diff'] = df_count_field.api - df_count_field.excel
df_count_field
#%% number of data per date
df_count_date = pd.concat((df_api_db.groupby('DATE').value.count().to_frame('api'),
                            df_excel_db.groupby('DATE').value.count().to_frame('excel')), axis =1)
df_count_date.loc['nb_data_points'] = df_count_date.sum()
df_count_date['diff'] = df_count_date.api - df_count_date.excel
df_count_date = df_count_date[df_count_date['diff']!=0]
df_count_date
#%% number of data per security_id
df_count_sec = pd.concat((df_api_db.groupby('security_id').value.count().to_frame('api'),
                            df_excel_db.groupby('security_id').value.count().to_frame('excel')), axis =1).fillna(0)
df_count_sec.loc['nb_data_points'] = df_count_sec.sum()
df_count_sec['diff'] = df_count_sec.api - df_count_sec.excel
df_count_sec['ticker'] = df_count_sec.index.map(m_sec_ticker)
df_count_sec = df_count_sec[df_count_sec['diff']!=0]
df_count_sec
#%% differende de données sur common index ('DATE', 'attribute_id', 'security_id')
common_idx = df_api.index.intersection(df_excel.index)

df_diff_com = (df_api.loc[common_idx] - df_excel.loc[common_idx]).value
df_diff_com = df_diff_com.replace({0:np.nan}).dropna().to_frame('diff_abs')
df_diff_com['api'] = df_api.loc[df_diff_com.index]
df_diff_com['excel'] = df_excel.loc[df_diff_com.index]
df_diff_com['diff_rel'] = (df_diff_com['api'] - df_diff_com['excel'])/df_diff_com['api']*100
df_diff_com['abs(diff)'] = df_diff_com['diff_rel'].abs()

df_diff_com = df_diff_com.round(2).reset_index()
df_diff_com['ticker'] = df_diff_com.security_id.map(m_sec_ticker)
df_diff_com = df_diff_com[['DATE', 'attribute_id', 'security_id', 'ticker', 'api', 'excel','diff_abs', 'diff_rel', 'abs(diff)']]
print('check df_diff_com')
df_groupby = df_diff_com.groupby(['DATE', 'attribute_id'])
df_stat = pd.concat((df_groupby.security_id.nunique().to_frame('nb_sec_id'),
                     df_groupby.diff_rel.max().to_frame('max'),
                     df_groupby.diff_rel.mean().to_frame('mean'),
                     df_groupby.diff_rel.min().to_frame('min'),
                     df_groupby.diff_rel.median().to_frame('median')),
                    axis =1).round(2)
df_stat
#%%

api_not_exc = df_api.index.difference(df_excel.index)

exc_not_api = df_excel.index.difference(df_api.index)

df_api.loc[api_not_exc].reset_index().DATE.value_counts()
df_api.loc[api_not_exc].reset_index().attribute_id.value_counts()
df_api.loc[api_not_exc].reset_index().security_id.value_counts()


df_excel.loc[exc_not_api].reset_index().DATE.value_counts()
df_excel.loc[exc_not_api].reset_index().attribute_id.value_counts()
df_excel.loc[exc_not_api].reset_index().security_id.value_counts()
#%%

df = df_api.loc[api_not_exc].reset_index()[['attribute_id', 'security_id']].value_counts().unstack()

df[df>3].dropna(how = 'all').dropna(how = 'all', axis =1)

#%%
df = df_excel.loc[exc_not_api].reset_index()[['security_id', 'DATE']].value_counts().unstack()
df[df>3].dropna(how = 'all').dropna(how = 'all', axis =1)



