# -*- coding: utf-8 -*-
"""
Created on Tue Sep 12 11:09:38 2023

@author: nle
"""
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from datetime import datetime


start_date = '20220901'
end_date = '20230831'
#%%
import tle.dbtools_dev_gkf.src.get_kc_flows as gkf
ts = datetime.now()
execution_dev = gkf.run_kech_flows(start_date = start_date, end_date = end_date, mode='execution', client_ids='v2',
                             filter_management=True, filter_research=True, filter_index = 'BKXP', dynamic = True, add_classification = False,
                             filter_tfm = True, sector = 'GICS',
                             client_type = ['Banks-Brokers','Funds','Hedge funds','Retail'])
te = datetime.now()

time_dev = te-ts

#%%
import dbtools.src.get_kc_flows as flows
ts = datetime.now()
execution_main = flows.run_kech_flows(start_date = start_date, end_date = end_date, mode='execution', client_ids='v2',
                             filter_management=True, filter_research=True, filter_index = 'BKXP', dynamic = True, add_classification = False,
                             filter_tfm = True, sector = 'GICS',
                             client_type = ['Banks-Brokers','Funds','Hedge funds','Retail'])
te = datetime.now()
time_main = te-ts

time_dev, time_main
#%%
df_time = pd.Series([time_dev.total_seconds()/60, time_main.total_seconds()/60], index = ['dev', 'main'])
df_time = df_time.to_frame('execution time (min)').T
df_shape = pd.DataFrame(index = execution_dev.keys(), columns = ['dev', 'main'])

df_turnover = pd.DataFrame(index = execution_dev.keys(), columns = ['dev', 'main'])
for k in execution_dev.keys():

    df_dev = execution_dev[k].copy()
    df_main = execution_main[k].copy()
    df_shape.loc[k] = (len(df_dev), len(df_main))
    df_turnover.loc[k] = (df_dev.amount_eur.sum(), df_main.amount_eur.sum())

df_shape['diff (dev - main)'] = df_shape.dev - df_shape.main
df_turnover['diff (dev - main)'] = df_turnover.dev - df_turnover.main
#%%

df_dev = execution_dev['index'].copy()
df_dev['source'] = 'dev'
df_main = execution_main['index'].copy()
df_main['source'] = 'main'
columns = df_dev.columns.intersection(df_main.columns)


df1 = df_dev.groupby(['date', 'client_type', 'side'])['amount_eur'].sum() #client_region

df2 = df_main.groupby(['date', 'client_type', 'side'])['amount_eur'].sum()

df_diff_turn_type = df1 - df2.loc[df1.index]
df_diff_turn_type = df_diff_turn_type.to_frame('diff (dev - main)')
df_diff_turn_type['% to dev'] = (df_diff_turn_type.iloc[:,0]/df1)*100

df_diff_turn_type[df_diff_turn_type.abs()>1].sort_values(by = '% to dev')

#%%
df1 = df_dev.groupby(['date', 'client_region', 'side'])['amount_eur'].sum() #client_region

df2 = df_main.groupby(['date', 'client_region', 'side'])['amount_eur'].sum()

df_diff_turn_region = df1 - df2.loc[df1.index]
df_diff_turn_region = df_diff_turn_region.to_frame('diff (dev - main)')
df_diff_turn_region['% to dev'] = (df_diff_turn_region.iloc[:,0]/df1)*100

df_diff_turn_region[df_diff_turn_region.abs()>1].sort_values(by = '% to dev')
#%%


print(df1.loc[df1.index.difference(df2.index)])
df_index_diff = df2.loc[df2.index.difference(df1.index)].reset_index()

df_diff = pd.concat((df_dev[columns], df_main[columns]))
df_diff = df_diff.drop_duplicates(keep = False, subset = columns.difference(['source']))











