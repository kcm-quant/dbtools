# -*- coding: utf-8 -*-
"""
Created on Fri Mar  8 18:28:42 2024

@author: krulik
"""

import warnings
import sqlalchemy
from dbtools.src.db_connexion import SqlConnector
from sqlalchemy import  String
import dbtools.src.DatabaseManager as db
import dbtools.src.bcp_import_to_sql as bcp_import_to_sql
import pandas as pd
import numpy as np
def setup_warnings():
    "Force showing all warings"
    with warnings.catch_warnings():
        warnings.simplefilter('default', category=sqlalchemy.exc.SAWarning)
    print("Warnings setup complete.")

def use_case_pd_read_sql():
    "Use case 1: via pd_read_sql"
    print("Executing use with pd.read_sql...")
    connector = SqlConnector()
    con_mis = connector.connection()
    req = """use QUANT_work EXEC dbo.get_kc_flows_execution_raw 
             @startDate = '2023-11-13',  
             @endDate = '2023-11-14', 
             @ALLTRADE_condition='',
             @ALLTRADE_add_col=0, 
             @ALLTRADE_col_names='', 
             @ALLTRADE_col_creation=''
             """
    df_init = pd.read_sql(req, con_mis)
    print("Use with pd.read_sql OK.")

# Add new use case function with a print statement
def use_case_direct_execute():
    print("Executing direct execute...")
    connector = SqlConnector()
    con_mis = connector.connection()
    con_mis.execute('USE QUANT_work')
    print("Use with direct execute OK.")
    
def use_case_bulk_copy(df_db) : 
    connector = SqlConnector()
    con_mis = connector.connection()
    con_mis.execute('USE QUANT_work')
    pd.bulk_copy(con_mis,
                 'test_bulk'
                 ,
                 df_db
                 )
    print("Use with bulk copy OK.")
    
def use_case_bulk_copy_not_existing(df_db) : 
    connector = SqlConnector()
    con_mis = connector.connection()
    con_mis.execute('USE QUANT_work')
    pd.bulk_copy(con_mis,
                 'test_bulk3'
                 ,
                 df_db
                 )
    drop_sql = "DROP TABLE IF EXISTS test_bulk3"
    con_mis.execute( drop_sql)
    print("Use with bulk copy table non existing OK.")

def use_case_db_manager(df_db) : 
    db_manager = db.DatabaseManager( "test_db")
    column_formats = {
    'C':String(200)
}
    db_manager.create_table(df = df_db.copy(),column_types=column_formats, 
                        auto_incremental_index='indicator_id' 
                        )
    data = {
    'C': ['f']
}

    # Create DataFrame
    df = pd.DataFrame(data)
    db_manager.append_table(df = df.copy())
    db_manager.delete_record('C','f')
    db_manager.drop_table()
    
if __name__ == "__main__":
    setup_warnings()
    data = {
    'A': np.random.randint(1, 100, 10),
    'B': np.random.random(10).round(2),
    'C': np.random.choice(['X', 'Y', 'Z'], 10),
    'D': pd.date_range(start='2023-01-01', periods=10, freq='D')
    }

    df_db = pd.DataFrame(data)
    use_case_pd_read_sql()
    use_case_direct_execute()
    use_case_bulk_copy(df_db)
    use_case_bulk_copy_not_existing(df_db)
    use_case_db_manager(df_db)
    
